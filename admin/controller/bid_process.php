<?php
session_start();
 include_once('../../utils/VayaBiddingUtility.php');

 
 function getFilteredVehicleTypes()
 {
   $jsonData = array(
     'type' => "getFilteredVehicleTypes"
   );
   return callVayaBiddingWebApiPost($jsonData);
 }


  function getAllCurrencies()
{
  $jsonData = array(
    'type' => "getAllCurrencies"
  );
  return callVayaBiddingWebApiPost($jsonData);
}


// Save Currency

if (isset($_POST['saveCurrency'])) {

  
  $errors = array(); 
  // $VCurrencyName  =  $_POST['VCurrencyName'];
  // $VSymbol = $_POST['VSymbol'];

  $VCurrencyName = filter_var($_POST['VCurrencyName'], FILTER_SANITIZE_STRING);
  $VSymbol = filter_var($_POST['VSymbol'], FILTER_SANITIZE_STRING);
  $type = 'createCurrency';

  

  if(empty($errors)==true){

    $jsonData = array(
      'type'  => $type,
      'name'  => $VCurrencyName,
      'symbol'  => $VSymbol
    );

    $result = callVayaBiddingWebApiPost($jsonData);
    

    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('Currency created sucessfully');";
        echo "window.location.href='../view/currency';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/currency';";
      echo "</script>";
    }
  }else{
    echo "<script>";
      echo "alert('".$errors."');";
      echo "window.location.href='../view/currency';";
    echo "</script>";
  }

}

//update currency

if (isset($_POST['editcurrency'])) {

  // receive all input values from the form
  $errors = array(); 

  // $currencyId   = $_GET['currencyId'];
  // $name  = $_POST['name'];
  // $symbol  = $_POST['symbol'];

  $currencyId =  filter_var($_GET['currencyId'], FILTER_SANITIZE_NUMBER_INT);
  $name = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
  $symbol = filter_var($_POST['symbol'], FILTER_SANITIZE_STRING);
 
  $type = 'updateCurrency';


  // var_dump($currencyId);
  // var_dump($name);
  // var_dump($symbol);
  // exit;

  if(empty($errors)==true){

    $jsonData = array(
      'type'  => $type,
      'currencyId'  => $currencyId,
      'name'  => $name,
      'symbol'  => $symbol
    );

    $result = callVayaBiddingWebApiPost($jsonData);

    // var_dump($result);
    // exit;

    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('Currency updated sucessfully');";
        echo "window.location.href='../view/currency';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/currency';";
      echo "</script>";
    }
  }else{
    echo "<script>";
      echo "alert('".$errors."');";
      echo "window.location.href='../view/currency';";
    echo "</script>";
  }

}

   // Delete Currency
   if (isset($_POST['deletecurrency'])) {

    $currencyId   = $_GET['currencyId'];
    
    if(isset($_GET['currencyId'])){
  
      $jsonData = array(
        'type' => "deleteCurrency",
        'currencyId' => $currencyId
      );
  
      $result = callVayaBiddingWebApiPost($jsonData);
      
  
      if ($result['Action'] == 1){
  
        echo "<script>";
          echo "alert('Currency deleted sucessfully');";
          echo "window.location.href='../view/currency';";
        echo "</script>";
        
      } else {
  
         echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/currency';";
      echo "</script>";
      }
      
    }else{
      echo "<script>";
        echo "alert('Currency not specified');";
        echo "window.location.href='../view/currency';";
      echo "</script>";
    }
  
  }
  //DOCUMENT TYPE PROCESS

  function getAllDocumentTypes()
  {
    $jsonData = array(
      'type' => "getAllDocumentTypes"
    );
    return callVayaBiddingWebApiPost($jsonData);
  }
  
  
  // function getOutletById($id)
  // {
  //   $jsonData = array(
  //     'type' => "getOutletById",
  //     'id' => $id
  //   );
  //   return  callVayaBiddingWebApiPost($jsonData);
  // }
  
  // Save Document
  
  if (isset($_POST['savedoc'])) {
  
    // receive all input values from the form
    $errors = array(); 
    // $docType  =  $_POST['docType'];
    // $partnerType  =  $_POST['partnerType'];

    $docType = filter_var($_POST['docType'], FILTER_SANITIZE_STRING);
    $partnerType = filter_var($_POST['partnerType'], FILTER_SANITIZE_STRING);
    $type = 'addDocumentType';
  
    // var_dump($documentType);
    // exit;
  
    if(empty($errors)==true){
  
      $jsonData = array(
        'type'  => $type,
        'docType'  => $docType,
        'partnerType'  => $partnerType,
       
      );
  
      $result = callVayaBiddingWebApiPost($jsonData);
  ;
  
      if ($result['Action'] == 1){
        echo "<script>";
          echo "alert('document created sucessfully');";
          echo "window.location.href='../view/document';";
        echo "</script>";
      } else {
        echo "<script>";
          echo "alert('".$result['Message']."');";
          echo "window.location.href='../view/document';";
        echo "</script>";
      }
    }else{
      echo "<script>";
        echo "alert('".$errors."');";
        echo "window.location.href='../view/document';";
      echo "</script>";
    }
  
  }
  
  //update document types
  
  if (isset($_POST['editdoc'])) {
  
    // receive all input values from the form
    $errors = array(); 
  
    // $docTypeId   = $_GET['docTypeId'];
    // $docType  = $_POST['docType'];
    // $partnerType  = $_POST['partnerType'];

    $docTypeId =  filter_var($_GET['docTypeId'], FILTER_SANITIZE_NUMBER_INT);
    $docType = filter_var($_POST['documentType'], FILTER_SANITIZE_STRING);
    $partnerType = filter_var($_POST['partnerType'], FILTER_SANITIZE_STRING);
    
    
    $type = 'updateDocumentType';
  
  
    // var_dump($currencyId);
    // var_dump($VCurrencyName);
    // var_dump($VSymbolName);
    // exit;
  
    if(empty($errors)==true){
  
      $jsonData = array(
        'type'  => $type,
        'docTypeId'  => $docTypeId,
        'docType'  => $docType,
        'partnerType'  => $partnerType,
        
      );
  
      $result = callVayaBiddingWebApiPost($jsonData);
  
       //var_dump($result);
      // exit;
  
      if ($result['Action'] == 1){
        echo "<script>";
          echo "alert('document type updated sucessfully');";
          echo "window.location.href='../view/document';";
        echo "</script>";
      } else {
        echo "<script>";
          echo "alert('".$result['Message']."');";
          echo "window.location.href='../view/document';";
        echo "</script>";
      }
    }else{
      echo "<script>";
        echo "alert('".$errors."');";
        echo "window.location.href='../view/document';";
      echo "</script>";
    }
  
  }
  
     // Delete Document
     if (isset($_POST['deletedoc'])) {
  
      $docTypeId   = $_GET['docTypeId'];
      
      if(isset($_GET['docTypeId'])){
    
        $jsonData = array(
          'type' => "deleteDocumentType",
          'docTypeId' => $docTypeId
        );
    
        $result = callVayaBiddingWebApiPost($jsonData);
    
        if ($result['Action'] == 1){
    
          echo "<script>";
            echo "alert('document Type deleted sucessfully');";
            echo "window.location.href='../view/document';";
          echo "</script>";
          
        } else {
    
           echo "<script>";
          echo "alert('".$result['Message']."');";
          echo "window.location.href='../view/document';";
        echo "</script>";
        }
        
      }else{
        echo "<script>";
          echo "alert('Document not specified');";
          echo "window.location.href='../view/document';";
        echo "</script>";
      }
    
    }

    //FAQ PROCESS
    function getAllFaqs()
    {
      $jsonData = array(
        'type' => "getAllFaqs"
      );
      return callVayaBiddingWebApiPost($jsonData);
    }
    
    
    // function getOutletById($id)
    // {
    //   $jsonData = array(
    //     'type' => "getOutletById",
    //     'id' => $id
    //   );
    //   return  callVayaBiddingWebApiPost($jsonData);
    // }
    
    // Save Currency
    
    if (isset($_POST['saveFAQ'])) {
    
      // receive all input values from the form
      $errors = array(); 
      $category  =  $_POST['category'];
      $question = $_POST['question'];
      $answer = $_POST['answer'];
      $type = 'createFaq';
    
      // var_dump($VCurrencyName);
      // var_dump($VSymbol);
      // exit;
    
      if(empty($errors)==true){
    
        $jsonData = array(
          'type'  => $type,
          'category'  => $category,
          'question'  => $question,
          'answer'  => $answer
        );
    
        $result = callVayaBiddingWebApiPost($jsonData);
        
    
        if ($result['Action'] == 1){
          echo "<script>";
            echo "alert('Faq created sucessfully');";
            echo "window.location.href='../view/faq';";
          echo "</script>";
        } else {
          echo "<script>";
            echo "alert('".$result['Message']."');";
            echo "window.location.href='../view/faq';";
          echo "</script>";
        }
      }else{
        echo "<script>";
          echo "alert('".$errors."');";
          echo "window.location.href='../view/faq';";
        echo "</script>";
      }
    
    }
    
    //update faq
    
    if (isset($_POST['editFAQ'])) {
    
      // receive all input values from the form
      $errors = array(); 
    
      $faqId   = $_GET['faqId'];
      $category  = $_POST['category'];
      $question  = $_POST['question'];
      $answer  = $_POST['answer'];
      $creationStatus  = "ACTIVE";
      $type = 'updateFaq';
    
    
      // var_dump($faqId);
      // var_dump($category);
      // var_dump($question);
      // var_dump($answer);
      // exit;
    
      
    
      if(empty($errors)==true){
    
        $jsonData = array(
          'type'  => $type,
          'FaqId'  => $faqId,
          'creationStatus'  => $creationStatus,
          'category'  => $category,
          'question'  => $question,
          'answer'  => $answer
        );
    
        $result = callVayaBiddingWebApiPost($jsonData);
    
        //  var_dump($result);
        //  exit;
    
        if ($result['Action'] == 1){
          echo "<script>";
            echo "alert('faq updated sucessfully');";
            echo "window.location.href='../view/faq';";
          echo "</script>";
        } else {
          echo "<script>";
            echo "alert('".$result['Message']."');";
            echo "window.location.href='../view/faq';";
          echo "</script>";
        }
      }else{
        echo "<script>";
          echo "alert('".$errors."');";
          echo "window.location.href='../view/faq';";
        echo "</script>";
      }
    
    }
    
       // Delete faq
       if (isset($_POST['deleteFAQ'])) {
    
        $faqId   = $_GET['faqId'];
        
        if(isset($_GET['faqId'])){
      
          $jsonData = array(
            'type' => "deleteFaq",
            'FaqId' => $faqId
          );
      
          $result = callVayaBiddingWebApiPost($jsonData);
      
          if ($result['Action'] == 1){
      
            echo "<script>";
              echo "alert('Faq deleted sucessfully');";
              echo "window.location.href='../view/faq';";
            echo "</script>";
            
          } else {
      
             echo "<script>";
            echo "alert('".$result['Message']."');";
            echo "window.location.href='../view/faq';";
          echo "</script>";
          }
          
        }else{
          echo "<script>";
            echo "alert('Faq not specified');";
            echo "window.location.href='../view/faq';";
          echo "</script>";
        }
      
      }
    
//ADMIN USERS PROCESS
 function getAllUsers()
{
  $jsonData = array(
    'type' => "getAllUsers"
  );
  return callVayaBiddingWebApiPost($jsonData);
}


// function getOutletById($id)
// {
//   $jsonData = array(
//     'type' => "getOutletById",
//     'id' => $id
//   );
//   return  callVayaBiddingWebApiPost($jsonData);
// }

// Save users

if (isset($_POST['saveUser'])) {

  // receive all input values from the form
  $errors = array(); 
  // $firstname  =  $_POST['firstname'];
  // $lastname = $_POST['lastname'];
  // $VEmail = $_POST['VEmail'];
  // $phoneNumber = $_POST['phoneNumber'];
  // $password = $_POST['password'];

  $firstname = filter_var($_POST['firstname'], FILTER_SANITIZE_STRING);
  $lastname = filter_var($_POST['lastname'], FILTER_SANITIZE_STRING);
  $VEmail = filter_var($_POST['VEmail'], FILTER_SANITIZE_EMAIL);
  $phoneNumber = filter_var($_POST['phoneNumber'], FILTER_SANITIZE_NUMBER_INT);
  $password = $_POST['password'];

  $type = 'createUser';

  // var_dump($firstname);
  // var_dump($lastname);
  // var_dump($email);
  // var_dump($phonenumber);
  // var_dump($invitecode);
  // var_dump($usertype);
  // var_dump($password);
  // exit;

  if(empty($errors)==true){

    $jsonData = array(
      'type'  => $type,
      'firstname'  => $firstname,    
      'lastname'  => $lastname,
      'VEmail'  => $VEmail,   
      'phoneNumber'  => $phoneNumber,
     
      
      'password'  => $password
    );

    $result = callVayaBiddingWebApiPost($jsonData);
    

    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('User created sucessfully');";
        echo "window.location.href='../view/manage_admin';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/manage_admin';";
      echo "</script>";
    }
  }else{
    echo "<script>";
      echo "alert('".$errors."');";
      echo "window.location.href='../view/manage_admin';";
    echo "</script>";
  }

}

//update user

if (isset($_POST['editUser'])) {

  // receive all input values from the form
  $errors = array(); 

  // $id   = $_GET['id'];
  // $firstname  = $_POST['firstname'];
  // $lastname  = $_POST['lastname'];
  // $VEmail  = $_POST['VEmail'];
  // $phoneNumber  = $_POST['phoneNumber'];
 
  $id =  filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT);
  $firstname = filter_var($_POST['firstname'], FILTER_SANITIZE_STRING);
  $lastname = filter_var($_POST['lastname'], FILTER_SANITIZE_STRING);
  $VEmail = filter_var($_POST['VEmail'], FILTER_SANITIZE_EMAIL);
  $phoneNumber = filter_var($_POST['phoneNumber'], FILTER_SANITIZE_NUMBER_INT);
  
 $type = 'updateUser';


  // var_dump($firstname);
  // var_dump($lastname);
  //  var_dump($vemail);
  //  var_dump($phoneNumber);
  //  exit;

  if(empty($errors)==true){

    $jsonData = array(
      'type'  => $type,
      'id'  => $id,
      'firstname'  => $firstname,
      'lastname'  => $lastname,
      'VEmail'  => $VEmail,
      'phoneNumber'  => $phoneNumber,
     
      
    );

    $result = callVayaBiddingWebApiPost($jsonData);

    // var_dump($result);
    // exit;

    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('User updated sucessfully');";
        echo "window.location.href='../view/manage_admin';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/manage_admin';";
      echo "</script>";
    }
  }else{
    echo "<script>";
      echo "alert('".$errors."');";
      echo "window.location.href='../view/manage_admin';";
    echo "</script>";
  }

}

   // Delete User
   if (isset($_POST['deleteUser'])) {


    $id = $_GET['id'];
    // var_dump($id);
    //   exit;
    
    if(isset($_GET['id'])){
  
      $jsonData = array(
        'type' => "deleteUser",
        'id' => $id
      );

      $result = callVayaBiddingWebApiPost($jsonData);
      // var_dump($result);
      // exit;
  
      if ($result['Action'] == 1){
  
        echo "<script>";
          echo "alert('user deleted sucessfully');";
          echo "window.location.href='../view/manage_admin';";
        echo "</script>";
        
      } else {
  
         echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/manage_admin';";
      echo "</script>";
      }
      
    }else{
      echo "<script>";
        echo "alert('User not specified');";
        echo "window.location.href='../view/manage_admin';";
      echo "</script>";
    }
  
  }

  //MANAGE CUSTOMERS PROCESS
  function getAllCustomers()
{
  $jsonData = array(
    'type' => "getAllCustomers"
  );
  return callVayaBiddingWebApiPost($jsonData);
}

// Save customer individual

if (isset($_POST['saveCustomer'])) {


  $errors = array(); 
  $vEmail =  filter_var($_POST['vEmail'], FILTER_SANITIZE_EMAIL);
  $vFirstName = filter_var($_POST['vFirstName'], FILTER_SANITIZE_STRING);
  $vPhone =  filter_var($_POST['vPhone'], FILTER_SANITIZE_NUMBER_INT);
  $vInviteCode =  filter_var($_POST['vInviteCode'], FILTER_SANITIZE_STRING);
  $vPassword =  filter_var($_POST['vPassword'], FILTER_SANITIZE_STRING);
  $vLastName = filter_var($_POST['vLastName'], FILTER_SANITIZE_STRING);
  $vCurrency =  filter_var($_POST['vCurrency'], FILTER_SANITIZE_STRING);
  $phoneCode =  filter_var($_POST['phoneCode'], FILTER_SANITIZE_NUMBER_INT);
  $countryCode =  filter_var($_POST['countryCode'], FILTER_SANITIZE_NUMBER_INT);
  $vLang =  filter_var($_POST['vLang'], FILTER_SANITIZE_STRING);
  $vDeviceType =  filter_var($_POST['vDeviceType'], FILTER_SANITIZE_STRING);
  $userType =  filter_var($_POST['userType'], FILTER_SANITIZE_STRING);

  $type = 'customerRegistration';

   //var_dump($vFirstName);
   //var_dump($vLastName);
   //var_dump($vEmail);
   //var_dump($vPhone);
   //var_dump($vPassword);
   //var_dump($vCurrency);
   //var_dump($countryCode);
   //var_dump($phoneCode);
   //var_dump($vDeviceType);
   //var_dump($vLang);
   //var_dump($userType);
  
   //exit;

  if(empty($errors)==true){

    $jsonData = array(
      'type'  => $type,
      'vFirstName'  => $vFirstName,    
      'vLastName'  => $vLastName,
      'vEmail'  => $vEmail,   
      'vPhone'  => $vPhone,
      'vInviteCode'  => $vInviteCode,
      'vPassword'  => $vPassword,
      'vCurrency'  => $vCurrency,
      'vLang'  => $vLang,
      'countryCode'  => $countryCode,
      'vDeviceType'  => $vDeviceType,
      'phoneCode'  => $phoneCode,
      'userType'  => $userType

    );

    $result = callVayaBiddingWebApiPost($jsonData);
    
//  var_dump($result);
    //  exit;
    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('Customer created sucessfully');";
        echo "window.location.href='../view/manage_customer';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/manage_customer';";
      echo "</script>";
    }
  }else{
    echo "<script>";
      echo "alert('".$errors."');";
      echo "window.location.href='../view/manage_customer';";
    echo "</script>";
  }

}



//update Individual user

if (isset($_POST['editCustomer'])) {

  
  $errors = array(); 


  $vEmail =  filter_var($_POST['vEmail'], FILTER_SANITIZE_EMAIL);
  $vFirstName = filter_var($_POST['vFirstName'], FILTER_SANITIZE_STRING);
  $vPhone =  filter_var($_POST['vPhone'], FILTER_SANITIZE_NUMBER_INT);
  $iuserId =  filter_var($_GET['iuserId'], FILTER_SANITIZE_NUMBER_INT);
  $vInviteCode =  filter_var($_POST['vInviteCode'], FILTER_SANITIZE_STRING);
  $vPassword =  filter_var($_POST['vPassword'], FILTER_SANITIZE_STRING);
  $vLastName = filter_var($_POST['vLastName'], FILTER_SANITIZE_STRING);
  $vCurrency =  filter_var($_POST['vCurrency'], FILTER_SANITIZE_STRING);
  $phoneCode =  filter_var($_POST['phoneCode'], FILTER_SANITIZE_NUMBER_INT);
  $countryCode =  filter_var($_POST['countryCode'], FILTER_SANITIZE_NUMBER_INT);
  $vLang =  filter_var($_POST['vLang'], FILTER_SANITIZE_STRING);
 
  $userType  = "Customer";
 $type = 'updateCustomer';


  if(empty($errors)==true){

    $jsonData = array(
      'type'  => $type,
      'customerId'  => $iuserId,
      'vFirstName'  => $vFirstName,
      'vLastName'  => $vLastName,
      'vEmail'  => $vEmail,
      'vPhone'  => $vPhone,
      'vInviteCode'  => $vInviteCode,
      'vCurrency'  => $vCurrency,
      'phoneCode'  => $phoneCode,
      'vPassword'  => $vPassword,
      'countryCode'  => $countryCode,
      'vLang'  => $vLang,
      'userType'  => $userType
      
    );
  
    $result = callVayaBiddingWebApiPost($jsonData);


    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('customer individual updated sucessfully');";
        echo "window.location.href='../view/manage_customer';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/manage_customer';";
      echo "</script>";
    }
  }else{
    echo "<script>";
      echo "alert('".$errors."');";
      echo "window.location.href='../view/manage_customer';";
    echo "</script>";
  }

}


   // Delete User customer
   if (isset($_POST['deleteCustomer'])) {

    $iuserId =  $_GET['iuserId'];
    
    if(isset($_GET['iuserId'])){
      
  
      $jsonData = array(
        'type' => "deleteCustomer",
        'iUserId' => $iuserId
      );

  
      $result = callVayaBiddingWebApiPost($jsonData);
  
      if ($result['Action'] == 1){
  
        echo "<script>";
          echo "alert('user deleted sucessfully');";
          echo "window.location.href='../view/manage_customer';";
        echo "</script>";
        
      } else {
  
         echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/manage_customer';";
      echo "</script>";
      }
      
    }else{
      echo "<script>";
        echo "alert('User not specified');";
        echo "window.location.href='../view/manage_customer';";
      echo "</script>";
    }
  
  }
  //MANAGE PARTNER PROCESS
  
  function getAllPartners()
{
  $jsonData = array(
    'type' => "getAllPartners"
  );
  return callVayaBiddingWebApiPost($jsonData);
}
function getVehicleType($vehicleTypeId)
{
  $jsonData = array(
    'type' => "getVehicleType",
    'vehicleTypeId' => $vehicleTypeId
  );
  return callVayaBiddingWebApiPost($jsonData);
}




// Save partner individual

if (isset($_POST['savePartnerIndividual'])) {


  $errors = array();

  $vEmail =  filter_var($_POST['vEmail'], FILTER_SANITIZE_EMAIL);
  $name = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
  $surname = filter_var($_POST['surname'], FILTER_SANITIZE_STRING);
  $phoneNumber =  filter_var($_POST['phoneNumber'], FILTER_SANITIZE_NUMBER_INT);
  $servicePillarId  = $_POST['servicePillarId'];
  $referralCode =  filter_var($_POST['referralCode'], FILTER_SANITIZE_STRING);
  $phoneCode  = '+263';
  $vPassword =  filter_var($_POST['vPassword'], FILTER_SANITIZE_STRING);
  $countryCode  = 'ZW';
  $country  = 'ZW';
  $vehicleId =  filter_var($_POST['vehicleTypeId'], FILTER_SANITIZE_NUMBER_INT);
  $vCaddress =  filter_var($_POST['vCaddress'], FILTER_SANITIZE_STRING);


  $partnerType = 'Individual';
  
  

  $type = 'partnerRegistration';

  if(empty($errors)==true){

    $jsonData = array(
      'type'  => $type,
      'name'  => $name,
      'surname'  => $surname,
      'vEmail'  => $vEmail,
      'phoneNumber'  => $phoneNumber,
      'servicePillarId'  => $servicePillarId,
      'referralCode'  => $referralCode,
      'vPassword'  => $vPassword,
      'phoneCode'  => $phoneCode,
      'countryCode'  => $countryCode,
      'country'  => $country,
      'vehicleId'  => $vehicleId,
      'vCaddress'  => $vCaddress,
      'partnerType'  => $partnerType
      
      

    );

    $result = callVayaBiddingWebApiPost($jsonData);
    
    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('Partner Individual created sucessfully');";
        echo "window.location.href='../view/manage_partner';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/manage_partner';";
      echo "</script>";
    }
  }else{
    echo "<script>";
      echo "alert('".$errors."');";
      echo "window.location.href='../view/manage_partner';";
    echo "</script>";
  }

}



// Save user Partner SME

if (isset($_POST['savePartnerSME'])) {
  $errors = array(); 

  $vEmail =  filter_var($_POST['vEmail'], FILTER_SANITIZE_EMAIL);
  $name = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
  $vCaddress =  filter_var($_POST['vCaddress'], FILTER_SANITIZE_STRING);
  $vPhone =  filter_var($_POST['vPhone'], FILTER_SANITIZE_NUMBER_INT);
  $zimraBpNumber =  filter_var($_POST['zimraBpNumber'], FILTER_SANITIZE_NUMBER_INT);
  $vPassword =  filter_var($_POST['vPassword'], FILTER_SANITIZE_STRING);
  $countryCode = filter_var($_POST['countryCode'], FILTER_SANITIZE_NUMBER_INT);
  $phoneCode =  filter_var($_POST['phoneCode'], FILTER_SANITIZE_NUMBER_INT);


  $type = 'companyRegistration';

  if(empty($errors)==true){

    $jsonData = array(
      'type'  => $type,
      'name'  =>  $name,
      'vCaddress' => $vCaddress,
      'zimraBpNumber' => $zimraBpNumber,
      'vPhone' => $vPhone,
      'vEmail' => $vEmail,
      'vPassword' => $vPassword,
      'countryCode' => $countryCode,
      'phoneCode' => $phoneCode     

    );

// var_dump($jsonData);
// exit;
    $result = callVayaBiddingWebApiPost($jsonData);

    
    

    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('Partner SME created sucessfully');";
        echo "window.location.href='../view/manage_partner';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/manage_partner';";
      echo "</script>";
    }
  }else{
    echo "<script>";
      echo "alert('".$errors."');";
      echo "window.location.href='../view/manage_partner';";
    echo "</script>";
  }

}

//update user individual

if (isset($_POST['editPartnerIndividual'])) {

  
  $errors = array(); 

  $vEmail =  filter_var($_POST['vemail'], FILTER_SANITIZE_EMAIL);
  $name = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
  //$vPassword =  filter_var($_POST['vPassword'], FILTER_SANITIZE_STRING);
  $surname = filter_var($_POST['surname'], FILTER_SANITIZE_STRING);
  $phoneNumber =  filter_var($_POST['phoneNumber'], FILTER_SANITIZE_NUMBER_INT);
  $vcaddress =  filter_var($_POST['vcaddress'], FILTER_SANITIZE_STRING);
  $servicePillarId  = filter_var($_POST['servicePillarId'], FILTER_SANITIZE_NUMBER_INT);
  $vehicleId  = filter_var($_POST['vehicleTypeId'], FILTER_SANITIZE_NUMBER_INT);
  $phoneCode  = '+263'; 
  $countryCode  = 'ZW';
  $country  = 'ZW';
  $idriverId =  filter_var($_GET['idriverId'], FILTER_SANITIZE_NUMBER_INT);
  // var_dump($idriverId);
  // var_dump($vEmail);
  // var_dump($name);
  // var_dump($surname);
  // var_dump($phoneNumber);
  // var_dump($vcaddress);
  // var_dump($servicePillarId);
  // var_dump($phoneCode);
  // var_dump($countryCode);
  // var_dump($country);
  //exit;
$type = 'updatePartner';
 $errors = array();

 $partnerType = 'Individual';

  

  if(empty($errors)==true){

    $jsonData = array(
      'type'  => $type,
      'partnerId'  => $idriverId,
      'name'  =>  $name,
      'surname' => $surname,
      //'vPassword'  => $vPassword,
      'vEmail' => $vEmail,
      'phoneNumber' => $phoneNumber,
      'vehicleId' => $vehicleId,
      'vCaddress' => $vcaddress,   
      'servicePillarId' => $servicePillarId,
      'countryCode' => $countryCode,
      'country' => $country,
      'phoneCode' => $phoneCode,
      'partnerType' => $partnerType
      
    );

    $result = callVayaBiddingWebApiPost($jsonData);

    

    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('partner individual updated sucessfully');";
        echo "window.location.href='../view/manage_partner';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/manage_partner';";
      echo "</script>";
    }
  }else{
    echo "<script>";
      echo "alert('".$errors."');";
      echo "window.location.href='../view/manage_partner';";
    echo "</script>";
  }

}

//update user SME

if (isset($_POST['editSME'])) {

  
  $errors = array(); 
  $idriverId =  filter_var($_GET['idriverId'], FILTER_SANITIZE_NUMBER_INT);
  $vEmail =  filter_var($_POST['vEmail'], FILTER_SANITIZE_EMAIL);
  $name = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
  $vcaddress =  filter_var($_POST['vCaddress'], FILTER_SANITIZE_STRING);
  $zimraBpNumber =  filter_var($_POST['zimraBpNumber'], FILTER_SANITIZE_NUMBER_INT);
  $vPhone =  filter_var($_POST['vPhone'], FILTER_SANITIZE_NUMBER_INT);
  $countryCode = filter_var($_POST['countryCode'], FILTER_SANITIZE_NUMBER_INT);
  $phoneCode = filter_var($_POST['phoneCode'], FILTER_SANITIZE_NUMBER_INT);



 $type = 'updateSME';

  

  if(empty($errors)==true){

    $jsonData = array(
      'type'  => $type,
      'partnerId'  => $idriverId,
      'name'  =>  $name,
      'vEmail' => $vEmail,
      'vPhone' => $vPhone,
      'zimraBpNumber' => $zimraBpNumber,
      'vCaddress' => $vCaddress,
      'countryCode' => $countryCode,    
      'phoneCode' => $phoneCode
      
      
    );

    $result = callVayaBiddingWebApiPost($jsonData);

    

    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('partner SME updated sucessfully');";
        echo "window.location.href='../view/manage_partner';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/manage_partner';";
      echo "</script>";
    }
  }else{
    echo "<script>";
      echo "alert('".$errors."');";
      echo "window.location.href='../view/manage_partner';";
    echo "</script>";
  }

}

   // Delete Partner
   if (isset($_POST['deletePartner'])) {


    $idriverId =  $_GET['idriverId'];
    
    if(isset($_GET['idriverId'])){
  
      $jsonData = array(
        'type' => "deletePartner",
        'partnerId' => $idriverId
      );
  
      $result = callVayaBiddingWebApiPost($jsonData);
  
      if ($result['Action'] == 1){
  
        echo "<script>";
          echo "alert('user deleted sucessfully');";
          echo "window.location.href='../view/manage_partner';";
        echo "</script>";
        
      } else {
  
         echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/manage_partner';";
      echo "</script>";
      }
      
    }else{
      echo "<script>";
        echo "alert('User not specified');";
        echo "window.location.href='../view/manage_partner';";
      echo "</script>";
    }
  
  }


  //SERVICE PILLAR PROCESS
function getAllServicePillars()
{
  $jsonData = array(
    'type' => "getAllServicePillar"
  );
  return callVayaBiddingWebApiPost($jsonData);
}




// Save Service Pillar

if (isset($_POST['saveServicePillar'])) {

  
  $errors = array(); 
  // $VPillarName  =  $_POST['VPillarName'];
  $VPillarName = filter_var($_POST['VPillarName'], FILTER_SANITIZE_STRING);
  $type = 'createServicePillar';


  if(empty($errors)==true){

    $jsonData = array(
      'type'  => $type,
      'name'  => $VPillarName
    );

    $result = callVayaBiddingWebApiPost($jsonData);
;

    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('Service pillar created sucessfully');";
        echo "window.location.href='../view/service_pillars';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/service_pillars';";
      echo "</script>";
    }
  }else{
    echo "<script>";
      echo "alert('".$errors."');";
      echo "window.location.href='../view/service_pillars';";
    echo "</script>";
  }

}

//update Service Pillar

if (isset($_POST['editServicePillar'])) {

  
  $errors = array(); 

  // $servicePillarId   = $_GET['servicePillarId'];
  // $VPillarName  = $_POST['VPillarName'];

  $servicePillarId =  filter_var($_GET['servicePillarId'], FILTER_SANITIZE_NUMBER_INT);
  $VPillarName = filter_var($_POST['VPillarName'], FILTER_SANITIZE_STRING);
  

  $type = 'updateServicePillar';

  if(empty($errors)==true){

    $jsonData = array(
      'type'  => $type,
      'name'  => $VPillarName,
      'servicePillarId'  => $servicePillarId
    );

    $result = callVayaBiddingWebApiPost($jsonData);

    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('Service pillar updated sucessfully');";
        echo "window.location.href='../view/service_pillars';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/service_pillars';";
      echo "</script>";
    }
  }else{
    echo "<script>";
      echo "alert('".$errors."');";
      echo "window.location.href='../view/service_pillars';";
    echo "</script>";
  }

}

   // Delete Service Pillar
   if (isset($_POST['deleteServicePillar'])) {

    $servicePillarId = $_GET['servicePillarId'];
    
    
    if(isset($_GET['servicePillarId'])){
  
      $jsonData = array(
        'type' => "deleteServicePillar",
        'servicePillarId' => $servicePillarId
      );
  
      $result = callVayaBiddingWebApiPost($jsonData);

     
  
      if ($result['Action'] == 1){
  
        echo "<script>";
          echo "alert('Service Pillar deleted sucessfully');";
          echo "window.location.href='../view/service_pillars';";
        echo "</script>";
        
      } else {
  
         echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/service_pillars';";
      echo "</script>";
      }
      
    }else{
      echo "<script>";
        echo "alert('Service pillar not specified');";
        echo "window.location.href='../view/service_pillars';";
      echo "</script>";
    }
  
  }

//VEHICLE CATEGORIES PROCESS
  
function getAllVehicleCategories()
{
  $jsonData = array(
    'type' => "getAllVehicleCategories"
  );
  return callVayaBiddingWebApiPost($jsonData);
}




// Save Vehicle Category

if (isset($_POST['saveVehicleCategory'])) {

  
  $errors = array(); 
  $VCategoryName = filter_var($_POST['VCategoryName'], FILTER_SANITIZE_STRING);
  $type = 'createVehicleCategory';


  if(empty($errors)==true){

    $jsonData = array(
      'type'  => $type,
      'name'  => $VCategoryName
    );

    $result = callVayaBiddingWebApiPost($jsonData);
;

    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('Vehicle category created sucessfully');";
        echo "window.location.href='../view/vehicle_category';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/vehicle_category';";
      echo "</script>";
    }
  }else{
    echo "<script>";
      echo "alert('".$errors."');";
      echo "window.location.href='../view/vehicle_category';";
    echo "</script>";
  }

}

//update Vehicle Category

if (isset($_POST['editVehicleCategory'])) {

  
  $errors = array(); 
  $vehicleCategoryId =  filter_var($_POST['vehicleCategoryId'], FILTER_SANITIZE_NUMBER_INT);
  $VCategoryName = filter_var($_POST['VCategoryName'], FILTER_SANITIZE_STRING);
  $type = 'updateVehicleCategory';




  if(empty($errors)==true){

    $jsonData = array(
      'type'  => $type,
      'vehicleCategoryId'  => $vehicleCategoryId,
      'name'  => $VCategoryName
    );

    $result = callVayaBiddingWebApiPost($jsonData);
    

    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('Vehicle category updated sucessfully');";
        echo "window.location.href='../view/vehicle_category';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/vehicle_category';";
      echo "</script>";
    }
  }else{
    echo "<script>";
      echo "alert('".$errors."');";
      echo "window.location.href='../view/vehicle_category';";
    echo "</script>";
  }

}

   // Delete Vehicle Category
   if (isset($_POST['deleteVehicleCategory'])) {

    $vehicleCategoryId =  $_GET['vehicleCategoryId'];

    
    
    if(isset($_GET['vehicleCategoryId'])){
  
      $jsonData = array(
        'type' => "deleteVehicleCategory",
        'vehicleCategoryId' => $vehicleCategoryId
      );
  
      $result = callVayaBiddingWebApiPost($jsonData);
  
      if ($result['Action'] == 1){
  
        echo "<script>";
          echo "alert('Vehicle category deleted sucessfully');";
          echo "window.location.href='../view/vehicle_category';";
        echo "</script>";
        
      } else {
  
         echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/vehicle_category';";
      echo "</script>";
      }
      
    }else{
      echo "<script>";
        echo "alert('Vehicle category not specified');";
        echo "window.location.href='../view/vehicle_category';";
      echo "</script>";
    }
  
  }
//VEHICLE Make PROCESS
function getAllMakes()
{
  $jsonData = array(
    'type' => "getAllMakes"
  );
  return callVayaBiddingWebApiPost($jsonData);
}




// Save Vehicle Make

if (isset($_POST['saveVehicleMake'])) {

  
  $errors = array(); 
  $vMake  = filter_var($_POST['vMake'], FILTER_SANITIZE_STRING);
  $type = 'createMake';


  if(empty($errors)==true){

    $jsonData = array(
      'type'  => $type,
      'vMake'  => $vMake
    );

    $result = callVayaBiddingWebApiPost($jsonData);
;

    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('Vehicle make created sucessfully');";
        echo "window.location.href='../view/vehicle_make';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/vehicle_make';";
      echo "</script>";
    }
  }else{
    echo "<script>";
      echo "alert('".$errors."');";
      echo "window.location.href='../view/vehicle_make';";
    echo "</script>";
  }

}

//update Vehicle Make

if (isset($_POST['editVehicleMake'])) {

 
  $errors = array(); 

  $imakeId   = filter_var($_GET['imakeId'], FILTER_VALIDATE_INT);
  $VMake = filter_var($_POST['VMake'], FILTER_SANITIZE_STRING);
  $type = 'updateMake';

  


  if(empty($errors)==true){

    $jsonData = array(
      'type'  => $type,
      'iMakeId'  => $imakeId,
      'VMake'  => $VMake
    );

    $result = callVayaBiddingWebApiPost($jsonData);
    

    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('Vehicle make updated sucessfully');";
        echo "window.location.href='../view/vehicle_make';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/vehicle_make';";
      echo "</script>";
    }
  }else{
    echo "<script>";
      echo "alert('".$errors."');";
      echo "window.location.href='../view/vehicle_make';";
    echo "</script>";
  }

}

   // Delete Vehicle Make
   if (isset($_POST['deleteVehicleMake'])) {


    $imakeId   = $_GET['imakeId'];
    
    if(isset($_GET['imakeId'])){
  
      $jsonData = array(
        'type' => "deleteMake",
        'iMakeId' => $imakeId
      );
  
      $result = callVayaBiddingWebApiPost($jsonData);
  
      if ($result['Action'] == 1){
  
        echo "<script>";
          echo "alert('Vehicle Make deleted sucessfully');";
          echo "window.location.href='../view/vehicle_make';";
        echo "</script>";
        
      } else {
  
         echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/vehicle_make';";
      echo "</script>";
      }
      
    }else{
      echo "<script>";
        echo "alert('Vehicle make not specified');";
        echo "window.location.href='../view/vehicle_make';";
      echo "</script>";
    }
  
  }


  //VEHICLE MODEL PROCESS
  function getAllModels()
  {
    $jsonData = array(
      'type' => "getAllModelsPagination",
      'page' => 0,
      'size' => 50
    );
    return callVayaBiddingWebApiPost($jsonData);
  }
  function getMake($iMakeId)
  {
    $jsonData = array(
      'type' => "getMake",
      'iMakeId' => $iMakeId
    );
    return callVayaBiddingWebApiPost($jsonData);
  }
  
  
  
  
  // Save Vehicle Model
  
  if (isset($_POST['saveVehicleModel'])) {
  
    // receive all input values from the form
    $errors = array(); 
    $imakeId =  filter_var($_POST['imakeId'], FILTER_SANITIZE_NUMBER_INT);
    $vTitle = filter_var($_POST['vTitle'], FILTER_SANITIZE_STRING);

    $type = 'createModel';
  
  
    
    if(empty($errors)==true){
  
      $jsonData = array(
        'type'  => $type,
        'vTitle'  => $vTitle,
        'iMakeId'  => $imakeId
      );
  
      $result = callVayaBiddingWebApiPost($jsonData);
      
  
  
      if ($result['Action'] == 1){
        echo "<script>";
          echo "alert('Vehicle model created sucessfully');";
          echo "window.location.href='../view/vehicle_model ';";
        echo "</script>";
      } else {
        echo "<script>";
          echo "alert('".$result['Message']."');";
          echo "window.location.href='../view/vehicle_model ';";
        echo "</script>";
      }
    }else{
      echo "<script>";
        echo "alert('".$errors."');";
        echo "window.location.href='../view/vehicle_model ';";
      echo "</script>";
    }
  
  }
  
  //update Vehicle Model
  
  if (isset($_POST['editVehicleModel'])) {
  
    
    $errors = array(); 
  
    $imodelId =  filter_var($_GET['imodelId'], FILTER_SANITIZE_NUMBER_INT);
    $vTitle = filter_var($_POST['vTitle'], FILTER_SANITIZE_STRING);
    $iMakeId =  filter_var($_POST['iMakeId'], FILTER_SANITIZE_NUMBER_INT);
    $type = 'updateModel';

  
    if(empty($errors)==true){
  
      $jsonData = array(
        'type'  => $type,
        'iModelId'  => $imodelId,
        'VTitle'  => $VTitle,
        'iMakeId'  => $iMakeId
      );
  
      $result = callVayaBiddingWebApiPost($jsonData);
      
  
      if ($result['Action'] == 1){
        echo "<script>";
          echo "alert('Vehicle model updated sucessfully');";
          echo "window.location.href='../view/vehicle_model';";
        echo "</script>";
      } else {
        echo "<script>";
          echo "alert('".$result['Message']."');";
          echo "window.location.href='../view/vehicle_model';";
        echo "</script>";
      }
    }else{
      echo "<script>";
        echo "alert('".$errors."');";
        echo "window.location.href='../view/vehicle_model';";
      echo "</script>";
    }
  
  }
  
     // Delete Vehicle model
     if (isset($_POST['deleteVehicleModel'])) {
  
      $imodelId =  $_GET['imodelId'];
  
      
      
      if(isset($_GET['imodelId'])){
    
        $jsonData = array(
          'type' => "deleteModel",
          'iModelId' => $imodelId
        );
    
        $result = callVayaBiddingWebApiPost($jsonData);
  
      
    
        if ($result['Action'] == 1){
    
          echo "<script>";
            echo "alert('Vehicle Model deleted sucessfully');";
            echo "window.location.href='../view/vehicle_model';";
          echo "</script>";
          
        } else {
    
           echo "<script>";
          echo "alert('".$result['Message']."');";
          echo "window.location.href='../view/vehicle_model';";
        echo "</script>";
        }
        
      }else{
        echo "<script>";
          echo "alert('Vehicle model not specified');";
          echo "window.location.href='../view/vehicle_model';";
        echo "</script>";
      }
    
    }

//VEHICLE TYPE PROCESS
  
function getAllVehicleTypes()
{
  $jsonData = array(
    'type' => "getAllVehicleTypes"
  );
  return callVayaBiddingWebApiPost($jsonData);
}

function getVehicleCategory($vehicleCategoryId)
{
  $jsonData = array(
    'type' => "getVehicleCategory",
    'vehicleCategoryId' => $vehicleCategoryId
  );
  return callVayaBiddingWebApiPost($jsonData);
}



// Save VEHICLE TYPE

if (isset($_POST['saveVehicleType'])) {

 
  $errors = array(); 


  $vehicleCategoryId =  filter_var($_POST['vehicleCategoryId'], FILTER_SANITIZE_NUMBER_INT);
  $capacity = filter_var($_POST['capacity'], FILTER_SANITIZE_STRING);
  $specification = filter_var($_POST['specification'], FILTER_SANITIZE_STRING);

  $type = 'createVehicleType';


  if(empty($errors)==true){

    $jsonData = array(
      'type'  => $type,
      'capacity'  => $capacity,
      'specification'  => $specification,
      'vehicleCategoryId'  => $vehicleCategoryId
    );

    $result = callVayaBiddingWebApiPost($jsonData);
;

    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('Vehicle type  created sucessfully');";
        echo "window.location.href='../view/vehicle_type';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/vehicle_type';";
      echo "</script>";
    }
  }else{
    echo "<script>";
      echo "alert('".$errors."');";
      echo "window.location.href='../view/vehicle_type';";
    echo "</script>";
  }

}

//update VEHICLE TYPE

if (isset($_POST['editVehicleType'])) {

  
  $errors = array(); 
  $vehicleTypeId =  filter_var($_GET['vehicleTypeId'], FILTER_SANITIZE_NUMBER_INT);
  $vehicleCategoryId =  filter_var($_POST['vehicleCategoryId'], FILTER_SANITIZE_NUMBER_INT);
  $capacity = filter_var($_POST['capacity'], FILTER_SANITIZE_STRING);
  $specification = filter_var($_POST['specification'], FILTER_SANITIZE_STRING);
  $type = 'updateVehicleType';


  

  if(empty($errors)==true){

    $jsonData = array(
      'type'  => $type,
      'capacity'  => $capacity,
      'specification'  => $specification,
      'vehicleTypeId'  => $vehicleTypeId,
      'vehicleCategoryId'  => $vehicleCategoryId
    );

    $result = callVayaBiddingWebApiPost($jsonData);

    

    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('Vehicle type  updated sucessfully');";
        echo "window.location.href='../view/vehicle_type';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/vehicle_type';";
      echo "</script>";
    }
  }else{
    echo "<script>";
      echo "alert('".$errors."');";
      echo "window.location.href='../view/vehicle_type';";
    echo "</script>";
  }

}

   // Delete VEHICLE TYPE
   if (isset($_POST['deleteVehicleType'])) {

    $vehicleTypeId =  $_GET['vehicleTypeId'];
    
    if(isset($_GET['vehicleTypeId'])){
  
      $jsonData = array(
        'type' => "deleteVehicleType",
        'vehicleTypeId' => $vehicleTypeId
      );
  
      $result = callVayaBiddingWebApiPost($jsonData);
  
      if ($result['Action'] == 1){
  
        echo "<script>";
          echo "alert('Vehicle type  deleted sucessfully');";
          echo "window.location.href='../view/vehicle_type';";
        echo "</script>";
        
      } else {
  
         echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/vehicle_type';";
      echo "</script>";
      }
      
    }else{
      echo "<script>";
        echo "alert('Vehicle type  not specified');";
        echo "window.location.href='../view/vehicle_type';";
      echo "</script>";
    }
  
  }


  // Admin User login

if (isset($_POST['userLogin'])) {

    
    $errors = array();

  $username = filter_var($_POST['username'], FILTER_SANITIZE_STRING);
  $vPassword = filter_var($_POST['vPassword'], FILTER_SANITIZE_STRING);
    $userType  = "Admin";
    $type = 'userLogin';
  
    
    if(empty($errors)==true){
  
      $jsonData = array(
        'type'  => $type,
        'username'  => $username,
        'vPassword'  => $vPassword,
        'userType'  => $userType
      );
  
      $result = callVayaBiddingWebApiPost($jsonData);
      // var_dump($result);

     
   
   
      if ($result['Action'] == 1){
        $_SESSION["adminUsername"] = $username;
        if(isset($_SESSION["adminUsername"])) {

        
          echo "<script>";
          echo "alert('Admin login successful');";
          echo "window.location.href='../view/vehicle_category';";
          echo "</script>";
          }else{
            echo "<script>";
            echo "alert('failed to create session please try again');";
          echo "window.location.href='../../vaya-bidding/admin_login';";
          echo "</script>";

          }
       
      } else {
        echo "<script>";
        echo "alert('".$result['Message']."');";
      echo "window.location.href='../../vaya-bidding/admin_login';";
      echo "</script>";
       
      }
    }else{
      echo "<script>";
        echo "alert('".$errors."');";
        echo "window.location.href='../../vaya-bidding/admin_login';";
      echo "</script>";
    }
  
  }


//admin emails

function getAllAdminEmails()
{
  $jsonData = array(
    'type' => "getAllAdminEmails"
  );
  return callVayaBiddingWebApiPost($jsonData);
}




// Save admin email

if (isset($_POST['saveEmail'])) {

  
  $errors = array(); 
  // $email  =  $_POST['email'];

  $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
  $type = 'addAdminEmail';

  
  if(empty($errors)==true){

    $jsonData = array(
      'type'  => $type,
      'email'  => $email,
      
    );

    $result = callVayaBiddingWebApiPost($jsonData);
    

    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('Email created sucessfully');";
        echo "window.location.href='../view/email ';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/email ';";
      echo "</script>";
    }
  }else{
    echo "<script>";
      echo "alert('".$errors."');";
      echo "window.location.href='../view/email ';";
    echo "</script>";
  }

}

//update ADMIN EMAIL

if (isset($_POST['editEmail'])) {

  
  $errors = array(); 

  $emailId =  filter_var($_GET['emailId'], FILTER_SANITIZE_NUMBER_INT);
  $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
  
  $type = 'updateAdminEmail';

  

  if(empty($errors)==true){

    $jsonData = array(
      'type'  => $type,
      'emailId'  => $emailId,
      'email'  => $email,


      
    );

    $result = callVayaBiddingWebApiPost($jsonData);

    
    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('email updated sucessfully');";
        echo "window.location.href='../view/email ';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/email ';";
      echo "</script>";
    }
  }else{
    echo "<script>";
      echo "alert('".$errors."');";
      echo "window.location.href='../view/email ';";
    echo "</script>";
  }

}

   // Delete email
   if (isset($_POST['deleteEmail'])) {

  
    $emailId =  $_GET['emailId'];
    
    if(isset($_GET['emailId'])){
  
      $jsonData = array(
        'type' => "deleteAdminEmail",
        'emailId' => $emailId
      );
  
      $result = callVayaBiddingWebApiPost($jsonData);
  
      if ($result['Action'] == 1){
  
        echo "<script>";
          echo "alert('Email deleted sucessfully');";
          echo "window.location.href='../view/email';";
        echo "</script>";
        
      } else {
  
         echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/email';";
      echo "</script>";
      }
      
    }else{
      echo "<script>";
        echo "alert('Email not specified');";
        echo "window.location.href='../view/email';";
      echo "</script>";
    }
  
  }
//Get history of exchange rates
  function getExchangeRateHistory()
  {
    $jsonData = array(
      'type' => "getExchangeRateHistory"
    );
    return callVayaBiddingWebApiPost($jsonData);
  }
  //update Exchange Rate 

if (isset($_POST['editRate'])) {

  
  $errors = array(); 

  $exchangeRateId =  filter_var($_GET['exchangeRateId'], FILTER_SANITIZE_NUMBER_INT);
  $exchangeRate = filter_var($_POST['exchangeRate'], FILTER_SANITIZE_NUMBER_INT);
  
  $type = 'updateExchangeRate';

  

  if(empty($errors)==true){

    $jsonData = array(
      'type'  => $type,
      'exchangeRateId'  => $exchangeRateId,
      
      'exchangeRate'  => $exchangeRate,
      
    );

    $result = callVayaBiddingWebApiPost($jsonData);

    
    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('exchange rate updated sucessfully');";
        echo "window.location.href='../view/exchangerate';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/exchangerate';";
      echo "</script>";
    }
  }else{
    echo "<script>";
      echo "alert('".$errors."');";
      echo "window.location.href='../view/exchangerate';";
    echo "</script>";
  }

}
  


//Comissions

//Get history of comissions
function getCommissionHistory()
{
  $jsonData = array(
    // 'type' => "getCurrentCommission"
    'type' => "getCommissionHistory"
  );
  return callVayaBiddingWebApiPost($jsonData);
}
//update commission Rate 

if (isset($_POST['editComission'])) {


$errors = array(); 

$commissionId =  filter_var($_GET['commissionId'], FILTER_SANITIZE_NUMBER_INT);
$commissionPercentage = filter_var($_POST['commissionPercentage'], FILTER_SANITIZE_NUMBER_INT);

$type = 'addCommission';



if(empty($errors)==true){

  $jsonData = array(
    'type'  => $type,
    'commissionId'  => $commissionId,
    
    'commissionPercentage'  => $commissionPercentage,
    
  );

  $result = callVayaBiddingWebApiPost($jsonData);
    // var_dump($result);
    //  exit;
  
  if ($result['Action'] == 1){
    echo "<script>";
      echo "alert(' comission updated sucessfully');";
      echo "window.location.href='../view/comission';";
    echo "</script>";
  } else {
    echo "<script>";
      echo "alert('".$result['Message']."');";
      echo "window.location.href='../view/comission';";
    echo "</script>";
  }
}else{
  echo "<script>";
    echo "alert('".$errors."');";
    echo "window.location.href='../view/comission';";
  echo "</script>";
}

}



?>
