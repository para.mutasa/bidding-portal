<?php
session_start();
if (!isset($_SESSION['adminUsername']))
{
    header("Location: ../../vaya-bidding/admin_login ");
    die();
}

$adminUsername = $_SESSION['adminUsername'];

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta name="Referrer-Policy" value="no-referrer | same-origin"/>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    VAYA eLogistics
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="../assets/css/material-dashboard.css" rel="stylesheet" />


<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="blue" data-background-color="black" data-image="../assets/img/gomo.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo"><a href="#" class="simple-text logo-normal">
          VAYA eLogistics
        </a></div>
      <div class="sidebar-wrapper">
        <ul class="nav">
        <!-- <li class="nav-item">
            <a class="nav-link" href="./dashboard ">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li> -->
          <li class="nav-item " >
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="material-icons">local_shipping</i>
            Manage Vehicles
          </a>
          <div style=" background: linear-gradient(60deg, #1C7ACD, #1C7ACD);color: #fff;" class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
     
            <a class="dropdown-item" href="vehicle_category" referrerpolicy="no-referrer | same-origin | origin | strict-origin | no-origin-when-downgrading">Vehicle Category</a>
            <a class="dropdown-item" href="vehicle_type" referrerpolicy="no-referrer | same-origin | origin | strict-origin | no-origin-when-downgrading">Vehicle Type</a>
            <a class="dropdown-item" href="vehicle_make" referrerpolicy="no-referrer | same-origin | origin | strict-origin | no-origin-when-downgrading">Vehicle Make</a>
            <a class="dropdown-item" href="vehicle_model" referrerpolicy="no-referrer | same-origin | origin | strict-origin | no-origin-when-downgrading">Vehicle Model</a>
          </div>
        </li>
          <li class="nav-item">
            <a class="nav-link" href="./service_pillars ">
              <i class="material-icons">business_center</i>
              <p>Service Pillars</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./document" referrerpolicy="no-referrer | same-origin | origin | strict-origin | no-origin-when-downgrading">
              <i class="material-icons">content_paste</i>
              <p>Documents</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./currency" referrerpolicy="no-referrer | same-origin | origin | strict-origin | no-origin-when-downgrading">
              <i class="material-icons">local_atm</i>
              <p>Currencies</p>
            </a>
          </li>
          <li class="nav-item" >
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="material-icons">group</i>
            Manage Users
          </a>
          <div style=" background: linear-gradient(60deg, #1C7ACD, #1C7ACD);color: #fff;" class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="manage_admin ">Admins</a>
            <a class="dropdown-item" href="manage_customer ">Customers</a>
            <a class="dropdown-item" href="manage_partner ">Partners</a>
          </div>
        </li>

          <!-- <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="material-icons">content_paste</i>
              Reports
            </a>
            <div style=" background: linear-gradient(60deg, #1C7ACD, #1C7ACD);color: #fff;" class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="#">RTGS Payment Report</a>
              <a class="dropdown-item" href="#">USD Payment Report</a>
              <a class="dropdown-item" href="#">Trips Report</a>
            </div>
          </li> -->

          <li class="nav-item " >
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="material-icons">content_paste</i>
            Configurations 
          </a>
          <div style=" background: linear-gradient(60deg, #1C7ACD, #1C7ACD);color: #fff;" class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="faq"referrerpolicy="no-referrer | same-origin | origin | strict-origin | no-origin-when-downgrading">FAQ </a>
            <a class="dropdown-item" href="email"referrerpolicy="no-referrer | same-origin | origin | strict-origin | no-origin-when-downgrading">EMAILS </a>
            <a class="dropdown-item" href="exchangerate"referrerpolicy="no-referrer | same-origin | origin | strict-origin | no-origin-when-downgrading">Exchange Rate </a>
            <a class="dropdown-item" href="comission"referrerpolicy="no-referrer | same-origin | origin | strict-origin | no-origin-when-downgrading">Comission Rate </a>
            
          </div>
        </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="javascript:;">Admin Dashboard</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <ul class="navbar-nav">
        
              <li class="nav-item dropdown">
              <?php
              echo $date = date('Y-m-d H:i:s');
               ?>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <!-- <a class="dropdown-item" href="#">Profile</a> -->
                  <!-- <a class="dropdown-item" href="#">Settings</a> -->
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="logout ">Log out</a>
                </div>
              </li>
            </ul>
          <!-- <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="./dashboard ">
                  <i class="material-icons">dashboard</i>
                  <p class="d-lg-none d-md-block">
                    Stats
                  </p>
                </a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">notifications</i>
                  <span class="notification">5</span>
                  <p class="d-lg-none d-md-block">
                    Some Actions
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#">Pending trips</a>
                  <a class="dropdown-item" href="#">Pending Requests</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="#">Profile</a>
                  <a class="dropdown-item" href="#">Settings</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Log out</a>
                </div>
              </li>
            </ul>
          </div> -->
        </div>
      </nav>
      <!-- End Navbar -->