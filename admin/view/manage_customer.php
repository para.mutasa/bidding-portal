<?php

//session_start();
// if(!isset($_SESSION['userData']['id'], $_SESSION['userData']['username'], $_SESSION['userData']['userType'], $_SESSION["sess_Token"]))
// {
// 	echo "<script>";
//     echo "window.location.href='../login ?lmsg=true';";
//   echo "</script>";
// 	exit;
// }

require_once('../controller/bid_process.php');


$users = getAllCustomers();

?>

<?php require_once('dash_side.php');?>
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <button style="float: right;" data-toggle="modal" data-target="#addUserModal" class="btn btn-info ">Register Customer</button>
                  <!-- <button style="float: right;" data-toggle="modal" data-target="#exchangeRateModal" class="btn btn-info ">Exchange Rate</button> -->
                </div>
                <div class="card-body">
                  <div class="card-content table-responsive table-full-width">
                    <table id="dt-bordered" width="100%"  class="table table-sm table-bordered table-hover table-striped ">
                    <thead class="text-primary">
                    <th width ="15%">First Name</th>
                        <th width ="15%"> Last Name</th>
                        <th width ="25%"> Email</th>
                        <th width ="15%">Phone Number </th>
                        
                        <th width ="25%">Action</th>
                      </thead>
                      <tbody>

                        <?php if ($users['Action'] == 1) : ?>



                          <?php

                          $users['result'] = isset($users['result']) ?   $users['result'] : '';
                          $users['result'] = is_array($users['result']) ? $users['result'] : array();
                          foreach ($users['result'] as $user) :

                          ?>
                            <tr>
                            <td><?= htmlspecialchars($user['vfirstName'], ENT_QUOTES, 'UTF-8');?></td>
                            <td><?= htmlspecialchars($user['vlastName'], ENT_QUOTES, 'UTF-8');?></td>
                            <td><?= htmlspecialchars($user['vemail'], ENT_QUOTES, 'UTF-8');?></td>
                            <td><?= htmlspecialchars($user['vphone'], ENT_QUOTES, 'UTF-8');?></td>
                   
                             
                            <td>
                                <button type="button" rel="tooltip" title="Edit" class="btn btn-primary btn-link btn-sm">
                                  <i data-target="#update<?= $user['iuserId'] ?>" data-toggle="modal" class="material-icons">edit</i>
                                </button>
                                <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-link btn-sm">
                                  <i data-target="#delete<?= $user['iuserId'] ?>" data-toggle="modal" class="material-icons">close</i>
                                </button>
                              </td>
                            </tr>
                            

                            <!--Edit User individual Modal -->
                            <div id="update<?= $user['iuserId'] ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form action ="../controller/bid_process?iuserId=<?=$user['iuserId']?>"  method = "post" enctype="multipart/form-data">
                                    <div class="modal-body">
                                      <div class="col">
                                        <div class="form-group label-floating">
                                          <input type="text" class="form-control" placeholder="First Name" name ="vFirstName" value="<?= $user['vfirstName'] ?>" required>
                                          
                                        </div>
                                      </div>

                                      <div class="col">
                                        <div class="form-group label-floating">
                                          <input type="text" class="form-control" placeholder="Last Name"  name ="vLastName" value="<?= $user['vlastName'] ?>" required>
                                          
                                        </div>
                                      </div>

                                      <div class="col">
                                        <div class="form-group label-floating">
                                          <input type="text" class="form-control" placeholder="Phone Number"  name ="vPhone" value="<?= $user['vphone'] ?>" required>
                                          
                                        </div>
                                      </div>

                                      <div class="col">
                                        <div class="form-group label-floating">
                                          <input type="text" class="form-control" placeholder="Email "  name="vEmail" value="<?= $user['vemail'] ?>"required>
                                          
                                        </div>
                                      </div>

                                      <div class="col">
                                        <div class="form-group label-floating">
                                          <input type="text" class="form-control" placeholder="Invite Code" name="vInviteCode" value="<?= $user['vinviteCode'] ?>">
                                        </div>
                                      </div>

                                      <div class="col">
                                        <div class="form-group label-floating">
                                          <input type="text" class="form-control" placeholder="Phone Code" name="phoneCode" value="<?= $user['phoneCode'] ?>">
                                        </div>
                                      </div>

                                      <div class="col">
                                        <div class="form-group label-floating">
                                          <input type="text" class="form-control" placeholder="Country Code" name="countryCode" value="<?= $user['countryCode'] ?>">
                                        </div>
                                      </div>

                                     

                                      <div class="col">
                                        <div class="form-group label-floating">
                                          <input type="text" class="form-control" placeholder="Language" name="vLang" value="<?= $user['vlang'] ?>">
                                        </div>
                                      </div>

                                      <div class="col">
                                        <div class="form-group label-floating">
                                          <input type="text" class="form-control" placeholder="Curreny" name="vCurrency" value="<?= $user['vcurrency'] ?>">
                                        </div>
                                      </div>
                                      <div class="col">
                                        <div class="form-group label-floating">
                                          <input type="text" class="form-control"  name="vPassword" value="<?= $user['vpassword'] ?>"required>
                                          <span id="passwords" class="text-danger font-weight-bold"></span>
                                        </div>
                                      </div>
<!-- 
                                      <div class="col">
                                        <div class="form-group label-floating">
                                          <input type="text" class="form-control" placeholder="Device Type" name="vDeviceType" value="<?= $user['vdeviceType'] ?>"required>
                                        </div>
                                      </div> -->

                                  

                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      <input type="submit" name="editCustomer" class="btn btn-primary" value="Edit" />
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                            <!--end modal-->



                            <!--Delete  User Modal -->
                            <div id="delete<?= $user['iuserId'] ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel"> Users</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form action ="../controller/bid_process?iuserId=<?=$user['iuserId']?>" method = "post" enctype="multipart/form-data">
                             
                                  <div class="modal-body">
                                    <h4 class="text-primary">Are you sure you want to delete this user?</h4>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <input type="submit" name="deleteCustomer" class="btn btn-primary" value="Confirm" />
                                  </div>
                                  </form>
                                </div>
                              </div>
                            </div>

                                   <!--end modal-->

                    
                          <?php endforeach; ?>

                        <?php else : ?>

                          <?= $users['Message'] ?>

                        <?php endif; ?>

                      </tbody>

                    </table>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
      <!-- Add Customer Modal -->
      <div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Register New User </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action ="../controller/bid_process"  onsubmit="return validation()" method = "POST" enctype="multipart/form-data">
              <div class="modal-body">
                <div class="col">
                  <div class="form-group label-floating">
                    <input type="text" class="form-control" placeholder="First Name" id ="vFirstName" name="vFirstName" required>
                    <span id="fname" class="text-danger font-weight-bold"></span>
                  </div>
                </div>

                <div class="col">
                  <div class="form-group label-floating">
                    <input type="text" class="form-control" placeholder="Last  Name "id="vLastName" name="vLastName" required>
                    <span id="sname" class="text-danger font-weight-bold"></span>
                  </div>
                </div>

                <div class="col">
                  <div class="form-group label-floating">
                    <input type="email" class="form-control" placeholder="Email" id="vEmail" name="vEmail" required>
                    <span id="mail" class="text-danger font-weight-bold"></span>
                  </div>
                </div>

                

                <div class="col">
                  <div class="form-group label-floating">
                    <input type="text" class="form-control" placeholder="Phone Number" id="vPhone" name="vPhone" required>
                    <span id="phone" class="text-danger font-weight-bold"></span>
                  </div>
                </div>

                <div class="col">
                  <div class="form-group label-floating">
                    <input type="password" class="form-control" placeholder="Password" id="vPassword" name="vPassword" required>
                    <span id="passwords" class="text-danger font-weight-bold"></span>
                  </div>
                </div>

                <div class="col">
                  <div class="form-group label-floating">
                    <input type="text" class="form-control" placeholder="Language"id="vLang" name="vLang" >
                    <span id="language" class="text-danger font-weight-bold"></span>
                  </div>
                </div>

                

                <div class="col">
                  <div class="form-group label-floating">
                    <input type="text" class="form-control" placeholder="Country Code" id="countryCode"name="countryCode" >
                    <span id="cCode" class="text-danger font-weight-bold"></span>
                  </div>
                </div>

                <div class="col">
                  <div class="form-group label-floating">
                    <input type="text" class="form-control" placeholder="Phone Code"id="phoneCode" name="phoneCode" >
                    <span id="pCode" class="text-danger font-weight-bold"></span>
                  </div>
                </div>

                

                <div class="col">
                  <div class="form-group label-floating">
                    <input type="text" class="form-control" placeholder="Invite Code" id="vInviteCode" name="vInviteCode">
                    <span id="invitecode" class="text-danger font-weight-bold"></span>
                  </div>
                </div>

                <div class="col">
                  <div class="form-group label-floating">
                    <input type="text" class="form-control" placeholder=" Currency" id="vCurrency" name="vCurrency" >
                    <span id="currency" class="text-danger font-weight-bold"></span>
                  </div>
                </div>

                <div class="col">
                  <div class="form-group label-floating">
                    <input type="text" class="form-control" placeholder=" Device Type" id="vDeviceType" name="vDeviceType">
                    <span id="devicetype" class="text-danger font-weight-bold"></span>
                  </div>
                </div>

                <div class="col">
                   <div class="form-group label-floating">
                      <select name="userType">
                      <!--<option value="0">--SELECT Partner Type--</option>-->
                      <option value="Customer">Customer</option>
                      <!--<option value="Corporate">Corporate</option>-->
                                          
                      </select>
                    </div>
                </div>
     
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <input type="submit" name="saveCustomer" class="btn btn-primary" value="Add" />
              </div>
            </form>
          </div>
        </div>
      </div>
      <footer class="footer">
        <div class="container-fluid">

          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>, designed with <i class="material-icons">favorite</i> by
            <a href="https://www.dev.vayaafrica.com/" target="_blank">VAYA</a>
          </div>
        </div>
      </footer>
    </div>
  </div>
<script>
  function convertCurrency() {
      var from = document.getElementById("from").value;
      var to = document.getElementById("to").value;
      var xmlhttp = new XMLHttpRequest();
      var url = "https://data.fixer.io/api/latest" + from + "," + to;
      xmlhttp.open("GET", url, true);
      xmlhttp.send();
      xmlhttp.onreadystatechange = function(){
        if(xmlhttp.readyState == 4  && xmlhttp.status == 200){
          var result = xmlhttp.responseText;
          alert(result);
          var jsResult = JSON.parse(result);
          var oneUnit = jsResult.rates[to]/jsResult.rates[from];
          var amt = document.getElementById("fromAmount").value;
          document.getElementById("toAmount").value = (oneUnit*amt).toFixed(2);
        }
      }
  }
</script>
  <script type="text/javascript">
            function validation() {
               
                

                var VCurrencyName = document.getElementById('VCurrencyName').value;
                if (!isNaN(VCurrencyName)) {
                    document.getElementById('currency').innerHTML = " ** Please ONLY characters are allowed";
                    return false;
                }

            

                // var VSymbol = document.getElementById('VSymbol').value;
               
                // if (VSymbol.length != 1) {
                //     document.getElementById('symbol').innerHTML = " ** currency symbol must have 1 character only";
                //     return false;
                // }

               

            }
        </script>

  <!--   Core JS Files   -->
  <script src="../assets/js/core/jquery.min.js"></script>
  <script src="../assets/js/core/popper.min.js"></script>
  <script src="../assets/js/core/bootstrap-material-design.min.js"></script>
  <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!-- Plugin for the momentJs  -->
  <script src="../assets/js/plugins/moment.min.js"></script>
  <!--  Plugin for Sweet Alert -->
  <script src="../assets/js/plugins/sweetalert2.js"></script>
  <!-- Forms Validations Plugin -->
  <script src="../assets/js/plugins/jquery.validate.min.js"></script>
  <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
  <script src="../assets/js/plugins/jquery.bootstrap-wizard.js"></script>
  <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
  <script src="../assets/js/plugins/bootstrap-selectpicker.js"></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src="../assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
  <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
  <script src="../assets/js/plugins/jquery.dataTables.min.js"></script>
  <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
  <script src="../assets/js/plugins/bootstrap-tagsinput.js"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="../assets/js/plugins/jasny-bootstrap.min.js"></script>
  <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
  <script src="../assets/js/plugins/fullcalendar.min.js"></script>
  <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
  <script src="../assets/js/plugins/jquery-jvectormap.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="../assets/js/plugins/nouislider.min.js"></script>
  <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
  <!-- Library for adding dinamically elements -->
  <script src="../assets/js/plugins/arrive.min.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chartist JS -->
  <script src="../assets/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="../assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../assets/js/material-dashboard.js?v=2.1.2" type="text/javascript"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <script src="../assets/demo/demo.js"></script>
  <script>
    $(document).ready(function() {
      $('#dt-bordered').dataTable({
        dom: 'Bfrtip',

        buttons: [
          'excel', 'csv'
        ],
        columnDefs: [{
          orderable: false,
          className: 'select-checkbox',
          targets: 0
        }],
        select: {
          style: 'multi',
          selector: 'td:first-child'
        }
      });
    });
  </script>
  <script>
    $(document).ready(function() {
      $().ready(function() {
        $sidebar = $('.sidebar');

        $sidebar_img_container = $sidebar.find('.sidebar-background');

        $full_page = $('.full-page');

        $sidebar_responsive = $('body > .navbar-collapse');

        window_width = $(window).width();

        fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

        if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
          if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
            $('.fixed-plugin .dropdown').addClass('open');
          }

        }

        $('.fixed-plugin a').click(function(event) {
          // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
          if ($(this).hasClass('switch-trigger')) {
            if (event.stopPropagation) {
              event.stopPropagation();
            } else if (window.event) {
              window.event.cancelBubble = true;
            }
          }
        });

        $('.fixed-plugin .active-color span').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-color', new_color);
          }

          if ($full_page.length != 0) {
            $full_page.attr('filter-color', new_color);
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.attr('data-color', new_color);
          }
        });

        $('.fixed-plugin .background-color .badge').click(function() {
          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('background-color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-background-color', new_color);
          }
        });

        $('.fixed-plugin .img-holder').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).parent('li').siblings().removeClass('active');
          $(this).parent('li').addClass('active');


          var new_image = $(this).find("img").attr('src');

          if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            $sidebar_img_container.fadeOut('fast', function() {
              $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
              $sidebar_img_container.fadeIn('fast');
            });
          }

          if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $full_page_background.fadeOut('fast', function() {
              $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
              $full_page_background.fadeIn('fast');
            });
          }

          if ($('.switch-sidebar-image input:checked').length == 0) {
            var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
          }
        });

        $('.switch-sidebar-image input').change(function() {
          $full_page_background = $('.full-page-background');

          $input = $(this);

          if ($input.is(':checked')) {
            if ($sidebar_img_container.length != 0) {
              $sidebar_img_container.fadeIn('fast');
              $sidebar.attr('data-image', '#');
            }

            if ($full_page_background.length != 0) {
              $full_page_background.fadeIn('fast');
              $full_page.attr('data-image', '#');
            }

            background_image = true;
          } else {
            if ($sidebar_img_container.length != 0) {
              $sidebar.removeAttr('data-image');
              $sidebar_img_container.fadeOut('fast');
            }

            if ($full_page_background.length != 0) {
              $full_page.removeAttr('data-image', '#');
              $full_page_background.fadeOut('fast');
            }

            background_image = false;
          }
        });

        $('.switch-sidebar-mini input').change(function() {
          $body = $('body');

          $input = $(this);

          if (md.misc.sidebar_mini_active == true) {
            $('body').removeClass('sidebar-mini');
            md.misc.sidebar_mini_active = false;

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

          } else {

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

            setTimeout(function() {
              $('body').addClass('sidebar-mini');

              md.misc.sidebar_mini_active = true;
            }, 300);
          }

          // we simulate the window Resize so the charts will get updated in realtime.
          var simulateWindowResize = setInterval(function() {
            window.dispatchEvent(new Event('resize'));
          }, 180);

          // we stop the simulation of Window Resize after the animations are completed
          setTimeout(function() {
            clearInterval(simulateWindowResize);
          }, 1000);

        });
      });
    });
  </script>

<script>
document.addEventListener('keydown', function() {
  if (event.keyCode == 123) {
    alert("This function has been disabled!");
    return false;
  } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) {
    alert("This function has been disabled!");
    return false;
  } else if (event.ctrlKey && event.keyCode == 85) {
    alert("This function has been disabled!");
    return false;
  }
}, false);

if (document.addEventListener) {
  document.addEventListener('contextmenu', function(e) {
    alert("This function has been disabled!");
    e.preventDefault();
  }, false);
} else {
  document.attachEvent('oncontextmenu', function() {
    alert("This function has been disabled!");
    window.event.returnValue = false;
  });
}
</script>

</body>

</html>