<?php

// session_start();
// if(!isset($_SESSION['userData']['id'], $_SESSION['userData']['username'], $_SESSION['userData']['userType'], $_SESSION["sess_Token"]))
// {
// 	echo "<script>";
//     echo "window.location.href='../login ?lmsg=true';";
//   echo "</script>";
// 	exit;
// }


require_once('../controller/bid_process.php');

 function getvehicleCategoryById($vehicleCategoryId)
 {
  $jsonData = array(
    'type' => "getVehicleCategory",
    'vehicleCategoryId' => $vehicleCategoryId
   );
   return callVayaBiddingWebApiPost($jsonData);
 }



$users = getAllPartners();
 $vehicle_types =  getAllVehicleTypes();
 $servicepillars =  getAllServicePillars();

 $filteredVehicles =  getFilteredVehicleTypes();


 $filteredJsonVehicles =  json_encode($filteredVehicles);


?>

<?php require_once('dash_side.php');?>
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <button style="float: right;" data-toggle="modal" data-target="#addUserModal" class="btn btn-info">Register New Partner [Individual]</button>
 
                
                  <button style="float: right;" data-toggle="modal" data-target="#addUserModal2" class="btn btn-info">Register New Partner [SME]</button>
 
                </div>
                <div class="card-body">
                  <div class="card-content table-responsive table-full-width">
                  <table id="dt-bordered" width="100%"  class="table table-sm table-bordered table-hover table-striped ">
                      <thead class="text-primary">
                        <th width ="15%"> Name</th>                      
                        <th width ="20%"> Email</th>
                        <th width ="15%"> Partner Type</th>
                        <th width ="15%">Phone Number </th>
                        <th width ="20%">Address </th>
                        
                        <th width ="15%">Action</th>
                      </thead>
                      <tbody>

                        <?php if ($users['Action'] == 1) : ?>

                          <?php
                           $users['result'] = isset($users['result']) ?   $users['result'] : '';
                            $users['result'] = is_array($users['result']) ? $users['result'] : array();
                             foreach ($users['result'] as $user) :
                              ?>
                            <tr>
                              <td><?= $user['name'] ?></td>
                              <td><?= $user['vemail'] ?></td>
                              <td><?= $user['partnerType'] ?></td>
                              <td><?= $user['phoneNumber'] ?></td>
                              <td><?= $user['vcaddress'] ?></td>

                              <td>
                              <?php
                              if ($user['partnerType'] == "Individual") {

                                echo "<button type='button' class='btn btn-primary'  data-whatever='@getbootstrap' data-toggle='modal' data-target='#updateIndividual" . $user['idriverId'] . "' >Edit
                                </button>"; 
                                  
                              } else {
                                echo "<button type='button' class='btn btn-primary'  data-whatever='@getbootstrap' data-toggle='modal' data-target='#updateCorporate" . $user['idriverId'] . "' >Edit</button>"; 
                              }
                              
                              ?>
                            
                                <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-link btn-sm">
                                  <i data-target="#delete<?= $user['idriverId'] ?>" data-toggle="modal" class="material-icons">close</i>
                                </button>
                              </td>
                            </tr>



                            <!--Edit  Individual User Modal -->
                            <div id="updateIndividual<?= $user['idriverId'] ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form action ="../controller/bid_process?idriverId=<?=$user['idriverId']?>"  method = "post" enctype="multipart/form-data">
                                    <div class="modal-body">
                                      <div class="col">
                                        <div class="form-group label-floating">
                                          <input type="text" class="form-control" placeholder="First Name"  name ="name" value="<?= $user['name'] ?>" required>
                                          
                                        </div>
                                      </div>

                                      <div class="col">
                                        <div class="form-group label-floating">
                                          <input type="text" class="form-control" placeholder="Last Name"  name ="surname" value="<?= $user['surname'] ?>" required>
                                         
                                        </div>
                                      </div>

                                      <div class="col">
                                        <div class="form-group label-floating">
                                          <input type="text" class="form-control" placeholder="Phone Number"  name ="phoneNumber" value="<?= $user['phoneNumber'] ?>" required>
                                        
                                        </div>
                                      </div>

                                      <div class="col">
                                        <div class="form-group label-floating">
                                          <input type="text" class="form-control" placeholder="Email "  name="vemail" value="<?= $user['vemail'] ?>"required>
                                          
                                        </div>
                                      </div>

                                      <div class="col">
                                        <div class="form-group label-floating">
                                          <input type="text" class="form-control" placeholder="Address " name ="vcaddress" value="<?= $user['vcaddress'] ?>"required>
                                        </div>
                                      </div>




   <div class="col">
      <div class="form-group label-floating">
       <?php
        
        $arr = array();
        foreach ($filteredVehicles['result']  as  $filteredVehicle) {
            $arr[] = $filteredVehicle["name"];
        }
        $unique_data = array_unique($arr);
      
      ?>
                    <select class="form-control" name="vehicleTypeId" required>
                        <?php foreach ($filteredVehicles['result']  as $filteredVehicle) : ?>
                          <option value="<?= $filteredVehicle["vehicleTypeId"] ?>"><?= $filteredVehicle["name"] . "" ?></option>
                            <?php endforeach; ?>
                    </select>
        </div>
    </div>



                <div class="col">
                  <div class="form-group label-floating">
                  <select class="form-control" name="vehicleTypeId" required>
                    <?php foreach ($vehicle_types['result']  as $vehicle_type) : ?>
                    <option value="<?= $vehicle_type["vehicleTypeId"] ?>"><?= $vehicle_type["capacity"] . " " .(getvehicleCategoryById($vehicle_type['vehicleCategoryId'])['result']['name'] ). "" ?></option>

                    <?php endforeach; ?>
                  </select>
                  </div>
                </div>


               <div class="col">
                  <div class="form-group label-floating">
                  <select class="form-control" name="servicePillarId" required>
                   <?php foreach ($servicepillars['result']  as $servicepillar) : ?>
                    <option value="<?= $servicepillar["servicePillarId"] ?>"><?= $servicepillar["name"] . "" ?></option>
                    <?php endforeach; ?>
                  </select>
                  </div>
                </div>
 

                                      <!-- <div class="col">
                                        <div class="form-group label-floating">
                                          <input type="text" class="form-control" Placeholder="Password" name="vPassword" value="<?= $user['vpassword'] ?>"required>
                                          <span id="passwords" class="text-danger font-weight-bold"></span>
                                        </div>
                                      </div> -->


                    
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      <input type="submit" name="editPartnerIndividual" class="btn btn-primary" value="Edit" />
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                            <!--end modal-->

                             <!--Edit  SME User Modal -->
                             <div id="updateCorporate<?= $user['idriverId'] ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form action ="../controller/bid_process?idriverId=<?=$user['idriverId']?>" method = "post" enctype="multipart/form-data">
                                    <div class="modal-body">
                                      <div class="col">
                                        <div class="form-group label-floating">
                                          <input type="text" class="form-control" placeholder=" Name" name ="name" value="<?= $user['name'] ?>" required>
                                         
                                        </div>
                                      </div>

                                      <div class="col">
                                        <div class="form-group label-floating">
                                          <input type="text" class="form-control" placeholder="address" name ="vCaddress" value="<?= $user['vcaddress'] ?>" required>
                                        </div>
                                      </div>

                                      <div class="col">
                                        <div class="form-group label-floating">
                                          <input type="text" class="form-control" placeholder="zimra BP Number" name ="zimraBpNumber" value="<?= $user['zimraBpNumber'] ?>" required>
                                        </div>
                                      </div>

                                      <div class="col">
                                        <div class="form-group label-floating">
                                          <input type="text" class="form-control" placeholder="Phone Number" name ="vPhone" value="<?= $user['phoneNumber'] ?>" required>
                                  
                                        </div>
                                      </div>

                                      <div class="col">
                                        <div class="form-group label-floating">
                                          <input type="text" class="form-control" placeholder="Email"  name ="vEmail" value="<?= $user['vemail'] ?>" required>
                                          
                                        </div>
                                      </div>

                                      <div class="col">
                                        <div class="form-group label-floating">
                                          <input type="text" class="form-control" placeholder="country code" name ="countryCode" value="<?= $user['countryCode'] ?>" required>
                                        </div>
                                      </div>

                                      <div class="col">
                                        <div class="form-group label-floating">
                                          <input type="text" class="form-control" placeholder="phone code" name ="phoneCode" value="<?= $user['phoneCode'] ?>" required>
                                        </div>
                                      </div>


                    
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      <input type="submit" name="editSME" class="btn btn-primary" value="Edit" />
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                            <!--end modal-->

                             <!--Delete  User Modal -->
                             <div id="delete<?= $user['idriverId'] ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel"> Users</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form action ="../controller/bid_process?idriverId=<?=$user['idriverId']?>" method = "post" enctype="multipart/form-data">
                             
                                  <div class="modal-body">
                                    <h4 class="text-primary">Are you sure you want to delete this user?</h4>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <input type="submit" name="deletePartner" class="btn btn-primary" value="Confirm" />
                                  </div>
                                  </form>
                                </div>
                              </div>
                            </div>

                                   <!--end modal-->
                          <?php endforeach; ?>

                        <?php else : ?>

                          <?= $users['Message'] ?>

                        <?php endif; ?>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
      <!-- Add Partner Individual   Modal -->
      <div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Register New Partner </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action ="../controller/bid_process" onsubmit="return validation()" method = "post" enctype="multipart/form-data">
              <div class="modal-body">
                <div class="col">
                  <div class="form-group label-floating">
                    <input type="text" class="form-control" placeholder="First Name" id="name" name="name" required>
                    <span id="fname" class="text-danger font-weight-bold"></span>
                  </div>
                </div>

                <div class="col">
                  <div class="form-group label-floating">
                    <input type="text" class="form-control" placeholder="Last Name" id="surname" name="surname" required>
                    <span id="sname" class="text-danger font-weight-bold"></span>
                  </div>
                </div>

               

                <div class="col">
                  <div class="form-group label-floating">
                    <input type="text" class="form-control" placeholder="Email" id="vEmail" name="vEmail" required>
                    <span id="mail" class="text-danger font-weight-bold"></span>
                  </div>
                </div>

                <div class="col">
                  <div class="form-group label-floating">
                    <input type="text" class="form-control" placeholder="address " name="vCaddress" required>
                    <span id="address" class="text-danger font-weight-bold"></span>
                  </div>
                </div>

                

              
                <div class="col">
                  <div class="form-group label-floating">
                    <input type="text" class="form-control" placeholder="Phone Number" id="phoneNumber" name="phoneNumber" required>
                    <span id="phone" class="text-danger font-weight-bold"></span>
                  </div>
                </div>

                <div class="col">
                  <div class="form-group label-floating">
                    <input type="password" class="form-control" placeholder="Password" id="vPassword" name="vPassword" required>
                    <span id="passwords" class="text-danger font-weight-bold"></span>
                  </div>
                </div>

              
                <div class="col">
                  <div class="form-group label-floating">
                  <?php
        
        $arr = array();
        foreach ($filteredVehicles['result']  as  $filteredVehicle) {
            $arr[] = $filteredVehicle["name"];
        }
        $unique_data = array_unique($arr);
      
      ?>
                  <select id="category" onChange="getCapacity();" class="form-control" name="vehicleTypeId">
          <option value="0">No Vehicle Category Selected</option>
          <?php foreach ($unique_data  as  $val) : ?>
            <option value="<?= $val ?>"><?= $val?></option>
          <?php endforeach; ?>
        </select>
                  </div>
                </div>






                

                <div class="col">
                  <div class="form-group label-floating">
                  <select  class="form-control" id="capacity">
          <option value="0">No Vehicle Size Selected</option>
          </select> 
          <input type="hidden" id="myInputID" class="form-control"  name ="vehicleTypeId" >
                  </div>
                </div>

                

                

              

                

                

                <div class="col">
                  <div class="form-group label-floating">
                  <select class="form-control" name="servicePillarId" required>
                                            <?php foreach ($servicepillars['result']  as $servicepillar) : ?>
                                              <option value="<?= $servicepillar["servicePillarId"] ?>"><?= $servicepillar["name"] . "" ?></option>
                                            <?php endforeach; ?>
                                          </select>
                  </div>
                </div>


                <div class="col">
                  <div class="form-group label-floating">
                    <input type="text" class="form-control" placeholder="referral Code" name="referralCode">
                    <span id="referralcode" class="text-danger font-weight-bold"></span>
                    
                  </div>
                </div>


               

                <div class="col">
                  <div class="form-group label-floating">
                     <select name="partnerType">
                       <!-- <option value="0">--SELECT Partner Type--</option>-->
                        <option value="Individual">Individual</option>
                       <!-- <option value="Corporate">Corporate</option>-->
                                          
                      </select>
                    </div>
                </div>

                


     
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <input type="submit" name="savePartnerIndividual" class="btn btn-primary" value="Add" />
              </div>
            </form>
          </div>
        </div>
      </div>

       <!-- Add Partner SME   Modal -->
       <div class="modal fade" id="addUserModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Register New Partner SME </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action ="../controller/bid_process" method = "post" enctype="multipart/form-data">
              <div class="modal-body">
                <div class="col">
                  <div class="form-group label-floating">
                    <input type="text" class="form-control" placeholder=" Name" name="name" required>
                
                    
                  </div>
                </div>

                
  
                <div class="col">
                  <div class="form-group label-floating">
                    <input type="text" class="form-control" placeholder="Email "name="vEmail" required>
                    
                  </div>
                </div>

                <div class="col">
                  <div class="form-group label-floating">
                    <input type="text" class="form-control" placeholder="Phone Code " name="phoneCode">
                    
                  </div>
                </div>
                       
                <div class="col">
                  <div class="form-group label-floating">
                    <input type="text" class="form-control" placeholder="Phone Number"  name="vPhone" required>
                   
                  </div>
                </div>
              

                <div class="col">
                  <div class="form-group label-floating">
                    <input type="password" class="form-control" placeholder="Password"  name="vPassword" required>
                    
                  </div>
                </div>

                <div class="col">
                  <div class="form-group label-floating">
                    <input type="text" class="form-control" placeholder="Address "  name="vCaddress" required>
                    
                  </div>
                </div>


                <div class="col">
                  <div class="form-group label-floating">
                    <input type="text" class="form-control" placeholder="Country Code"  name="countryCode">
               
                  </div>
                </div>

                <div class="col">
                  <div class="form-group label-floating">
                    <input type="text" class="form-control" placeholder="Zimra Bp Number"  name="zimraBpNumber" required>
                    
                  </div>
                </div>


                


     
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <input type="submit" name="savePartnerSME" class="btn btn-primary" value="Add" />
              </div>
            </form>
          </div>
        </div>
      </div>
  
      <footer class="footer">
        <div class="container-fluid">

          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>, designed with <i class="material-icons">favorite</i> by
            <a href="https://www.dev.vayaafrica.com/" referrerpolicy="no-referrer | same-origin | origin | strict-origin | no-origin-when-downgrading"target="_blank">VAYA</a>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <script type="text/javascript">
            function validation() {
                var vEmail = document.getElementById('vEmail').value;
                

                var name = document.getElementById('name').value;
                if (!isNaN(name)) {
                    document.getElementById('fname').innerHTML = " ** Please ONLY characters are allowed";
                    return false;
                }

                var surname = document.getElementById('surname').value;
                if (!isNaN(surname)) {
                    document.getElementById('sname').innerHTML = " ** Please ONLY characters are allowed";
                    return false;
                }

                var phoneNumber = document.getElementById('phoneNumber').value;
                if (isNaN(phoneNumber)) {
                    document.getElementById('phone').innerHTML = " ** Please enter numbers only";
                    return false;
                }
                // if (phoneNumber.length != 10) {
                //     document.getElementById('phone').innerHTML = " ** phone number must have 10 numbers only";
                //     return false;
                // }

               

                var vPassword = document.getElementById('vPassword').value;
                if ((vPassword.length) <= 2 || (vPassword.length > 20)) {
                    document.getElementById('passwords').innerHTML = " ** password must be between 3 and 10 characters";
                    return false;
                }


                if (vEmail == "") {
                    document.getElementById('mail').innerHTML = "**Please fill in your email";
                    return false;
                }

                if (vEmail.indexOf('@') <= 0) {
                    document.getElementById("mail").innerHTML = "** @ invalid position";
                    return false;

                }
                if ((vEmail.charAt(vEmail.length - 4) != '.') && (vEmail.charAt(VEmail.length - 3) != '.')) {
                    document.getElementById("mail").innerHTML = "**invalid email";
                    return false;
                }
            }
        </script>



  <!--   Core JS Files   -->
  <script src="../assets/js/core/jquery.min.js"></script>
  <script src="../assets/js/core/popper.min.js"></script>
  <script src="../assets/js/core/bootstrap-material-design.min.js"></script>
  <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!-- Plugin for the momentJs  -->
  <script src="../assets/js/plugins/moment.min.js"></script>
  <!--  Plugin for Sweet Alert -->
  <script src="../assets/js/plugins/sweetalert2.js"></script>
  <!-- Forms Validations Plugin -->
  <script src="../assets/js/plugins/jquery.validate.min.js"></script>
  <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
  <script src="../assets/js/plugins/jquery.bootstrap-wizard.js"></script>
  <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
  <script src="../assets/js/plugins/bootstrap-selectpicker.js"></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src="../assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
  <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
  <script src="../assets/js/plugins/jquery.dataTables.min.js"></script>
  <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
  <script src="../assets/js/plugins/bootstrap-tagsinput.js"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="../assets/js/plugins/jasny-bootstrap.min.js"></script>
  <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
  <script src="../assets/js/plugins/fullcalendar.min.js"></script>
  <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
  <script src="../assets/js/plugins/jquery-jvectormap.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="../assets/js/plugins/nouislider.min.js"></script>
  <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
  <!-- Library for adding dinamically elements -->
  <script src="../assets/js/plugins/arrive.min.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chartist JS -->
  <script src="../assets/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="../assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../assets/js/material-dashboard.js?v=2.1.2" type="text/javascript"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <script src="../assets/demo/demo.js"></script>
  <script>
$(document).ready(function () {
  $('#dt-bordered').dataTable({
    dom: 'Bfrtip',
    
    buttons: [
       'excel', 'csv'
        ],
    columnDefs: [{
      orderable: false,
      className: 'select-checkbox',
      targets: 0
    }],
    select: {
      style: 'multi',
      selector: 'td:first-child'
    }
  });
});
  </script>
  <script>
    $(document).ready(function() {
      $().ready(function() {
        $sidebar = $('.sidebar');

        $sidebar_img_container = $sidebar.find('.sidebar-background');

        $full_page = $('.full-page');

        $sidebar_responsive = $('body > .navbar-collapse');

        window_width = $(window).width();

        fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

        if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
          if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
            $('.fixed-plugin .dropdown').addClass('open');
          }

        }

        $('.fixed-plugin a').click(function(event) {
          // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
          if ($(this).hasClass('switch-trigger')) {
            if (event.stopPropagation) {
              event.stopPropagation();
            } else if (window.event) {
              window.event.cancelBubble = true;
            }
          }
        });

        $('.fixed-plugin .active-color span').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-color', new_color);
          }

          if ($full_page.length != 0) {
            $full_page.attr('filter-color', new_color);
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.attr('data-color', new_color);
          }
        });

        $('.fixed-plugin .background-color .badge').click(function() {
          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('background-color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-background-color', new_color);
          }
        });

        $('.fixed-plugin .img-holder').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).parent('li').siblings().removeClass('active');
          $(this).parent('li').addClass('active');


          var new_image = $(this).find("img").attr('src');

          if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            $sidebar_img_container.fadeOut('fast', function() {
              $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
              $sidebar_img_container.fadeIn('fast');
            });
          }

          if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $full_page_background.fadeOut('fast', function() {
              $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
              $full_page_background.fadeIn('fast');
            });
          }

          if ($('.switch-sidebar-image input:checked').length == 0) {
            var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
          }
        });

        $('.switch-sidebar-image input').change(function() {
          $full_page_background = $('.full-page-background');

          $input = $(this);

          if ($input.is(':checked')) {
            if ($sidebar_img_container.length != 0) {
              $sidebar_img_container.fadeIn('fast');
              $sidebar.attr('data-image', '#');
            }

            if ($full_page_background.length != 0) {
              $full_page_background.fadeIn('fast');
              $full_page.attr('data-image', '#');
            }

            background_image = true;
          } else {
            if ($sidebar_img_container.length != 0) {
              $sidebar.removeAttr('data-image');
              $sidebar_img_container.fadeOut('fast');
            }

            if ($full_page_background.length != 0) {
              $full_page.removeAttr('data-image', '#');
              $full_page_background.fadeOut('fast');
            }

            background_image = false;
          }
        });

        $('.switch-sidebar-mini input').change(function() {
          $body = $('body');

          $input = $(this);

          if (md.misc.sidebar_mini_active == true) {
            $('body').removeClass('sidebar-mini');
            md.misc.sidebar_mini_active = false;

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

          } else {

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

            setTimeout(function() {
              $('body').addClass('sidebar-mini');

              md.misc.sidebar_mini_active = true;
            }, 300);
          }

          // we simulate the window Resize so the charts will get updated in realtime.
          var simulateWindowResize = setInterval(function() {
            window.dispatchEvent(new Event('resize'));
          }, 180);

          // we stop the simulation of Window Resize after the animations are completed
          setTimeout(function() {
            clearInterval(simulateWindowResize);
          }, 1000);

        });
      });
    });
  </script>

<script>
document.addEventListener('keydown', function() {
  if (event.keyCode == 123) {
    alert("This function has been disabled!");
    return false;
  } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) {
    alert("This function has been disabled!");
    return false;
  } else if (event.ctrlKey && event.keyCode == 85) {
    alert("This function has been disabled!");
    return false;
  }
}, false);

if (document.addEventListener) {
  document.addEventListener('contextmenu', function(e) {
    alert("This function has been disabled!");
    e.preventDefault();
  }, false);
} else {
  document.attachEvent('oncontextmenu', function() {
    alert("This function has been disabled!");
    window.event.returnValue = false;
  });
}
</script>
<script>

  function setCapacities(category) {
    const base_url = curl_url+VCategoryName;
    let capacities = [];
    let selector =  document.getElementById("capacity");
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        const capacities = JSON.parse(this.responseText);
        console.log(capacities);
        for (let i = 0; i < capacities.length; i++) {
          let option = document.createElement("option");
          option.value = capacities[i].capacity;
          option.innerHTML = capacities[i].capacity;
          selector.appendChild(option);
        }
      }
    };
    xhr.open("GET", base_url, true);
    xhr.send();

  }

  function getCapacity() {
    
      var category_name =  document.getElementById("category").value;
      var filteredJsonVehicles = <?php echo $filteredJsonVehicles; ?>;
      
      var newArray = filteredJsonVehicles.result.filter(function (el){
        return el.name ==category_name;
      });

      var flags = [], output = [], l = newArray.length, i;
      for( i=0; i<l; i++) {
          if( flags[newArray[i].capacity]) continue;
          flags[newArray[i].capacity] = true;
          output.push(newArray[i]);
      }

      var sel = document.querySelector('#capacity');
      $("#capacity").empty();
      output.forEach(element => {
        var capacityvalue  = element['capacity'];
        var vehicleTypeId  = element['vehicleTypeId'];
        
        $('#capacity').append('<option value="'+ vehicleTypeId +'">'+ capacityvalue +'</option>');
        document.getElementById("myInputID").value = vehicleTypeId;
      });

  
  }
</script>
</body>

</html>