<?php
require_once('../controller/bid_process.php');


$users = getAllUsers();

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta name="Referrer-Policy" value="no-referrer | same-origin"/>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    VAYA eLogistics
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="../assets/css/material-dashboard.css" rel="stylesheet" />


<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="blue" data-background-color="black" data-image="../assets/img/gomo.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo"><a href="#" class="simple-text logo-normal">
          VAYA eLogistics
        </a></div>
      <div class="sidebar-wrapper">
        <ul class="nav">
        <!-- <li class="nav-item">
            <a class="nav-link" href="./dashboard ">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li> -->
          <li class="nav-item " >
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="material-icons">local_shipping</i>
            Manage Vehicles
          </a>
          <div style=" background: linear-gradient(60deg, #1C7ACD, #1C7ACD);color: #fff;" class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
     
            <a class="dropdown-item" href="vehicle_category" referrerpolicy="no-referrer | same-origin | origin | strict-origin | no-origin-when-downgrading">Vehicle Category</a>
            <a class="dropdown-item" href="vehicle_type" referrerpolicy="no-referrer | same-origin | origin | strict-origin | no-origin-when-downgrading">Vehicle Type</a>
            <a class="dropdown-item" href="vehicle_make" referrerpolicy="no-referrer | same-origin | origin | strict-origin | no-origin-when-downgrading">Vehicle Make</a>
            <a class="dropdown-item" href="vehicle_model" referrerpolicy="no-referrer | same-origin | origin | strict-origin | no-origin-when-downgrading">Vehicle Model</a>
          </div>
        </li>
          <li class="nav-item">
            <a class="nav-link" href="./service_pillars ">
              <i class="material-icons">business_center</i>
              <p>Service Pillars</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./document" referrerpolicy="no-referrer | same-origin | origin | strict-origin | no-origin-when-downgrading">
              <i class="material-icons">content_paste</i>
              <p>Documents</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./currency" referrerpolicy="no-referrer | same-origin | origin | strict-origin | no-origin-when-downgrading">
              <i class="material-icons">local_atm</i>
              <p>Currencies</p>
            </a>
          </li>
          <li class="nav-item" >
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="material-icons">group</i>
            Manage Users
          </a>
          <div style=" background: linear-gradient(60deg, #1C7ACD, #1C7ACD);color: #fff;" class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="manage_admin ">Admins</a>
            <a class="dropdown-item" href="manage_customer ">Customers</a>
            <a class="dropdown-item" href="manage_partner ">Partners</a>
          </div>
        </li>

          <!-- <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="material-icons">content_paste</i>
              Reports
            </a>
            <div style=" background: linear-gradient(60deg, #1C7ACD, #1C7ACD);color: #fff;" class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="#">RTGS Payment Report</a>
              <a class="dropdown-item" href="#">USD Payment Report</a>
              <a class="dropdown-item" href="#">Trips Report</a>
            </div>
          </li> -->

          <li class="nav-item " >
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="material-icons">content_paste</i>
            Configurations 
          </a>
          <div style=" background: linear-gradient(60deg, #1C7ACD, #1C7ACD);color: #fff;" class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="faq"referrerpolicy="no-referrer | same-origin | origin | strict-origin | no-origin-when-downgrading">FAQ </a>
            <a class="dropdown-item" href="email"referrerpolicy="no-referrer | same-origin | origin | strict-origin | no-origin-when-downgrading">EMAILS </a>
            <a class="dropdown-item" href="exchangerate"referrerpolicy="no-referrer | same-origin | origin | strict-origin | no-origin-when-downgrading">Exchange Rate </a>
            <a class="dropdown-item" href="comission"referrerpolicy="no-referrer | same-origin | origin | strict-origin | no-origin-when-downgrading">Comission Rate </a>
            
          </div>
        </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="javascript:;">Admin Dashboard</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <ul class="navbar-nav">
        
              <li class="nav-item dropdown">
              <?php
              echo $date = date('Y-m-d H:i:s');
               ?>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <!-- <a class="dropdown-item" href="#">Profile</a> -->
                  <!-- <a class="dropdown-item" href="#">Settings</a> -->
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="logout ">Log out</a>
                </div>
              </li>
            </ul>
          <!-- <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="./dashboard ">
                  <i class="material-icons">dashboard</i>
                  <p class="d-lg-none d-md-block">
                    Stats
                  </p>
                </a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">notifications</i>
                  <span class="notification">5</span>
                  <p class="d-lg-none d-md-block">
                    Some Actions
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#">Pending trips</a>
                  <a class="dropdown-item" href="#">Pending Requests</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="#">Profile</a>
                  <a class="dropdown-item" href="#">Settings</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Log out</a>
                </div>
              </li>
            </ul>
          </div> -->
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <button style="float: right;" data-toggle="modal" data-target="#addUserModal" class="btn btn-info">Register New Admin</button>
 
                </div>
                <div class="card-body">
                  <div class="card-content table-responsive table-full-width">
                  <table id="dt-bordered" width="100%"  class="table table-sm table-bordered table-hover table-striped ">
                      <thead class="text-primary">
                        <th width ="15%">First Name</th>
                        <th width ="15%"> Last Name</th>
                        <th width ="25%"> Email</th>
                        <th width ="15%">Phone Number </th>
                        
                        <th width ="25%">Action</th>
                      </thead>
                      <tbody>

                        <?php if ($users['Action'] == 1) : ?>

                          <?php
                           $users['result'] = isset($users['result']) ?   $users['result'] : '';
                            $users['result'] = is_array($users['result']) ? $users['result'] : array();
                             foreach ($users['result'] as $user) :
                              ?>
                            <tr>
                              

                              <td><?= htmlspecialchars($user['firstname'], ENT_QUOTES, 'UTF-8');?></td>
                              <td><?= htmlspecialchars($user['lastname'], ENT_QUOTES, 'UTF-8');?></td>
                              <td><?= htmlspecialchars($user['vemail'], ENT_QUOTES, 'UTF-8');?></td>
                              <td><?= htmlspecialchars($user['phoneNumber'], ENT_QUOTES, 'UTF-8');?></td>
                              

                              <td>
                                <button type="button" rel="tooltip" title="Edit" class="btn btn-primary btn-link btn-sm">
                                  <i data-target="#update<?= $user['id'] ?>" data-toggle="modal" class="material-icons">edit</i>
                                </button>
                                <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-link btn-sm">
                                  <i data-target="#delete<?= $user['id'] ?>" data-toggle="modal" class="material-icons">close</i>
                                </button>
                              </td>
                            </tr>



                            <!--Edit User Modal -->
                            <div id="update<?= $user['id'] ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form action ="../controller/bid_process?id=<?=$user['id']?>"   method = "post" enctype="multipart/form-data">
                                    <div class="modal-body">
                                      <div class="col">
                                        <div class="form-group label-floating">
                                          <input type="text" class="form-control" placeholder="First Name"  pattern="([A-Za-z]).{1,}" title="Only letters are allowed" name ="firstname" value="<?= htmlspecialchars($user['firstname'], ENT_QUOTES, 'UTF-8'); ?>" required>
                                          
                                        </div>
                                      </div>

                                      <div class="col">
                                        <div class="form-group label-floating">
                                          <input type="text" class="form-control" placeholder="Last Name"  pattern="([A-Za-z]).{1,}" title="Only letters are allowed" name ="lastname" value="<?= htmlspecialchars($user['lastname'], ENT_QUOTES, 'UTF-8'); ?>" required>
                                          
                                        </div>
                                      </div>

                                      <div class="col">
                                        <div class="form-group label-floating">
                                          <input type="number" class="form-control" placeholder="Phone Number"  title="Only numbers are allowed" name ="phoneNumber" value="<?= htmlspecialchars($user['phoneNumber'], ENT_QUOTES, 'UTF-8'); ?>" required>
                                     
                                        </div>
                                      </div>

                                      <div class="col">
                                        <div class="form-group label-floating">
                                          <input type="email" class="form-control" placeholder="email "  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" name="VEmail" value="<?= $user['vemail'] ?>"required>
                                        </div>
                                      </div>

                                    
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      <input type="submit" name="editUser" class="btn btn-primary" value="Edit" />
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                            <!--end modal-->

                            <!--Delete  User Modal -->
                            <div id="delete<?= $user['id']?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel"> Users</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form action ="../controller/bid_process?id=<?=$user['id']?>" method = "post">
                             
                                  <div class="modal-body">
                                    <h4 class="text-primary">Are you sure you want to delete this user?</h4>
                                  </div>
                                  <!-- <input type="hidden" class="form-control"  name="id" > -->
                                
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <input type="submit" name="deleteUser" class="btn btn-primary" value="Confirm" />
                                  </div>
                                  </form>
                                </div>
                              </div>
                            </div>

                                   <!--end modal-->
                          <?php endforeach; ?>

                        <?php else : ?>

                          <?= $users['Message'] ?>

                        <?php endif; ?>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
      <!-- Add User   Modal -->
      <div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Register New User </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="../controller/bid_process"  onsubmit="return validation()"  method="post" enctype="multipart/form-data">
              <div class="modal-body">
                <div class="col">
                  <div class="form-group label-floating">
                    <input type="text" class="form-control" placeholder="First Name" id="firstname" name="firstname" required>
                    <span id="fname" class="text-danger font-weight-bold"></span>
                  </div>
                </div>

                <div class="col">
                  <div class="form-group label-floating">
                    <input type="text" class="form-control" placeholder="Last  Name " id="lastname" name="lastname" required>
                    <span id="sname" class="text-danger font-weight-bold"></span>
                  </div>
                </div>

                <div class="col">
                  <div class="form-group label-floating">
                    <input type="text" class="form-control" placeholder="email " id="VEmail" name="VEmail" required>
                    <span id="mail" class="text-danger font-weight-bold"></span>

                  </div>
                </div>

                

                <div class="col">
                  <div class="form-group label-floating">
                    <input type="text" class="form-control" placeholder="Phone Number" id="phoneNumber" name="phoneNumber" required>
                    <span id="phone" class="text-danger font-weight-bold"></span>
                  </div>
                </div>

                <div class="col">
                  <div class="form-group label-floating">
                    <input type="password" class="form-control" placeholder="Password" id="password" name="password" required>
                    <span id="passwords" class="text-danger font-weight-bold"></span>
                  </div>
                </div>

                

                

                

                
     
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <input type="submit" name="saveUser" class="btn btn-primary" value="Add" />
              </div>
            </form>
          </div>
        </div>
      </div>
  
      <footer class="footer">
        <div class="container-fluid">

          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>, designed with <i class="material-icons">favorite</i> by
            <a href="https://www.vayaafrica.com/" referrerpolicy="no-referrer | same-origin | origin | strict-origin | no-origin-when-downgrading" target="_blank">VAYA</a>
          </div>
        </div>
      </footer>
    </div>
  </div> 
  <script type="text/javascript">
            function validation() {
              var VEmail = document.getElementById('VEmail').value;
                

                var firstname = document.getElementById('firstname').value;
                if (!isNaN(firstname)) {
                    document.getElementById('fname').innerHTML = " ** Please ONLY characters are allowed";
                    return false;
                }
                
                var lastname = document.getElementById('lastname').value;
                if (!isNaN(lastname)) {
                    document.getElementById('sname').innerHTML = " ** Please ONLY characters are allowed";
                    return false;
                }

                var phoneNumber = document.getElementById('phoneNumber').value;
                if (isNaN(phoneNumber)) {
                    document.getElementById('phone').innerHTML = " ** Please enter numbers only";
                    return false;
                }
                // if (phoneNumber.length != 10) {
                //     document.getElementById('phone').innerHTML = " ** phone number must have 10 numbers only";
                //     return false;
                // }

               

                var password = document.getElementById('password').value;
                if ((password.length) <= 2 || (password.length > 20)) {
                    document.getElementById('passwords').innerHTML = " ** password must be between 3 and 10 characters";
                    return false;
                }


                if (VEmail == "") {
                    document.getElementById('mail').innerHTML = "**Please fill in your email";
                    return false;
                }

                if (VEmail.indexOf('@') <= 0) {
                    document.getElementById("mail").innerHTML = "** @ invalid position";
                    return false;

                }
                if ((VEmail.charAt(VEmail.length - 4) != '.') && (VEmail.charAt(VEmail.length - 3) != '.')) {
                    document.getElementById("mail").innerHTML = "**invalid email";
                    return false;
                }              

            }
        </script>         
  <!--   Core JS Files   -->
  <script src="../assets/js/core/jquery.min.js"></script>
  <script src="../assets/js/core/popper.min.js"></script>
  <script src="../assets/js/core/bootstrap-material-design.min.js"></script>
  <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!-- Plugin for the momentJs  -->
  <script src="../assets/js/plugins/moment.min.js"></script>
  <!--  Plugin for Sweet Alert -->
  <script src="../assets/js/plugins/sweetalert2.js"></script>
  <!-- Forms Validations Plugin -->
  <script src="../assets/js/plugins/jquery.validate.min.js"></script>
  <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
  <script src="../assets/js/plugins/jquery.bootstrap-wizard.js"></script>
  <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
  <script src="../assets/js/plugins/bootstrap-selectpicker.js"></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src="../assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
  <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
  <script src="../assets/js/plugins/jquery.dataTables.min.js"></script>
  <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
  <script src="../assets/js/plugins/bootstrap-tagsinput.js"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="../assets/js/plugins/jasny-bootstrap.min.js"></script>
  <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
  <script src="../assets/js/plugins/fullcalendar.min.js"></script>
  <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
  <script src="../assets/js/plugins/jquery-jvectormap.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="../assets/js/plugins/nouislider.min.js"></script>
  <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
  <!-- Library for adding dinamically elements -->
  <script src="../assets/js/plugins/arrive.min.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chartist JS -->
  <script src="../assets/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="../assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../assets/js/material-dashboard.js?v=2.1.2" type="text/javascript"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <script src="../assets/demo/demo.js"></script>
  <script>
$(document).ready(function () {
  $('#dt-bordered').dataTable({
    dom: 'Bfrtip',
    
    buttons: [
       'excel', 'csv'
        ],
    columnDefs: [{
      orderable: false,
      className: 'select-checkbox',
      targets: 0
    }],
    select: {
      style: 'multi',
      selector: 'td:first-child'
    }
  });
});
  </script>
  <script>
    $(document).ready(function() {
      $().ready(function() {
        $sidebar = $('.sidebar');

        $sidebar_img_container = $sidebar.find('.sidebar-background');

        $full_page = $('.full-page');

        $sidebar_responsive = $('body > .navbar-collapse');

        window_width = $(window).width();

        fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

        if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
          if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
            $('.fixed-plugin .dropdown').addClass('open');
          }

        }

        $('.fixed-plugin a').click(function(event) {
          // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
          if ($(this).hasClass('switch-trigger')) {
            if (event.stopPropagation) {
              event.stopPropagation();
            } else if (window.event) {
              window.event.cancelBubble = true;
            }
          }
        });

        $('.fixed-plugin .active-color span').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-color', new_color);
          }

          if ($full_page.length != 0) {
            $full_page.attr('filter-color', new_color);
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.attr('data-color', new_color);
          }
        });

        $('.fixed-plugin .background-color .badge').click(function() {
          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('background-color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-background-color', new_color);
          }
        });

        $('.fixed-plugin .img-holder').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).parent('li').siblings().removeClass('active');
          $(this).parent('li').addClass('active');


          var new_image = $(this).find("img").attr('src');

          if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            $sidebar_img_container.fadeOut('fast', function() {
              $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
              $sidebar_img_container.fadeIn('fast');
            });
          }

          if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $full_page_background.fadeOut('fast', function() {
              $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
              $full_page_background.fadeIn('fast');
            });
          }

          if ($('.switch-sidebar-image input:checked').length == 0) {
            var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
          }
        });

        $('.switch-sidebar-image input').change(function() {
          $full_page_background = $('.full-page-background');

          $input = $(this);

          if ($input.is(':checked')) {
            if ($sidebar_img_container.length != 0) {
              $sidebar_img_container.fadeIn('fast');
              $sidebar.attr('data-image', '#');
            }

            if ($full_page_background.length != 0) {
              $full_page_background.fadeIn('fast');
              $full_page.attr('data-image', '#');
            }

            background_image = true;
          } else {
            if ($sidebar_img_container.length != 0) {
              $sidebar.removeAttr('data-image');
              $sidebar_img_container.fadeOut('fast');
            }

            if ($full_page_background.length != 0) {
              $full_page.removeAttr('data-image', '#');
              $full_page_background.fadeOut('fast');
            }

            background_image = false;
          }
        });

        $('.switch-sidebar-mini input').change(function() {
          $body = $('body');

          $input = $(this);

          if (md.misc.sidebar_mini_active == true) {
            $('body').removeClass('sidebar-mini');
            md.misc.sidebar_mini_active = false;

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

          } else {

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

            setTimeout(function() {
              $('body').addClass('sidebar-mini');

              md.misc.sidebar_mini_active = true;
            }, 300);
          }

          // we simulate the window Resize so the charts will get updated in realtime.
          var simulateWindowResize = setInterval(function() {
            window.dispatchEvent(new Event('resize'));
          }, 180);

          // we stop the simulation of Window Resize after the animations are completed
          setTimeout(function() {
            clearInterval(simulateWindowResize);
          }, 1000);

        });
      });
    });
  </script>

<script>
document.addEventListener('keydown', function() {
  if (event.keyCode == 123) {
    alert("This function has been disabled!");
    return false;
  } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) {
    alert("This function has been disabled!");
    return false;
  } else if (event.ctrlKey && event.keyCode == 85) {
    alert("This function has been disabled!");
    return false;
  }
}, false);

if (document.addEventListener) {
  document.addEventListener('contextmenu', function(e) {
    alert("This function has been disabled!");
    e.preventDefault();
  }, false);
} else {
  document.attachEvent('oncontextmenu', function() {
    alert("This function has been disabled!");
    window.event.returnValue = false;
  });
}
</script>

</body>

</html>