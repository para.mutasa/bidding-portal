<?php
 include_once('../../utils/VayaBiddingUtility.php');

 function getPartner($partnerId)
 {
   $jsonData = array(
     'type' => "getPartner",
     'partnerId' => $partnerId
   );
   return callVayaBiddingWebApiPost($jsonData);
 }
 
 function getCustomer($iuserId)
 {
   $jsonData = array(
     'type' => "getCustomer",
     'customerId' => $iuserId
   );
   return callVayaBiddingWebApiPost($jsonData);
 }
 function getAllVehicleTypes()
{
  $jsonData = array(
    'type' => "getAllVehicleTypes"
  );
  return callVayaBiddingWebApiPost($jsonData);
}
function getAllServicePillars()
{
  $jsonData = array(
    'type' => "getAllServicePillar"
  );
  return callVayaBiddingWebApiPost($jsonData);
}

function getAllCurrencies()
{
  $jsonData = array(
    'type' => "getAllCurrencies"
  );
  return callVayaBiddingWebApiPost($jsonData);
}


function getAllBanks()
{
  $jsonData = array(
    'type' => "getAllBanks"
  );
  return callVayaBiddingWebApiPost($jsonData);
}


function getCurrencyById($currencyId)
{
  $jsonData = array(
    'type' => "getCurrencyById",
    'currencyId' => $currencyId
  );
  return callVayaBiddingWebApiPost($jsonData);
}


 function getVehicleType($vehicleTypeId)
{
  $jsonData = array(
    'type' => "getVehicleType",
    'vehicleTypeId' => $vehicleTypeId
  );
  return callVayaBiddingWebApiPost($jsonData);
}

function getServicePillar($servicePillarId)
{
  $jsonData = array(
    'type' => "getServicePillar",
    'servicePillarId' => $servicePillarId
  );
  return callVayaBiddingWebApiPost($jsonData);
}


function listAllPendingJobsByIUserId($iuserId)
{
  $jsonData = array(
    'type' => "getAllJobsByIUserIdAndJobStatus",
    'iUserId' => $iuserId,
    'page' => 0,
    'size' => 50,
    "jobStatus" => "Pending"
  );
  return callVayaBiddingWebApiPost($jsonData);
}

function listAllAssignedJobsByIUserId($iuserId)
{
  $jsonData = array(
    'type' => "getAllJobsByIUserIdAndJobStatus",
    'iUserId' => $iuserId,
    'page' => 0,
    'size' => 2000,
    "jobStatus" => "Assigned"
  );
  return callVayaBiddingWebApiPost($jsonData);
}

function listAllDeliveredJobsByIUserId($iuserId)
{
  $jsonData = array(
    'type' => "getAllJobsByIUserIdAndJobStatus",
    'iUserId' => $iuserId,
    'page' => 0,
    'size' => 2000,
    "jobStatus" => "Delivered"
  );
  return callVayaBiddingWebApiPost($jsonData);
}

function listAllCancelledJobsByIUserId($iuserId)
{
  $jsonData = array(
    'type' => "getAllJobsByIUserIdAndJobStatus",
    'iUserId' => $iuserId,
    'page' => 0,
    'size' => 2000,
    "jobStatus" => "Cancelled"
  );
  return callVayaBiddingWebApiPost($jsonData);
}

function listAllCollectedJobsByIUserId($iuserId)
{
  $jsonData = array(
    'type' => "getAllJobsByIUserIdAndJobStatus",
    'iUserId' => $iuserId,
    'page' => 0,
    'size' => 2000,
    "jobStatus" => "Collected"
  );
  return callVayaBiddingWebApiPost($jsonData);
}

function listAllInTransitJobsByIUserId($iuserId)
{
  $jsonData = array(
    'type' => "getAllJobsByIUserIdAndJobStatus",
    'iUserId' => $iuserId,
    'page' => 0,
    'size' => 2000,
    "jobStatus" => "InTransit"
  );
  return callVayaBiddingWebApiPost($jsonData);
}


function customerGetAllBidsByJobId($jobId)
{
  $jsonData = array(
    'type' => "customerGetAllBidsByJobId",
    'jobId' => $jobId
  );
  return callVayaBiddingWebApiPost($jsonData);
}


// createCargoDetails

if (isset($_POST['jobpost'])) {

    // receive all input values from the form
    
    $description   = $_POST['description']; 
    $weight  = $_POST['weight'] .''. $_POST['weight_unit'];
    $typeOfGoods  = $_POST['typeOfGoods'];
    $goodsValue  = $_POST['goodsValue'];
    $iUserId  = $_POST['iUserId']; 
    $dimensions  = $_POST['length'] .'-'. $_POST['width'] .'-'. $_POST['height'] .''. $_POST['dimension_unit'];
    $type = 'createCargoDetails';
    //manifestUpload
    $manifestUpload_name = $_FILES['manifestUpload']['name'];
    // $manifestUpload__size = $_FILES['manifestUpload']['size'];
    // $manifestUpload__tmp = $_FILES['manifestUpload']['tmp_name'];
    // $manifestUpload__type = $_FILES['manifestUpload']['type'];
    // $manifestUpload_exploded = explode('.', $_FILES['manifestUpload']['name']);
    // $manifestUpload_ext = strtolower(end($manifestUpload_exploded));

   
  
  // wayBills
  
  $wayBills_name = $_FILES['wayBills']['name'];
  // $wayBills_size = $_FILES['wayBills']['size'];
  // $wayBills_tmp = $_FILES['wayBills']['tmp_name'];
  // $wayBills_type = $_FILES['wayBills']['type'];
  // $wayBills_exploded = explode('.', $_FILES['wayBills']['name']);
  // $wayBills_ext = strtolower(end($wayBills_exploded));

  // var_dump($description);
  // var_dump($weight);
  // var_dump($typeOfGoods);
  // var_dump($goodsValue);
  // var_dump($manifestUpload_name);
  // var_dump($wayBills_name);
  // var_dump($dimensions);
  // exit;

      $jsonData = array(
        'type'  => $type,
        'description'  => $description,
        'weight'  => $weight,
        'typeOfGoods'  => $typeOfGoods,
        'goodsValue'  => $goodsValue,
        'manifestUpload'  => $manifestUpload_name,
        'wayBills'  => $wayBills_name,
        'dimensions'  => $dimensions,
        'iUserId'  => $iUserId
      );
  
      $cargo_result = callVayaBiddingWebApiPost($jsonData);
  
      // var_dump($cargo_result);
      // exit;
  

        // move_uploaded_file($manifestUpload__tmp,"../../uploads/".$manifestUpload_name);
        // move_uploaded_file($wayBills_tmp,"../../uploads/".$wayBills_name);
  
    $cargoId_c = $cargo_result['result']['cargoId'];
      
    $collectionTime   = $_POST['collectionTime']; 
    $collection_time  = date('Y-m-d\Th:i', strtotime($collectionTime));
    $deliveryTime  = $_POST['deliveryTime'];
    $delivery_time  = date('Y-m-d\Th:i', strtotime($deliveryTime));
    $currencyId  = $_POST['currencyld'];
  
    $loadingRequirements   = $_POST['loadingRequirements']; 
    $offloadingRequirements   = $_POST['offloadingRequirements']; 
    $bidStartTime  = $_POST['bidStartTime'];

    $bidstart  = date('Y-m-d\Th:i', strtotime($bidStartTime));
    $bidEndTime  = $_POST['bidEndTime'];
    $bidend  = date('Y-m-d\Th:i', strtotime($bidEndTime));
 
    $amountOffer   = $_POST['amountOffer']; 
    $customerType  = "Individual";
    $customerName  = $_POST['customerName'];
  
    $pickupAddress   = $_POST['pickupAddress']; 
    $pickupLatitude  = $_POST['pickupLatitude'];
    $pickupLongitude   = $_POST['pickupLongitude']; 
  
    $dropOffAddress  = $_POST['dropOffAddress'];
    $dropOffLatitude   = $_POST['dropOffLatitude']; 
    $dropOffLongitude  = $_POST['dropOffLongitude'];
  
    $vehicleTypeId  = $_POST['vehicleTypeId'];
    $servicePillarId   = $_POST['servicePillarId']; 
    $paymentMode   = $_POST['paymentMode']; 
    $goodsInTransit   = $_POST['goodsInTransit']; 
  
   
    $job_type = 'createJob';
   
  // var_dump($cargoId_c);
  // var_dump($collection_time);
  // var_dump($delivery_time);
  
  // var_dump($loadingRequirements);
  // var_dump($offloadingRequirements);
  // var_dump($bidstart);
  // var_dump($bidend);
  // var_dump($amountOffer);
  // var_dump($customerType);
  // var_dump($customerName);
  
  
  
  // var_dump($pickupAddress);
  // var_dump($pickupLatitude);
  // var_dump($pickupLongitude);
  
  // var_dump($dropOffAddress);
  // var_dump($dropOffLatitude);
  // var_dump($dropOffLongitude);
  
  
  // var_dump($vehicleTypeId);
  // var_dump($servicePillarId);
  // var_dump($currencyId);
  //  var_dump($goodsInTransit);
  // var_dump($paymentMode);
  
  
  // exit;
  
  
  
      $jsonData = array(
       'type' => $job_type,
        'iUserId' => $iUserId,
        'collectionTime' => $collection_time,
        'deliveryTime' => $delivery_time,
        'cargoId' => $cargoId_c,
        'loadingRequirements' => $loadingRequirements,
        'offloadingRequirements' => $offloadingRequirements,
        'bidStartTime' => $bidstart,
        'bidEndTime' => $bidend,
        'servicePillarId' => $servicePillarId,
        'amountOffer' => $amountOffer,
        'customerType' => $customerType,
        'customerName' => $customerName,
        'dropOffAddress' => $dropOffAddress,
        'pickupAddress' => $pickupAddress,
        'dropOffLatitude' => $dropOffLatitude,
        'pickupLatitude' => $pickupLatitude,
        'dropOffLongitude' => $dropOffLongitude,
        'pickupLongitude' => $pickupLongitude,
        'vehicleTypeId' => $vehicleTypeId,
        'currencyId' => $currencyId,
        'goodsInTransit' => $goodsInTransit,
        'paymentMode' => $paymentMode
      );
  
      // var_dump($jsonData);
      // exit;
  
      $job_result = callVayaBiddingWebApiPost($jsonData);
  
      // var_dump($job_result);
      // exit;

      if ($job_result['Action'] == 1){
        echo "<script>";
          echo "alert('Job posted sucessfully');";
          echo "window.location='../view/jobs';";
        echo "</script>";
      } else {
        echo "<script>";
          echo "alert('".$result['Message']."');";
          // echo "alert('Please try again later');";
          echo "window.location='../view/jobs';";
        echo "</script>";
      }


}

    
  
// Assign Partner

if (isset($_POST['assign_partner'])) {

  // receive all input values from the form
  $bidId =  filter_var($_POST['bidId'], FILTER_SANITIZE_NUMBER_INT);
  $partnerId =  filter_var($_POST['partnerId'], FILTER_SANITIZE_NUMBER_INT);
  $type = 'assignDriver';



    $jsonData = array(
      'type'  => $type,
      'bidId'  => $bidId,
      'iDriverId'  => $partnerId
    );

    $assign_partner_result = callVayaBiddingWebApiPost($jsonData);

    echo json_encode($assign_partner_result);
    exit;



  }



// Query

if (isset($_POST['query'])) {

  // receive all input values from the form

  $name = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
  $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
  $message = filter_var($_POST['message'], FILTER_SANITIZE_STRING);

  $type = 'createQuery';

  
    $jsonData = array(
      'type'  => $type,
      'name'  => $name,
      'email'  => $email,
      'message'  => $message,
    );

    $result = callVayaBiddingWebApiPost($jsonData);


    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('Message has been sent sucessfully');";
        echo "window.location='../../vaya-bidding/contact';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        // echo "alert('Please try again later');";
        echo "window.location='../../vaya-bidding/contact';";
      echo "</script>";
    }
  }

//createPayment  (bids)

if (isset($_POST['createPayment'])) {

  // receive all input values from the form

  $amount =  filter_var($_POST['amount'], FILTER_SANITIZE_NUMBER_INT);
  $msisdn =  filter_var($_POST['msisdn'], FILTER_SANITIZE_NUMBER_INT);
  $jobId =  filter_var($_POST['jobId'], FILTER_SANITIZE_NUMBER_INT);
  $payType  = "Cash";
  $type = 'createPayment';

  
    $jsonData = array(
      'type'  => $type,
      'amount'  => $amount,
      'msisdn'  => $msisdn,
      'jobId'  => $jobId,
      'payType'  => $payType
    );

    $create_payment_result = callVayaBiddingWebApiPost($jsonData);

    echo json_encode($create_payment_result);
    exit;


  }


//createPayment  (collected & Assigned)

if (isset($_POST['cashPayment'])) {

  // receive all input values from the form

  $amount =  filter_var($_POST['amount'], FILTER_SANITIZE_NUMBER_INT);
  $msisdn =  filter_var($_POST['msisdn'], FILTER_SANITIZE_NUMBER_INT);
  $jobId =  filter_var($_POST['jobId'], FILTER_SANITIZE_NUMBER_INT);
  $payType  = "Cash";
  $type = 'createPayment';

  
    $jsonData = array(
      'type'  => $type,
      'amount'  => $amount,
      'msisdn'  => $msisdn,
      'jobId'  => $jobId,
      'payType'  => $payType
    );

    $cash_result = callVayaBiddingWebApiPost($jsonData);

    echo json_encode($cash_result);
    exit;

  }


//makePayment (bids)

if (isset($_POST['makePayment'])) {

  // receive all input values from the form
  $amount =  filter_var($_POST['amount'], FILTER_SANITIZE_NUMBER_INT);
  $msisdn =  filter_var($_POST['msisdn'], FILTER_SANITIZE_NUMBER_INT);
  $jobId =  filter_var($_POST['jobId'], FILTER_SANITIZE_NUMBER_INT);
  $payType  = "Ecocash";
  $type = 'makePayment';


    $jsonData = array(
      'type'  => $type,
      'amount'  => $amount,
      'msisdn'  => $msisdn,
      'jobId'  => $jobId,
      'payType'  => $payType
    );

    $make_payment_result = callVayaBiddingWebApiPost($jsonData);

    echo json_encode($make_payment_result);
    exit;

  }

//makePayment Ecocash (assigned & collected)

if (isset($_POST['ecoPayment'])) {
  
  ob_clean();# discard any previous buffer data
  // receive all input values from the form
  $amount =  filter_var($_POST['amount'], FILTER_SANITIZE_NUMBER_INT);
  $msisdn =  filter_var($_POST['msisdn'], FILTER_SANITIZE_NUMBER_INT);
  $jobId =  filter_var($_POST['jobId'], FILTER_SANITIZE_NUMBER_INT);
  $payType  = "Ecocash";
  $type = 'makePayment';


    $jsonData = array(
      'type'  => $type,
      'amount'  => $amount,
      'msisdn'  => $msisdn,
      'jobId'  => $jobId,
      'payType'  => $payType
    );

     $ecocash_result = callVayaBiddingWebApiPost($jsonData);

     echo json_encode($ecocash_result);
     exit;

  }

  //makePayment RTGS Transfer (assigned & collected)


if (isset($_POST['rtPayment'])) {
  
  // ob_clean();# discard any previous buffer data
  // receive all input values from the form
  $bankCode =  filter_var($_POST['bankCode'], FILTER_SANITIZE_STRING);
  $customerAccount =  filter_var($_POST['customerAccount'], FILTER_SANITIZE_NUMBER_INT);
  $customerName =  filter_var($_POST['customerName'], FILTER_SANITIZE_STRING);
  $amount =  filter_var($_POST['amount'], FILTER_SANITIZE_NUMBER_INT);
  $currency =  filter_var($_POST['currency'], FILTER_SANITIZE_STRING);
  $reason =  filter_var($_POST['reason'], FILTER_SANITIZE_STRING);
  $jobId =  filter_var($_POST['jobId'], FILTER_SANITIZE_NUMBER_INT);
  $transactionType = 'RTGS';

    $jsonData = array(
      'bankCode'  => $bankCode,
      'transactionType'  => $transactionType,
      'jobId'  => $jobId,
      'customerAccount'  => $customerAccount,
      'customerName'  => $customerName,
      'amount'  => $amount,
      'currency'  => $currency,
      'reason'  => $reason
    );

     $rtgs_result = callVayaBiddingBankPaymentsWebApiPost($jsonData);


     echo json_encode($rtgs_result);
     exit;

  }


  //makePayment Internal Transfer (assigned & collected)


if (isset($_POST['inPayment'])) {
  
  // ob_clean();# discard any previous buffer data
  // receive all input values from the form
  $customerAccount =  filter_var($_POST['customerAccount'], FILTER_SANITIZE_NUMBER_INT);
  $customerName =  filter_var($_POST['customerName'], FILTER_SANITIZE_STRING);
  $amount =  filter_var($_POST['amount'], FILTER_SANITIZE_NUMBER_INT);
  $currency =  filter_var($_POST['currency'], FILTER_SANITIZE_STRING);
  $reason =  filter_var($_POST['reason'], FILTER_SANITIZE_STRING);
  $jobId =  filter_var($_POST['jobId'], FILTER_SANITIZE_NUMBER_INT);
  $transactionType = 'INTERNAL_FUNDS_TRANSFER';

    $jsonData = array(
      'transactionType'  => $transactionType,
      'jobId'  => $jobId,
      'customerAccount'  => $customerAccount,
      'customerName'  => $customerName,
      'amount'  => $amount,
      'currency'  => $currency,
      'reason'  => $reason
    );

     $internal_funds_result = callVayaBiddingBankPaymentsWebApiPost($jsonData);


     echo json_encode($internal_funds_result);
     exit;

  }


//updatePaymentStatus

if (isset($_POST['updatePaymentStatus'])) {

  // receive all input values from the form
  $jobId =  filter_var($_POST['jobId'], FILTER_SANITIZE_NUMBER_INT);
  $paymentStatus  = "Paid";
  $type = 'updatePaymentStatus';


    $jsonData = array(
      'type'  => $type,
      'jobId'  => $jobId,
      'paymentStatus'  => $paymentStatus
    );

    $result = callVayaBiddingWebApiPost($jsonData);


    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('Cash payment made sucessfully');";
        echo "window.location='../view/jobs ';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        // echo "alert('Please try again later');";
        echo "window.location='../view/jobs ';";
      echo "</script>";
    }
  }


// cancelAnOffer 

if (isset($_POST['cancelAnOffer'])) {

  // receive all input values from the form
  $bidId =  filter_var($_POST['bidId'], FILTER_SANITIZE_NUMBER_INT);
  $jobId =  filter_var($_POST['jobId'], FILTER_SANITIZE_NUMBER_INT);
  $rejectReason  = $_POST['rejectReason'];
  $type = 'cancelAnOffer';

    $jsonData = array(
      'type'  => $type,
      'bidId'  => $bidId,
      'rejectReason'  => $rejectReason 
    );

    $result = callVayaBiddingWebApiPost($jsonData);

    // var_dump($result);
    // exit;


    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('Counter offer Successfully Cancelled');";
        echo "window.location='../view/bids ?jobId=$jobId';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        // echo "alert('Please try again later');";
        echo "window.location='../view/bids ?jobId=$jobId';";
      echo "</script>";
    }
  }

// cancel Job

if (isset($_POST['cancelJob'])) {

  // receive all input values from the form
  $jobId =  filter_var($_POST['jobId'], FILTER_SANITIZE_NUMBER_INT);
  $customerId =  filter_var($_POST['customerId'], FILTER_SANITIZE_NUMBER_INT);
  $type = 'cancelJob';

    $jsonData = array(
      'type'  => $type,
      'jobId'  => $jobId,
      'customerId'  => $customerId 
    );

    $result = callVayaBiddingWebApiPost($jsonData);

    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('Job Successfully Cancelled');";
        echo "window.location='../view/jobs ';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        // echo "alert('Please try again later');";
        echo "window.location='../view/jobs ';";
      echo "</script>";
    }
  }



// ratingDriver

if (isset($_POST['createRating'])) {

  // receive all input values from the form
  $userId =  filter_var($_POST['idriverId'], FILTER_SANITIZE_NUMBER_INT);
  $comment = filter_var($_POST['comment'], FILTER_SANITIZE_STRING);
  $scale =  filter_var($_POST['scale'], FILTER_SANITIZE_NUMBER_INT);
  $tripId =  filter_var($_POST['tripId'], FILTER_SANITIZE_NUMBER_INT);
  $userType  = "Customer";
  $type = 'createRating';


    $jsonData = array(
      'type'  => $type,
      'userId'  => $userId,
      'comment'  => $comment,
      'scale'  => $scale,
      'tripId'  => $tripId,
      'userType'  => $userType
    );

    $rating_driver_result = callVayaBiddingWebApiPost($jsonData);

    echo json_encode($rating_driver_result);
    exit;

  }



// customerRegistration
if (isset($_POST['customerRegistration'])) {

  // receive all input values from the form

  $vEmail =  filter_var($_POST['vEmail'], FILTER_SANITIZE_EMAIL);
  $vFirstName = filter_var($_POST['vFirstName'], FILTER_SANITIZE_STRING);
  $vPhone =  filter_var($_POST['vPhone'], FILTER_SANITIZE_NUMBER_INT);
  $vLastName = filter_var($_POST['vLastName'], FILTER_SANITIZE_STRING);
  $vPassword = filter_var($_POST['vPassword'], FILTER_SANITIZE_STRING);
  $vInviteCode = filter_var($_POST['vInviteCode'], FILTER_SANITIZE_STRING);

  $userType  = "Customer";
  $type = 'customerRegistration';


    $jsonData = array(
      'type'  => $type,
      'vFirstName'  => $vFirstName,
      'vLastName'  => $vLastName,
      'vEmail'  => $vEmail,
      'vPhone'  => $vPhone,
      'vPassword'  => $vPassword,
      'userType'  => $userType,
      'vInviteCode'  => $vInviteCode
    );

    $result = callVayaBiddingWebApiPost($jsonData);


    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('Customer registration successful.');";
        echo "window.location='../../vaya-bidding/customer_login ';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        // echo "alert('Please try again later');";
        echo "window.location='../../customer ';";
      echo "</script>";
    }
  }


  // customerRegistration

  if (isset($_POST['companyRegistration'])) {

    // receive all input values from the form

  $vEmail =  filter_var($_POST['vEmail'], FILTER_SANITIZE_EMAIL);
  $name = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
  $vPhone =  filter_var($_POST['vPhone'], FILTER_SANITIZE_NUMBER_INT);
  $vCaddress = filter_var($_POST['vCaddress'], FILTER_SANITIZE_STRING);
  $vPassword = filter_var($_POST['vPassword'], FILTER_SANITIZE_STRING);
  $zimraBPNumber = filter_var($_POST['zimraBPNumber'], FILTER_SANITIZE_STRING);
  $countryCode = filter_var($_POST['countryCode'], FILTER_SANITIZE_STRING);
  $phoneCode = filter_var($_POST['phoneCode'], FILTER_SANITIZE_STRING);
    $type = 'companyRegistration';
    $userType  = "Customer";

    // var_dump($jobId);
    // var_dump($customerId);
    // exit;

    $jsonData = array(
      'type'  => $type,
      'name'  => $name,
      'vCaddress'  => $vCaddress,
      'vEmail'  => $vEmail,
      'vPhone'  => $vPhone,
      'vPassword'  => $vPassword,
      'phoneCode'  => $phoneCode,
      'countryCode'  => $countryCode,
      'zimraBPNumber'  => $zimraBPNumber,
      'userType'  => $userType
    );

    $result = callVayaBiddingWebApiPost($jsonData);


    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('Customer SME registration successful. An OTP has been sent to your registered mobile number, please use it to login');";
        echo "window.location='../../vaya-bidding/customer_login ';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        // echo "alert('Please try again later');";
        echo "window.location='../../customersms ';";
      echo "</script>";
    }
  }


// CustomerForgotPassword

if (isset($_POST['CustomerForgotPassword'])) {

  // receive all input values from the form
  $vEmail  = $_POST['vEmail'];
  $userType  = "Customer";
  $type = 'forgotPassword';


    $jsonData = array(
      'type'  => $type,
      'vEmail'  => $vEmail,
      'userType'  => $userType
    );

    $result = callVayaBiddingWebApiPost($jsonData);


    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('Token sent successfully to your registered email');";
        echo "window.location='../../customer_reset_password ';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        // echo "alert('Please try again later');";
        echo "window.location='../../customer_forgot_password ';";
      echo "</script>";
    }
  }



// resetPassword

if (isset($_POST['CustomerResetPassword'])) {

  // receive all input values from the form

  $token = filter_var($_POST['token'], FILTER_SANITIZE_STRING);
  $newPassword = filter_var($_POST['newPassword'], FILTER_SANITIZE_STRING);
  $userType  = "Customer";
  $type = 'resetPassword';

    $jsonData = array(
      'type'  => $type,
      'token'  => $token,
      'newPassword'  => $newPassword,
      'userType'  => $userType
    );

    $result = callVayaBiddingWebApiPost($jsonData);



    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('Password changed successfully');";
        echo "window.location='../../customer_login ';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        // echo "alert('Please try again later');";
        echo "window.location='../../customer_login ';";
      echo "</script>";
    }
  }
?>