<?php
session_start();

if (!isset($_SESSION['customer_id'])) {
  header("Location: ../../vaya-bidding/customer_login ");
  exit();
}


// session_start();
// if(!isset($_SESSION['userData']['id'], $_SESSION['userData']['username'], $_SESSION['userData']['userType'], $_SESSION["sess_Token"]))
// {
// 	echo "<script>";
//     echo "window.location.href='../login ?lmsg=true';";
//   echo "</script>";
// 	exit;
// }
$jobId =  $_GET['jobId'];
// $bidId =  $_GET['bidId'];

// var_dump($jobId);
// exit;

require_once('../controller/bid_process.php');

$responses = customerGetAllBidsByJobId($jobId);

// $assign_bids = customerGetAllBidsByJobId($bidId);
// $jobs = listAllJobsByIUserId();

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    VAYA eLogistics
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="../assets/css/material-dashboard.css" rel="stylesheet" />

</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="blue" data-background-color="black" data-image="../assets/img/gomo.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo"><a href="#" class="simple-text logo-normal">
          VAYA eLogistics
        </a></div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <!-- <li class="nav-item ">
            <a class="nav-link" href="./dashboard ">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./trip_management ">
              <i class="material-icons">content_paste</i>
              <p>Trip Management</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./fleet_management ">
              <i class="material-icons">content_paste</i>
              <p>Fleet Management</p>
            </a>
          </li> -->

          <li class="nav-item active">
            <a class="nav-link" href="./jobs" referrerpolicy="no-referrer | same-origin | origin | strict-origin | no-origin-when-downgrading">
              <i class="material-icons">local_shipping</i>
              <p>My Jobs</p>
            </a>
          </li>
          <!-- <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="material-icons">content_paste</i>
              Reports
            </a>
            <div style=" background: linear-gradient(60deg, #1C7ACD, #1C7ACD);color: #fff;" class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="#">RTGS Payment Report</a>
              <a class="dropdown-item" href="#">USD Payment Report</a>
              <a class="dropdown-item" href="#">Trips Report</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./configurations ">
              <i class="material-icons">content_paste</i>
              <p>Configurations</p>
            </a>
          </li> -->
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="javascript:;">Responses</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <!-- <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div> -->
            </form>
            <ul class="navbar-nav">
            
            <li class="nav-item dropdown">
              <?php
              echo $date = date('Y-m-d H:i:s');
               ?>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <!-- <a class="dropdown-item" href="#">Profile</a> -->
                  <!-- <a class="dropdown-item" href="#">Settings</a> -->
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="logout ">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-tabs card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <ul class="nav nav-tabs" data-tabs="tabs">
                        <li class="nav-item">
                          <a class="nav-link active" href="#response" data-toggle="tab">
                            <i class="material-icons">lens</i> RESPONSES
                            <div class="ripple-container"></div>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" onClick="location.href='jobs '" data-toggle="tab">
                            <i class="material-icons">lens</i>JOBS
                            <div class="ripple-container"></div>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="card-body">
                  <div class="tab-content">
                    <div class="tab-pan active card-content table-responsive table-full-width" id="response">
                      <table id="dt-response" width="150%" cellspacing="0" class="table table-sm table-bordered table-hover table-striped ">
                        <thead class=" text-primary">
                          <th width="10%">Bid Number</th>
                          <th width="15%">Job Number</th>
                          <th width="10%">Payment Method</th>
                          <th width="10%">Payment Status</th>
                          <th width="15%">Pick-up Location</th>
                          <th width="15%">Drop-off Location</th>
                          <th width="10%">Bid Status</th>
                          <th width="10%">Offer Amount</th>
                          <th width="10%">Bid Amount</th>
                          <th width="13%">Reject Reason</th>
                          <th width="10%">Confirmed</th>
                          <th width="10%">Action</th>
                        

                        </thead>
                        <tbody>

                          <?php if ($responses['Action'] == 1) : ?>

                            <?php
                            $responses['result'] = isset($responses['result']) ?   $responses['result'] : '';
                            $responses['result'] = is_array($responses['result']) ? $responses['result'] : array();
                            foreach ($responses['result'] as $response) :

                            ?>
                                <tr>
                                <td><?= $response['bidId'] ?></td>
                                <td><?= $response['jobId'] ?></td>
                                <td><?= $response['paymentMode'] ?></td>
                                <td><?= $response['paymentStatus'] ?></td>
                                <td><?= $response['pickupAddress'] ?></td>
                                <td><?= $response['dropOffAddress'] ?></td>
                                <td><?= $response['bidStatus'] ?></td>
                                <td><?=getCurrencyById($response['currencyId'])['result']['name'] . getCurrencyById($response['currencyId'])['result']['symbol'] . ' '. $response['offerAmount'] ?></td>
                                <td><?=getCurrencyById($response['currencyId'])['result']['name'] . getCurrencyById($response['currencyId'])['result']['symbol'] . ' '. $response['bidAmount'] ?></td>
                                <td><?= $response['rejectReason'] ?></td>
                                <td><?= $response['confirmed'] ? 'true' : 'false' ?></td>
                                <td>
                                <button type="button" rel="tooltip" title="View" class="btn btn-primary btn-link btn-sm">
                                  <i  data-target="#bid<?= $response['bidId'] ?>" data-toggle="modal" class="material-icons">lens</i>
                                </button>
    
                              </td>
                           


                              <!-- Bid Details Modal  -->
                              <div class="modal fade" id="bid<?= $response['bidId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Bid Details</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?bidId=<?= $response['bidId'] ?>" method="post" enctype="multipart/form-data">
                                      <div class="modal-body">
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid No:</label>
                                            <input type="text" class="form-control" placeholder="Bid No" value="<?= $response['bidId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Status:</label>
                                            <input type="text" class="form-control" value="<?= $response['bidStatus'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Payment Method:</label>
                                            <input type="text" class="form-control"  value="<?= $response['paymentMode'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control" placeholder="Job No" value="<?= $response['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Partner Name:</label>
                                            <input type="text" class="form-control" placeholder="Partner Name" value="<?= $response['partnerName'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Partner Phone:</label>
                                            <input type="text" class="form-control" placeholder="partner Phone" value="<?= $response['partnerPhone'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Offer Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($response['currencyId'])['result']['name'] . getCurrencyById($response['currencyId'])['result']['symbol'] . ' '. $response['offerAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Amount:</label>
                                            <input type="text" class="form-control" placeholder="Bid Amount" value="<?= getCurrencyById($response['currencyId'])['result']['name'] . getCurrencyById($response['currencyId'])['result']['symbol'] . ' '. $response['bidAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                        <input type="hidden" class="form-control" name="jobId" value="<?= $response['jobId'] ?>" readonly>

                                      </div>
                                      <div class="modal-footer">

                                
                                      <?php

if ($response['bidStatus']  == 'Accepted') {

  echo "<button type='button' class='btn btn-secondary'  data-whatever='@getbootstrap' data-toggle='modal' data-target='#reject" . $response['bidId'] . "' >REJECT PARTNER</button>"; 
                              
  echo "<button type='button' class='btn btn-primary'  data-whatever='@getbootstrap' data-toggle='modal' data-target='#assign" . $response['bidId'] . "' >ASSIGN PARTNER</button>"; 
                              
} elseif ($response['bidStatus']  == 'Rejected') {

  echo "<h4 class='text-center text-primary'>Please Call or Email The Partner For Negotiation.</h4>"; 

} elseif ($response['bidStatus']  == 'Assigned' && $response['paymentMode'] == 'Ecocash') {

  echo "<button type='button' class='btn btn-primary'  data-whatever='@getbootstrap' data-toggle='modal' data-target='#ecocash" . $response['bidId'] . "' >PAY</button>"; 
                              
}
elseif ($response['bidStatus']  == 'Assigned' && $response['paymentMode'] == 'Cash') {

  echo "<button type='button' class='btn btn-primary'  data-whatever='@getbootstrap' data-toggle='modal' data-target='#cash" . $response['bidId'] . "' >PAY</button>"; 
                              
}
elseif ($response['bidStatus']  == 'Counter') {

  echo "<button type='button' class='btn btn-secondary'  data-whatever='@getbootstrap' data-toggle='modal' data-target='#reject" . $response['bidId'] . "' >REJECT PARTNER</button>"; 
                              
  echo "<button type='button' class='btn btn-primary'  data-whatever='@getbootstrap' data-toggle='modal' data-target='#assign" . $response['bidId'] . "' >ASSIGN PARTNER</button>"; 
                              

}
elseif ($response['bidStatus']  == 'Cancelled') {

  echo "<h4 class='text-center text-primary'>Please Call or Email The Partner For Negotiation.</h4>"; 

}
elseif ($response['bidStatus']  == 'Assigned' && $response['paymentMode'] == 'null') {

  echo "<h4 class='text-center text-primary'>Please Choose  Payment Method.</h4>"; 

}


else {

  echo "<button type='button' class='btn btn-secondary'  data-whatever='@getbootstrap' data-toggle='modal' data-target='#bid" . $response['bidId'] . "' >CaANCEL</button>"; 
                              
}

?>

                                        </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->

                              <!-- Assign Bid Modal  -->
                              <div class="modal fade" id="assign<?= $response['bidId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Assign Partner</h4>
                                   
                                    </div>
                                    <form id ="assignpartner" >
                                      <div class="modal-body">

                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid No:</label>
                                            <input type="text" class="form-control" placeholder="Bid No" value="<?= $response['bidId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Status:</label>
                                            <input type="text" class="form-control" value="<?= $response['bidStatus'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Payment Method:</label>
                                            <input type="text" class="form-control"  value="<?= $response['paymentMode'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control" placeholder="Job No" value="<?= $response['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Partner Name:</label>
                                            <input type="text" class="form-control" placeholder="Partner Name" value="<?= $response['partnerName'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Partner Phone:</label>
                                            <input type="text" class="form-control" placeholder="partner Phone" value="<?= $response['partnerPhone'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Offer Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($response['currencyId'])['result']['name'] . getCurrencyById($response['currencyId'])['result']['symbol'] . ' '. $response['offerAmount'] ?>"  readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Amount:</label>
                                            <input type="text" class="form-control" placeholder="Bid  Amount" value="<?= getCurrencyById($response['currencyId'])['result']['name'] . getCurrencyById($response['currencyId'])['result']['symbol'] . ' '. $response['bidAmount'] ?>"  readonly>
                                          </div>
                                        </div>
                                       
                                        <input type="hidden" class="form-control" name="partnerId" value="<?= $response['partnerId'] ?>">
                                        <input type="hidden" class="form-control" name="bidId" value="<?= $response['bidId'] ?>">

                                      </div>
                                      <div  style ="color: #1C7ACD; text-align: center;" class='assignPartner'></div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    
                                        <input type="hidden" class="form-control" name="assign_partner" value="true">
                                        <button type="button" name ="assign_partner" onClick="assignPartner()"  class="btn btn-primary"> Confirm </button>
                                        
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->

                              <!-- Accept Bid Modal  -->
                              <div class="modal fade" id="accept<?= $response['bidId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Accept Bid</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?bidId=<?= $response['bidId'] ?>" method="post" enctype="multipart/form-data">
                                      <div class="modal-body">

                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid No:</label>
                                            <input type="text" class="form-control" placeholder="Bid No" value="<?= $response['bidId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Status:</label>
                                            <input type="text" class="form-control" value="<?= $response['bidStatus'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Payment Method:</label>
                                            <input type="text" class="form-control"  value="<?= $response['paymentMode'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control" placeholder="Job No" value="<?= $response['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Partner Name:</label>
                                            <input type="text" class="form-control" placeholder="Partner Name" value="<?= $response['partnerName'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Partner Phone:</label>
                                            <input type="text" class="form-control" placeholder="partner Phone" value="<?= $response['partnerPhone'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Offer Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($response['currencyId'])['result']['name'] . getCurrencyById($response['currencyId'])['result']['symbol'] . ' '. $response['offerAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Amount:</label>
                                            <input type="text" class="form-control" placeholder="Bid Amount" value="<?= getCurrencyById($response['currencyId'])['result']['name'] . getCurrencyById($response['currencyId'])['result']['symbol'] . ' '. $response['bidAmount'] ?>"  readonly>
                                          </div>
                                        </div>
                                        <input type="hidden" class="form-control" name="jobId" value="<?= $response['jobId'] ?>" readonly>

                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <input type="submit" name="accept_bid" class="btn btn-primary" value="Confirm" />
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->

                              <!-- cancelAnOffer Modal  -->
                              <div class="modal fade" id="reject<?= $response['bidId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Cancel An Offer</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?bidId=<?= $response['bidId'] ?>" method="post" enctype="multipart/form-data">
                                      <div class="modal-body">

                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid No:</label>
                                            <input type="text" class="form-control" placeholder="Bid No" value="<?= $response['bidId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Status:</label>
                                            <input type="text" class="form-control" value="<?= $response['bidStatus'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Payment Method:</label>
                                            <input type="text" class="form-control"  value="<?= $response['paymentMode'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control" placeholder="Job No" value="<?= $response['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Partner Name:</label>
                                            <input type="text" class="form-control" placeholder="Partner Name" value="<?= $response['partnerName'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Partner Phone:</label>
                                            <input type="text" class="form-control" placeholder="partner Phone" value="<?= $response['partnerPhone'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Offer Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($response['currencyId'])['result']['name'] . getCurrencyById($response['currencyId'])['result']['symbol'] . ' '. $response['offerAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Amount:</label>
                                            <input type="text" class="form-control" placeholder="Bid Amount" value="<?= getCurrencyById($response['currencyId'])['result']['name'] . getCurrencyById($response['currencyId'])['result']['symbol'] . ' '. $response['bidAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                        <input type="hidden" class="form-control" name="jobId" value="<?= $response['jobId'] ?>">
                                        <div class="col">
                                          <div class="form-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Cancel Reason:</label>
                                            <textarea class="form-control" name="rejectReason" pattern="[a-zA-Z0-9]+" rows="2"></textarea>
                                          </div>
                                        </div>
                                      </div>
                                      <input type="hidden"  name="bidId"  value="<?= $response['bidId'] ?>" >
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <input type="submit" name="cancelAnOffer" class="btn btn-primary" value="Confirm" />
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->

                              <!-- Not working  Bid Modal  -->
                              <div class="modal fade" id="delete<?= $response['bidId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Delete Bid</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?bidId=<?= $response['bidId'] ?>" method="post" enctype="multipart/form-data">
                                      <div class="modal-body">

                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid No:</label>
                                            <input type="text" class="form-control" placeholder="Bid No" value="<?= $response['bidId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Status:</label>
                                            <input type="text" class="form-control" value="<?= $response['bidStatus'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Payment Method:</label>
                                            <input type="text" class="form-control"  value="<?= $response['paymentMode'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control" placeholder="Job No" value="<?= $response['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Partner Name:</label>
                                            <input type="text" class="form-control" placeholder="Partner Name" value="<?= $response['partnerName'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Partner Phone:</label>
                                            <input type="text" class="form-control" placeholder="partner Phone" value="<?= $response['partnerPhone'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Offer Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($response['currencyId'])['result']['name'] . getCurrencyById($response['currencyId'])['result']['symbol'] . ' '. $response['offerAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Amount:</label>
                                            <input type="text" class="form-control" placeholder="Bid Amount" value="<?= getCurrencyById($response['currencyId'])['result']['name'] . getCurrencyById($response['currencyId'])['result']['symbol'] . ' '. $response['bidAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                        <input type="hidden" class="form-control" name="jobId" value="<?= $response['jobId'] ?>" readonly>

                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <input type="submit" name="accept_bid" class="btn btn-primary" value="Confirm" />
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->


                        

                                <!-- Ecocash Modal  -->
                               <div class="modal fade" id="ecocash<?= $response['bidId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog w-40">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Ecocash Payment</h4>
                                 
                                    </div>
                                    <form id ="ecoPayment" >
                                      <div class="modal-body">
                                      <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid No:</label>
                                            <input type="text" class="form-control" placeholder="Bid No" value="<?= $response['bidId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Status:</label>
                                            <input type="text" class="form-control" value="<?= $response['bidStatus'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Payment Method:</label>
                                            <input type="text" class="form-control"  value="<?= $response['paymentMode'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control" placeholder="Job No" value="<?= $response['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Partner Name:</label>
                                            <input type="text" class="form-control" placeholder="Partner Name" value="<?= $response['partnerName'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Partner Phone:</label>
                                            <input type="text" class="form-control" placeholder="partner Phone" value="<?= $response['partnerPhone'] ?>" readonly>
                                          </div>
                                        </div>

                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Partner Email:</label>
                                            <input type="text" class="form-control"   placeholder="partner Phone" value="<?= $response['partnerEmail'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Offer Amount:</label>
                                            <input type="text" class="form-control"  placeholder="Offer Amount" value="<?= getCurrencyById($response['currencyId'])['result']['name'] . getCurrencyById($response['currencyId'])['result']['symbol'] . ' '. $response['offerAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Amount:</label>
                                            <input type="text" class="form-control" placeholder="Bid Amount" value="<?= getCurrencyById($response['currencyId'])['result']['name'] . getCurrencyById($response['currencyId'])['result']['symbol'] . ' '. $response['bidAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="form-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Ecocash Number:</label>
                                            <input type="text" class="form-control"   pattern="07[7-8][0-9]{7}$"  title="Format: 07XXXXXXXX"  name="msisdn" required>
                                          </div>
                                        </div>
                                        <input type="hidden" class="form-control" name="jobId" value="<?= $response['jobId'] ?>" readonly>
                                        <input type="hidden" class="form-control" name="amount" value="<?= $response['bidAmount'] ?>" readonly>
                               
                                      </div>
                                      <div  style ="color: #1C7ACD; text-align: center;" class='ecoResponseBE'></div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                                        <input type="hidden" class="form-control" name="makePayment" value="true" >
                                        <button type="button" name ="makePayment" onClick="ecoBPay()"  class="btn btn-primary"> Confirm </button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->
                                 <!-- Cash Modal  -->
                                 <div class="modal fade" id="cash<?= $response['bidId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog w-40">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Cash Payment</h4>
                                  
                                    </div>
                                    <form id ="cashPayment" >
                                      <div class="modal-body">
                                      <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid No:</label>
                                            <input type="text" class="form-control" placeholder="Bid No" value="<?= $response['bidId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control" placeholder="Job No" value="<?= $response['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Status:</label>
                                            <input type="text" class="form-control" value="<?= $response['bidStatus'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Payment Method:</label>
                                            <input type="text" class="form-control"  value="<?= $response['paymentMode'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Partner Name:</label>
                                            <input type="text" class="form-control" placeholder="Partner Name" value="<?= $response['partnerName'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Partner Phone:</label>
                                            <input type="number" class="form-control" placeholder="partner Phone" value="<?= $response['partnerPhone'] ?>" readonly>
                                          </div>
                                        </div>

                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Partner Email:</label>
                                            <input type="email" class="form-control"   placeholder="partner Phone" value="<?= $response['partnerEmail'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Offer Amount:</label>
                                            <input type="text" class="form-control"  placeholder="Offer Amount" value="<?= getCurrencyById($response['currencyId'])['result']['name'] . getCurrencyById($response['currencyId'])['result']['symbol'] . ' '. $response['offerAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Amount:</label>
                                            <input type="text" class="form-control" placeholder="Bid Amount" value="<?= getCurrencyById($response['currencyId'])['result']['name'] . getCurrencyById($response['currencyId'])['result']['symbol'] . ' '. $response['bidAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                        <input type="hidden" class="form-control" name="jobId" value="<?= $response['jobId'] ?>" readonly>
                                        <input type="hidden" class="form-control" name="amount" value="<?= $response['bidAmount'] ?>" readonly>
                                        <input type="hidden" class="form-control" name="msisdn" value="<?= $response['partnerPhone'] ?>" readonly>
                               
                                      </div>
                                      <div  style ="color: #1C7ACD; text-align: center;" class='cashResponseBC'></div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                       

                                        <input type="hidden" class="form-control" name="createPayment" value="true" >
                                        <button type="button" name ="createPayment" onClick="cashBPay()"  class="btn btn-primary"> Confirm </button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->

                            <?php endforeach; ?>

                          <?php else : ?>

                            <?= $responses['Message'] ?>

                          <?php endif; ?>

                        </tbody>
                      </table>
                    </div>

                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

      <footer class="footer">
        <div class="container-fluid">

          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>, designed with <i class="material-icons">favorite</i> by
            <a href="https://www.dev.vayaafrica.com/" target="_blank">VAYA</a>
          </div>
        </div>
      </footer>
    </div>
  </div>

  <!--   Core JS Files   -->
  <script src="../assets/js/core/jquery.min.js"></script>
  <script src="../assets/js/core/popper.min.js"></script>
  <script src="../assets/js/core/bootstrap-material-design.min.js"></script>
  <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!-- Plugin for the momentJs  -->
  <script src="../assets/js/plugins/moment.min.js"></script>
  <!--  Plugin for Sweet Alert -->
  <script src="../assets/js/plugins/sweetalert2.js"></script>
  <!-- Forms Validations Plugin -->
  <script src="../assets/js/plugins/jquery.validate.min.js"></script>
  <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
  <script src="../assets/js/plugins/jquery.bootstrap-wizard.js"></script>
  <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
  <script src="../assets/js/plugins/bootstrap-selectpicker.js"></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src="../assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
  <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
  <script src="../assets/js/plugins/jquery.dataTables.min.js"></script>
  <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
  <script src="../assets/js/plugins/bootstrap-tagsinput.js"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="../assets/js/plugins/jasny-bootstrap.min.js"></script>
  <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
  <script src="../assets/js/plugins/fullcalendar.min.js"></script>
  <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
  <script src="../assets/js/plugins/jquery-jvectormap.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="../assets/js/plugins/nouislider.min.js"></script>
  <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
  <!-- Library for adding dinamically elements -->
  <script src="../assets/js/plugins/arrive.min.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chartist JS -->
  <script src="../assets/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="../assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../assets/js/material-dashboard.js?v=2.1.2" type="text/javascript"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <script src="../assets/demo/demo.js"></script>
  <script>
    $(document).ready(function() {
      $('#dt-response').dataTable({
        dom: 'Bfrtip',

        buttons: [
          'excel', 'csv'
        ],
        columnDefs: [{
          orderable: false,
          className: 'select-checkbox',
          targets: 0
        }],
        select: {
          style: 'multi',
          selector: 'td:first-child'
        }
      });
    });
  </script>

  <script>
    $(document).ready(function() {
      $().ready(function() {
        $sidebar = $('.sidebar');

        $sidebar_img_container = $sidebar.find('.sidebar-background');

        $full_page = $('.full-page');

        $sidebar_responsive = $('body > .navbar-collapse');

        window_width = $(window).width();

        fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

        if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
          if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
            $('.fixed-plugin .dropdown').addClass('open');
          }

        }

        $('.fixed-plugin a').click(function(event) {
          // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
          if ($(this).hasClass('switch-trigger')) {
            if (event.stopPropagation) {
              event.stopPropagation();
            } else if (window.event) {
              window.event.cancelBubble = true;
            }
          }
        });

        $('.fixed-plugin .active-color span').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-color', new_color);
          }

          if ($full_page.length != 0) {
            $full_page.attr('filter-color', new_color);
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.attr('data-color', new_color);
          }
        });

        $('.fixed-plugin .background-color .badge').click(function() {
          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('background-color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-background-color', new_color);
          }
        });

        $('.fixed-plugin .img-holder').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).parent('li').siblings().removeClass('active');
          $(this).parent('li').addClass('active');


          var new_image = $(this).find("img").attr('src');

          if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            $sidebar_img_container.fadeOut('fast', function() {
              $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
              $sidebar_img_container.fadeIn('fast');
            });
          }

          if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $full_page_background.fadeOut('fast', function() {
              $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
              $full_page_background.fadeIn('fast');
            });
          }

          if ($('.switch-sidebar-image input:checked').length == 0) {
            var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
          }
        });

        $('.switch-sidebar-image input').change(function() {
          $full_page_background = $('.full-page-background');

          $input = $(this);

          if ($input.is(':checked')) {
            if ($sidebar_img_container.length != 0) {
              $sidebar_img_container.fadeIn('fast');
              $sidebar.attr('data-image', '#');
            }

            if ($full_page_background.length != 0) {
              $full_page_background.fadeIn('fast');
              $full_page.attr('data-image', '#');
            }

            background_image = true;
          } else {
            if ($sidebar_img_container.length != 0) {
              $sidebar.removeAttr('data-image');
              $sidebar_img_container.fadeOut('fast');
            }

            if ($full_page_background.length != 0) {
              $full_page.removeAttr('data-image', '#');
              $full_page_background.fadeOut('fast');
            }

            background_image = false;
          }
        });

        $('.switch-sidebar-mini input').change(function() {
          $body = $('body');

          $input = $(this);

          if (md.misc.sidebar_mini_active == true) {
            $('body').removeClass('sidebar-mini');
            md.misc.sidebar_mini_active = false;

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

          } else {

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

            setTimeout(function() {
              $('body').addClass('sidebar-mini');

              md.misc.sidebar_mini_active = true;
            }, 300);
          }

          // we simulate the window Resize so the charts will get updated in realtime.
          var simulateWindowResize = setInterval(function() {
            window.dispatchEvent(new Event('resize'));
          }, 180);

          // we stop the simulation of Window Resize after the animations are completed
          setTimeout(function() {
            clearInterval(simulateWindowResize);
          }, 1000);

        });
      });
    });
  </script>
<script>
//Ecocash Payment (Bids)
function ecoBPay(){

  $.ajax({
			type: "POST",
			url: "../controller/bid_process",     
			data: $('form#ecoPayment').serialize(),
      cache: false,
			success: function(response){
        var json = $.parseJSON(response);
        // console.log(json.Action);
        if(json.Action == 1) {
         $('.ecoResponseBE').empty(); //clear apend
         $('.ecoResponseBE').append("Ecocash payment was initiated successfully");
            $(".modal").on("hidden.bs.modal", function() {
              $(".ecoResponseBE").html("");
            });
          setTimeout(function(){ window.location="jobs"; },2000);
     
        }else {
          $('.ecoResponseBE').empty();
          $('.ecoResponseBE').append(json.Message);
              $(".modal").on("hidden.bs.modal", function() {
              $(".ecoResponseBE").html("");
            });
          // setTimeout(function(){ window.location="bids"; },5000);
        }
        
			},
      error: function(jqXHR, textStatus, errorThrown) {
          //  console.log(jqXHR, textStatus, errorThrown);
                $('.ecoResponseBE').empty();
                $('.ecoResponseBE').append(errorThrown);
                    $(".modal").on("hidden.bs.modal", function() {
              $(".ecoResponseBE").html("");
            });
                // setTimeout(function(){ window.location="bids"; },5000);
        }
		});
}
</script>

<script>
  //Cash Payment (Bids)
function cashBPay(){
  $.ajax({
			type: "POST",
			url: "../controller/bid_process",     
			data: $('form#cashPayment').serialize(),
      cache: false,
			success: function(response){
        var json = $.parseJSON(response);
        // console.log(json.Action);
        if(json.Action == 1) {
        $('.cashResponseBC').empty(); //clear apend
         $('.cashResponseBC').append("Cash payment was initiated successfully");
             $(".modal").on("hidden.bs.modal", function() {
              $(".cashResponseBC").html("");
            });
          setTimeout(function(){ window.location="jobs"; },2000);
        }else {
          $('.cashResponseBC').empty();
          $('.cashResponseBC').append(json.Message);
                $(".modal").on("hidden.bs.modal", function() {
              $(".cashResponseBC").html("");
            });
          // setTimeout(function(){ window.location="bids"; },5000);

        }
        
			},
      error: function(jqXHR, textStatus, errorThrown) {
          //  console.log(jqXHR, textStatus, errorThrown);
          $('.cashResponseBC').empty();
          $('.cashResponseBC').append(errorThrown);
                $(".modal").on("hidden.bs.modal", function() {
              $(".cashResponseBC").html("");
            });
          // setTimeout(function(){ window.location="bids"; },5000);
        }
		});
}
</script>

<script>
  //Assign partner  (Bids)
function assignPartner(){
  $.ajax({
			type: "POST",
			url: "../controller/bid_process",     
			data: $('form#assignpartner').serialize(),
      cache: false,
			success: function(response){
        var json = $.parseJSON(response);
        // console.log(json.Action);
        if(json.Action == 1) {
        $('.assignPartner').empty(); //clear apend
         $('.assignPartner').append("Partner assigned successfully");
           $(".modal").on("hidden.bs.modal", function() {
              $(".assignPartner").html("");
            });
          setTimeout(function(){ window.location="jobs"; },2000);
        }else {
          $('.assignPartner').empty();
          $('.assignPartner').append(json.Message);
           $(".modal").on("hidden.bs.modal", function() {
              $(".assignPartner").html("");
            });
          // setTimeout(function(){ window.location="bids"; },5000);

        }
        
			},
      error: function(jqXHR, textStatus, errorThrown) {
          //  console.log(jqXHR, textStatus, errorThrown);
          $('.assignPartner').empty();
          $('.assignPartner').append(errorThrown);
           $(".modal").on("hidden.bs.modal", function() {
              $(".assignPartner").html("");
            });
          // setTimeout(function(){ window.location="bids"; },5000);
        }
		});
}
</script>

<script>
document.addEventListener('keydown', function() {
  if (event.keyCode == 123) {
    alert("This function has been disabled!");
    return false;
  } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) {
    alert("This function has been disabled!");
    return false;
  } else if (event.ctrlKey && event.keyCode == 85) {
    alert("This function has been disabled!");
    return false;
  }
}, false);

if (document.addEventListener) {
  document.addEventListener('contextmenu', function(e) {
    alert("This function has been disabled!");
    e.preventDefault();
  }, false);
} else {
  document.attachEvent('oncontextmenu', function() {
    alert("This function has been disabled!");
    window.event.returnValue = false;
  });
}
</script>
</body>

</html>