<?php
session_start();

if (!isset($_SESSION['customer_id'])) {
  header("Location: ../../vaya-bidding/customer_login ");
  exit();
}

$iuserId = $_SESSION["customer_id"];
$customer_vphone = $_SESSION["customer_vphone"];
$vfirstName = $_SESSION["vfirstName"];
$vlastName = $_SESSION["vlastName"];

// echo $iuserId;
// echo $user_vphone;
// exit;
// session_start();
// if(!isset($_SESSION['userData']['id'], $_SESSION['userData']['username'], $_SESSION['userData']['userType'], $_SESSION["sess_Token"]))
// {
// 	echo "<script>";
//     echo "window.location.href='../login ?lmsg=true';";
//   echo "</script>";
// 	exit;
// }

// Turn off all error reporting

include_once('../../utils/curl_url.php');
require_once('../controller/bid_process.php');


$pending_jobs = listAllPendingJobsByIUserId($iuserId);
$assigned_jobs = listAllAssignedJobsByIUserId($iuserId);
$collected_jobs = listAllCollectedJobsByIUserId($iuserId);
$intransit_jobs = listAllInTransitJobsByIUserId($iuserId);
$delivered_jobs = listAllDeliveredJobsByIUserId($iuserId);
$cancelled_jobs = listAllCancelledJobsByIUserId($iuserId);
$vehicle_types =  getAllVehicleTypes();
$servicepillars =  getAllServicePillars();
$currencies =  getAllCurrencies();
$banks =  getAllBanks();
$customers =  getCustomer($iuserId);
function getvehicleCategoryById($vehicleCategoryId)
{
 $jsonData = array(
   'type' => "getVehicleCategory",
   'vehicleCategoryId' => $vehicleCategoryId
  );
  return callVayaBiddingWebApiPost($jsonData);
}

function getFilteredVehicleTypes()
{
  $jsonData = array(
    'type' => "getFilteredVehicleTypes"
  );
  return callVayaBiddingWebApiPost($jsonData);
}

$filteredVehicles =  getFilteredVehicleTypes();


$filteredJsonVehicles =  json_encode($filteredVehicles);


?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    VAYA eLogistics
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


  <link href="../assets/css/material-dashboard.css" rel="stylesheet" />
  <link href="../assets/css/material-form-wizard.css" rel="stylesheet" />


  <link href="../assets/date/css/query.datetimepicker.min.css" rel="stylesheet" />
<script src="../assets/date/js/jquery.min.js"></script>
<script src="../assets/date/js/jquery.datetimepicker.js"></script>


    <link rel="stylesheet" href="../assets/rating/css/star-rating.css" media="all" type="text/css"/>
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
    <script src="../assets/rating/js/star-rating.js" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <style>
  .required:after {
    content:" *";
    color: red;
  }
    /*auto show address on map*/
    div.pac-container {
            z-index: 99999999999 !important;
        }

</style>



</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="blue" data-background-color="black" data-image="../assets/img/gomo.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo"><a href="#" class="simple-text logo-normal">
          VAYA eLogistics
        </a></div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <!-- <li class="nav-item ">
            <a class="nav-link" href="./dashboard ">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./trip_management ">
              <i class="material-icons">content_paste</i>
              <p>Trip Management</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./fleet_management ">
              <i class="material-icons">content_paste</i>
              <p>Fleet Management</p>
            </a>
          </li> -->

          <li class="nav-item active">
            <a class="nav-link" href="./jobs ">
              <i class="material-icons">local_shipping</i>
              <p>My Jobs</p>
            </a>
          </li>
          <!-- <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="material-icons">content_paste</i>
              Reports
            </a>
            <div style=" background: linear-gradient(60deg, #1C7ACD, #1C7ACD);color: #fff;" class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="#">RTGS Payment Report</a>
              <a class="dropdown-item" href="#">USD Payment Report</a>
              <a class="dropdown-item" href="#">Trips Report</a>
            </div>
          </li> -->
          <!-- <li class="nav-item">
            <a class="nav-link" href="./configurations ">
              <i class="material-icons">content_paste</i>
              <p>Configurations</p>
            </a>
          </li> -->
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
             <h4 style="margin-left: 45px; color: #1C7ACD"> Welcome (<?php echo $vfirstName . ' ' . $vlastName;?>)</h4>
            <!-- <a class="navbar-brand" href="javascript:;">My Jobs</a> -->
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <!-- <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div> -->
            </form>
            <ul class="navbar-nav">
        
              <li class="nav-item dropdown">
              <?php
              echo $date = date('Y-m-d H:i:s');
               ?>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <!-- <a class="dropdown-item" href="#">Profile</a> -->
                  <!-- <a class="dropdown-item" href="#">Settings</a> -->
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="logout ">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-tabs card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <ul class="nav nav-tabs" data-tabs="tabs">
                        <li class="nav-item">
                          <a class="nav-link active" href="#pending" data-toggle="tab">
                            <i class="material-icons">lens</i> PENDING
                            <div class="ripple-container"></div>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#assigned" data-toggle="tab">
                            <i class="material-icons">person_outline</i>ASSIGNED
                            <div class="ripple-container"></div>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#collected" data-toggle="tab">
                            <i class="material-icons">check</i>COLLECTED
                            <div class="ripple-container"></div>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#in_transit" data-toggle="tab">
                            <i class="material-icons">local_shipping</i> IN TRANSIT
                            <div class="ripple-container"></div>
                          </a>
                        </li>

                        <li class="nav-item">
                          <a class="nav-link" href="#delivered" data-toggle="tab">
                            <i class="material-icons">home</i> DELIVERED
                            <div class="ripple-container"></div>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#cancelled" data-toggle="tab">
                            <i class="material-icons">cancel</i>CANCELLED
                            <div class="ripple-container"></div>
                          </a>
                        </li>

                        <li class="nav-item">

                          <button style="float: right; margin-left: 20vh;" data-target="#postjobmodal" data-toggle="modal" class="btn btn-info">Post A New JOB</button>

                        </li>

                      </ul>
                    </div>
                  </div>
                </div>
                <div class="card-body">
                  <div class="tab-content">
                    <!--pending jobs-->
                    <div class="tab-pane active card-content table-responsive table-full-width" id="pending">
                      <table id="dt-pending" width="140%" cellspacing="0" class="table table-sm table-bordered table-hover table-striped ">
                        <thead class=" text-primary">
                          <th width="15%">Job Number</th>
                          <th width="15%">Pick-up Location</th>
                          <th width="15%">Drop-off Location</th>
                          <th width="10%">Collection Time</th>
                          <th width="10%">DeliveryTime</th>
                          <th width="10%">Vehicle Capacity</th>
                          <th width="10%">Service Pillar</th>
                          <th width="10%">Offer Amount</th>
                          <th width="10%">Type Of Goods</th>
                          <th width="10%">Created At</th>
                          <th width="10%">Action</th>
                   
                        </thead>
                        <tbody>

                          <?php if ($pending_jobs['Action'] == 1) : ?>
                            <?php
                            $pending_jobs['result']['content'] = isset($pending_jobs['result']['content']) ?   $pending_jobs['result']['content'] : '';
                            $pending_jobs['result']['content'] = is_array($pending_jobs['result']['content']) ? $pending_jobs['result']['content'] : array();
                            foreach ($pending_jobs['result']['content'] as $pending_job) :

                            ?>
                              <tr>
                                <td><?= $pending_job['jobId'] ?></td>
                                <td><?= $pending_job['pickupAddress'] ?></td>
                                <td><?= $pending_job['dropOffAddress'] ?></td>
                                <td><?= $pending_job['collectionTime'] ?></td>
                                <td><?= $pending_job['deliveryTime'] ?></td>
                                <td><?= getVehicleType($pending_job['vehicleType'])['result']['capacity'] ?></td>
                                <td><?= getServicePillar($pending_job['servicePillarId'])['result']['name'] ?></td>
                                <td><?=getCurrencyById($pending_job['currencyId'])['result']['name'] . getCurrencyById($pending_job['currencyId'])['result']['symbol'] . ' '. $pending_job['amountOffer'] ?></td>
                                <td><?= $pending_job['typeOfGoods'] ?></td>
                                <td><?=date('Y-m-d\ h:i', strtotime($pending_job['createdDate'])); ?></td>
 
                                <td>
                                <button type="button" rel="tooltip" title="View" class="btn btn-primary btn-link btn-sm">
                                  <i data-target="#myjob<?= $pending_job['jobId'] ?>" data-toggle="modal" class="material-icons">lens</i>
                                </button>
    
                              </td>
                             
                              </tr>

                              <!-- Listing Details Modal  -->
                              <div class="modal fade" id="myjob<?= $pending_job['jobId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Job Details</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?jobId=<?= $pending_job['jobId'] ?>" method="post" enctype="multipart/form-data">
                                      <div class="modal-body">
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control" placeholder="Listing No" value="<?= $pending_job['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job Status:</label>
                                            <input type="text" class="form-control" placeholder="Job Status" value="<?= $pending_job['jobStatus'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">pickUpLocation:</label>
                                            <input type="text" class="form-control" placeholder="partner Phone" value="<?=  $pending_job['pickupAddress'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">dropOffLocation:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?=  $pending_job['dropOffAddress'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Offer Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($pending_job['currencyId'])['result']['name'] . getCurrencyById($pending_job['currencyId'])['result']['symbol'] . ' '. $pending_job['amountOffer'] ?>" readonly>
                                          </div>
                                        </div>
                                        <input type="hidden" class="form-control" name="jobId" value="<?= $pending_job['jobId'] ?>" >
                                        <input type="hidden" class="form-control" name="customerId" value="<?php echo $iuserId; ?>" >

                                      </div>
                                      <div class="modal-footer">
                                      <!-- <button type="button" data-target="#updateJob<?= $pending_job['jobId'] ?>" data-toggle="modal" class="btn btn-primary">Update Job</button> -->
                                        <button type="button" data-target="#cancelJob<?= $pending_job['jobId'] ?>" data-toggle="modal" class="btn btn-secondary">Cancel Job</button>
                                        <a href="bids?jobId=<?= $pending_job['jobId'] ?>"><button type="button" class="btn btn-primary">View Bids</button></a>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->

                              <!-- cancel Listing Modal  -->
                              <div class="modal fade" id="cancelJob<?= $pending_job['jobId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Job Listing</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?jobId=<?= $pending_job['jobId'] ?>" method="post" enctype="multipart/form-data">
                                      <div class="modal-body">
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control" placeholder="Listing No" value="<?= $pending_job['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job Status:</label>
                                            <input type="text" class="form-control" placeholder="Job Status" value="<?= $pending_job['jobStatus'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">pickUpLocation:</label>
                                            <input type="text" class="form-control" placeholder="partner Phone" value="<?=  $pending_job['pickupAddress'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">dropOffLocation:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?=  $pending_job['dropOffAddress'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Offer Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($pending_job['currencyId'])['result']['name'] . getCurrencyById($pending_job['currencyId'])['result']['symbol'] . ' '. $pending_job['amountOffer'] ?>"readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <p class="text-primary">Are you sure you want to cancel the Job?</p>
                                          </div>
                                        </div>
                                        <input type="hidden" class="form-control" name="jobId" value="<?= $pending_job['jobId'] ?>">
                                        <input type="hidden" class="form-control" name="customerId" value="<?php echo $iuserId; ?>" >
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <input type="submit" name="cancelJob" class="btn btn-primary" value="Confirm" />
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->


                            <?php endforeach; ?>

                          <?php else : ?>

                            <?= $pending_jobs['Message'] ?>

                          <?php endif; ?>

                        </tbody>
                      </table>
                    </div>
                    <!--assigned jobs-->
                    <div class="tab-pane card-content table-responsive table-full-width" id="assigned">
                      <table id="dt-assigned" width="140%" cellspacing="0" class="table table-sm table-bordered table-hover table-striped ">
                        <thead class=" text-primary">
                          <th width="15%">Job Number</th>
                          <th width="15%">Pick-up Location</th>
                          <th width="15%">Drop-off Location</th>
                          <th width="10%">Collection Time</th>
                          <th width="10%">DeliveryTime</th>
                          <!-- <th width="10%">Payment Method</th> -->
                          <th width="10%">Vehicle Capacity</th>
                          <th width="10%">Payment Status</th>
                          <th width="10%">Type Of Goods</th>
                          <th width="10%">Created At</th>
                          <th width="10%">Action</th>
                   

                       

                        </thead>
                        <tbody>

                          <?php if ($assigned_jobs['Action'] == 1) : ?>

                            <?php
                            $assigned_jobs['result']['content'] = isset($assigned_jobs['result']['content']) ?   $assigned_jobs['result']['content'] : '';
                            $assigned_jobs['result']['content'] = is_array($assigned_jobs['result']['content']) ? $assigned_jobs['result']['content'] : array();
                            foreach ($assigned_jobs['result']['content'] as $assigned_job) :

                            ?>
                              <tr >
                                <td><?= $assigned_job['jobId'] ?></td>
                                <td><?= $assigned_job['pickupAddress'] ?></td>
                                <td><?= $assigned_job['dropOffAddress'] ?></td>
                                <td><?= $assigned_job['collectionTime'] ?></td>
                                <td><?= $assigned_job['deliveryTime'] ?></td>
                                <td><?= getVehicleType($assigned_job['vehicleType'])['result']['capacity'] ?></td>
                                <!-- <td><?= $assigned_job['paymentMode'] ?></td> -->
                                <td><?= $assigned_job['paymentStatus'] ?></td>
                                <td><?= $assigned_job['typeOfGoods'] ?></td>
                                <td><?=date('Y-m-d\ h:i', strtotime($assigned_job['createdDate'])); ?></td>
                                <td>
                                <button type="button" rel="tooltip" title="View" class="btn btn-primary btn-link btn-sm">
                                  <i data-target="#pay<?= $assigned_job['jobId'] ?>" data-toggle="modal" class="material-icons">lens</i>
                                </button>
    
                              </td>
              
                              </tr>
                            <!-- Listing Details Modal  -->
                          <div class="modal fade jobdetails" id="pay<?= $assigned_job['jobId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog  w-40">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Job Details</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?jobId=<?= $assigned_job['jobId'] ?>" method="post" enctype="multipart/form-data">
                                      <div class="modal-body">
                                 
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control" placeholder="Listing No" value="<?= $assigned_job['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job Status:</label>
                                            <input type="text" class="form-control"  value="<?= $assigned_job['jobStatus'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Pickuplocation:</label>
                                            <input type="text" class="form-control"  value="<?= $assigned_job['pickupAddress'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Dropofflocation:</label>
                                            <input type="text" class="form-control"  value="<?= $assigned_job['dropOffAddress'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Offer Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="$<?= $assigned_job['amountOffer'] ?>" readonly>
                                          </div>
                                        </div>
                                   
                                        <input type="hidden" class="form-control" name="jobId" value="<?= $assigned_job['jobId'] ?>" readonly>

                                      </div>
                                      <!-- <h4 class='text-center text-primary'>Select Payment Method</h4> -->
                                      <div class="modal-footer">

                                      <?php

                                    if($assigned_job['paymentStatus'] == "Paid"){
                                      
                                      echo "<h4  class='text-primary'>The job was paid successfully.</h4>"; 
                                    }
                                    else {

                                      // echo "<h5 style ='text-align: center;'  text-primary'>Select Payment Method.</h5>"; 
                                      echo "<button type='button' class='btn btn-primary'   data-toggle='modal' data-target='#ecocash" . $assigned_job['jobId'] . "' >Ecocash</button>";       
                                      echo "<button type='button' class='btn btn-secondary'   data-toggle='modal' data-target='#internaltransfer" . $assigned_job['jobId'] . "' >Internal Transfer</button>";    
                                      echo "<button type='button' class='btn btn-primary'   data-toggle='modal' data-target='#rtgs" . $assigned_job['jobId'] . "' >Rtgs</button>";         

                                            

                                    }
                                              

                                        ?>
                                  
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                         
                    
                                <!-- Ecocash Modal  -->
                               <div  class="modal fade" id="ecocash<?= $assigned_job['jobId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog w-40">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Ecocash</h4>
                                     
                                    </div>
                                    <form id ="ecoPayment<?= $assigned_job['jobId'] ?>" >
                                      <div class="modal-body">
                                          <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control" placeholder="Listing No" value="<?= $assigned_job['jobId'] ?>" readonly>
                                          </div>
                                          </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job Status:</label>
                                            <input type="text" class="form-control"  value="<?= $assigned_job['jobStatus'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Pickuplocation:</label>
                                            <input type="text" class="form-control"  value="<?= $assigned_job['pickupAddress'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Dropofflocation:</label>
                                            <input type="text" class="form-control"  value="<?= $assigned_job['dropOffAddress'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Total Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($assigned_job['currencyId'])['result']['name'] . ' '. $assigned_job['bidAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                   
                                        <input type="hidden" class="form-control" name="jobId" value="<?= $assigned_job['jobId'] ?>" readonly>
                                        <div class="col">
                                          <div class="form-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Ecocash Number:</label>
                                            <input type="text" class="form-control" pattern="07[7-8][0-9]{7}$"  title="Format: 07XXXXXXXX"  name="msisdn" required>
                                          </div>
                                        </div>
                                        <input type="hidden" class="form-control" name="jobId" value="<?= $assigned_job['jobId'] ?>" readonly>
                                        <input type="hidden" class="form-control" name="amount" value="<?= $assigned_job['bidAmount'] ?>" readonly>
                               
                                      </div>
                                      <div  style ="color: #1C7ACD; text-align: center;" class='ecoResponseAE'></div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          
                                        <input type="hidden" class="form-control" name="ecoPayment" value="true" >
                                        <button type="button" name ="ecoPayment"  onClick="ecoAPay('<?= $assigned_job["jobId"] ?>')" class="btn btn-primary"> Confirm </button>

                                 
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                              </div>

                              <!--end modal-->
                               <!-- Internal Transfer Modal  -->
                               <div  class="modal fade" id="internaltransfer<?= $assigned_job['jobId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog w-40">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Internal Transfer</h4>
                                     
                                    </div>
                                    <form id ="inPayment<?= $assigned_job['jobId'] ?>" >
                                      <div class="modal-body">
                                          <div class="col">
                                          <div class="form-group">
                                            <input type="text" class="form-control" name ="customerAccount" placeholder="Customer Account Number" required>
                                          </div>
                                          </div>
                                          <div class="col">
                                          <div class="form-group">
                                            <input type="text" class="form-control" name ="customerName" placeholder="Customer Name" required >
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="form-group">
                                            <input type="text" class="form-control" name ="reason" placeholder="Reason" required >
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="form-group">
                                            <input type="text" class="form-control" value="<?= getCurrencyById($assigned_job['currencyId'])['result']['name'] . ' '. $assigned_job['bidAmount'] ?>" readonly >
                                          </div>
                                        </div>
                                        <input type="hidden" class="form-control" name="jobId" value="<?= $assigned_job['jobId'] ?>">
                                        <input type="hidden" class="form-control" name="currency" value="<?= getCurrencyById($assigned_job['currencyId'])['result']['name'] ?>">
                                        <input type="hidden" class="form-control" name="amount" value="<?= $assigned_job['bidAmount'] ?>">
                               
                                      </div>
                                      <div  style ="color: #1C7ACD; text-align: center;" class='inResponseAE'></div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <input type="hidden" class="form-control" name="inPayment" value="true" >
                                        <button type="button" name ="inPayment" onClick="inAPay('<?= $assigned_job["jobId"] ?>')" class="btn btn-primary"> Confirm </button>

                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                              </div>

                              <!--end modal-->
                               <!-- RTGS Modal  -->
                               <div  class="modal fade" id="rtgs<?= $assigned_job['jobId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog w-40">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">RTGS</h4>
                                     
                                    </div>
                                    <form id ="rtPayment<?= $assigned_job['jobId'] ?>" >

                                      <div class="modal-body">
                                      <div class="col">
                                          <div class="form-group">
                                            <input type="text" class="form-control" name ="customerAccount" placeholder="Customer Account Number" required>
                                          </div>
                                          </div>
                                          <div class="col">
                                          <div class="form-group">
                                            <input type="text" class="form-control" name ="customerName" placeholder="Customer Name" required >
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="form-group">
                                            <input type="text" class="form-control" name ="reason" placeholder="Reason" required >
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="form-group">
                                          <label>Choose Bank:</label>
                                           <select class=" form-control"  name="bankCode"  value="<?= $bank['swiftCode'] ?>">
                                            <?php foreach ($banks['result']  as $bank) : ?>
                                              <option selected="selected" value="<?= $bank["swiftCode"] ?>"><?= $bank["name"] . "" ?></option>
                                            <?php endforeach; ?>
                                          </select>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="form-group">
                                            <input type="text" class="form-control" value="<?= getCurrencyById($assigned_job['currencyId'])['result']['name'] . ' '. $assigned_job['bidAmount'] ?>" readonly >
                                          </div>
                                        </div>
                                     
                                        <input type="hidden" class="form-control" name="jobId" value="<?= $assigned_job['jobId'] ?>">
                                        <input type="hidden" class="form-control" name="currency" value="<?= getCurrencyById($assigned_job['currencyId'])['result']['name'] ?>">
                                        <input type="hidden" class="form-control" name="amount" value="<?= $assigned_job['bidAmount'] ?>">
                               
                                      </div>
                                      <div  style ="color: #1C7ACD; text-align: center;" class='rtResponseAE'></div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          
                                        <input type="hidden" class="form-control" name="rtPayment" value="true" >
                                        <button type="button" name ="rtPayment"  onClick="rtAPay('<?= $assigned_job["jobId"] ?>')" class="btn btn-primary"> Confirm </button>
                                 
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                              </div>

                              <!--end modal-->
                             <!-- Cash Modal  -->
                              <div class="modal fade" id="cash<?= $assigned_job['jobId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog w-40">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Cash</h4>
                                   
                                    </div>
                                    <form id ="cashPayment" >
                                      <div class="modal-body">
                                  
                                      <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control" placeholder="Listing No" value="<?= $assigned_job['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job Status:</label>
                                            <input type="text" class="form-control"  value="<?= $assigned_job['jobStatus'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Pickuplocation:</label>
                                            <input type="text" class="form-control"  value="<?= $assigned_job['pickupAddress'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Dropofflocation:</label>
                                            <input type="text" class="form-control"  value="<?= $assigned_job['dropOffAddress'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Total Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($assigned_job['currencyId'])['result']['name'] . getCurrencyById($assigned_job['currencyId'])['result']['symbol'] . ' '. $assigned_job['bidAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                        <!-- <div class="col">
                                          <div class="form-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Phone Number:</label>
                                            <input type="text" class="form-control" name="msisdn" value="<?php echo $customer_vphone; ?>" readonly >
                                          </div>
                                        </div> -->
                                        <input type="hidden" class="form-control" name="jobId" value="<?= $assigned_job['jobId'] ?>" >
                                        <input type="hidden" class="form-control" name="msisdn" value="<?php echo $customer_vphone; ?>" >
                                        <input type="hidden" class="form-control" name="amount" value="<?= $assigned_job['bidAmount'] ?>">
                              
                                      </div>
                                      <div  style ="color: #1C7ACD; text-align: center;" class='cashResponseAC'></div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                
                                        <input type="hidden" class="form-control" name="cashPayment" value="true" >
                                        <button type="button" name ="cashPayment" onClick="cashAPay()"  class="btn btn-primary"> Confirm </button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->
                            <?php endforeach; ?>

                          <?php else : ?>

                            <?= $assigned_jobs['Message'] ?>

                          <?php endif; ?>

                        </tbody>
                      </table>
                    </div>

                    <!--collected -->
                    <div class="tab-pane card-content table-responsive table-full-width" id="collected">
                      <table id="dt-collected" width="140%" cellspacing="0" class="table  table-sm table-bordered table-hover table-striped ">
                        <thead class=" text-primary">
                        <th width="15%">Job Number</th>
                          <th width="15%">Pick-up Location</th>
                          <th width="15%">Drop-off Location</th>
                          <th width="10%">Collection Time</th>
                          <th width="10%">DeliveryTime</th>
                          <!-- <th width="10%">Payment Method</th> -->
                          <th width="10%">Payment Status</th>
                          <th width="10%">Type Of Goods</th>
                          <th width="10%">Created At</th>
                          <th width="10%">Action</th>

                        </thead>
                        <tbody>

                          <?php if ($collected_jobs['Action'] == 1) : ?>

                            <?php
                            $collected_jobs['result']['content'] = isset($collected_jobs['result']['content']) ?   $collected_jobs['result']['content'] : '';
                            $collected_jobs['result']['content'] = is_array($collected_jobs['result']['content']) ? $collected_jobs['result']['content'] : array();
                            foreach ($collected_jobs['result']['content'] as $collected_job) :

                            ?>
                              <tr >
                                <td><?= $collected_job['jobId'] ?></td>
                          
                                <td><?= $collected_job['pickupAddress'] ?></td>
                                <td><?= $collected_job['dropOffAddress'] ?></td>
                                <td><?= $collected_job['collectionTime'] ?></td>
                                <td><?= $collected_job['deliveryTime'] ?></td>
                                <!-- <td><?= $collected_job['paymentMode'] ?></td> -->
                                <td><?= $collected_job['paymentStatus'] ?></td>  
                                <td><?= $collected_job['typeOfGoods'] ?></td>
                                <td><?=date('Y-m-d\ h:i', strtotime($collected_job['createdDate'])); ?></td>
                                <td>
                                <button type="button" rel="tooltip" title="View" class="btn btn-primary btn-link btn-sm">
                                  <i data-target="#pay<?= $collected_job['jobId'] ?>" data-toggle="modal" class="material-icons">lens</i>
                                </button>
    
                              </td>
              
                           
                            
                              </tr>
                 
                           <!-- List Details Modal  -->
                           <div class="modal fade" id="pay<?= $collected_job['jobId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog  w-40">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Job Details</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?jobId=<?= $collected_job['jobId'] ?>" method="post" enctype="multipart/form-data">
                                      <div class="modal-body">
                                 
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control" placeholder="Listing No" value="<?= $collected_job['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job Status:</label>
                                            <input type="text" class="form-control"  value="<?= $collected_job['jobStatus'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Pickuplocation:</label>
                                            <input type="text" class="form-control"  value="<?= $collected_job['pickupAddress'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Dropofflocation:</label>
                                            <input type="text" class="form-control"  value="<?= $collected_job['dropOffAddress'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Total Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($collected_job['currencyId'])['result']['name'] . getCurrencyById($collected_job['currencyId'])['result']['symbol'] . ' '. $collected_job['bidAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                   
                                        <input type="hidden" class="form-control" name="jobId" value="<?= $collected_job['jobId'] ?>" readonly>

                                      </div>
                                      <!-- <h4 class='text-center text-primary'>Select Payment Method</h4> -->
                                      <div class="modal-footer">

                                      <?php

                                    if($collected_job['paymentStatus'] == "Paid"){
                                      
                                      echo "<h4  class='text-primary'>The job was paid successfully.Thank you.</h4>"; 
                                    }
                                    else {

                                      // echo "<h5 style ='text-align: center;'  text-primary'>Select Payment Method.</h5>"; 
                                      echo "<button type='button' class='btn btn-primary'   data-toggle='modal' data-target='#cecocash" . $collected_job['jobId'] . "' >Ecocash</button>";       
                                      echo "<button type='button' class='btn btn-secondary'   data-toggle='modal' data-target='#cinternaltransfer" . $collected_job['jobId'] . "' >Internal Transfer</button>";    
                                      echo "<button type='button' class='btn btn-primary'   data-toggle='modal' data-target='#crtgs" . $collected_job['jobId'] . "' >Rtgs</button>";         

                                            

                                    }
                                              

                                        ?>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                              <!--end modal-->
                        
                             <!-- Ecocash Modal  -->
                             <div  class="modal fade" id="cecocash<?= $collected_job['jobId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog w-40">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Ecocash</h4>
                                     
                                    </div>
                                    <form id ="ecoCPayment<?= $collected_job['jobId'] ?>" >
                                      <div class="modal-body">
                                          <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control" placeholder="Listing No" value="<?= $collected_job['jobId'] ?>" readonly>
                                          </div>
                                          </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job Status:</label>
                                            <input type="text" class="form-control"  value="<?= $collected_job['jobStatus'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Pickuplocation:</label>
                                            <input type="text" class="form-control"  value="<?= $collected_job['pickupAddress'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Dropofflocation:</label>
                                            <input type="text" class="form-control"  value="<?= $collected_job['dropOffAddress'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Total Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($collected_job['currencyId'])['result']['name'] . ' '. $collected_job['bidAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                   
                                        <input type="hidden" class="form-control" name="jobId" value="<?= $collected_job['jobId'] ?>" readonly>
                                        <div class="col">
                                          <div class="form-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Ecocash Number:</label>
                                            <input type="text" class="form-control" pattern="07[7-8][0-9]{7}$"  title="Format: 07XXXXXXXX"  name="msisdn" required>
                                          </div>
                                        </div>
                                        <input type="hidden" class="form-control" name="jobId" value="<?= $collected_job['jobId'] ?>" readonly>
                                        <input type="hidden" class="form-control" name="amount" value="<?= $collected_job['bidAmount'] ?>" readonly>
                               
                                      </div>
                                      <div  style ="color: #1C7ACD; text-align: center;" class='ecoResponseCE'></div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          
                                        <input type="hidden" class="form-control" name="ecoPayment" value="true" >
                                        <button type="button" name ="ecoPayment"  onClick="ecoCPay('<?= $collected_job["jobId"] ?>')" class="btn btn-primary"> Confirm </button>

                                 
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                              </div>

                              <!--end modal-->
                               <!-- Internal Transfer Modal  -->
                               <div  class="modal fade" id="cinternaltransfer<?= $collected_job['jobId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog w-40">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Internal Transfer</h4>
                                     
                                    </div>
                                    <form id ="inCPayment<?= $collected_job['jobId'] ?>" >
                                      <div class="modal-body">
                                          <div class="col">
                                          <div class="form-group">
                                            <input type="text" class="form-control" name ="customerAccount" placeholder="Customer Account Number" required>
                                          </div>
                                          </div>
                                          <div class="col">
                                          <div class="form-group">
                                            <input type="text" class="form-control" name ="customerName" placeholder="Customer Name" required >
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="form-group">
                                            <input type="text" class="form-control" name ="reason" placeholder="Reason" required>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="form-group">
                                            <input type="text" class="form-control" value="<?= getCurrencyById($collected_job['currencyId'])['result']['name'] . ' '. $collected_job['bidAmount'] ?>" readonly >
                                          </div>
                                        </div>
                                        <input type="hidden" class="form-control" name="jobId" value="<?= $collected_job['jobId'] ?>">
                                        <input type="hidden" class="form-control" name="currency" value="<?= getCurrencyById($collected_job['currencyId'])['result']['name'] ?>">
                                        <input type="hidden" class="form-control" name="amount" value="<?= $collected_job['bidAmount'] ?>">
                               
                                      </div>
                                      <div  style ="color: #1C7ACD; text-align: center;" class='inResponseCE'></div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <input type="hidden" class="form-control" name="inPayment" value="true" >
                                        <button type="button" name ="inPayment"    onClick="inCPay('<?= $collected_job["jobId"] ?>')"   class="btn btn-primary"> Confirm </button>

                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                              </div>

                              <!--end modal-->
                               <!-- RTGS Modal  -->
                               <div  class="modal fade" id="crtgs<?= $collected_job['jobId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog w-40">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">RTGS</h4>
                                     
                                    </div>
                                    <form id ="rtCPayment<?= $collected_job['jobId'] ?>" >

                                      <div class="modal-body">
                                      <div class="col">
                                          <div class="form-group">
                                            <input type="text" class="form-control" name ="customerAccount" placeholder="Customer Account Number" required>
                                          </div>
                                          </div>
                                          <div class="col">
                                          <div class="form-group">
                                            <input type="text" class="form-control" name ="customerName" placeholder="Customer Name" required >
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="form-group">
                                            <input type="text" class="form-control" name ="reason" placeholder="Reason" required >
                                          </div>
                                        </div>
                                        <div class="col">
                                        <div class="form-group">
                                          <label>Choose Bank:</label>
                                           <select class=" form-control"  name="bankCode"  value="<?= $bank['swiftCode'] ?>">
                                            <?php foreach ($banks['result']  as $bank) : ?>
                                              <option selected="selected" value="<?= $bank["swiftCode"] ?>"><?= $bank["name"] . "" ?></option>
                                            <?php endforeach; ?>
                                          </select>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="form-group">
                                            <input type="text" class="form-control" value="<?= getCurrencyById($collected_job['currencyId'])['result']['name'] . ' '. $collected_job['bidAmount'] ?>" readonly >
                                          </div>
                                        </div>
                                     
                                        <input type="hidden" class="form-control" name="jobId" value="<?= $collected_job['jobId'] ?>">
                                        <input type="hidden" class="form-control" name="currency" value="<?= getCurrencyById($collected_job['currencyId'])['result']['name'] ?>">
                                        <input type="hidden" class="form-control" name="amount" value="<?= $collected_job['bidAmount'] ?>">
                               
                                      </div>
                                      <div  style ="color: #1C7ACD; text-align: center;" class='rtResponseCE'></div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          
                                        <input type="hidden" class="form-control" name="rtPayment" value="true" >
                                        <button type="button" name ="rtPayment"  onClick="rtCPay('<?= $collected_job["jobId"] ?>')"  class="btn btn-primary"> Confirm </button>

                                 
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                              </div>

                              <!--end modal-->
                             <!-- Cash Modal  -->
                              <div class="modal fade" id="cash<?= $collected_job['jobId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog w-40">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Cash</h4>
                                   
                                    </div>
                                    <form id ="cashPayment" >
                                      <div class="modal-body">
                                  
                                      <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control" placeholder="Listing No" value="<?= $collected_job['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Listing Status:</label>
                                            <input type="text" class="form-control"  value="<?= $collected_job['jobStatus'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Pickuplocation:</label>
                                            <input type="text" class="form-control"  value="<?= $collected_job['pickupAddress'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Dropofflocation:</label>
                                            <input type="text" class="form-control"  value="<?= $collected_job['dropOffAddress'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Total Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount"  value="<?= getCurrencyById($collected_job['currencyId'])['result']['name'] . getCurrencyById($collected_job['currencyId'])['result']['symbol'] . ' '. $collected_job['bidAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                        <!-- <div class="col">
                                          <div class="form-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Phone Number:</label>
                                            <input type="text" class="form-control" name="msisdn" value="<?php echo $customer_vphone; ?>" readonly >
                                          </div>
                                        </div> -->
                                        <input type="hidden" class="form-control" name="jobId" value="<?= $collected_job['jobId'] ?>" >
                               
                                        <input type="hidden" class="form-control" name="msisdn" value="<?php echo $customer_vphone; ?>" >
                                        <input type="hidden" class="form-control" name="amount" value="<?= $collected_job['bidAmount'] ?>">
                              
                                      </div>
                                      <div  style ="color: #1C7ACD; text-align: center;" class='cashResponseAC'></div>
                                      <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      <input type="hidden" class="form-control" name="cashPayment" value="true" >
                                      <button type="button" name ="cashPayment" onClick="cashCPay()"  class="btn btn-primary"> Confirm </button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->
                            <?php endforeach; ?>

                          <?php else : ?>

                            <?= $collected_jobs['Message'] ?>

                          <?php endif; ?>

                        </tbody>
                      </table>
                    </div>

                    <!--intransit-->
                    <div class="tab-pane card-content table-responsive table-full-width" id="in_transit">
                      <table id="dt-intransit" width="140%" cellspacing="0" class="table table-sm  table-bordered table-hover table-striped ">
                        <thead class=" text-primary">
                        <th width="15%">Job Number</th>
                        <th width="15%">Pick-up Location</th>
                          <th width="15%">Drop-off Location</th>
                          <th width="10%">Collection Time</th>
                          <th width="10%">DeliveryTime</th>
                          <!-- <th width="10%">Payment Method</th> -->
                          <th width="10%">Payment Status</th>
                          <th width="10%">Delivery Code</th>
                          <th width="10%">Offer Amount</th>
                          <th width="10%">Type Of Goods</th>
                          <th width="10%">Created At</th>
                          <!-- <th width="10%">Tracking</th> -->

                        </thead>
                        <tbody>


                          <?php if ($intransit_jobs['Action'] == 1) : ?>

                            <?php
                            $intransit_jobs['result']['content'] = isset($intransit_jobs['result']['content']) ?   $intransit_jobs['result']['content'] : '';
                            $intransit_jobs['result']['content'] = is_array($intransit_jobs['result']['content']) ? $intransit_jobs['result']['content'] : array();
                            foreach ($intransit_jobs['result']['content'] as $intransit_job) :

                            ?>
                              <tr>
                                <td><?= $intransit_job['jobId'] ?></td>
                                <td><?= $intransit_job['pickupAddress'] ?></td>
                                <td><?= $intransit_job['dropOffAddress'] ?></td>
                                <td><?= $intransit_job['collectionTime'] ?></td>
                                <td><?= $intransit_job['deliveryTime'] ?></td>
                                <!-- <td><?= $intransit_job['paymentMode'] ?></td> -->
                                <td><?= $intransit_job['paymentStatus'] ?></td>  
                                <td><?= $intransit_job['deliveryCode'] ?></td>
                                <td><?=getCurrencyById($intransit_job['currencyId'])['result']['name'] . getCurrencyById($intransit_job['currencyId'])['result']['symbol'] . ' '. $intransit_job['amountOffer'] ?></td>
                                <td><?= $intransit_job['typeOfGoods'] ?></td>
                                <td><?=date('Y-m-d\ h:i', strtotime($intransit_job['createdDate'])); ?></td>

                              <!-- <td>
                                <button type="button" rel="tooltip" title="View" class="btn btn-primary btn-link btn-sm">
                                  <i data-target="#intransitjob<?= $intransit_job['jobId'] ?>" data-toggle="modal" class="material-icons">lens</i>
                                </button>
    
                              </td>
              
                               -->
              
                           
                              </tr>

                              <!-- Real Time Tracking  Modal  -->
                              <div class="modal fade" id="intransitjob<?= $intransit_job['jobId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog  w-40">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Real Time Tracking</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?jobId=<?= $intransit_job['jobId'] ?>" method="post">
                                      <div class="modal-body">
                                        <input type="hidden" id="start1" value="<?=  $intransit_job['pickupAddress'] ?>"><!-- Starting Location -->

                                        <input type="hidden" id="end1" value="<?=  $intransit_job['dropOffAddress'] ?>"><!-- Ending Location -->

                                        <div class="container" id="map" style="height: 380px;width: 400px;"></div>
                                        <input type="hidden" id="myInput" class="form-control" value="<?= $intransit_job['tripId'] ?>">
                                      </div>

                                      <div class="modal-footer">

                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <!-- <input type="submit" name="getTripLocationsByTripId" class="btn btn-primary" value="Current Location" /> -->
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                              <!--end modal-->
                            <?php endforeach; ?>

                          <?php else : ?>

                            <?= $intransit_jobs['Message'] ?>

                          <?php endif; ?>

                        </tbody>
                      </table>
                    </div>

                    <!--delivered-->
                    <div class="tab-pane card-content table-responsive table-full-width" id="delivered">
                      <table id="dt-delivered" width="150%" cellspacing="0" class="table table-sm table-bordered table-hover table-striped ">
                        <thead class=" text-primary">
                        <th width="15%">Job Number</th>
                        <th width="15%">Pick-up Location</th>
                          <th width="15%">Drop-off Location</th>
                          <th width="10%">Collection Time</th>
                          <th width="10%">DeliveryTime</th>
                          <!-- <th width="10%">Payment Method</th> -->
                          <th width="10%">Payment Status</th>
                          <th width="10%">Delivery Code</th>
                          <th width="10%">Offer Amount</th>
                          <th width="10%">Type Of Goods</th>
                          <th width="10%">Created At</th>
                          <th width="10%">Action</th>

                        </thead>
                        <tbody>

                          <?php if ($delivered_jobs['Action'] == 1) : ?>

                            <?php
                            $delivered_jobs['result']['content'] = isset($delivered_jobs['result']['content']) ?   $delivered_jobs['result']['content'] : '';
                            $delivered_jobs['result']['content'] = is_array($delivered_jobs['result']['content']) ? $delivered_jobs['result']['content'] : array();
                            foreach ($delivered_jobs['result']['content'] as $delivered_job) :

                            ?>
                              <tr >
                                <td><?= $delivered_job['jobId'] ?></td>
                                <td><?= $delivered_job['pickupAddress'] ?></td>
                                <td><?= $delivered_job['dropOffAddress'] ?></td>
                                <td><?= $delivered_job['collectionTime'] ?></td>
                                <td><?= $delivered_job['deliveryTime'] ?></td>
                                <!-- <td><?= $delivered_job['paymentMode'] ?></td> -->
                                <td><?= $delivered_job['paymentStatus'] ?></td> 
                                <td><?= $delivered_job['deliveryCode'] ?></td>
                                <td><?=getCurrencyById($delivered_job['currencyId'])['result']['name'] . getCurrencyById($delivered_job['currencyId'])['result']['symbol'] . ' '. $delivered_job['amountOffer'] ?></td>
                                <td><?= $delivered_job['typeOfGoods'] ?></td>
                                <td><?=date('Y-m-d\ h:i', strtotime($delivered_job['createdDate'])); ?></td>
                          
                              <td>
                                <button type="button" rel="tooltip" title="View" class="btn btn-primary btn-link btn-sm">
                                  <i data-target="#jobDelivered<?= $delivered_job['jobId'] ?>" data-toggle="modal" class="material-icons">lens</i>
                                </button>
    
                              </td>
              
                              
              
                            
                              </tr>

                                <!-- Job Details Modal  -->
                                <div class="modal fade" id="jobDelivered<?= $delivered_job['jobId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog w-40">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Job Details</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?jobId=<?= $delivered_job['jobId'] ?>" method="POST">
                                      <div class="modal-body">
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control" placeholder="Listing No" value="<?= $delivered_job['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job Status:</label>
                                            <input type="text" class="form-control" placeholder="Job Status" value="<?= $delivered_job['jobStatus'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">pickUpLocation:</label>
                                            <input type="text" class="form-control" placeholder="partner Phone" value="<?=  $delivered_job['pickupAddress'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">dropOffLocation:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?=  $delivered_job['dropOffAddress'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Offer Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($delivered_job['currencyId'])['result']['name'] . getCurrencyById($delivered_job['currencyId'])['result']['symbol'] . ' '. $delivered_job['amountOffer'] ?>" readonly>
                                          </div>
                                        </div>

                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Vehicle Capacity:</label>
                                            <input type="text" class="form-control" placeholder="1 tonne" value="<?= getVehicleType($delivered_job['vehicleType'])['result']['capacity'] ?>" readonly>
                                          </div>
                                        </div>
                                    
                                        <input type="hidden" class="form-control" name="tripId" value="<?= $delivered_job['tripId'] ?>">
                                        <input type="hidden" class="form-control" name="idriverId" value="<?= $delivered_job['idriverId'] ?>">
                                      </div>
                                      <div class="modal-footer">
                                    
                                       <button type="button" data-target="#ratingDriver<?= $delivered_job['jobId'] ?>" data-toggle="modal" class="btn btn-secondary">Rate Partner</button>
                                     
                                      <!-- <button type="button" data-target="#repostob<?= $delivered_job['jobId'] ?>" data-toggle="modal" class="btn btn-primary">Re-Post Job</button>
                                       -->
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->
                              <!-- Rating Modal  -->
                              <div class="modal fade" id="ratingDriver<?= $delivered_job['jobId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog w-40">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Rating Driver</h4>
                                     
                                    </div>
                                    <form id ="ratingdriver" >
                                      <div class="modal-body">
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control" placeholder="Listing No" value="<?= $delivered_job['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job Status:</label>
                                            <input type="text" class="form-control" placeholder="Job Status" value="<?= $delivered_job['jobStatus'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">pickUpLocation:</label>
                                            <input type="text" class="form-control" placeholder="partner Phone" value="<?=  $delivered_job['pickupAddress'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">dropOffLocation:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?=  $delivered_job['dropOffAddress'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Offer Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($delivered_job['currencyId'])['result']['name'] . getCurrencyById($delivered_job['currencyId'])['result']['symbol'] . ' '. $delivered_job['amountOffer'] ?>" readonly>
                                          </div>
                                        </div>

                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Vehicle Capacity:</label>
                                            <input type="text" class="form-control" placeholder="1 tonne" value="<?= getVehicleType($delivered_job['vehicleType'])['result']['capacity'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                      
                                            <label for="input-2" class="control-label col-sm-4">Rating Scale:</label>
                                            <!-- <input type="number"  class="rating" name="scale" data-size="lg" title=""> -->
                                            <select class="form-control" name="scale">
                                              <option value ="1">1</option>
                                              <option value ="2">2</option>
                                              <option value ="3">3</option>
                                              <option value ="4">4</option>
                                              <option value ="5">5</option>
                                            </select>
                                          </div>
                                        </div> 
                                    
                                        <div class="col">
                                          <div class="form-group">
                                            <label for="rejectReason" class="control-label col-sm-4">Comment:</label>
                                            <textarea class="form-control" name="comment" rows="3" pattern="[A-Za-z0-9]+" title="Letters and numbers only, no punctuation or special characters" required></textarea>
                                          </div>
                                        </div>
                                        <input type="hidden" class="form-control" name="tripId" value="<?= $delivered_job['tripId'] ?>">
                                        <input type="hidden" class="form-control" name="idriverId" value="<?= $delivered_job['idriverId'] ?>">
                                      </div>
                                      <div  style ="color: #1C7ACD; text-align: center;" class='ratingDriver'></div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                       
                                        <input type="hidden" class="form-control" name="createRating" value="true" >
                                        <button type="button" name ="createRating" onClick="ratingDriver()"  class="btn btn-primary"> Confirm </button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->






                            <?php endforeach; ?>

                          <?php else : ?>

                            <?= $delivered_jobs['Message'] ?>

                          <?php endif; ?>

                        </tbody>
                      </table>
                    </div>
                    <!--cancelled-->

                    <div class="tab-pane card-content table-responsive table-full-width" id="cancelled">
                      <table id="dt-cancelled" width="140%" cellspacing="0" class="table  table-sm table-bordered table-hover table-striped ">
                        <thead class=" text-primary">
                        <th width="15%">Job Number</th>
                        <th width="15%">Pick-up Location</th>
                          <th width="15%">Drop-off Location</th>
                          <th width="10%">Collection Time</th>
                          <th width="10%">DeliveryTime</th>
                          <!-- <th width="10%">Payment Method</th> -->
                          <th width="10%">Payment Status</th>
                          <th width="10%">Offer Amount</th>
                          <th width="10%">Type Of Goods</th>
                          <th width="10%">Created At</th>

                        </thead>
                        <tbody>

                          <?php if ($cancelled_jobs['Action'] == 1) : ?>

                            <?php
                            $cancelled_jobs['result']['content'] = isset($cancelled_jobs['result']['content']) ?   $cancelled_jobs['result']['content'] : '';
                            $cancelled_jobs['result']['content'] = is_array($cancelled_jobs['result']['content']) ? $cancelled_jobs['result']['content'] : array();
                            foreach ($cancelled_jobs['result']['content'] as $cancelled_job) :

                            ?>
                              <tr>
                                <td><?= $cancelled_job['jobId'] ?></td>
                                <td><?= $cancelled_job['pickupAddress'] ?></td>
                                <td><?= $cancelled_job['dropOffAddress'] ?></td>
                                <td><?= $cancelled_job['collectionTime'] ?></td>
                                <td><?= $cancelled_job['deliveryTime'] ?></td>
                                <!-- <td><?= $cancelled_job['paymentMode'] ?></td> -->
                                <td><?= $cancelled_job['paymentStatus'] ?></td>
                                <td><?=getCurrencyById($cancelled_job['currencyId'])['result']['name'] . getCurrencyById($cancelled_job['currencyId'])['result']['symbol'] . ' '. $cancelled_job['amountOffer'] ?></td>  
                                <td><?= $cancelled_job['typeOfGoods'] ?></td>
                                <td><?=date('Y-m-d\ h:i', strtotime($cancelled_job['createdDate'])); ?></td>
                             
                              </tr>

                            <?php endforeach; ?>

                          <?php else : ?>

                            <?= $cancelled_jobs['Message'] ?>

                          <?php endif; ?>

                        </tbody>
                      </table>
                    </div>
                    <!--end cancelled-->

                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    
      <div id="postjobmodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="wizard-title">Post A New Job</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              
    

      <!--multisteps-form-->
      <div class="multisteps-form">
        <!--progress bar-->
        <div class="row">
          <div class="col-12 col-lg-8 ml-auto mr-auto mb-4">
            <div class="multisteps-form__progress">
              <button class="multisteps-form__progress-btn js-active" type="button" title="Cargo Details">Cargo Details</button>
              <button class="multisteps-form__progress-btn" type="button" title="Delivery Details">Delivery Details</button>
              <button class="multisteps-form__progress-btn" type="button" title="Listing Details">Job Details</button>
            </div>
          </div>
        </div>
        <!--form panels-->
        <div class="row">
          <div class="col-12 col-lg-8 m-auto">
          
          <form  id="form1"  class="multisteps-form__form" action="../controller/bid_process" method="post" >
              <!--single Cargo Details form panel-->
              <div class="multisteps-form__panel shadow p-4 rounded bg-white js-active" data-animation="scaleIn">
        
                <div class="multisteps-form__content">
                <div class="form-row mt-4">
                <!-- <div class="col-12 col-sm-12">
                      <label class="required">Description:</label>
                      <textarea  class="multisteps-form__input form-control" id ="description" name ="description" placeholder="Type.." rows="1"  onkeyup='saveValue(this);' required></textarea>
                  </div> -->

                      <div class="col-12 col-sm-6">
                      <label class="required">Description:</label>
             
                      <textarea  class="multisteps-form__input form-control" id ="description" name ="description" pattern="[A-Za-z0-9]+" title="Letters and numbers only, no  special characters"  rows="1"  onkeyup='saveValue(this);' required></textarea>
              
                    </div>
                    <div class="col-6 col-sm-6 mt-5 mt-sm-0">
                      <label class="required">Value Of Goods:</label>
                      <input class="multisteps-form__input form-control" type="number" id ="goodsValue" pattern="[0-9]+([\.,][0-9]+)?" step="0.01"  title="This should be a number with up to 2 decimal places." name ="goodsValue" onkeyup='saveValue(this);' required/>
                    </div>
                  </div>
                  <div class="form-row mt-4">
                    <div class="col-12 col-sm-6">
                      <label class="required">Type Of Goods:</label>
                      <input class="multisteps-form__input form-control" type="text" id ="typeOfGoods" name="typeOfGoods" pattern="[A-Za-z0-9]+" title="Letters and numbers only, no  special characters"   onkeyup='saveValue(this);'required/>
                    </div>
                    <div class="col-6 col-sm-3 mt-4 mt-sm-0">
                      <label class="required">Weight:</label>
                      <input class="multisteps-form__input form-control" type="number" id ="weight" pattern="[0-9]+([\.,][0-9]+)?" step="0.01"  title="This should be a number with up to 2 decimal places." name ="weight"   onkeyup='saveValue(this);'required/>
                    </div>
                    <div class="col-6 col-sm-3 mt-4 mt-sm-0">
                      <label>Unit:</label>
                      <select class="multisteps-form__select form-control" name = "weight_unit">
                        <option selected="selected" value = "g">g</option>
                        <option value = "kg">kg</option>
                        <option value = "t">t</option>
                      </select>
                    </div>
           
                  
                  </div>
                  <div class="form-row mt-4">
                  <div class="col-6 col-sm-3 mt-4 mt-sm-0">
                      <label class="required">Length:</label>
                      <input class="multisteps-form__input form-control" type="number"  pattern="[0-9]+([\.,][0-9]+)?" step="0.01"  title="This should be a number with up to 2 decimal places." id ="length" name ="length"  onkeyup='saveValue(this);'required/>
                    </div>
                    <div class="col-6 col-sm-3 mt-4 mt-sm-0">
                      <label class="required">Width:</label>
                      <input class="multisteps-form__input form-control" type="number" pattern="[0-9]+([\.,][0-9]+)?" step="0.01"  title="This should be a number with up to 2 decimal places." id ="width" name="width" placeholder="3" onkeyup='saveValue(this);' required/>
                    </div>
                    <div class="col-6 col-sm-3 mt-4 mt-sm-0">
                      <label class="required">Height:</label>
                      <input class="multisteps-form__input form-control" type="number" pattern="[0-9]+([\.,][0-9]+)?" step="0.01"  title="This should be a number with up to 2 decimal places." id ="height" name="height" placeholder="3" onkeyup='saveValue(this);' required/>
                    </div>
                    <div class="col-6 col-sm-3 mt-4 mt-sm-0">
                      <label>Unit:</label>
                      <select class="multisteps-form__select form-control" name = "dimension_unit">
                      <option selected="selected" value = "cm">cm</option>
                        <option value ="inch">inch</option>
                        <option value = "m">m</option>
                        </select>
                    </div>
                  </div>
                  <div class="form-row mt-4">
                  <!-- <div class="col-12 col-sm-6">
                      <label class="required">Currency:</label>
                      <select class="multisteps-form__select form-control" id ="currencyld"  name="currencyld"  onkeyup='saveValue(this);' >
                                            <?php foreach ($currencies['result']  as $currency) : ?>
                                              <option selected="selected" value="<?= $currency["currencyId"] ?>"><?= $currency["name"] . "" ?></option>
                                            <?php endforeach; ?>
                                          </select>
            
                    </div>
                    <div class="col-6 col-sm-6 mt-5 mt-sm-0">
                      <label class="required">Value Of Goods:</label>
                      <input class="multisteps-form__input form-control" type="text" id ="goodsValue" name ="goodsValue" placeholder="5" onkeyup='saveValue(this);' required/>
                    </div> -->

                  </div>
                  <div class="form-row mt-4">
                    <div class="col-12 col-sm-6">
                      <label class="required">Manifest:</label>
                      <input class="multisteps-form__input form-control" type="file" id ="manifestUpload" name="manifestUpload"  accept="image/*,.pdf,.doc,.docx" onkeyup='saveValue(this);' /> 
                    </div>
                    <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                      <label class="required">Waybill:</label>
                      <input class="multisteps-form__input form-control" type="file" id ="wayBills" name ="wayBills" accept="image/*,.pdf,.doc,.docx" onkeyup='saveValue(this);' />
                    </div>
                  </div>
            
                  <div class="form-row mt-4">
                  <input class="multisteps-form__input form-control" type="hidden" name="iUserId" value="<?php echo $iuserId; ?>" >
                  <div class="button-row d-flex mt-4">
                    <button class="btn btn-primary ml-auto js-btn-next"  id="search" type="button" title="Next">Next</button>
                    <!-- <input id="search" class="btn btn-primary ml-auto js-btn-next"   value="Next" /> -->
                  </div>
                </div>
              </div>
              </div>

              <!--single Delivery Details form panel-->
              <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleIn">
                <div class="multisteps-form__content">
            
                  <div class="form-row mt-4">
                    <div class="col-12 col-sm-6">
                      <label class="required">Pick-Up Location(Mark location):</label>
                      <input class="multisteps-form__input form-control" type="text" id="pick_place" data-toggle="modal" data-target=".pickUpLocation" name="pickupAddress" readonly required/>
                      <input name="pickupLatitude" class="MapPickLat" type="hidden" id ="pickupLatitude"   required readonly="readonly">
											<input name="pickupLongitude" class="MapPickLon" type="hidden"  id ="pickupLongitude"  required readonly="readonly">
                    </div>
                    <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                      <label class="required">Drop-Off Location(Mark location):</label>
                      <input class="multisteps-form__input form-control" type="text" id="drop_place" data-toggle="modal" data-target=".dropOFfLocation" name ="dropOffAddress"  readonly required/>
                      <input name="dropOffLatitude" class="MapDropLat"  type="hidden" id ="dropOffLatitude"  style="width: 161px;"  required readonly="readonly">
											<input name="dropOffLongitude" class="MapDropLon"  type="hidden"  id ="dropOffLongitude" style="width: 161px;"  required readonly="readonly">
                    </div>
                  </div>
                  <div class="form-row mt-4">
                    <div class="col-12 col-sm-6">
                      <label class="required">Collection Time:</label>
                      <input class="multisteps-form__input form-control" type="datetime-local"   name="collectionTime" onkeyup='saveValue(this);' required />
                      <!-- <input class="multisteps-form__input form-control" type="text"  id="datetimepicker3" name="collectionTime" onkeyup='saveValue(this);' required /> -->
                    </div>
                    <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                      <label class="required">DeliveryTime:</label>
                      <input class="multisteps-form__input form-control" type="datetime-local"   name ="deliveryTime" onkeyup='saveValue(this);' required />
                      <!-- <input class="multisteps-form__input form-control" type="text"  id="datetimepicker4"  name ="deliveryTime" onkeyup='saveValue(this);' required /> -->
                    </div>
                  </div>
                  <div class="form-row mt-4">
                  <div class="col-6 col-sm-3 mt-4 mt-sm-0">
                  
                 
        <?php
        
        $arr = array();
        foreach ($filteredVehicles['result']  as  $filteredVehicle) {
            $arr[] = $filteredVehicle["name"];
        }
        $unique_data = array_unique($arr);
      
      ?>

      <select id="category" onChange="getCapacity();" class="form-control">
        <option value="0">No Vehicle Category Selected</option>
        <?php foreach ($unique_data  as  $val) : ?>
          <option value="<?= $val ?>"><?= $val?></option>
        <?php endforeach; ?>
      </select>

                    </div>

                        <div class="col-6 col-sm-3 mt-4 mt-sm-0">
              
                        <select  class="form-control" id="capacity" name ="vehicleTypeId">
                        <option value="0">No Vehicle Size Selected</option>
                        </select> 
                        </div>
<!-- 
                        <input type="hidden" id="myInputID" class="form-control"  name ="vehicleTypeId" > -->

                    <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                      <label class="required">Service Pillar:</label>
                     
                        <select class="multisteps-form__select form-control" id ="servicePillarId" name="servicePillarId" onkeyup='saveValue(this);' required>
                                            <?php foreach ($servicepillars['result']  as $servicepillar) : ?>
                                              <option selected="selected" value="<?= $servicepillar["servicePillarId"] ?>"><?= $servicepillar["name"] . "" ?></option>
                                            <?php endforeach; ?>
                                          </select>
                    
                    </div>
                  </div>
      
                  <div class="button-row d-flex mt-4">
                    <button class="btn btn-primary js-btn-prev" type="button" title="Prev">Prev</button>
                    <button class="btn btn-primary ml-auto js-btn-next"  type="button" title="Next">Next</button>
                  </div>
                </div>
              </div>
              <!--single Job Details form panel-->
              <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleIn">

                <div class="multisteps-form__content">
                  <div class="form-row mt-4">
                    <div class="col-12 col-sm-6">
                      <label class="required">Loading Requirements:</label>
                      <textarea class="multisteps-form__textarea form-control" id ="loadingRequirements"   pattern="[A-Za-z0-9]+" title="Letters and numbers only, no special characters" name ="loadingRequirements" placeholder="Type.."  onkeyup='saveValue(this);' required></textarea>
                    </div>
                    <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                      <label class="required">Offloading Requirements:</label>
                      <textarea class="multisteps-form__textarea form-control" id ="offloadingRequirements"   pattern="[A-Za-z0-9]+" title="Letters and numbers only, no special characters" name ="offloadingRequirements" placeholder="Type.." onkeyup='saveValue(this);' required></textarea>
                    </div>
                  </div>
                  <div class="form-row mt-4">
                  <div class="col-12 col-sm-6">
                      <label class="required">Bid-Start Time:</label>
                      <input class="multisteps-form__input form-control" type="datetime-local"  title="Job will be viewed at the posted bid-start time" name="bidStartTime" onkeyup='saveValue(this);' required />
                      <!-- <input class="multisteps-form__input form-control" type="text"  id="datetimepicker1" name="bidStartTime" onkeyup='saveValue(this);' required /> -->
                    </div>
                    <div class="col-6 col-sm-3 mt-4 mt-sm-0">
                      <label class="required">Bid-End Time:</label>
                      <input class="multisteps-form__input form-control" type="datetime-local"  name ="bidEndTime" onkeyup='saveValue(this);' required/>
                      <!-- <input class="multisteps-form__input form-control" type="text" id="datetimepicker2" name ="bidEndTime" onkeyup='saveValue(this);' required/> -->
                    </div>
                    <div class="col-6 col-sm-3 mt-4 mt-sm-0">
                      <label class="required" title = "Goods in-transit is 1% of goods value.">Goods In-Transit:</label>
                      <select class="multisteps-form__select form-control" id ="goodsInTransit"  name="goodsInTransit"  onkeyup='saveValue(this);' required>
                      <option  value = "true">Yes</option>
                      <option value = "false">No</option>               
                       </select>
                    </div>
                  </div>
                  <div class="form-row mt-4">
                    <div class="col-12 col-sm-6">
                      <label class="required">Payment Method:</label>
                      <select  class="multisteps-form__select form-control" id ="paymentMode" name="paymentMode" onkeyup='saveValue(this);' required>
                        <option value="Ecocash">Ecocash</option>
                        <option value="Bank">Bank</option>
                      </select>
                    </div>
                    <div class="col-6 col-sm-3 mt-4 mt-sm-0">
                      <label class="required">Currency:</label>
                      <select class="multisteps-form__select form-control" id ="currencyld"  name="currencyld"  onkeyup='saveValue(this);' required>
                                            <?php foreach ($currencies['result']  as $currency) : ?>
                                              <option selected="selected" value="<?= $currency["currencyId"] ?>"><?= $currency["name"] . "" ?></option>
                                            <?php endforeach; ?>
                                          </select>
                    </div>
                    <div class="col-6 col-sm-3 mt-4 mt-sm-0">
                      <label class="required">Offer Amount:</label>
                      <input class="multisteps-form__input form-control" type="number" id ="amountOffer" pattern="[0-9]+([\.,][0-9]+)?" step="0.01"  title="This should be a number with up to 2 decimal places." name ="amountOffer" onkeyup='saveValue(this);' required/>
                    </div>
                 
                  
                  </div>
                    <input class="multisteps-form__input form-control" type="hidden" name="customerName" value="<?php echo $vfirstName .'' . $vlastName; ?>" />
                  </div>
                  <div class="row">
                    <div class="button-row d-flex mt-4 col-12">
                      <button class="btn btn-primary js-btn-prev" type="button" title="Prev">Prev</button>
                      <input type="submit" name="jobpost" id="search" class="btn btn-success ml-auto"  value="Post For Bidding" />
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>

            <!-- <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="button" class="btn btn-primary">Save for later</button>
            </div> -->
          </div>
        </div>
      </div>
       </div>
      <div>
        		<!-- Pick Up Map Modal -->
<div class="modal fade pickUpLocation" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" >
    <!-- <div class="modal fade slide-up disable-scroll" id="modalSlideUp" tabindex="-1" role="dialog" aria-hidden="false"> -->
    <div class="modal-dialog modal-lg">

        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title" id="myLargeModalLabel">Pick Up Location</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <input type="text" id="searchPickTextField" size="50" style="width:350px; height: 40px; border: 3px solid #180e29; margin-left: 25%;" placeholder="Enter location">
            <br>
            <!-- latitude:<input name="latitude" class="MapPickLat" value="" type="text" placeholder="Latitude" style="width: 161px;" disabled>
            longitude:<input name="longitude" class="MapPickLon" value="" type="text" placeholder="Longitude" style="width: 161px;" disabled> -->

            <div class="container" id="map_pick" style="height: 380px;width: 680px;"></div>

            <br />

            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Use This Location</button>
            </div>

        </div>
    </div>
</div>


<!-- Drop Of Map Modal -->

<div class="modal fade dropOFfLocation" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myLargeModalLabel">Drop Off Location</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <input type="text" id="searchDropTextField" size="50" style="width:350px; height: 40px; border: 3px solid #180e29; margin-left: 25%;" placeholder="Enter location">
            <br>
            <!-- latitude:<input name="latitude" class="MapDropLat" value="" type="text" placeholder="Latitude" style="width: 161px;" disabled>
            longitude:<input name="longitude" class="MapDropLon" value="" type="text" placeholder="Longitude" style="width: 161px;" disabled> -->

            <div class="container" id="map_drop" style="height: 380px;width: 680px;"></div>
            <br />

            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Use This Location</button>
            </div>

        </div>
    </div>
</div>

      <footer class="footer">
        <div class="container-fluid">

          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>, designed with <i class="material-icons">favorite</i> by
            <a href="https://www.dev.vayaafrica.com/" referrerpolicy="no-referrer | same-origin | origin | strict-origin | no-origin-when-downgrading" target="_blank">VAYA</a>
          </div>
        </div>
      </footer>
    </div>
 

  <!--   Core JS Files   -->
  <script src="../assets/js/core/jquery.min.js"></script>
  <script src="../assets/js/core/popper.min.js"></script>
  <script src="../assets/js/core/material-form-wizard.js"></script>

  <script src="../assets/js/core/bootstrap-material-design.min.js"></script>
  <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!-- Plugin for the momentJs  -->
  <script src="../assets/js/plugins/moment.min.js"></script>
  <!--  Plugin for Sweet Alert -->
  <script src="../assets/js/plugins/sweetalert2.js"></script>
  <!-- Forms Validations Plugin -->
  <script src="../assets/js/plugins/jquery.validate.min.js"></script>

  <!--	Plugin for Select, full documentation here: https://silviomoreto.github.io/bootstrap-select -->
  <script src="../assets/js/plugins/bootstrap-selectpicker.js"></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src="../assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
  <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
  <script src="../assets/js/plugins/jquery.dataTables.min.js"></script>
  <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
  <script src="../assets/js/plugins/bootstrap-tagsinput.js"></script>
  <!-- Plugin for Fileupload, full documentation here: https://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="../assets/js/plugins/jasny-bootstrap.min.js"></script>
  <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
  <script src="../assets/js/plugins/fullcalendar.min.js"></script>
  <!-- Vector Map plugin, full documentation here: https://jvectormap.com/documentation/ -->
  <script src="../assets/js/plugins/jquery-jvectormap.js"></script>
  <!--  Plugin for the Sliders, full documentation here: https://refreshless.com/nouislider/ -->
  <script src="../assets/js/plugins/nouislider.min.js"></script>
  <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
  <!-- Library for adding dinamically elements -->
  <script src="../assets/js/plugins/arrive.min.js"></script>
  <!--  Google Maps Plugin    -->
  <!-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQTpHIDA-oUPXuJhXW7PHxRo_9OWUnoMs&callback=initMap&libraries=places,geometry"></script> -->
  <script type="text/javascript" src="https://maps.google.com/maps/api/js?libraries=places&key=AIzaSyDQTpHIDA-oUPXuJhXW7PHxRo_9OWUnoMs"></script>

  <!-- Chartist JS -->
  <script src="../assets/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="../assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../assets/js/material-dashboard.js?v=2.1.2" type="text/javascript"></script>



 <!-- date JS -->
<link rel="stylesheet" type="text/css" href="../assets/date/css/jquery.datetimepicker.min.css"/>
<script src="../assets/date/js/jquery.datetimepicker.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#datetimepicker1').datetimepicker();
        
    });

    $(document).ready(function() {
        $('#datetimepicker2').datetimepicker();
    });

    $(document).ready(function() {
        $('#datetimepicker3').datetimepicker();
    });
    $(document).ready(function() {
        $('#datetimepicker4').datetimepicker();
    });

    // $('#datetimepicker3').datetimepicker({
		// 		datepicker:false,
		// 		format:'H:i'
    //   });
    //   $('#datetimepicker4').datetimepicker({
		// 		datepicker:false,
		// 		format:'H:i'
		// 	});

</script>

  <script>
function submitForm(){
document.getElementById('job').submit();

}

  </script>

  <script>
    $(function() {
      $('#modalToggle').click(function() {
        $('#modal').modal({
          backdrop: 'static'
        });
      });

      $('#infoContinue').click(function(e) {
        e.preventDefault();
        $('.progress-bar').css('width', '40%');
        $('.progress-bar').html('Step 2 of 5');
        $('#myTab a[href="#ads"]').tab('show');
      });

      $('#adsContinue').click(function(e) {
        e.preventDefault();
        $('.progress-bar').css('width', '60%');
        $('.progress-bar').html('Step 3 of 5');
        $('#myTab a[href="#placementPanel"]').tab('show');
      });

      $('#placementContinue').click(function(e) {
        e.preventDefault();
        $('.progress-bar').css('width', '80%');
        $('.progress-bar').html('Step 4 of 5');
        $('#myTab a[href="#schedulePanel"]').tab('show');
      });

      $('#scheduleContinue').click(function(e) {
        e.preventDefault();
        $('.progress-bar').css('width', '100%');
        $('.progress-bar').html('Step 5 of 5');
        $('#myTab a[href="#reviewPanel"]').tab('show');
      });

      $('#activate').click(function(e) {
        e.preventDefault();
        var formData = {
          campaign_name: $('#campaignName').val(),
          start_date: $('#start-date').val(),
          end_date: $('#end-date').val(),
          days: {
            sunday: $('#sunday').prop('checked'),
            monday: $('#monday').prop('checked'),
            tuesday: $('#tuesday').prop('checked'),
            wednesday: $('#wednesday').prop('checked'),
            thurday: $('#thursday').prop('checked'),
            friday: $('#friday').prop('checked'),
            saturday: $('#saturday').prop('checked'),
          },
          start_time: $('#start-time').val(),
          end_time: $('#end-time').val()
        }
        alert(JSON.stringify(formData));
      })
    })
  </script>

  <script>
    $(document).ready(function() {
      $('#dt-pending').dataTable({
        dom: 'Bfrtip',

        buttons: [
          'excel', 'csv'
        ],
        columnDefs: [{
          orderable: false,
          className: 'select-checkbox',
          targets: 0
        }],
        select: {
          style: 'multi',
          selector: 'td:first-child'
        }
      });
    });
  </script>

  <script>
    $(document).ready(function() {
      $('#dt-collected').dataTable({
        dom: 'Bfrtip',

        buttons: [
          'excel', 'csv'
        ],
        columnDefs: [{
          orderable: false,
          className: 'select-checkbox',
          targets: 0
        }],
        select: {
          style: 'multi',
          selector: 'td:first-child'
        }
      });
    });
  </script>
  <script>
    $(document).ready(function() {
      $('#dt-assigned').dataTable({
        dom: 'Bfrtip',

        buttons: [
          'excel', 'csv'
        ],
        columnDefs: [{
          orderable: false,
          className: 'select-checkbox',
          targets: 0
        }],
        select: {
          style: 'multi',
          selector: 'td:first-child'
        }
      });
    });
  </script>
  <script>
    $(document).ready(function() {
      $('#dt-cancelled').dataTable({
        dom: 'Bfrtip',

        buttons: [
          'excel', 'csv'
        ],
        columnDefs: [{
          orderable: false,
          className: 'select-checkbox',
          targets: 0
        }],
        select: {
          style: 'multi',
          selector: 'td:first-child'
        }
      });
    });
  </script>
  <script>
    $(document).ready(function() {
      $('#dt-intransit').dataTable({
        dom: 'Bfrtip',

        buttons: [
          'excel', 'csv'
        ],
        columnDefs: [{
          orderable: false,
          className: 'select-checkbox',
          targets: 0
        }],
        select: {
          style: 'multi',
          selector: 'td:first-child'
        }
      });
    });
  </script>
  <script>
    $(document).ready(function() {
      $('#dt-delivered').dataTable({
        dom: 'Bfrtip',

        buttons: [
          'excel', 'csv'
        ],
        columnDefs: [{
          orderable: false,
          className: 'select-checkbox',
          targets: 0
        }],
        select: {
          style: 'multi',
          selector: 'td:first-child'
        }
      });
    });
  </script>
  <script>
    $(document).ready(function() {
      $().ready(function() {
        $sidebar = $('.sidebar');

        $sidebar_img_container = $sidebar.find('.sidebar-background');

        $full_page = $('.full-page');

        $sidebar_responsive = $('body > .navbar-collapse');

        window_width = $(window).width();

        fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

        if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
          if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
            $('.fixed-plugin .dropdown').addClass('open');
          }

        }

        $('.fixed-plugin a').click(function(event) {
          // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
          if ($(this).hasClass('switch-trigger')) {
            if (event.stopPropagation) {
              event.stopPropagation();
            } else if (window.event) {
              window.event.cancelBubble = true;
            }
          }
        });

        $('.fixed-plugin .active-color span').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-color', new_color);
          }

          if ($full_page.length != 0) {
            $full_page.attr('filter-color', new_color);
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.attr('data-color', new_color);
          }
        });

        $('.fixed-plugin .background-color .badge').click(function() {
          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('background-color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-background-color', new_color);
          }
        });

        $('.fixed-plugin .img-holder').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).parent('li').siblings().removeClass('active');
          $(this).parent('li').addClass('active');


          var new_image = $(this).find("img").attr('src');

          if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            $sidebar_img_container.fadeOut('fast', function() {
              $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
              $sidebar_img_container.fadeIn('fast');
            });
          }

          if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $full_page_background.fadeOut('fast', function() {
              $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
              $full_page_background.fadeIn('fast');
            });
          }

          if ($('.switch-sidebar-image input:checked').length == 0) {
            var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
          }
        });

        $('.switch-sidebar-image input').change(function() {
          $full_page_background = $('.full-page-background');

          $input = $(this);

          if ($input.is(':checked')) {
            if ($sidebar_img_container.length != 0) {
              $sidebar_img_container.fadeIn('fast');
              $sidebar.attr('data-image', '#');
            }

            if ($full_page_background.length != 0) {
              $full_page_background.fadeIn('fast');
              $full_page.attr('data-image', '#');
            }

            background_image = true;
          } else {
            if ($sidebar_img_container.length != 0) {
              $sidebar.removeAttr('data-image');
              $sidebar_img_container.fadeOut('fast');
            }

            if ($full_page_background.length != 0) {
              $full_page.removeAttr('data-image', '#');
              $full_page_background.fadeOut('fast');
            }

            background_image = false;
          }
        });

        $('.switch-sidebar-mini input').change(function() {
          $body = $('body');

          $input = $(this);

          if (md.misc.sidebar_mini_active == true) {
            $('body').removeClass('sidebar-mini');
            md.misc.sidebar_mini_active = false;

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

          } else {

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

            setTimeout(function() {
              $('body').addClass('sidebar-mini');

              md.misc.sidebar_mini_active = true;
            }, 300);
          }

          // we simulate the window Resize so the charts will get updated in realtime.
          var simulateWindowResize = setInterval(function() {
            window.dispatchEvent(new Event('resize'));
          }, 180);

          // we stop the simulation of Window Resize after the animations are completed
          setTimeout(function() {
            clearInterval(simulateWindowResize);
          }, 1000);

        });
      });
    });
  </script>

  
<script>
$(document).ready(function(){
  $(function() {
        var lat = -17.7808295,
            lng = 31.0740816,

            latlng = new google.maps.LatLng(lat, lng),
            image = 'https://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png';

        //zoomControl: true,
        //zoomControlOptions: google.maps.ZoomControlStyle.LARGE,

        var mapOptions = {
                center: new google.maps.LatLng(lat, lng),
                zoom: 13,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                panControl: true,
                panControlOptions: {
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE,
                    position: google.maps.ControlPosition.TOP_left
                }
            },
            map = new google.maps.Map(document.getElementById('map_pick'), mapOptions),
            marker = new google.maps.Marker({
                position: latlng,
                map: map,
                // draggable: true,
                icon: image
            });

        var input = document.getElementById('searchPickTextField');
        var autocomplete = new google.maps.places.Autocomplete(input, {
            types: ["geocode"]
        });

        autocomplete.bindTo('bounds', map);
        var infowindow = new google.maps.InfoWindow();

        google.maps.event.addListener(autocomplete, 'place_changed', function(event) {
            infowindow.close();
            var place = autocomplete.getPlace();
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }

            moveMarker(place.name, place.geometry.location);
            $('.MapPickLat').val(place.geometry.location.lat());
            $('.MapPickLon').val(place.geometry.location.lng());
        });
        google.maps.event.addListener(map, 'click', function(event) {
            $('.MapPickLat').val(event.latLng.lat());
            $('.MapPickLon').val(event.latLng.lng());
            infowindow.close();
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({
                "latLng": event.latLng
            }, function(results, status) {
                console.log(results, status);
                if (status == google.maps.GeocoderStatus.OK) {
                    console.log(results);
                    var lat = results[0].geometry.location.lat(),
                        lng = results[0].geometry.location.lng(),
                        placeName = results[0].address_components[0].long_name,
                        latlng = new google.maps.LatLng(lat, lng);


                    moveMarker(placeName, latlng);
                    $("#searchPickTextField").val(results[0].formatted_address);
                    $("#pick_place").val(results[0].formatted_address);


                }
            });
        });

        function moveMarker(placeName, latlng) {
            marker.setIcon(image);
            marker.setPosition(latlng);
            infowindow.setContent(placeName);
            infowindow.open(map, marker);
        }
    });
});

</script>



<script>
$(document).ready(function(){
  $(function() {
        var lat = -17.7808295,
            lng = 31.0740816,

            latlng = new google.maps.LatLng(lat, lng),
            image = 'https://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png';

        //zoomControl: true,
        //zoomControlOptions: google.maps.ZoomControlStyle.LARGE,

        var mapOptions = {
                center: new google.maps.LatLng(lat, lng),
                zoom: 13,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                panControl: true,
                panControlOptions: {
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE,
                    position: google.maps.ControlPosition.TOP_left
                }
            },
            map = new google.maps.Map(document.getElementById('map_drop'), mapOptions),
            marker = new google.maps.Marker({
                position: latlng,
                map: map,
                // draggable: true,
                icon: image
            });

        var input = document.getElementById('searchDropTextField');
        var autocomplete = new google.maps.places.Autocomplete(input, {
            types: ["geocode"]
        });

        autocomplete.bindTo('bounds', map);
        var infowindow = new google.maps.InfoWindow();

        google.maps.event.addListener(autocomplete, 'place_changed', function(event) {
            infowindow.close();
            var place = autocomplete.getPlace();
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }

            moveMarker(place.name, place.geometry.location);
            $('.MapDropLat').val(place.geometry.location.lat());
            $('.MapDropLon').val(place.geometry.location.lng());
        });
        google.maps.event.addListener(map, 'click', function(event) {
            $('.MapDropLat').val(event.latLng.lat());
            $('.MapDropLon').val(event.latLng.lng());
            infowindow.close();
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({
                "latLng": event.latLng
            }, function(results, status) {
                console.log(results, status);
                if (status == google.maps.GeocoderStatus.OK) {
                    console.log(results);
                    var lat = results[0].geometry.location.lat(),
                        lng = results[0].geometry.location.lng(),
                        placeName = results[0].address_components[0].long_name,
                        latlng = new google.maps.LatLng(lat, lng);


                    moveMarker(placeName, latlng);
                    $("#searchDropTextField").val(results[0].formatted_address);
                    $("#drop_place").val(results[0].formatted_address);

                }
            });
        });

        function moveMarker(placeName, latlng) {
            marker.setIcon(image);
            marker.setPosition(latlng);
            infowindow.setContent(placeName);
            infowindow.open(map, marker);
        }
    });
});
</script>

<script>

$(document).ready(function(){
  function calcDistance(p1, p2) { //p1 and p2 in the form of google.maps.LatLng object
      return (google.maps.geometry.spherical.computeDistanceBetween(p1, p2) / 1000).toFixed(3); //distance in KiloMeters
    }

    function getLocation() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
      } else {
        alert("Geolocation is not supported by this browser.");
      }
    }

    function showPosition(position) {
      alert("Latitude: " + position.coords.latitude + " Longitude: " + position.coords.longitude);
    }

    function showError(error) {
      switch (error.code) {
        case error.PERMISSION_DENIED:
          alert("User denied the request for Geolocation.");
          break;
        case error.POSITION_UNAVAILABLE:
          alert("Location information is unavailable.");
          break;
        case error.TIMEOUT:
          alert("The request to get user location timed out.");
          break;
        case error.UNKNOWN_ERROR:
          alert("An unknown error occurred.");
          break;
      }
    }

    function initMap() {

      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 13,
        center: {
          lat: -17.7808295,
          lng: 31.0740816
        }
      });

      var waypts = []; //origin to destination via waypoints
      //waypts.push({location: 'indore', stopover: true});

      function continuouslyUpdatePosition(location) { //Take current location from location  and set marker to that location
        var inputVal = document.getElementById("myInput").value;
        // Displaying the value
        // alert(inputVal);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            var pos = JSON.parse(this.responseText);
            location.setPosition(new google.maps.LatLng(pos.lat, pos.lng));
            setTimeout(function() {
              continuouslyUpdatePosition(location);
            }, 5000);
          }
        };
        // xhttp.open("GET", "location ?tripId=1", true);

        xhttp.open("GET", "location ?tripId=" + inputVal, true);
        xhttp.send();
      }

      /* Distance between p1 & p2
      var p1 = new google.maps.LatLng(45.463688, 9.18814);
      var p2 = new google.maps.LatLng(46.0438317, 9.75936230000002);
      alert(calcDistance(p1,p2)+" Kilimeters");
      */

      //Make marker at any position in form of {lat,lng}
      function makeMarker(position /*, icon*/ ) {
        var marker = new google.maps.Marker({
          position: position,
          map: map,
          /*animation: google.maps.Animation.DROP,*/
          /*icon: icon,*/
        });
        return marker;
      }

      var icons = {
        end: new google.maps.MarkerImage('https://icons.iconarchive.com/icons/icons-land/vista-map-markers/32/Map-Marker-Push-Pin-1-Left-Pink-icon.png'),
        start: new google.maps.MarkerImage('https://icons.iconarchive.com/icons/icons-land/vista-map-markers/32/Map-Marker-Push-Pin-1-Left-Chartreuse-icon.png')
      };

      //Show suggestions for places, requires libraries=places in the google maps api script link
      var autocomplete1 = new google.maps.places.Autocomplete(document.getElementById('start1'));
      var autocomplete2 = new google.maps.places.Autocomplete(document.getElementById('end1'));

      var directionsService1 = new google.maps.DirectionsService;
      var directionsDisplay1 = new google.maps.DirectionsRenderer({
        polylineOptions: {
          strokeColor: "red"
        }, //path color
        //draggable: true,// change start, waypoints and destination by dragging
        /* Start and end marker with same image
        markerOptions : {icon: 'https://icons.iconarchive.com/icons/icons-land/vista-map-markers/32/Map-Marker-Push-Pin-1-Left-Pink-icon.png'},
        */
        //suppressMarkers: true
      });
      directionsDisplay1.setMap(map);
      var onChangeHandler1 = function() {
        calculateAndDisplayRoute(directionsService1, directionsDisplay1, $('#start1'), $('#end1'));
      };
      $('#start1,#end1').change(onChangeHandler1);

      function calculateAndDisplayRoute(directionsService, directionsDisplay, start, end) {
        directionsService.route({
          origin: start.val(),
          destination: end.val(),
          waypoints: waypts,
          travelMode: 'DRIVING'
        }, function(response, status) {
          if (status === 'OK') {
            directionsDisplay.setDirections(response);
            var leg = response.routes[0].legs[0];
            // Move marker along path from A to B
            var markers = [];
            for (var i = 0; i < leg.steps.length; i++) {
              var marker = makeMarker(leg.steps[i].start_location);
              markers.push(marker);
              marker.setMap(null);
            }
            tracePath(markers, 0);

            var location = makeMarker(leg.steps[0].start_location);
            continuouslyUpdatePosition(location);

          } else {
            window.alert('Directions request failed due to ' + status);
          }
        });
      }

      function tracePath(markers, index) { // move marker along path from A to B
        if (index == markers.length) return;
        markers[index].setMap(map);
        setTimeout(function() {
          markers[index].setMap(null);
          tracePath(markers, index + 1);
        }, 500);
      }

      calculateAndDisplayRoute(directionsService1, directionsDisplay1, $('#start1'), $('#end1'));
    }
});
   
  </script>
    <script type="text/javascript">
    // keep values for cargo details
    document.getElementById("description").value = getSavedValue("description"); // set the value to this input
    document.getElementById("typeOfGoods").value = getSavedValue("typeOfGoods"); // set the value to this input
    document.getElementById("goodsValue").value = getSavedValue("goodsValue"); // set the value to this input

    document.getElementById("weight").value = getSavedValue("weight"); // set the value to this input
    document.getElementById("length").value = getSavedValue("length"); // set the value to this input

    document.getElementById("width").value = getSavedValue("width"); // set the value to this input
    document.getElementById("goodsInTransit").value = getSavedValue("goodsInTransit"); // set the value to this input

    document.getElementById("height").value = getSavedValue("height"); // set the value to this input
    document.getElementById("manifestUpload").value = getSavedValue("manifestUpload"); // set the value to this input
    document.getElementById("wayBills").value = getSavedValue("wayBills"); // set the value to this input

    // Keep values for delivery details

    document.getElementById("datetimepicker3").value = getSavedValue("datetimepicker3"); // set the value to this input
    document.getElementById("datetimepicker4").value = getSavedValue("datetimepicker4"); // set the value to this input

    document.getElementById("vehicleTypeId").value = getSavedValue("vehicleTypeId"); // set the value to this input
    document.getElementById("servicePillarId").value = getSavedValue("servicePillarId"); // set the value to this input



    //keep values for job details

    document.getElementById("loadingRequirements").value = getSavedValue("loadingRequirements"); // set the value to this inpu
    document.getElementById("offloadingRequirements").value = getSavedValue("offloadingRequirements"); // set the value to this input
    document.getElementById("datetimepicker1").value = getSavedValue("datetimepicker1"); // set the value to this input
    document.getElementById("datetimepicker2").value = getSavedValue("datetimepicker2"); // set the value to this input
    document.getElementById("paymentMode").value = getSavedValue("paymentMode"); // set the value to this input
    document.getElementById("currencyld").value = getSavedValue("currencyld"); // set the value to this input
    document.getElementById("amountOffer").value = getSavedValue("amountOffer"); // set the value to this input


    //Save the value function - save it to localStorage as (ID, VALUE)
    function saveValue(e) {
      var id = e.id; // get the sender's id to save it . 
      var val = e.value; // get the value. 
      localStorage.setItem(id, val); // Every time user writing something, the localStorage's value will override . 
    }

    //get the saved value function - return the value of "v" from localStorage. 
    function getSavedValue(v) {
      if (!localStorage.getItem(v)) {
        return ""; // You can change this to your defualt value. 
      }
      return localStorage.getItem(v);
    }
  </script>
  

<script>
$(function() {

    //click event for first button
     //$("#form1").validate();
    $('#search').click(function() {
        $("#form1").valid(); //validate form 1
    });
});
</script>

<script>
//Ecocash Payment (Assigned)
function ecoAPay(jobld){

  $.ajax({
			type: "POST",
			url: "../controller/bid_process",     
			data: $('form#ecoPayment'+jobld).serialize(),
      cache: false,
			success: function(response){
        var json = $.parseJSON(response);
        // console.log(json.Action);
        if(json.Action == 1) {
         $('.ecoResponseAE').empty(); //clear apend
         $('.ecoResponseAE').append("Ecocash payment was initiated successfully");
             $(".modal").on("hidden.bs.modal", function() {
              $(".ecoResponseAE").html("");
            });
          setTimeout(function(){ window.location="jobs"; },2000);
     
        }else {
          $('.ecoResponseAE').empty();
          $('.ecoResponseAE').append(json.Message);
               $(".modal").on("hidden.bs.modal", function() {
              $(".ecoResponseAE").html("");
            });
        }
        
			},
      error: function(jqXHR, textStatus, errorThrown) {
          //  console.log(jqXHR, textStatus, errorThrown);
                $('.ecoResponseAE').empty();
                $('.ecoResponseAE').append(errorThrown);
        }
		});
}
</script>

<script>
//Internal Transfer Payment (Assigned)
function inAPay(jobld){

  // console.log(jobld);
  //  return;
  $.ajax({
			type: "POST",
			url: "../controller/bid_process",     
			data: $('form#inPayment'+jobld).serialize(),
      cache: false,
			success: function(response){
        var json = $.parseJSON(response);
        // console.log(json.Action);
        if(json.Action == 1) {
         $('.inResponseAE').empty(); //clear apend
         $('.inResponseAE').append("Internal Transfer payment was successfully");
             $(".modal").on("hidden.bs.modal", function() {
              $(".inResponseAE").html("");
            });
          setTimeout(function(){ window.location="jobs"; },2000);
     
        }else {
          $('.inResponseAE').empty();
          $('.inResponseAE').append(json.Message);
               $(".modal").on("hidden.bs.modal", function() {
              $(".inResponseAE").html("");
            });
        }
        
			},
      error: function(jqXHR, textStatus, errorThrown) {
          //  console.log(jqXHR, textStatus, errorThrown);
                $('.inResponseAE').empty();
                $('.inResponseAE').append(errorThrown);
        }
		});
}
</script>


<script>
//RTGS Payment (Assigned)
function rtAPay(jobld){

  $.ajax({
			type: "POST",
			url: "../controller/bid_process",     
			data: $('form#rtPayment'+jobld).serialize(),
      cache: false,
			success: function(response){
        var json = $.parseJSON(response);
        // console.log(json.Action);
        if(json.Action == 1) {
         $('.rtResponseAE').empty(); //clear apend
         $('.rtResponseAE').append("RTGS payment was successfully");
             $(".modal").on("hidden.bs.modal", function() {
              $(".rtResponseAE").html("");
            });
          setTimeout(function(){ window.location="jobs"; },2000);
     
        }else {
          $('.rtResponseAE').empty();
          $('.rtResponseAE').append(json.Message);
               $(".modal").on("hidden.bs.modal", function() {
              $(".rtResponseAE").html("");
            });
        }
        
			},
      error: function(jqXHR, textStatus, errorThrown) {
          //  console.log(jqXHR, textStatus, errorThrown);
                $('.rtResponseAE').empty();
                $('.rtResponseAE').append(errorThrown);
        }
		});
}
</script>

<script>
  //Cash Payment (Assigned)
function cashAPay(){
  $.ajax({
			type: "POST",
			url: "../controller/bid_process",     
			data: $('form#cashPayment').serialize(),
      cache: false,
			success: function(response){
        var json = $.parseJSON(response);
        // console.log(json.Action);
        if(json.Action == 1) {
        $('.cashResponseAC').empty(); //clear apend
         $('.cashResponseAC').append("Cash payment was initiated successfully");
              $(".modal").on("hidden.bs.modal", function() {
              $(".cashResponseAC").html("");
            });
          setTimeout(function(){ window.location="jobs"; },2000);
        }else {
          $('.cashResponseAC').empty();
          $('.cashResponseAC').append(json.Message);
                $(".modal").on("hidden.bs.modal", function() {
              $(".cashResponseAC").html("");
            });

        }
        
			},
      error: function(jqXHR, textStatus, errorThrown) {
          //  console.log(jqXHR, textStatus, errorThrown);
          $('.cashResponseAC').empty();
          $('.cashResponseAC').append(errorThrown);
                $(".modal").on("hidden.bs.modal", function() {
              $(".cashResponseAC").html("");
            });
        }
		});
}
</script>

<script>
//Ecocash Payment (Collected)
function ecoCPay(jobld){

  $.ajax({
			type: "POST",
			url: "../controller/bid_process",     
			data: $('form#ecoCPayment'+jobld).serialize(),
      cache: false,
			success: function(response){
        var json = $.parseJSON(response);
        // console.log(json.Action);
        if(json.Action == 1) {
         $('.ecoResponseCE').empty(); //clear apend
         $('.ecoResponseCE').append("Ecocash payment was initiated successfully");
             $(".modal").on("hidden.bs.modal", function() {
              $(".ecoResponseCE").html("");
            });
          setTimeout(function(){ window.location="jobs"; },2000);
     
        }else {
          $('.ecoResponseCE').empty();
          $('.ecoResponseCE').append(json.Message);
               $(".modal").on("hidden.bs.modal", function() {
              $(".ecoResponseCE").html("");
            });
        }
        
			},
      error: function(jqXHR, textStatus, errorThrown) {
          //  console.log(jqXHR, textStatus, errorThrown);
                $('.ecoResponseCE').empty();
                $('.ecoResponseCE').append(errorThrown);
        }
		});
}
</script>

<script>
//Internal Transfer Payment (Collected)
function inCPay(jobld){

  $.ajax({
			type: "POST",
			url: "../controller/bid_process",     
			data: $('form#inCPayment'+jobld).serialize(),
      cache: false,
			success: function(response){
        var json = $.parseJSON(response);
        // console.log(json.Action);
        if(json.Action == 1) {
         $('.inResponseCE').empty(); //clear apend
         $('.inResponseCE').append("Internal Transfer payment was successfully");
             $(".modal").on("hidden.bs.modal", function() {
              $(".inResponseCE").html("");
            });
          setTimeout(function(){ window.location="jobs"; },2000);
     
        }else {
          $('.inResponseCE').empty();
          $('.inResponseCE').append(json.Message);
               $(".modal").on("hidden.bs.modal", function() {
              $(".inResponseCE").html("");
            });
        }
        
			},
      error: function(jqXHR, textStatus, errorThrown) {
          //  console.log(jqXHR, textStatus, errorThrown);
                $('.inResponseCE').empty();
                $('.inResponseCE').append(errorThrown);
        }
		});
}
</script>


<script>
//RTGS Payment (Collected)
function rtCPay(jobld){

  $.ajax({
			type: "POST",
			url: "../controller/bid_process",     
			data: $('form#rtCPayment'+jobld).serialize(),
      cache: false,
			success: function(response){
        var json = $.parseJSON(response);
        // console.log(json.Action);
        if(json.Action == 1) {
         $('.rtResponseCE').empty(); //clear apend
         $('.rtResponseCE').append("RTGS payment was successfully");
             $(".modal").on("hidden.bs.modal", function() {
              $(".rtResponseCE").html("");
            });
          setTimeout(function(){ window.location="jobs"; },2000);
     
        }else {
          $('.rtResponseCE').empty();
          $('.rtResponseCE').append(json.Message);
               $(".modal").on("hidden.bs.modal", function() {
              $(".rtResponseCE").html("");
            });
        }
        
			},
      error: function(jqXHR, textStatus, errorThrown) {
          //  console.log(jqXHR, textStatus, errorThrown);
                $('.rtResponseCE').empty();
                $('.rtResponseCE').append(errorThrown);
        }
		});
}
</script>

<script>
  //Cash Payment (Collected)
function cashCPay(){
  $.ajax({
			type: "POST",
			url: "../controller/bid_process",     
			data: $('form#cashPayment').serialize(),
      cache: false,
			success: function(response){
        var json = $.parseJSON(response);
        // console.log(json.Action);
        if(json.Action == 1) {
        $('.cashResponseAC').empty(); //clear apend
         $('.cashResponseAC').append("Cash payment was initiated successfully");
                $(".modal").on("hidden.bs.modal", function() {
              $(".cashResponseAC").html("");
            });
          setTimeout(function(){ window.location="jobs"; },2000);
        }else {
          $('.cashResponseAC').empty();
          $('.cashResponseAC').append(json.Message);
               $(".modal").on("hidden.bs.modal", function() {
              $(".cashResponseAC").html("");
            });

        }
        
			},
      error: function(jqXHR, textStatus, errorThrown) {
          //  console.log(jqXHR, textStatus, errorThrown);
          $('.cashResponseAC').empty();
          $('.cashResponseAC').append(errorThrown);
               $(".modal").on("hidden.bs.modal", function() {
              $(".cashResponseAC").html("");
            });
        }
		});
}
</script>

<script>
  //rating Driver
function ratingDriver(){
  $.ajax({
			type: "POST",
			url: "../controller/bid_process",     
			data: $('form#ratingdriver').serialize(),
      cache: false,
			success: function(response){
        var json = $.parseJSON(response);
        // console.log(json.Action);
        if(json.Action == 1) {
        $('.ratingDriver').empty(); //clear apend
         $('.ratingDriver').append("Driver rated successfully");
              $(".modal").on("hidden.bs.modal", function() {
              $(".ratingDriver").html("");
            });
          setTimeout(function(){ window.location="jobs "; },2000);
        }else {
          $('.ratingDriver').empty();
          $('.ratingDriver').append(json.Message);
               $(".modal").on("hidden.bs.modal", function() {
              $(".ratingDriver").html("");
            });

        }
        
			},
      error: function(jqXHR, textStatus, errorThrown) {
          //  console.log(jqXHR, textStatus, errorThrown);
          $('.ratingDriver').empty();
          $('.ratingDriver').append(errorThrown);
               $(".modal").on("hidden.bs.modal", function() {
              $(".ratingDriver").html("");
            });
        }
		});
}
</script>

<!-- <script>
document.addEventListener('keydown', function() {
  if (event.keyCode == 123) {
    alert("This function has been disabled!");
    return false;
  } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) {
    alert("This function has been disabled!");
    return false;
  } else if (event.ctrlKey && event.keyCode == 85) {
    alert("This function has been disabled!");
    return false;
  }
}, false);

if (document.addEventListener) {
  document.addEventListener('contextmenu', function(e) {
    alert("This function has been disabled!");
    e.preventDefault();
  }, false);
} else {
  document.attachEvent('oncontextmenu', function() {
    alert("This function has been disabled!");
    window.event.returnValue = false;
  });
}
</script> -->

<script>

  function setCapacities(category) {
    const base_url = curl_url+VCategoryName;
    let capacities = [];
    let selector =  document.getElementById("capacity");
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        const capacities = JSON.parse(this.responseText);
        console.log(capacities);
        for (let i = 0; i < capacities.length; i++) {
          let option = document.createElement("option");
          option.value = capacities[i].capacity;
          option.innerHTML = capacities[i].capacity;
          selector.appendChild(option);
        }
      }
    };
    xhr.open("GET", base_url, true);
    xhr.send();

  }

  function getCapacity() {
    
      var category_name =  document.getElementById("category").value;
      var filteredJsonVehicles = <?php echo $filteredJsonVehicles; ?>;
      
      var newArray = filteredJsonVehicles.result.filter(function (el){
        return el.name ==category_name;
      });

      var flags = [], output = [], l = newArray.length, i;
      for( i=0; i<l; i++) {
          if( flags[newArray[i].capacity]) continue;
          flags[newArray[i].capacity] = true;
          output.push(newArray[i]);
      }

      var sel = document.querySelector('#capacity');
      $("#capacity").empty();
      output.forEach(element => {
        var capacityvalue  = element['capacity'];
        var vehicleTypeId  = element['vehicleTypeId'];
        
        $('#capacity').append('<option value="'+ vehicleTypeId +'">'+ capacityvalue +'</option>');
        // document.getElementById("myInputID").value = vehicleTypeId;
      });

  
  }
</script>




</body>

</html>