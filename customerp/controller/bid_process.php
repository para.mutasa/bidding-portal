<?php
include_once('../../utils/VayaBiddingUtility.php');

function getPartner($partnerId)
{
  $jsonData = array(
    'type' => "getPartner",
    'partnerId' => $partnerId
  );
  return callVayaBiddingWebApiPost($jsonData);
}

function getCustomer($iuserId)
{
  $jsonData = array(
    'type' => "getCustomer",
    'customerId' => $iuserId
  );
  return callVayaBiddingWebApiPost($jsonData);
}
function getAllVehicleTypes()
{
  $jsonData = array(
    'type' => "getAllVehicleTypes"
  );
  return callVayaBiddingWebApiPost($jsonData);
}
function getAllServicePillars()
{
  $jsonData = array(
    'type' => "getAllServicePillar"
  );
  return callVayaBiddingWebApiPost($jsonData);
}

function getAllCurrencies()
{
  $jsonData = array(
    'type' => "getAllCurrencies"
  );
  return callVayaBiddingWebApiPost($jsonData);
}


function getCurrencyById($currencyId)
{
  $jsonData = array(
    'type' => "getCurrencyById",
    'currencyId' => $currencyId
  );
  return callVayaBiddingWebApiPost($jsonData);
}


function getVehicleType($vehicleTypeId)
{
  $jsonData = array(
    'type' => "getVehicleType",
    'vehicleTypeId' => $vehicleTypeId
  );
  return callVayaBiddingWebApiPost($jsonData);
}

function getServicePillar($servicePillarId)
{
  $jsonData = array(
    'type' => "getServicePillar",
    'servicePillarId' => $servicePillarId
  );
  return callVayaBiddingWebApiPost($jsonData);
}


function listAllPendingJobsByIUserId($iuserId)
{
  $jsonData = array(
    'type' => "getAllJobsByIUserIdAndJobStatus",
    'iUserId' => $iuserId,
    'page' => 0,
    'size' => 50,
    "jobStatus" => "Pending"
  );
  return callVayaBiddingWebApiPost($jsonData);
}

function listAllAssignedJobsByIUserId($iuserId)
{
  $jsonData = array(
    'type' => "getAllJobsByIUserIdAndJobStatus",
    'iUserId' => $iuserId,
    'page' => 0,
    'size' => 2000,
    "jobStatus" => "Assigned"
  );
  return callVayaBiddingWebApiPost($jsonData);
}

function listAllDeliveredJobsByIUserId($iuserId)
{
  $jsonData = array(
    'type' => "getAllJobsByIUserIdAndJobStatus",
    'iUserId' => $iuserId,
    'page' => 0,
    'size' => 2000,
    "jobStatus" => "Delivered"
  );
  return callVayaBiddingWebApiPost($jsonData);
}

function listAllCancelledJobsByIUserId($iuserId)
{
  $jsonData = array(
    'type' => "getAllJobsByIUserIdAndJobStatus",
    'iUserId' => $iuserId,
    'page' => 0,
    'size' => 2000,
    "jobStatus" => "Cancelled"
  );
  return callVayaBiddingWebApiPost($jsonData);
}

function listAllCollectedJobsByIUserId($iuserId)
{
  $jsonData = array(
    'type' => "getAllJobsByIUserIdAndJobStatus",
    'iUserId' => $iuserId,
    'page' => 0,
    'size' => 2000,
    "jobStatus" => "Collected"
  );
  return callVayaBiddingWebApiPost($jsonData);
}

function listAllInTransitJobsByIUserId($iuserId)
{
  $jsonData = array(
    'type' => "getAllJobsByIUserIdAndJobStatus",
    'iUserId' => $iuserId,
    'page' => 0,
    'size' => 2000,
    "jobStatus" => "InTransit"
  );
  return callVayaBiddingWebApiPost($jsonData);
}


function customerGetAllBidsByJobId($jobId)
{
  $jsonData = array(
    'type' => "customerGetAllBidsByJobId",
    'jobId' => $jobId
  );
  return callVayaBiddingWebApiPost($jsonData);
}


// createCargoDetails

if (isset($_POST['jobpost'])) {

  // receive all input values from the form

  $description   = $_POST['description'];
  $weight  = $_POST['weight'] . '' . $_POST['weight_unit'];
  $typeOfGoods  = $_POST['typeOfGoods'];
  $goodsValue  = $_POST['goodsValue'];
  $iUserId  = $_POST['iUserId'];
  $dimensions  = $_POST['length'] . '-' . $_POST['width'] . '-' . $_POST['height'] . '' . $_POST['dimension_unit'];
  $type = 'createCargoDetails';
  //manifestUpload
  $manifestUpload_name = $_FILES['manifestUpload']['name'];
  // $manifestUpload__size = $_FILES['manifestUpload']['size'];
  // $manifestUpload__tmp = $_FILES['manifestUpload']['tmp_name'];
  // $manifestUpload__type = $_FILES['manifestUpload']['type'];
  // $manifestUpload_exploded = explode('.', $_FILES['manifestUpload']['name']);
  // $manifestUpload_ext = strtolower(end($manifestUpload_exploded));



  // wayBills

  $wayBills_name = $_FILES['wayBills']['name'];
  // $wayBills_size = $_FILES['wayBills']['size'];
  // $wayBills_tmp = $_FILES['wayBills']['tmp_name'];
  // $wayBills_type = $_FILES['wayBills']['type'];
  // $wayBills_exploded = explode('.', $_FILES['wayBills']['name']);
  // $wayBills_ext = strtolower(end($wayBills_exploded));

  // var_dump($description);
  // var_dump($weight);
  // var_dump($typeOfGoods);
  // var_dump($goodsValue);
  // var_dump($manifestUpload_name);
  // var_dump($wayBills_name);
  // var_dump($dimensions);
  // exit;

  $jsonData = array(
    'type'  => $type,
    'description'  => $description,
    'weight'  => $weight,
    'typeOfGoods'  => $typeOfGoods,
    'goodsValue'  => $goodsValue,
    'manifestUpload'  => $manifestUpload_name,
    'wayBills'  => $wayBills_name,
    'dimensions'  => $dimensions,
    'iUserId'  => $iUserId
  );

  $cargo_result = callVayaBiddingWebApiPost($jsonData);

  // var_dump($cargo_result);
  // exit;


  // move_uploaded_file($manifestUpload__tmp,"../../uploads/".$manifestUpload_name);
  // move_uploaded_file($wayBills_tmp,"../../uploads/".$wayBills_name);

  $cargoId_c = $cargo_result['result']['cargoId'];

  $collectionTime   = $_POST['collectionTime'];
  $collection_time  = date('Y-m-d\Th:i', strtotime($collectionTime));
  $deliveryTime  = $_POST['deliveryTime'];
  $delivery_time  = date('Y-m-d\Th:i', strtotime($deliveryTime));
  $currencyId  = $_POST['currencyld'];

  $loadingRequirements   = $_POST['loadingRequirements'];
  $offloadingRequirements   = $_POST['offloadingRequirements'];
  $bidStartTime  = $_POST['bidStartTime'];

  $bidstart  = date('Y-m-d\Th:i', strtotime($bidStartTime));
  $bidEndTime  = $_POST['bidEndTime'];
  $bidend  = date('Y-m-d\Th:i', strtotime($bidEndTime));

  $amountOffer   = $_POST['amountOffer'];
  $customerType  = "Individual";
  $customerName  = $_POST['customerName'];

  $pickupAddress   = $_POST['pickupAddress'];
  $pickupLatitude  = $_POST['pickupLatitude'];
  $pickupLongitude   = $_POST['pickupLongitude'];

  $dropOffAddress  = $_POST['dropOffAddress'];
  $dropOffLatitude   = $_POST['dropOffLatitude'];
  $dropOffLongitude  = $_POST['dropOffLongitude'];

  $vehicleTypeId  = $_POST['vehicleTypeId'];
  $servicePillarId   = $_POST['servicePillarId'];
  $paymentMode   = $_POST['paymentMode'];
  $goodsInTransit   = $_POST['goodsInTransit'];


  $job_type = 'createJob';

  // var_dump($cargoId_c);
  // var_dump($collection_time);
  // var_dump($delivery_time);

  // var_dump($loadingRequirements);
  // var_dump($offloadingRequirements);
  // var_dump($bidstart);
  // var_dump($bidend);
  // var_dump($amountOffer);
  // var_dump($customerType);
  // var_dump($customerName);



  // var_dump($pickupAddress);
  // var_dump($pickupLatitude);
  // var_dump($pickupLongitude);

  // var_dump($dropOffAddress);
  // var_dump($dropOffLatitude);
  // var_dump($dropOffLongitude);


  // var_dump($vehicleTypeId);
  // var_dump($servicePillarId);
  // var_dump($currencyId);
  //  var_dump($goodsInTransit);
  // var_dump($paymentMode);


  // exit;



  $jsonData = array(
    'type' => $job_type,
    'iUserId' => $iUserId,
    'collectionTime' => $collection_time,
    'deliveryTime' => $delivery_time,
    'cargoId' => $cargoId_c,
    'loadingRequirements' => $loadingRequirements,
    'offloadingRequirements' => $offloadingRequirements,
    'bidStartTime' => $bidstart,
    'bidEndTime' => $bidend,
    'servicePillarId' => $servicePillarId,
    'amountOffer' => $amountOffer,
    'customerType' => $customerType,
    'customerName' => $customerName,
    'dropOffAddress' => $dropOffAddress,
    'pickupAddress' => $pickupAddress,
    'dropOffLatitude' => $dropOffLatitude,
    'pickupLatitude' => $pickupLatitude,
    'dropOffLongitude' => $dropOffLongitude,
    'pickupLongitude' => $pickupLongitude,
    'vehicleTypeId' => $vehicleTypeId,
    'currencyId' => $currencyId,
    'goodsInTransit' => $goodsInTransit,
    'paymentMode' => $paymentMode
  );

  // var_dump($jsonData);
  // exit;


  $job_result = callVayaBiddingWebApiPost($jsonData);

  echo json_encode($job_result);
  exit;
}



// Assign Partner

if (isset($_POST['assignPartner'])) {

  // receive all input values from the form
  $bidId   = $_POST['bidId'];
  $partnerId  = $_POST['partnerId'];
  $jobId  = $_POST['jobId'];
  $type = 'assignDriver';

  // var_dump($bidId);
  // exit;

  $jsonData = array(
    'type'  => $type,
    'bidId'  => $bidId,
    'iDriverId'  => $partnerId
  );

  $assign_partner_result = callVayaBiddingWebApiPost($jsonData);

  echo json_encode($assign_partner_result);
  exit;
}



// Query

if (isset($_POST['query'])) {

  // receive all input values from the form

  $name  = $_POST['name'];
  $email  = $_POST['email'];
  $message  = $_POST['message'];
  $type = 'createQuery';


  // var_dump($name);
  // var_dump($email);
  // var_dump($message);
  // exit;


  $jsonData = array(
    'type'  => $type,
    'name'  => $name,
    'email'  => $email,
    'message'  => $message,
  );

  $result = callVayaBiddingWebApiPost($jsonData);

  // var_dump($result);
  // exit;


  if ($result['Action'] == 1) {
    echo "<script>";
    echo "alert('Message has been sent sucessfully');";
    echo "window.location='../../contact ';";
    echo "</script>";
  } else {
    echo "<script>";
    echo "alert('" . $result['Message'] . "');";
    // echo "alert('Please try again later');";
    echo "window.location='../../contact ';";
    echo "</script>";
  }
}

//createPayment  (bids)

if (isset($_POST['createPayment'])) {

  // receive all input values from the form
  $amount  = $_POST['amount'];
  $msisdn  = $_POST['msisdn'];
  $jobId  = $_POST['jobId'];
  $payType  = "Cash";
  $type = 'createPayment';


  // var_dump($amount);
  // var_dump($msisdn);
  // var_dump($jobId);

  // exit;

  $jsonData = array(
    'type'  => $type,
    'amount'  => $amount,
    'msisdn'  => $msisdn,
    'jobId'  => $jobId,
    'payType'  => $payType
  );

  $create_payment_result = callVayaBiddingWebApiPost($jsonData);

  echo json_encode($create_payment_result);
  exit;
}


//createPayment  (collected & Assigned)

if (isset($_POST['cashPayment'])) {

  // receive all input values from the form
  $amount  = $_POST['amount'];
  $msisdn  = $_POST['msisdn'];
  $jobId  = $_POST['jobId'];
  $payType  = "Cash";
  $type = 'createPayment';


  // var_dump($amount);
  // var_dump($msisdn);
  // var_dump($jobId);

  // exit;

  $jsonData = array(
    'type'  => $type,
    'amount'  => $amount,
    'msisdn'  => $msisdn,
    'jobId'  => $jobId,
    'payType'  => $payType
  );

  $cash_result = callVayaBiddingWebApiPost($jsonData);

  echo json_encode($cash_result);
  exit;
}


//makePayment (bids)

if (isset($_POST['makePayment'])) {

  // receive all input values from the form
  $amount  = $_POST['amount'];
  $msisdn  = $_POST['msisdn'];
  $jobId  = $_POST['jobId'];
  $payType  = "Ecocash";
  $type = 'makePayment';


  // var_dump($amount);
  // var_dump($msisdn);
  // var_dump($jobId);

  // exit;


  $jsonData = array(
    'type'  => $type,
    'amount'  => $amount,
    'msisdn'  => $msisdn,
    'jobId'  => $jobId,
    'payType'  => $payType
  );

  $make_payment_result = callVayaBiddingWebApiPost($jsonData);

  echo json_encode($make_payment_result);
  exit;
}

//makePayment  (assigned & collected)


if (isset($_POST['ecoPayment'])) {

  ob_clean(); # discard any previous buffer data
  // receive all input values from the form
  $amount  = $_POST['amount'];
  $msisdn  = $_POST['msisdn'];
  $jobId  = $_POST['jobId'];
  $payType  = "Ecocash";
  $type = 'makePayment';


  // var_dump($amount);
  // var_dump($msisdn);
  // var_dump($jobId);

  // exit;



  $jsonData = array(
    'type'  => $type,
    'amount'  => $amount,
    'msisdn'  => $msisdn,
    'jobId'  => $jobId,
    'payType'  => $payType
  );

  $ecocash_result = callVayaBiddingWebApiPost($jsonData);

  echo json_encode($ecocash_result);
  exit;
}

//updatePaymentStatus

if (isset($_POST['updatePaymentStatus'])) {

  // receive all input values from the form
  $jobId  = $_POST['jobId'];
  $paymentStatus  = "Paid";
  $type = 'updatePaymentStatus';


  // var_dump($amount);
  // var_dump($msisdn);
  // var_dump($jobId);

  // exit;


  $jsonData = array(
    'type'  => $type,
    'jobId'  => $jobId,
    'paymentStatus'  => $paymentStatus
  );

  $result = callVayaBiddingWebApiPost($jsonData);

  // var_dump($result);

  // exit;


  if ($result['Action'] == 1) {
    echo "<script>";
    echo "alert('Cash payment made sucessfully');";
    echo "window.location='../view/jobs ';";
    echo "</script>";
  } else {
    echo "<script>";
    echo "alert('" . $result['Message'] . "');";
    // echo "alert('Please try again later');";
    echo "window.location='../view/jobs ';";
    echo "</script>";
  }
}


// cancelAnOffer 

if (isset($_POST['cancelAnOffer'])) {

  // receive all input values from the form
  $bidId   = $_GET['bidId'];
  $jobId  = $_POST['jobId'];
  $rejectReason  = $_POST['rejectReason'];
  $type = 'cancelAnOffer';

  // var_dump($bidId);
  // var_dump($rejectReason);
  // exit;


  $jsonData = array(
    'type'  => $type,
    'bidId'  => $bidId,
    'rejectReason'  => $rejectReason
  );

  $result = callVayaBiddingWebApiPost($jsonData);

  // var_dump($result);
  // exit;


  if ($result['Action'] == 1) {
    echo "<script>";
    echo "alert('Counter offer Successfully Cancelled');";
    echo "window.location='../view/bids ?jobId=$jobId';";
    echo "</script>";
  } else {
    echo "<script>";
    echo "alert('" . $result['Message'] . "');";
    // echo "alert('Please try again later');";
    echo "window.location='../view/bids ?jobId=$jobId';";
    echo "</script>";
  }
}

// cancel Job

if (isset($_POST['cancelJob'])) {

  // receive all input values from the form
  $jobId  = $_POST['jobId'];
  $customerId  = $_POST['customerId'];
  $type = 'cancelJob';

  // var_dump($jobId);
  // var_dump($customerId);
  // exit;


  $jsonData = array(
    'type'  => $type,
    'jobId'  => $jobId,
    'customerId'  => $customerId
  );

  $cancelJob = callVayaBiddingWebApiPost($jsonData);

  echo json_encode($cancelJob);
  exit;
}



// ratingDriver

if (isset($_POST['createRating'])) {

  // receive all input values from the form
  $userId  = $_POST['idriverId'];
  $comment  = $_POST['comment'];
  $scale  = $_POST['scale'];
  // $scale  = 4;
  $tripId  = $_POST['tripId'];
  $userType  = "Customer";
  $type = 'createRating';

  // var_dump($userId);
  // var_dump($comment);
  // var_dump($scale);
  // var_dump($tripId);
  // exit;


  $jsonData = array(
    'type'  => $type,
    'userId'  => $userId,
    'comment'  => $comment,
    'scale'  => $scale,
    'tripId'  => $tripId,
    'userType'  => $userType
  );

  $rating_driver_result = callVayaBiddingWebApiPost($jsonData);

  echo json_encode($rating_driver_result);
  exit;
}



// customerRegistration
if (isset($_POST['customerRegistration'])) {

  // receive all input values from the form
  $vFirstName  = $_POST['vFirstName'];
  $vLastName  = $_POST['vLastName'];
  $vEmail  = $_POST['vEmail'];
  $vPhone  = $_POST['vPhone'];
  $vPassword  =  $_POST['vPassword'];
  $userType  = "Customer";
  $vInviteCode  = $_POST['vInviteCode'];
  $type = 'customerRegistration';

  // var_dump($jobId);
  // var_dump($customerId);
  // exit;


  $jsonData = array(
    'type'  => $type,
    'vFirstName'  => $vFirstName,
    'vLastName'  => $vLastName,
    'vEmail'  => $vEmail,
    'vPhone'  => $vPhone,
    'vPassword'  => $vPassword,
    'userType'  => $userType,
    'vInviteCode'  => $vInviteCode
  );

  $customerRegistration_result = callVayaBiddingWebApiPost($jsonData);

  echo json_encode($customerRegistration_result);
  exit;
}


// customerRegistration

if (isset($_POST['companyRegistration'])) {

  // receive all input values from the form
  $name  = $_POST['name'];
  $vCaddress  = $_POST['vCaddress'];
  $zimraBPNumber  = $_POST['zimraBPNumber'];
  $vEmail  = $_POST['vEmail'];
  $vPhone  = $_POST['vPhone'];
  $vPassword  =  $_POST['vPassword'];
  $countryCode  = $_POST['countryCode'];
  $phoneCode  = $_POST['phoneCode'];
  $type = 'companyRegistration';
  $userType  = "Customer";

  // var_dump($jobId);
  // var_dump($customerId);
  // exit;

  $jsonData = array(
    'type'  => $type,
    'name'  => $name,
    'vCaddress'  => $vCaddress,
    'vEmail'  => $vEmail,
    'vPhone'  => $vPhone,
    'vPassword'  => $vPassword,
    'phoneCode'  => $phoneCode,
    'countryCode'  => $countryCode,
    'zimraBPNumber'  => $zimraBPNumber,
    'userType'  => $userType
  );

  $companyRegistration_result = callVayaBiddingWebApiPost($jsonData);

  echo json_encode($companyRegistration_result);
  exit;
}


// CustomerForgotPassword

if (isset($_POST['CustomerForgotPassword'])) {

  // receive all input values from the form
  $vEmail  = $_POST['vEmail'];
  $userType  = "Customer";
  $type = 'forgotPassword';

  // var_dump($vEmail);
  // var_dump($userType);
  // exit;

  $jsonData = array(
    'type'  => $type,
    'vEmail'  => $vEmail,
    'userType'  => $userType
  );

  $CustomerForgotPassword_result = callVayaBiddingWebApiPost($jsonData);

  echo json_encode($CustomerForgotPassword_result);
  exit;
}



// resetPassword

if (isset($_POST['CustomerResetPassword'])) {

  // receive all input values from the form

  $token  = trim($_POST['token']);
  $newPassword  = trim($_POST['newPassword']);
  $userType  = "Customer";
  $type = 'resetPassword';

  // var_dump($token);
  // var_dump($newPassword);
  // var_dump($userType);
  // exit;


  $jsonData = array(
    'type'  => $type,
    'token'  => $token,
    'newPassword'  => $newPassword,
    'userType'  => $userType
  );

  $CustomerResetPassword_result = callVayaBiddingWebApiPost($jsonData);

  echo json_encode($CustomerResetPassword_result);
  exit;
}
