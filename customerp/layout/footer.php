<!-- BEGIN: Footer-->
<footer class="footer footer-static footer-light navbar-border navbar-shadow">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2021 <a class="text-bold-800 grey darken-2" href="#" target="_blank">VAYA TECHNOLOGIES</a></span><span class="float-md-right d-none d-lg-block">Hand-crafted & Made with<i class="ft-heart pink"></i><span id="scroll-top"></span></span></p>
</footer>
<!-- END: Footer-->


<!-- BEGIN: Vendor JS-->
<script src="../app-assets/vendors/js/material-vendors.min.js"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="../app-assets/vendors/js/extensions/jquery.steps.min.js"></script>
<script src="../app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
<script src="../app-assets/vendors/js/forms/icheck/icheck.min.js"></script>
<script src="../app-assets/vendors/js/extensions/datedropper.min.js"></script>
<script src="../app-assets/vendors/js/extensions/timedropper.min.js"></script>
<script src="../app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="../app-assets/vendors/js/forms/validation/jqBootstrapValidation.js"></script>


<!-- END: Page Vendor JS-->


<!-- BEGIN: Theme JS-->
<script src="../app-assets/js/core/app-menu.js"></script>
<script src="../app-assets/js/core/app.js"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<script src="../app-assets/js/scripts/pages/material-app.js"></script>
<script src="../assets/js/form_wizard.js"></script>
<script src="../app-assets/js/scripts/extensions/date-time-dropper.js"></script>
<script src="../app-assets/js/scripts/pages/hospital-patients-list.js"></script>
<script src="../app-assets/js/scripts/forms/validation/form-validation.js"></script>
<!-- END: Page JS-->

<!-- BEGIN: Custom JS-->
<script src="../assets/js/scripts.js"></script>
<!-- END: Custom JS-->



  
