<!-- ============================================================== -->

<div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

        <li class=" nav-item"><a href="#"><i class="material-icons">local_shipping</i><span class="menu-title" data-i18n="Patients">My Jobs</span></a>
            <ul class="menu-content">
                <?php
                $serverHost = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";

                // var_dump($serverHost);
                // exit;

                if ($serverHost == "http://102.133.235.48:2002") {

                    //Zim Dev Environment Left Side Menu

                    if ($_SESSION['user_type'] == "Customer") {

                        //Customer portal

                        echo '<li><a class="menu-item" href="pending_jobs.php"><i class="material-icons"></i><span>Pending</span></a></li>';
                        echo '<li><a class="menu-item" href="assigned_jobs.php"><i class="material-icons"></i><span>Assigned</span></a> </li>';
                        echo '<li><a class="menu-item" href="collected_jobs.php"><i class="material-icons"></i><span>Collected</span></a> </li>';
                        echo '<li><a class="menu-item" href="intransit_jobs.php"><i class="material-icons"></i><span>In-Transit</span></a> </li>';
                        echo ' <li><a class="menu-item" href="delivered_jobs.php"><i class="material-icons"></i><span>Delivered</span></a> </li>';
                        echo ' <li><a class="menu-item" href="cancelled_jobs.php"><i class="material-icons"></i><span>Cancelled</span></a> </li>';
                    } else if ($_SESSION['user_type'] == "Partner") {

                        //Partner  portal

                        echo '<li class="active"><a class="menu-item" href="pending_jobs.php"><i class="material-icons"></i><span>Pending</span></a></li>';
                        echo '<li><a class="menu-item" href="assigned_jobs.php"><i class="material-icons"></i><span>Assigned</span></a> </li>';
                        echo '<li><a class="menu-item" href="intransit_jobs.php"><i class="material-icons"></i><span>In-Transit</span></a> </li>';
                        echo ' <li><a class="menu-item" href="delivered_jobs.php"><i class="material-icons"></i><span>Delivered</span></a> </li>';
                        echo ' <li><a class="menu-item" href="cancelled_jobs.php"><i class="material-icons"></i><span>Cancelled</span></a> </li>';
                    } else {

                        // Driver portal

                        echo '<li class="active"><a class="menu-item" href="pending_jobs.php"><i class="material-icons"></i><span>Pending</span></a></li>';
                        echo '<li><a class="menu-item" href="assigned_jobs.php"><i class="material-icons"></i><span>Assigned</span></a> </li>';
                        echo '<li><a class="menu-item" href="intransit_jobs.php"><i class="material-icons"></i><span>In-Transit</span></a> </li>';
                        echo ' <li><a class="menu-item" href="delivered_jobs.php"><i class="material-icons"></i><span>Delivered</span></a> </li>';
                        echo ' <li><a class="menu-item" href="cancelled_jobs.php"><i class="material-icons"></i><span>Cancelled</span></a> </li>';
                    }
                } else {

                    //Zim Local Host left Side Menu

                    if ($_SESSION['user_type'] == "Customer") {

                        //Customer portal

                        echo '<li class="active"><a class="menu-item" href="pending_jobs.php"><i class="material-icons"></i><span>Pending</span></a></li>';
                        echo '<li><a class="menu-item" href="assigned_jobs.php"><i class="material-icons"></i><span>Assigned</span></a> </li>';
                        echo '<li><a class="menu-item" href="collected_jobs.php"><i class="material-icons"></i><span>Collected</span></a> </li>';
                        echo '<li><a class="menu-item" href="intransit_jobs.php"><i class="material-icons"></i><span>In-Transit</span></a> </li>';
                        echo ' <li><a class="menu-item" href="delivered_jobs.php"><i class="material-icons"></i><span>Delivered</span></a> </li>';
                        echo ' <li><a class="menu-item" href="cancelled_jobs.php"><i class="material-icons"></i><span>Cancelled</span></a> </li>';
                    } else if ($_SESSION['user_type'] == "Partner") {

                        //Partner  portal

                        echo '<li class="active"><a class="menu-item" href="pending_jobs.php"><i class="material-icons"></i><span>Pending</span></a></li>';
                        echo '<li><a class="menu-item" href="assigned_jobs.php"><i class="material-icons"></i><span>Assigned</span></a> </li>';
                        echo '<li><a class="menu-item" href="intransit_jobs.php"><i class="material-icons"></i><span>In-Transit</span></a> </li>';
                        echo ' <li><a class="menu-item" href="delivered_jobs.php"><i class="material-icons"></i><span>Delivered</span></a> </li>';
                        echo ' <li><a class="menu-item" href="cancelled_jobs.php"><i class="material-icons"></i><span>Cancelled</span></a> </li>';
                    } else {

                        // Driver portal

                        echo '<li class="active"><a class="menu-item" href="pending_jobs.php"><i class="material-icons"></i><span>Pending</span></a></li>';
                        echo '<li><a class="menu-item" href="assigned_jobs.php"><i class="material-icons"></i><span>Assigned</span></a> </li>';
                        echo '<li><a class="menu-item" href="intransit_jobs.php"><i class="material-icons"></i><span>In-Transit</span></a> </li>';
                        echo ' <li><a class="menu-item" href="delivered_jobs.php"><i class="material-icons"></i><span>Delivered</span></a> </li>';
                        echo ' <li><a class="menu-item" href="cancelled_jobs.php"><i class="material-icons"></i><span>Cancelled</span></a> </li>';
                    }
                }


                ?>

            </ul>
        </li>

    </ul>
</div>

