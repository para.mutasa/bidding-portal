<?php
session_start();

// if (!isset($_SESSION['user_type']))
// {
//     header("Location: customer_login ");
//     die();
// }
?>
<!-- BEGIN: Head-->
<?php require_once('../layout/header '); ?>

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu material-vertical-layout material-layout 1-column  bg-full-screen-image blank-page" data-open="click" data-menu="vertical-menu" data-col="1-column">
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-header row">
        </div>
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <section class="row flexbox-container">
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class="col-lg-4 col-md-10 col-10 box-shadow-2 p-0">
                            <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                                <div class="card-header border-0 pb-0">
                                    <div class="card-title text-center">
                                        <img src="../app-assets/images/logo/vaya_logo.jpg" alt="branding logo">
                                    </div>
                                    <h6 class="card-subtitle line-on-side text-muted text-center primary font-small-3 pt-2"><span>CORPORATE CUSTOMER</span></h6>
                                </div>
                                <div style="color: #00bcd4; text-align: center;" class='coCustomerResponse'></div>
                                <div class="card-content">
                                    <!-- <div class="text-center">
                                        <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-facebook"><span class="la la-facebook"></span></a>
                                        <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-twitter"><span class="la la-twitter"></span></a>
                                        <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-linkedin"><span class="la la-linkedin font-medium-4"></span></a>
                                        <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-github"><span class="la la-github font-medium-4"></span></a>
                                    </div>
                                    <p class="card-subtitle line-on-side text-muted text-center font-small-3 mx-2 my-1"><span>OR Using
                                            Email</span></p> -->
                                    <div class="card-body">
                                    <form class="form-horizontal" id="registercocustomer" >
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <!-- <label for="projectinput1">Company Name</label><span class="danger">*</span>     -->
                                                        <input type="text" id="projectinput1" class="form-control border-primary"  placeholder="Company Name (*)"  name="name" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <!-- <label for="projectinput2">Company Address</label><span class="danger">*</span> -->
                                                        <input type="text" id="projectinput2" class="form-control border-primary" placeholder="Company Address (*)" name="vCaddress" required>
                                                    </div>
                                                </div>
                                              
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <!-- <label for="projectinput1">Country Code</label> <span class="danger">*</span> -->
                                                        <input type="text" id="projectinput1" class="form-control border-primary" placeholder="Country Code (*)" name="countryCode" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <!-- <label for="projectinput2">Phone Code</label><span class="danger">*</span> -->
                                                        <input type="text" id="projectinput2" class="form-control border-primary" placeholder="Phone Code (*)" name="phoneCode" required>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <!-- <label  for="projectinput3">E-mail</label> -->
                                                        <input type="text" id="projectinput3" class="form-control border-primary" placeholder="E-mail (*)" name="vEmail" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <!-- <label for="projectinput4">Phone Number</label> <span class="danger">*</span> -->
                                                        <input type="text" id="projectinput4" class="form-control border-primary" placeholder="Phone Number (*)" name ="vPhone" required>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="projectinput3">Password</label> 
                                                        <input type="password" id="user-password" class="form-control border-primary"  placeholder ="Password (*)" name="vPassword"  required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="projectinput4">Zimra BP-Number</label> 

                                                        <input type="text"  class="form-control border-primary" placeholder="Zimra BP-Number (*)"  required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-6 col-12 text-center text-sm-left pr-0">
                                                    <!-- <fieldset>
                                                        <input type="checkbox" id="remember-me" class="chk-remember">
                                                        <label for="remember-me"> Remember Me</label>
                                                    </fieldset> -->
                                                </div>
                                                <div class="col-sm-6 col-12 float-sm-left text-center text-sm-right"><a href="forgot_password " class="card-link">Forgot Password?</a></div>
                                            </div>
                            
                                            <input type="hidden" class="form-control" name="companyRegistration" value="true">
                                            <button type="button" name="companyRegistration" onClick="registerCoCustomer()" class="btn btn-outline-info btn-block"><i class="la la-user"></i> Register</button>
                                      
                                        </form>
                                    </div>
                                    <div class="card-body">
                                        <a href="customer_login " class="btn btn-outline-danger btn-block"><i class="ft-unlock"></i>
                                            Login</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <!-- END: Content-->

   <!-- BEGIN: Footer-->
   <?php require_once('../layout/footer '); ?>
    <!-- END: Footer-->

        <!-- BEGIN: AJAX CALLS-->
        <script>
        //Cancel Job
        function registerCoCustomer() {
            //   var showTimeout = setTimeout(function() {
            //     $('.spinner').show();
            //  }, 5000);
            $.ajax({
                type: "POST",
                url: "../controller/bid_process ",
                data: $('form#registercocustomer').serialize(),
                cache: false,
                success: function(response) {
                    var json = $.parseJSON(response);
                    // console.log(json.Action);
                    if (json.Action == 1) {
                        // clearTimeout(showTimeout);
                        // $('.spinner').hide();
                        $('.coCustomerResponse').empty(); //clear apend
                        $('.coCustomerResponse').append("Corporate registration successful");
                        $(".form-horizontal").on("hidden.bs.form-horizontal", function() {
                            $(".coCustomerResponse").html("");
                        });
                        setTimeout(function() {
                            window.location = "customer_login ";
                        }, 3000);

                    } else {
                        // clearTimeout(showTimeout);
                        // $('.spinner').hide();
                        $('.coCustomerResponse').empty();
                        $('.coCustomerResponse').append(json.Message);
                        $(".form-horizontal").on("hidden.bs.form-horizontal", function() {
                            $(".coCustomerResponse").html("");
                        });
                        setTimeout(function() {
                            window.location = "corporate_customer ";
                        }, 5000);
                    }

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //  console.log(jqXHR, textStatus, errorThrown);
                    //  clearTimeout(showTimeout);
                    // $('.spinner').hide();
                    $('.coCustomerResponse').empty();
                    $('.coCustomerResponse').append(errorThrown);
                    $(".form-horizontal").on("hidden.bs.form-horizontal", function() {
                        $(".coCustomerResponse").html("");
                    });
                    setTimeout(function() {
                            window.location = "corporate_customer ";
                        }, 5000);
                }
            });
        }
    </script>
    <!-- END: AJAX CALLS-->

</body>
<!-- END: Body-->

</html>