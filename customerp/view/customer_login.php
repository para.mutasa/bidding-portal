
<?php
session_start();

include_once('../../utils/VayaBiddingUtility.php');


// userLogin

if (isset($_POST['CustomerLogin'])) {

  // receive all input values from the form

  $username =  trim(filter_var($_POST['username'], FILTER_SANITIZE_STRING));
  $vPassword = trim(filter_var($_POST['vPassword'], FILTER_SANITIZE_STRING));
  $userType  = "Customer";
  $type = 'userLogin';

  // var_dump($username);
  // var_dump($vPassword);
  // var_dump($userType);
  // exit;


    $jsonData = array(
      'type'  => $type,
      'username'  => $username,
      'vPassword'  => $vPassword,
      'userType'  => $userType
    );

    $result = callVayaBiddingWebApiPost($jsonData);

    // var_dump($result);
    // exit;

    if ($result['Action'] == 1){

      $_SESSION["customer_id"] = $result['result']['iuserId'];
      $_SESSION["customer_vphone"] = $result['result']['vphone'];
      $_SESSION["vemail"] = $result['result']['vemail'];
      $_SESSION["vfirstName"] = $result['result']['vfirstName'];
      $_SESSION["vlastName"] = $result['result']['vlastName'];
      $_SESSION["user_type"] = "Customer";


      if(isset($_SESSION["user_type"])) {
      
        header("location: pending_jobs ");
        }

    } else {
   
      header("location: customer_login ");
    }
 }

?>
<!-- BEGIN: Head-->
<?php require_once('../layout/header '); ?>

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu material-vertical-layout material-layout 1-column  bg-full-screen-image blank-page" data-open="click" data-menu="vertical-menu" data-col="1-column">
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-header row">
        </div>
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <section class="row flexbox-container">
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class="col-lg-4 col-md-8 col-10 box-shadow-2 p-0">
                            <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                                <div class="card-header border-0">
                                    <div class="card-title text-center">
                                        <img src="../app-assets/images/logo/vaya_logo.jpg" alt="branding logo">
                                    </div>
                                    <h6 class="card-subtitle line-on-side text-muted text-center primary font-small-3 pt-2"><span>CUSTOMER LOGIN</span></h6>
                                </div>
                                <div class="card-content">
                                    <!-- <div class="text-center">
                                        <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-facebook"><span class="la la-facebook"></span></a>
                                        <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-twitter"><span class="la la-twitter"></span></a>
                                        <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-linkedin"><span class="la la-linkedin font-medium-4"></span></a>
                                        <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-github"><span class="la la-github font-medium-4"></span></a>
                                    </div>
                                    <p class="card-subtitle line-on-side text-muted text-center font-small-3 mx-2 my-1"><span>OR Using Account
                                            Details</span></p> -->
                                    <div class="card-body">
                                        <form class="form-horizontal"  method="post" action="customer_login ">
                                       
                                            <fieldset class="form-group position-relative has-icon-left">
                                                <input type="email" class="form-control border-primary " id="user-name" name ="username" pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$" placeholder="Your Email" required>
                                                <div class="form-control-position">
                                                    <i class="ft-mail"></i>
                                                </div>
                                            </fieldset>
                                            <fieldset class="form-group position-relative has-icon-left">
                                                <input type="password" class="form-control  border-primary " id="user-password" name="vPassword" placeholder="Enter Password" required>
                                                <div class="form-control-position">
                                                    <i class="la la-key"></i>
                                                </div>
                                            </fieldset>
                                            <div class="form-group row">
                                                <div class="col-sm-6 col-12 text-center text-sm-left pr-0">
                                                    <!-- <fieldset>
                                                        <input type="checkbox" id="remember-me" class="chk-remember">
                                                        <label for="remember-me"> Remember Me</label>
                                                    </fieldset> -->
                                                </div>
                                                <div class="col-sm-6 col-12 float-sm-left text-center text-sm-right"><a href="forgot_password " class="card-link">Forgot Password?</a></div>
                                            </div>
                                            <button type="submit"  name="CustomerLogin"  class="btn btn-outline-info btn-block"><i class="ft-unlock"></i> Login</button>
                                        </form>
                                    </div>
                                    <p class="card-subtitle line-on-side text-muted text-center font-small-3 mx-2 my-1"><span>NEW TO  VAYA TECHNOLOGIES
                                            ?</span></p>
                                    <div class="card-body">
                                        <a href="individual_customer " class="btn btn-outline-danger btn-block"><i class="la la-user"></i>
                                            Register As Individual Customer</a>
                                    </div>
                                    <div class="card-body">
                                        <a href="corporate_customer " class="btn btn-outline-danger btn-block"><i class="la la-user"></i>
                                            Register As A Corporate</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <!-- END: Content-->

   <!-- BEGIN: Footer-->
   <?php require_once('../layout/footer '); ?>
    <!-- END: Footer-->

</body>
<!-- END: Body-->

</html>