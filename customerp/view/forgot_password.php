<?php
session_start();

// if (!isset($_SESSION['user_type']))
// {
//     header("Location: customer_login ");
//     die();
// }
?>

<!-- BEGIN: Head-->
<?php require_once('../layout/header '); ?>

<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu material-vertical-layout material-layout 1-column  bg-full-screen-image blank-page" data-open="click" data-menu="vertical-menu" data-col="1-column">
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-header row">
        </div>
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <section class="row flexbox-container">
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class="col-lg-4 col-md-8 col-10 box-shadow-2 p-0">
                            <div class="card border-grey border-lighten-3 px-2 py-2 m-0">
                                <div class="card-header border-0 pb-0">
                                    <div class="card-title text-center">
                                        <img src="../app-assets/images/logo/vaya_logo.jpg" alt="branding logo">
                                    </div>
                                    <h6 class="card-subtitle line-on-side text-muted text-center  primary font-small-3 pt-2"><span>We will send you a link
                                            to reset password.</span></h6>
                                </div>
                                <div style="color: #00bcd4; text-align: center;" class='forgotPasswordResponse'></div>
                                <div class="card-content">
                                    <div class="card-body">
                                    <form class="form-horizontal" id="forgotpassword" >
                                            <fieldset class="form-group position-relative has-icon-left">
                                                <input type="email" class="form-control border-primary" id="user-email" name="vEmail" placeholder="Your Email Address" required>
                                                <div class="form-control-position">
                                                    <i class="la la-envelope"></i>
                                                </div>
                                            </fieldset>
                                            <input type="hidden" class="form-control" name="CustomerForgotPassword" value="true">
                                            <button type="button"  name="CustomerForgotPassword" onClick="forgotPassword()" class="btn btn-outline-info btn-lg btn-block"><i class="ft-unlock"></i> Recover
                                                Password</button>

                                             
                                        </form>
                                    </div>
                                </div>
                                <div class="card-footer border-0">
                                    <p class="float-sm-left text-center"><a href="customer_login " class="card-link">Login</a></p>
                                    <p class="float-sm-right text-center">NEW TO VAYA TECHNOLOGIES ? <a href="individual_customer " class="card-link">Create
                                            Account</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <!-- END: Content-->

   <!-- BEGIN: Footer-->
   <?php require_once('../layout/footer '); ?>
    <!-- END: Footer-->
       <!-- BEGIN: AJAX CALLS-->
       <script>
        //Cancel Job
        function forgotPassword() {
            //   var showTimeout = setTimeout(function() {
            //     $('.spinner').show();
            //  }, 5000);
            $.ajax({
                type: "POST",
                url: "../controller/bid_process ",
                data: $('form#forgotpassword').serialize(),
                cache: false,
                success: function(response) {
                    var json = $.parseJSON(response);
                    // console.log(json.Action);
                    if (json.Action == 1) {
                        // clearTimeout(showTimeout);
                        // $('.spinner').hide();
                        $('.forgotPasswordResponse').empty(); //clear apend
                        $('.forgotPasswordResponse').append("Token sent successfully to your registered email");
                        $(".form-horizontal").on("hidden.bs.form-horizontal", function() {
                            $(".forgotPasswordResponse").html("");
                        });
                        setTimeout(function() {
                            window.location = "reset_password ";
                        }, 5000);

                    } else {
                        // clearTimeout(showTimeout);
                        // $('.spinner').hide();
                        $('.forgotPasswordResponse').empty();
                        $('.forgotPasswordResponse').append(json.Message);
                        $(".form-horizontal").on("hidden.bs.form-horizontal", function() {
                            $(".forgotPasswordResponse").html("");
                        });
                        setTimeout(function() {
                            window.location = "forgot_password ";
                        }, 5000);
                    }

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //  console.log(jqXHR, textStatus, errorThrown);
                    //  clearTimeout(showTimeout);
                    // $('.spinner').hide();
                    $('.forgotPasswordResponse').empty();
                    $('.forgotPasswordResponse').append(errorThrown);
                    $(".form-horizontal").on("hidden.bs.form-horizontal", function() {
                        $(".forgotPasswordResponse").html("");
                    });
                    setTimeout(function() {
                            window.location = "forgot_password ";
                        }, 5000);
                }
            });
        }
    </script>
    <!-- END: AJAX CALLS-->

</body>
<!-- END: Body-->

</html>