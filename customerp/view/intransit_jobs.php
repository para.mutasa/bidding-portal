<?php
session_start();

if (!isset($_SESSION['user_type'])) {
    header("Location: customer_login ");
    die();
}

$iuserId = $_SESSION["customer_id"];
$customer_vphone = $_SESSION["customer_vphone"];
$vfirstName = $_SESSION["vfirstName"];
$vlastName = $_SESSION["vlastName"];


require_once('../controller/bid_process ');

$intransit_jobs = listAllInTransitJobsByIUserId($iuserId);
$vehicle_types =  getAllVehicleTypes();
$servicepillars =  getAllServicePillars();
$currencies =  getAllCurrencies();
$customers =  getCustomer($iuserId);
?>
<!-- BEGIN: Head-->
<?php require_once('../layout/header '); ?>

<!-- END: Head-->
<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu material-vertical-layout material-layout 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">

    <!-- BEGIN: Header-->
    <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-dark bg-light navbar-shadow">
        <div class="navbar-wrapper">
            <div class="navbar-header">
                <ul class="nav navbar-nav flex-row">
                    <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                    <li class="nav-item"><a class="navbar-brand" href="pending_jobs ">
                            <h3 class="brand-text">VAYA TECHNOLOGIES</h3>
                        </a></li>
                    <li class="nav-item d-md-none"><a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="material-icons mt-50">more_vert</i></a></li>
                </ul>
            </div>
            <div class="navbar-container content">
                <div class="collapse navbar-collapse" id="navbar-mobile">
                    <ul class="nav navbar-nav mr-auto float-left">
                        <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle" href="#"><i class="ft-menu"></i></a></li>


                        </li>


                    </ul>
                    <ul class="nav navbar-nav float-right">



                        <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown"><span class="mr-1 user-name text-bold-700">John Doe</span><span class="avatar avatar-online"><img src="../app-assets/images/portrait/small/vaya_avatah.png" alt="avatar"><i></i></span></a>
                            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#"><i class="material-icons">person_outline</i> Edit Profile</a>
                                <div class="dropdown-divider"></div><a class="dropdown-item" href="customer_login "><i class="material-icons">power_settings_new</i> Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- END: Header-->


    <!-- BEGIN: Main Menu-->

    <div class="main-menu material-menu menu-fixed menu-dark menu-accordion    menu-shadow " data-scroll-to-active="true">
        <div class="user-profile">
            <!-- <div class="user-info text-center pb-2"><img class="user-img img-fluid rounded-circle w-25 mt-2" src="../../../app-assets/images/portrait/small/avatar-s-1.png" alt="" />
                <div class="name-wrapper d-block dropdown mt-1"><a class="white dropdown-toggle ml-2" id="user-account" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="user-name">Charlie Adams</span></a>
                    <div class="text-light">UX Designer</div>
                    <div class="dropdown-menu arrow"><a class="dropdown-item"><i class="material-icons align-middle mr-1">person</i><span class="align-middle">Profile</span></a><a class="dropdown-item"><i class="material-icons align-middle mr-1">message</i><span class="align-middle">Messages</span></a><a class="dropdown-item"><i class="material-icons align-middle mr-1">attach_money</i><span class="align-middle">Balance</span></a><a class="dropdown-item"><i class="material-icons align-middle mr-1">settings</i><span class="align-middle">Settings</span></a><a class="dropdown-item"><i class="material-icons align-middle mr-1">power_settings_new</i><span class="align-middle">Log Out</span></a></div>
                </div>
            </div> -->
        </div>
        <?php require_once('../layout/left_sidebar '); ?>
    </div>

    <!-- END: Main Menu-->
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-header row">

            <div class="content-header-dark bg-img col-12">
                <div class="row">

                    <div class="content-header-left col-md-9 col-12 mb-2">
                        <h3 class="content-header-title white">In-Transit Jobs List</h3>
                        <div class="row breadcrumbs-top">
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="pending_jobs ">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="intransit_jobs ">In-Transit Jobs</a>
                                    </li>

                                </ol>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <!-- List Of All Patients -->

                <section id="patients-list">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h2 class="card-title blue">In-Transit Jobs List</h2>
                                  
                                </div>
                                <div class="card-body collapse show">
                                    <div class="card-body card-dashboard">
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered patients-list">
                                            <thead>
                                                <tr>
                                                    <th>Job Number</th>
                                                    <!-- <th>Pick-Up Location</th>
                                                    <th>Drop-Off Location</th> -->
                                                    <th>Collection Time</th>
                                                    <th>Delivery Time </th>
                                                    <th>Payment Method</th>
                                                    <th>Payment Status</th>
                                                    <th>Delivery Code</th>
                                                    <th>Created At</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if ($intransit_jobs['Action'] == 1) : ?>

                                                    <?php
                                                    $intransit_jobs['result']['content'] = isset($intransit_jobs['result']['content']) ?   $intransit_jobs['result']['content'] : '';
                                                    $intransit_jobs['result']['content'] = is_array($intransit_jobs['result']['content']) ? $intransit_jobs['result']['content'] : array();
                                                    foreach ($intransit_jobs['result']['content'] as $intransit_job) :

                                                    ?>
                                                        <tr>
                                                            <td><?= $intransit_job['jobId'] ?></td>
                                                            <!-- <td><?= $intransit_job['pickupAddress'] ?></td>
                                                            <td><?= $intransit_job['dropOffAddress'] ?></td> -->
                                                            <td><?= $intransit_job['collectionTime'] ?></td>
                                                            <td><?= $intransit_job['deliveryTime'] ?></td>
                                                            <td><?= $intransit_job['paymentMode'] ?></td>
                                                            <td><?= $intransit_job['paymentStatus'] ?></td>
                                                            <td><?= $intransit_job['deliveryCode'] ?></td>
                                                            <td><?= date('Y-m-d\ h:i', strtotime($intransit_job['createdDate'])); ?></td>

                                                            <td>
                                                                <button type="button" rel="tooltip" title="View" class="btn btn-blue btn-link btn-sm">
                                                                    <i data-target="#intransitjob<?= $intransit_job['jobId'] ?>" data-toggle="modal" class="material-icons">lens</i>
                                                                </button>

                                                            </td>




                                                        </tr>

                                                        <!-- Real Time Tracking  Modal  -->
                                                        <div class="modal fade text-left" id="intransitjob<?= $intransit_job['jobId'] ?>" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header bg-blue white">
                                                                        <label class="modal-title white" id="myModalLabel33">Real Time Tracking</label>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="#">
                                                                        <div class="modal-body">
                                                                            <input type="hidden" id="start1" value="<?= $intransit_job['pickupAddress'] ?>"><!-- Starting Location -->

                                                                            <input type="hidden" id="end1" value="<?= $intransit_job['dropOffAddress'] ?>"><!-- Ending Location -->

                                                                            <div class="container" id="map" style="height: 380px;width: 400px;"></div>
                                                                            <!-- <div id="maps-leaflet-user-location" class="height-30 width-450"></div> -->
                                                
                                                                            <input type="hidden" id="myInput" class="form-control" value="<?= $intransit_job['tripId'] ?>">
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                        <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--end modal-->
                                                    <?php endforeach; ?>

                                                <?php else : ?>

                                                    <?= $intransit_jobs['Message'] ?>

                                                <?php endif; ?>
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>
    <!-- BEGIN: Footer-->
    <?php require_once('../layout/footer '); ?>
    <!-- END: Footer-->

    <!-- BEGIN: Map-->

 

    <script>
        $(document).ready(function() {
            function calcDistance(p1, p2) { //p1 and p2 in the form of google.maps.LatLng object
                return (google.maps.geometry.spherical.computeDistanceBetween(p1, p2) / 1000).toFixed(3); //distance in KiloMeters
            }

            function getLocation() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(showPosition, showError);
                } else {
                    alert("Geolocation is not supported by this browser.");
                }
            }

            function showPosition(position) {
                alert("Latitude: " + position.coords.latitude + " Longitude: " + position.coords.longitude);
            }

            function showError(error) {
                switch (error.code) {
                    case error.PERMISSION_DENIED:
                        alert("User denied the request for Geolocation.");
                        break;
                    case error.POSITION_UNAVAILABLE:
                        alert("Location information is unavailable.");
                        break;
                    case error.TIMEOUT:
                        alert("The request to get user location timed out.");
                        break;
                    case error.UNKNOWN_ERROR:
                        alert("An unknown error occurred.");
                        break;
                }
            }

            function initMap() {

                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 13,
                    center: {
                        lat: -17.7808295,
                        lng: 31.0740816
                    }
                });

                var waypts = []; //origin to destination via waypoints
                //waypts.push({location: 'indore', stopover: true});

                function continuouslyUpdatePosition(location) { //Take current location from location  and set marker to that location
                    var inputVal = document.getElementById("myInput").value;
                    // Displaying the value
                    // alert(inputVal);
                    var xhttp = new XMLHttpRequest();
                    xhttp.onreadystatechange = function() {
                        if (this.readyState == 4 && this.status == 200) {
                            var pos = JSON.parse(this.responseText);
                            location.setPosition(new google.maps.LatLng(pos.lat, pos.lng));
                            setTimeout(function() {
                                continuouslyUpdatePosition(location);
                            }, 5000);
                        }
                    };
                    // xhttp.open("GET", "location ?tripId=1", true);

                    xhttp.open("GET", "location ?tripId=" + inputVal, true);
                    xhttp.send();
                }

                /* Distance between p1 & p2
                var p1 = new google.maps.LatLng(45.463688, 9.18814);
                var p2 = new google.maps.LatLng(46.0438317, 9.75936230000002);
                alert(calcDistance(p1,p2)+" Kilimeters");
                */

                //Make marker at any position in form of {lat,lng}
                function makeMarker(position /*, icon*/ ) {
                    var marker = new google.maps.Marker({
                        position: position,
                        map: map,
                        /*animation: google.maps.Animation.DROP,*/
                        /*icon: icon,*/
                    });
                    return marker;
                }

                var icons = {
                    end: new google.maps.MarkerImage('http://icons.iconarchive.com/icons/icons-land/vista-map-markers/32/Map-Marker-Push-Pin-1-Left-Pink-icon.png'),
                    start: new google.maps.MarkerImage('http://icons.iconarchive.com/icons/icons-land/vista-map-markers/32/Map-Marker-Push-Pin-1-Left-Chartreuse-icon.png')
                };

                //Show suggestions for places, requires libraries=places in the google maps api script link
                var autocomplete1 = new google.maps.places.Autocomplete(document.getElementById('start1'));
                var autocomplete2 = new google.maps.places.Autocomplete(document.getElementById('end1'));

                var directionsService1 = new google.maps.DirectionsService;
                var directionsDisplay1 = new google.maps.DirectionsRenderer({
                    polylineOptions: {
                        strokeColor: "red"
                    }, //path color
                    //draggable: true,// change start, waypoints and destination by dragging
                    /* Start and end marker with same image
                    markerOptions : {icon: 'http://icons.iconarchive.com/icons/icons-land/vista-map-markers/32/Map-Marker-Push-Pin-1-Left-Pink-icon.png'},
                    */
                    //suppressMarkers: true
                });
                directionsDisplay1.setMap(map);
                var onChangeHandler1 = function() {
                    calculateAndDisplayRoute(directionsService1, directionsDisplay1, $('#start1'), $('#end1'));
                };
                $('#start1,#end1').change(onChangeHandler1);

                function calculateAndDisplayRoute(directionsService, directionsDisplay, start, end) {
                    directionsService.route({
                        origin: start.val(),
                        destination: end.val(),
                        waypoints: waypts,
                        travelMode: 'DRIVING'
                    }, function(response, status) {
                        if (status === 'OK') {
                            directionsDisplay.setDirections(response);
                            var leg = response.routes[0].legs[0];
                            // Move marker along path from A to B
                            var markers = [];
                            for (var i = 0; i < leg.steps.length; i++) {
                                var marker = makeMarker(leg.steps[i].start_location);
                                markers.push(marker);
                                marker.setMap(null);
                            }
                            tracePath(markers, 0);

                            var location = makeMarker(leg.steps[0].start_location);
                            continuouslyUpdatePosition(location);

                        } else {
                            window.alert('Directions request failed due to ' + status);
                        }
                    });
                }

                function tracePath(markers, index) { // move marker along path from A to B
                    if (index == markers.length) return;
                    markers[index].setMap(map);
                    setTimeout(function() {
                        markers[index].setMap(null);
                        tracePath(markers, index + 1);
                    }, 500);
                }

                calculateAndDisplayRoute(directionsService1, directionsDisplay1, $('#start1'), $('#end1'));
            }
        });
    </script>
    

    <!-- END: Map-->
</body>
<!-- END: Body-->

</html>