<?php
session_start();

if (!isset($_SESSION['user_type'])) {
  header("Location: customer_login ");
  die();
}

$iuserId = $_SESSION["customer_id"];
$customer_vphone = $_SESSION["customer_vphone"];
$vfirstName = $_SESSION["vfirstName"];
$vlastName = $_SESSION["vlastName"];

require_once('../controller/bid_process ');

$pending_jobs = listAllPendingJobsByIUserId($iuserId);
$vehicle_types =  getAllVehicleTypes();
$servicepillars =  getAllServicePillars();
$currencies =  getAllCurrencies();
$customers =  getCustomer($iuserId);


?>
<!-- BEGIN: Head-->
<?php require_once('../layout/header '); ?>

<!-- END: Head-->
<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu material-vertical-layout material-layout 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">

  <!-- BEGIN: Header-->
  <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-dark bg-light navbar-shadow">
    <div class="navbar-wrapper">
      <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
          <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
          <li class="nav-item"><a class="navbar-brand" href="pending_jobs ">
              <h3 class="brand-text">VAYA TECHNOLOGIES</h3>
            </a></li>
          <li class="nav-item d-md-none"><a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="material-icons mt-50">more_vert</i></a></li>
        </ul>
      </div>
      <div class="navbar-container content">
        <div class="collapse navbar-collapse" id="navbar-mobile">
          <ul class="nav navbar-nav mr-auto float-left">
            <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle" href="#"><i class="ft-menu"></i></a></li>
            <li class="dropdown nav-item mega-dropdown d-none d-lg-block text-bold-700"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">
                <?php
                echo $date = date('Y-m-d H:i:s');
                ?>
              </a>
            </li>
          </ul>
          <ul class="nav navbar-nav float-right">
            <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown"><span class="mr-1 user-name text-bold-700">Welcome (<?php echo $vfirstName . ' ' . $vlastName; ?>)</span><span class="avatar avatar-online"><img src="../app-assets/images/portrait/small/vaya_avatah.png" alt="avatar"><i></i></span></a>
              <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#"><i class="material-icons">person_outline</i> Edit Profile</a>
                <div class="dropdown-divider"></div><a class="dropdown-item" href="customer_login "><i class="material-icons">power_settings_new</i> Logout</a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>
  <!-- END: Header-->


  <!-- BEGIN: Main Menu-->

  <div class="main-menu material-menu menu-fixed menu-dark menu-accordion  menu-shadow " data-scroll-to-active="true">
    <!-- <div class="user-profile">
            <div class="user-info text-center pb-2"><img class="user-img img-fluid rounded-circle w-25 mt-2" src="../../../app-assets/images/portrait/small/avatar-s-1.png" alt="" />
                <div class="name-wrapper d-block dropdown mt-1"><a class="white dropdown-toggle ml-2" id="user-account" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="user-name">Charlie Adams</span></a>
                    <div class="text-light">UX Designer</div>
                    <div class="dropdown-menu arrow"><a class="dropdown-item"><i class="material-icons align-middle mr-1">person</i><span class="align-middle">Profile</span></a><a class="dropdown-item"><i class="material-icons align-middle mr-1">message</i><span class="align-middle">Messages</span></a><a class="dropdown-item"><i class="material-icons align-middle mr-1">attach_money</i><span class="align-middle">Balance</span></a><a class="dropdown-item"><i class="material-icons align-middle mr-1">settings</i><span class="align-middle">Settings</span></a><a class="dropdown-item"><i class="material-icons align-middle mr-1">power_settings_new</i><span class="align-middle">Log Out</span></a></div>
                </div>
            </div>
        </div> -->
    <?php require_once('../layout/left_sidebar '); ?>
  </div>

  <!-- END: Main Menu-->
  <!-- BEGIN: Content-->
  <div class="app-content content">
    <div class="content-header row">
      <div class="content-header-dark bg-img col-12">
        <div class="row">
          <div class="content-header-left col-md-9 col-12 mb-2">
            <h3 class="content-header-title white">Pending Jobs List</h3>
            <div class="row breadcrumbs-top">
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="pending_jobs ">Home</a>
                  </li>
                  <li class="breadcrumb-item"><a href="pending_jobs ">Pending Jobs</a>
                  </li>
                </ol>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
    <div class="content-overlay"></div>
    <div class="content-wrapper">
      <div class="content-body">
        <!-- List Of All Patients -->

        <section id="patients-list">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h2 class="card-title blue">Pending Jobs List</h2>
                  <div class="heading-elements">
                    <a href="post_job "  class="btn btn-blue round btn-sm"><i class="la la-plus font-small-2"></i>
                      POST NEW
                      JOB</a>
                  </div>
                </div>
                <div class="card-body collapse show">
                  <div class="card-body card-dashboard">
                  </div>
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered patients-list">
                      <thead>
                        <tr>
                          <th>Job Number</th>
                          <th>Pick-Up Location</th>
                          <th>Drop-Off Location</th>
                          <th>Collection Time</th>
                          <th>Delivery Time </th>
                          <th>Vehicle Capacity</th>
                          <th>Service Pillar</th>
                          <th>Offer Amount</th>
                          <th>Type Of Goods</th>
                          <th>Created At</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if ($pending_jobs['Action'] == 1) : ?>
                          <?php
                          $pending_jobs['result']['content'] = isset($pending_jobs['result']['content']) ?   $pending_jobs['result']['content'] : '';
                          $pending_jobs['result']['content'] = is_array($pending_jobs['result']['content']) ? $pending_jobs['result']['content'] : array();
                          foreach ($pending_jobs['result']['content'] as $pending_job) :

                          ?>
                            <tr>
                              <td><?= $pending_job['jobId'] ?></td>
                              <td><?= $pending_job['pickupAddress'] ?></td>
                              <td><?= $pending_job['dropOffAddress'] ?></td>
                              <td><?= $pending_job['collectionTime'] ?></td>
                              <td><?= $pending_job['deliveryTime'] ?></td>
                              <td><?= getVehicleType($pending_job['vehicleType'])['result']['capacity'] ?></td>
                              <td><?= getServicePillar($pending_job['servicePillarId'])['result']['name'] ?></td>
                              <td><?= getCurrencyById($pending_job['currencyId'])['result']['name'] . getCurrencyById($pending_job['currencyId'])['result']['symbol'] . ' ' . $pending_job['amountOffer'] ?></td>
                              <td><?= $pending_job['typeOfGoods'] ?></td>
                              <td><?= date('Y-m-d\ h:i', strtotime($pending_job['createdDate'])); ?></td>

                              <td>
                                <button type="button" rel="tooltip" title="View" class="btn btn-blue btn-link btn-sm">
                                  <i data-target="#myjob<?= $pending_job['jobId'] ?>" data-toggle="modal" class="material-icons">lens</i>
                                </button>

                              </td>

                            </tr>

                            <!-- Job Details Modal  -->
                            <div class="modal fade text-left" id="myjob<?= $pending_job['jobId'] ?>" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header bg-blue white">
                                    <label class="modal-title white" id="myModalLabel33">Job Details</label>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form action="#">
                                    <div class="modal-body">
                                      <label class="text-black">Job Number: </label>
                                      <div class="form-group">
                                        <input type="text" class="form-control" value="<?= $pending_job['jobId'] ?>" readonly>
                                      </div>

                                      <label class="text-black">Job Status: </label>
                                      <div class="form-group">
                                        <input type="text" class="form-control" value="<?= $pending_job['jobStatus'] ?>" readonly>
                                      </div>
                                      <label class="text-black">Pick-Up Location: </label>
                                      <div class="form-group">
                                        <input type="text" class="form-control" value="<?= $pending_job['pickupAddress'] ?>" readonly>
                                      </div>

                                      <label class="text-black">Drop-Off Location: </label>
                                      <div class="form-group">
                                        <input type="text" class="form-control" value="<?= $pending_job['dropOffAddress'] ?>" readonly>
                                      </div>
                                      <label class="text-black">Offer Amount: </label>
                                      <div class="form-group">
                                        <input type="text" class="form-control" value="<?= getCurrencyById($pending_job['currencyId'])['result']['name'] . getCurrencyById($pending_job['currencyId'])['result']['symbol'] . ' ' . $pending_job['amountOffer'] ?>" readonly>
                                      </div>
                                      <input type="hidden" class="form-control" name="jobId" value="<?= $pending_job['jobId'] ?>" readonly>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" data-target="#cancelJob<?= $pending_job['jobId'] ?>" data-dismiss='modal' data-toggle="modal" class="btn grey btn-outline-secondary" data-dismiss="modal">Cancel Job</button>
                                      <button type="button" onclick="window.location.href='bids '" class="btn btn-outline-blue">View Bids</button>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>

                            <!--end modal-->

                            <!-- Cancel Job Modal  -->

                            <div class="modal fade text-left" id="cancelJob<?= $pending_job['jobId'] ?>" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header bg-blue white">
                                    <label class="modal-title white" id="myModalLabel33">Job Details</label>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form id="cancelJob">
                                    <div class="modal-body">
                                      <label class="text-black">Job Number: </label>
                                      <div class="form-group">
                                        <input type="text" class="form-control" value="<?= $pending_job['jobId'] ?>" readonly>
                                      </div>

                                      <label class="text-black">Job Status: </label>
                                      <div class="form-group">
                                        <input type="text" class="form-control" value="<?= $pending_job['jobStatus'] ?>" readonly>
                                      </div>
                                      <label class="text-black">Pick-Up Location: </label>
                                      <div class="form-group">
                                        <input type="text" class="form-control" value="<?= $pending_job['pickupAddress'] ?>" readonly>
                                      </div>

                                      <label class="text-black">Drop-Off Location: </label>
                                      <div class="form-group">
                                        <input type="text" class="form-control" value="<?= $pending_job['dropOffAddress'] ?>" readonly>
                                      </div>
                                      <label class="text-black">Offer Amount: </label>
                                      <div class="form-group">
                                        <input type="text" class="form-control" value="<?= getCurrencyById($pending_job['currencyId'])['result']['name'] . getCurrencyById($pending_job['currencyId'])['result']['symbol'] . ' ' . $pending_job['amountOffer'] ?>" readonly>
                                      </div>
                                      <div class="form-group">
                                        <p class="text-black">Are you sure you want to cancel the Job?</p>
                                      </div>
                                      <input type="hidden" class="form-control" name="jobId" value="<?= $pending_job['jobId'] ?>">
                                      <input type="hidden" class="form-control" name="customerId" value="<?php echo $iuserId; ?>">
                                    </div>
                                    <div style="color: #1C7ACD; text-align: center;" class='cancelJobResponse'></div>
                                    <!-- <div class="spinner" style="display: none">
                                      <div class="center">
                                        <img src="http://cdn.nirmaltv.com/images/generatorphp-thumb.gif" alt="Wait" alt="Loading"/>
                                      </div>
                                    </div> -->
                                    <div class="modal-footer">
                                      <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                                      <input type="hidden" class="form-control" name="cancelJob" value="true">
                                      <button type="button" name="cancelJob" onClick="cancelJOB()" class="btn btn-outline-blue">Confirm</button>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>

                            <!--end modal-->


                          <?php endforeach; ?>

                        <?php else : ?>

                          <?= $pending_jobs['Message'] ?>

                        <?php endif; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div>
    </div>
  </div>

 
  <!-- END: Content-->

  <div class="sidenav-overlay"></div>
  <div class="drag-target"></div>
  <!-- BEGIN: Footer-->
  <?php require_once('../layout/footer '); ?>
  <!-- END: Footer-->

  <!-- BEGIN: AJAX CALLS-->
  <script>
    //Cancel Job
    function cancelJOB() {
      //   var showTimeout = setTimeout(function() {
      //     $('.spinner').show();
      //  }, 5000);
      $.ajax({
        type: "POST",
        url: "../controller/bid_process ",
        data: $('form#cancelJob').serialize(),
        cache: false,
        success: function(response) {
          var json = $.parseJSON(response);
          // console.log(json.Action);
          if (json.Action == 1) {
            // clearTimeout(showTimeout);
            // $('.spinner').hide();
            $('.cancelJobResponse').empty(); //clear apend
            $('.cancelJobResponse').append("Job Successfully Cancelled");
            $(".modal").on("hidden.bs.modal", function() {
              $(".cancelJobResponse").html("");
            });
            setTimeout(function() {
              window.location = "pending_jobs ";
            }, 10000);

          } else {
            // clearTimeout(showTimeout);
            // $('.spinner').hide();
            $('.cancelJobResponse').empty();
            $('.cancelJobResponse').append(json.Message);
            $(".modal").on("hidden.bs.modal", function() {
              $(".cancelJobResponse").html("");
            });
          }

        },
        error: function(jqXHR, textStatus, errorThrown) {
          //  console.log(jqXHR, textStatus, errorThrown);
          //  clearTimeout(showTimeout);
          // $('.spinner').hide();
          $('.cancelJobResponse').empty();
          $('.cancelJobResponse').append(errorThrown);
          $(".modal").on("hidden.bs.modal", function() {
            $(".cancelJobResponse").html("");
          });
        }
      });
    }
  </script>
  <!-- END: AJAX CALLS-->
</body>
<!-- END: Body-->



</html>