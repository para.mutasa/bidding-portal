<?php
session_start();

if (!isset($_SESSION['user_type'])) {
    header("Location: customer_login ");
    die();
}

$iuserId = $_SESSION["customer_id"];
$customer_vphone = $_SESSION["customer_vphone"];
$vfirstName = $_SESSION["vfirstName"];
$vlastName = $_SESSION["vlastName"];

require_once('../controller/bid_process ');

$vehicle_types =  getAllVehicleTypes();
$servicepillars =  getAllServicePillars();
$currencies =  getAllCurrencies();
$customers =  getCustomer($iuserId);
?>
<!-- BEGIN: Head-->
<?php require_once('../layout/header '); ?>

<!-- END: Head-->
<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu material-vertical-layout material-layout 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">

    <!-- BEGIN: Header-->
    <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-dark bg-light navbar-shadow">
        <div class="navbar-wrapper">
            <div class="navbar-header">
                <ul class="nav navbar-nav flex-row">
                    <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                    <li class="nav-item"><a class="navbar-brand" href="pending_jobs ">
                            <h3 class="brand-text">VAYA TECHNOLOGIES</h3>
                        </a></li>
                    <li class="nav-item d-md-none"><a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="material-icons mt-50">more_vert</i></a></li>
                </ul>
            </div>
            <div class="navbar-container content">
                <div class="collapse navbar-collapse" id="navbar-mobile">
                    <ul class="nav navbar-nav mr-auto float-left">
                        <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle" href="#"><i class="ft-menu"></i></a></li>
                        <li class="dropdown nav-item mega-dropdown d-none d-lg-block text-bold-700"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">
                                <?php
                                echo $date = date('Y-m-d H:i:s');
                                ?>
                            </a>
                        </li>

                    </ul>
                    <ul class="nav navbar-nav float-right">



                        <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown"><span class="mr-1 user-name text-bold-700">John Doe</span><span class="avatar avatar-online"><img src="../app-assets/images/portrait/small/vaya_avatah.png" alt="avatar"><i></i></span></a>
                            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#"><i class="material-icons">person_outline</i> Edit Profile</a>
                                <div class="dropdown-divider"></div><a class="dropdown-item" href="customer_login "><i class="material-icons">power_settings_new</i> Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- END: Header-->


    <!-- BEGIN: Main Menu-->

    <div class="main-menu material-menu menu-fixed menu-dark menu-accordion    menu-shadow " data-scroll-to-active="true">
        <div class="user-profile">
            <!-- <div class="user-info text-center pb-2"><img class="user-img img-fluid rounded-circle w-25 mt-2" src="../../../app-assets/images/portrait/small/avatar-s-1.png" alt="" />
                <div class="name-wrapper d-block dropdown mt-1"><a class="white dropdown-toggle ml-2" id="user-account" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="user-name">Charlie Adams</span></a>
                    <div class="text-light">UX Designer</div>
                    <div class="dropdown-menu arrow"><a class="dropdown-item"><i class="material-icons align-middle mr-1">person</i><span class="align-middle">Profile</span></a><a class="dropdown-item"><i class="material-icons align-middle mr-1">message</i><span class="align-middle">Messages</span></a><a class="dropdown-item"><i class="material-icons align-middle mr-1">attach_money</i><span class="align-middle">Balance</span></a><a class="dropdown-item"><i class="material-icons align-middle mr-1">settings</i><span class="align-middle">Settings</span></a><a class="dropdown-item"><i class="material-icons align-middle mr-1">power_settings_new</i><span class="align-middle">Log Out</span></a></div>
                </div>
            </div> -->
        </div>
        <?php require_once('../layout/left_sidebar '); ?>
    </div>

    <!-- END: Main Menu-->
    <!-- BEGIN: Content-->
    <div class="app-content content">

        <div class="content-header row">
            <div class="content-header-dark bg-img col-12">
                <div class="row">

                    <div class="content-header-left col-md-9 col-12 mb-2">
                        <h3 class="content-header-title white">Post New Job</h3>
                        <div class="row breadcrumbs-top">
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="pending_jobs ">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="post_job ">Post Job</a>
                                    </li>

                                </ol>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <!-- Add Patient Form Wizard -->
                <section id="add-patient">
                    <div class="icon-tabs">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title blue">
                                            Post A New Job
                                        </h4>
                                        <a class="heading-elements-toggle" href="#">
                                            <i class="la la-ellipsis-h font-medium-3"> </i>
                                        </a>
                                    </div>

                                    <div class="card-content collapse show">
                                        <div class="card-body">
                                        <form action="#" class="add-patient-tabs steps-validation wizard-notification">
                                                <!-- step 1 => Personal Details -->
                                                <h6>
                                                    <i class="step-icon la la-user font-medium-3"> </i>
                                                    Personal Details
                                                </h6>
                                                <fieldset>
                                                <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="firstName">
                                                                    Description:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input class="form-control required" id="description" name="description" onkeyup='saveValue(this);' type="text" />
                                                                <!-- <textarea class="form-control required" cols="5" id="history" name="history" rows="1"></textarea> -->
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="lastName">
                                                                    Value Of Goods:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input class="form-control required" id="goodsValue" name="goodsValue" onkeyup='saveValue(this);' type="text" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="address">
                                                                    Type Of Goods:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input class="form-control required" id="typeOfGoods" name="typeOfGoods" onkeyup='saveValue(this);' type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="city">
                                                                    Weight:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input class="form-control required" id="weight" name="weight" onkeyup='saveValue(this);' type="number" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="state">
                                                                    Unit:
                                                                </label>
                                                                <select class="custom-select" name="weight_unit">

                                                                    <option selected="selected" value="gr">
                                                                        gr
                                                                    </option>
                                                                    <option value="newyork">
                                                                        kgs
                                                                    </option>
                                                                    <option value="texas">
                                                                        tonnes
                                                                    </option>
                                                                    <option value="california">
                                                                        pounds
                                                                    </option>

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="city">
                                                                    Length:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input class="form-control required" id="length" name="length" onkeyup='saveValue(this);' type="number" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="state">
                                                                    Width:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input class="form-control required" id="width" name="width" onkeyup='saveValue(this);' type="number" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="city">
                                                                    Height:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input class="form-control required" id="height" name="height" onkeyup='saveValue(this);' type="number" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="state">
                                                                    Unit:
                                                                </label>
                                                                <select class="custom-select" name="dimension_unit">

                                                                    <option value="gr">
                                                                        cm
                                                                    </option>
                                                                    <option value="newyork">
                                                                        m
                                                                    </option>
                                                                    <option value="texas">
                                                                        inches
                                                                    </option>


                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="contact">
                                                                    Manifest:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input class="form-control required" type="file" id="manifestUpload" name="manifestUpload" accept="image/*,.pdf,.doc,.docx" onkeyup='saveValue(this);'>
                                                                <span class="file-custom"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="contact">
                                                                    Waybill:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input class="form-control required" type="file" id="wayBills" name="wayBills" accept="image/*,.pdf,.doc,.docx" onkeyup='saveValue(this);'>
                                                                <span class="file-custom"></span>
                                                            </div>
                                                        </div>
                                                        <input class="form-control required" type="hidden" name="iUserId" value="<?php echo $iuserId; ?>">
                                                    </div>
                                                </fieldset>
                                                <!-- Step 2 => Emergency Details-->
                                                <h6>
                                                    <i class="step-icon la la-ambulance font-medium-3"> </i>
                                                    In Case Of Emergency
                                                </h6>
                                                <fieldset>
                                                <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="fnemergency">
                                                                    Pick-Up Location(Mark location):
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input class="form-control required" id="pick_place" data-toggle="modal" data-target=".pickUpLocation" name="pickupAddress" type="text" />
                                                                <input name="pickupLatitude" class="MapPickLat" type="hidden" id="pickupLatitude" required readonly="readonly">
                                                                <input name="pickupLongitude" class="MapPickLon" type="hidden" id="pickupLongitude" required readonly="readonly">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="lnemergency">
                                                                    Drop-Off Location(Mark location):
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input class="form-control " id="drop_place" data-toggle="modal" data-target=".dropOFfLocation" name="dropOffAddress"  type="text" />
                                                                <input name="dropOffLatitude" class="MapDropLat" type="hidden" id="dropOffLatitude" style="width: 161px;" required readonly="readonly">
                                                                <input name="dropOffLongitude" class="MapDropLon" type="hidden" id="dropOffLongitude" style="width: 161px;" required readonly="readonly">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="city">
                                                                    Collection Date:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input type="text" class="form-control input-lg " id="minYear" name="collectionDate" onkeyup='saveValue(this);'>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="state">
                                                                    Collection Time:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>

                                                                <input type="text" class="form-control input-lg " id="textColor" name="collectionTime" onkeyup='saveValue(this);'>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="city">
                                                                    Delivery Date:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input type="text" class="form-control input-lg " id="dropTextWeight" name="deliveryDate" onkeyup='saveValue(this);'>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="state">
                                                                    Delivery Time:
                                                                </label>
                                                                <input type="text" class="form-control input-lg " id="timeformat" name="deliveryTime" onkeyup='saveValue(this);'>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="cn">
                                                                    Vehicle Capacity:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <select class="custom-select " id="vehicleTypeId" name="vehicleTypeId" onkeyup='saveValue(this);'>

                                                                    <?php foreach ($vehicle_types['result']  as $vehicle_type) : ?>
                                                                        <option selected="selected" value="<?= $vehicle_type["vehicleTypeId"] ?>"><?= $vehicle_type["capacity"] . "" ?></option>
                                                                    <?php endforeach; ?>
                                                                </select>

                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="alternate">
                                                                    Service Pillar:
                                                                </label>
                                                                <select class="custom-select " id="servicePillarId" name="servicePillarId" onkeyup='saveValue(this);'>

                                                                    <?php foreach ($servicepillars['result']  as $servicepillar) : ?>
                                                                        <option selected="selected" value="<?= $servicepillar["servicePillarId"] ?>"><?= $servicepillar["name"] . "" ?></option>
                                                                    <?php endforeach; ?>


                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                             
                                                <!-- Step 4 => Insaurance Details -->
                                                <h6>
                                                    <i class="step-icon font-medium-3 ft-file-text"> </i>
                                                    Insaurance Details
                                                </h6>
                                                <fieldset>
                                                <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="company">
                                                                    Loading Requirements
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input class="form-control required" id="loadingRequirements" name="loadingRequirements" onkeyup='saveValue(this);' type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="card">
                                                                    Offloading Requirements:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input class="form-control required" id="offloadingRequirements" name="offloadingRequirements" placeholder="Type.." onkeyup='saveValue(this);' type="text" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="city">
                                                                    Bid-Start Date:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input type="text" class="form-control input-lg required" id="dropTextColor" name="bidStartDate" onkeyup='saveValue(this);'>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="state">
                                                                    Bid-Start Time:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>

                                                                <input type="text" class="form-control input-lg required" id="time_init_animation" name="bidStartTime" onkeyup='saveValue(this);'>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="city">
                                                                    Bid-End Date:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input type="text" class="form-control input-lg required" id="dropPrimaryColor" name="bidEndDate" onkeyup='saveValue(this);'>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="state">
                                                                    Bid-End Time:
                                                                </label>
                                                                <input type="text" class="form-control input-lg required" id="primaryColor" name="bidEndTime" onkeyup='saveValue(this);'>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">

                                                    </div>
                                                    <div class="row">

                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="city">
                                                                    Payment Method:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <select class="custom-select required" id="paymentMode" name="paymentMode" onkeyup='saveValue(this);'>
                                                                    <option value="Ecocash">Ecocash</option>
                                                                    <option value="Cash">Cash</option>

                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="state">
                                                                    Goods In-Transit:
                                                                </label>
                                                                <select class="custom-select required" name="goodsInTransit" onkeyup='saveValue(this);'>
                                                                    <option value="Yes">Yes</option>
                                                                    <option value="No">No</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="city">
                                                                    Currency:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <select class="custom-select required" id="currencyld" name="currencyld" onkeyup='saveValue(this);'>
                                                                    <?php foreach ($currencies['result']  as $currency) : ?>
                                                                        <option selected="selected" value="<?= $currency["currencyId"] ?>"><?= $currency["name"] . "" ?></option>
                                                                    <?php endforeach; ?>

                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="state">
                                                                    Offer Amount:
                                                                </label>
                                                                <input class="form-control required" id="amountOffer" name="amountOffer" onkeyup='saveValue(this);' type="number" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        <div style="color: #00bcd4; text-align: center;" class='postJobResponse'></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>



    <!-- Pick up location Modal -->

    <div class="modal fade text-left" id="pickup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel17">Pick-Up Location
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="maps-leaflet-user-location" class="height-250"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-outline-primary">Use This Location</button>
                </div>
            </div>
        </div>
    </div>

    <!--Drof-Off  Modal -->
    <div class="modal fade text-left" id="dropoff" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel17">Drop-Off Location
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="maps-leaflet-user-location" class="height-250"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-outline-primary">Use This Location</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>
    <!-- BEGIN: Footer-->
    <?php require_once('../layout/footer '); ?>
    <!-- END: Footer-->

    <!-- BEGIN: AJAX CALLS-->
    <script>
        //Cancel Job
        function postJob() {
            //   var showTimeout = setTimeout(function() {
            //     $('.spinner').show();
            //  }, 5000);
            $.ajax({
                type: "POST",
                url: "../controller/bid_process ",
                data: $('form#postjob').serialize(),
                cache: false,
                success: function(response) {
                    var json = $.parseJSON(response);
                    // console.log(json.Action);
                    if (json.Action == 1) {
                        // clearTimeout(showTimeout);
                        // $('.spinner').hide();
                        $('.postJobResponse').empty(); //clear apend
                        $('.postJobResponse').append("Job posted  successful");
                        $(".form-horizontal").on("hidden.bs.form-horizontal", function() {
                            $(".postJobResponse").html("");
                        });
                        setTimeout(function() {
                            window.location = "pending_jobs ";
                        }, 3000);

                    } else {
                        // clearTimeout(showTimeout);
                        // $('.spinner').hide();
                        $('.postJobResponse').empty();
                        $('.postJobResponse').append(json.Message);
                        $(".form-horizontal").on("hidden.bs.form-horizontal", function() {
                            $(".postJobResponse").html("");
                        });
                        setTimeout(function() {
                            window.location = "post_job ";
                        }, 5000);
                    }

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //  console.log(jqXHR, textStatus, errorThrown);
                    //  clearTimeout(showTimeout);
                    // $('.spinner').hide();
                    $('.postJobResponse').empty();
                    $('.postJobResponse').append(errorThrown);
                    $(".form-horizontal").on("hidden.bs.form-horizontal", function() {
                        $(".postJobResponse").html("");
                    });
                    setTimeout(function() {
                        window.location = "post_job ";
                    }, 5000);
                }
            });
        }
    </script>
    <!-- END: AJAX CALLS-->
</body>
<!-- END: Body-->

</html>