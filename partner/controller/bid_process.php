<?php

include_once('../../utils/VayaBiddingUtility.php');

function getAllVehicleTypes()
{
  $jsonData = array(
    'type' => "getAllVehicleTypes"
  );
  return callVayaBiddingWebApiPost($jsonData);
}

 function getVehicleType($vehicleTypeId)
{
  $jsonData = array(
    'type' => "getVehicleType",
    'vehicleTypeId' => $vehicleTypeId
  );
  return callVayaBiddingWebApiPost($jsonData);
}
function getCurrencyById($currencyId)
{
  $jsonData = array(
    'type' => "getCurrencyById",
    'currencyId' => $currencyId
  );
  return callVayaBiddingWebApiPost($jsonData);
}

 function getAllJobs($idriverVehicleId)
 {
   $jsonData = array(
     'type' => "getAllJobs",
     'page' => 0,
     'size' => 100,
     'vehicleTypeId' => $idriverVehicleId

   );
   return callVayaBiddingWebApiPost($jsonData);
 }
 function getAllDrivers()
 {
   $jsonData = array(
     'type' => "getAllPartners"
   );
   return callVayaBiddingWebApiPost($jsonData);
 }

function listAllAssignedJobsByiDriverId($idriverId)
{
  $jsonData = array(
    'type' => "findAllBidsByDriverIdAndStatus",
    'iDriverId' => $idriverId,
    'page' => 0,
    'size' => 2000,
    "bidStatus" => "Assigned"
  );
  return callVayaBiddingWebApiPost($jsonData);
}

function listAllDeliveredJobsByiDriverId($idriverId)
{
  $jsonData = array(
    'type' => "findAllBidsByDriverIdAndStatus",
    'iDriverId' => $idriverId,
    'page' => 0,
    'size' => 2000,
    "bidStatus" => "Delivered"
  );
  return callVayaBiddingWebApiPost($jsonData);
}

function listAllCancelledJobsByiDriverId($idriverId)
{
  $jsonData = array(
    'type' => "findAllBidsByDriverIdAndStatus",
    'iDriverId' => $idriverId,
    'page' => 0,
    'size' => 2000,
    "bidStatus" => "Cancelled"
  );
  return callVayaBiddingWebApiPost($jsonData);
}

function listAllCollectedJobsByiDriverId($idriverId)
{
  $jsonData = array(
    'type' => "findAllBidsByDriverIdAndStatus",
    'iDriverId' => $idriverId,
    'page' => 0,
    'size' => 2000,
    "bidStatus" => "Collected"
  );
  return callVayaBiddingWebApiPost($jsonData);
}

function listAllInTransitJobsByiDriverId($idriverId)
{
  $jsonData = array(
    'type' => "findAllBidsByDriverIdAndStatus",
    'iDriverId' => $idriverId,
    'page' => 0,
    'size' => 2000,
    "bidStatus" => "InTransit"
  );
  return callVayaBiddingWebApiPost($jsonData);
}

  // Assign Driver

  if (isset($_POST['assignDriverB'])) {

    // receive all input values from the form
    $bidId =  filter_var($_POST['bidId'], FILTER_SANITIZE_NUMBER_INT);
    $idriverId =  filter_var($_POST['idriverId'], FILTER_SANITIZE_NUMBER_INT);
    $type = 'assignDriver';
  
  // var_dump($bidId);
  // var_dump($idriverId);
  // exit;
  
      $jsonData = array(
        'type'  => $type,
        'bidId'  => $bidId,
        'iDriverId'  => $idriverId
      );
  
      $assign_driver_result = callVayaBiddingWebApiPost($jsonData);
  
      echo json_encode($assign_driver_result);
      exit;
  
    }

//Accept Job

if (isset($_POST['accept'])) {

  // receive all input values from the form

  $jobId =  filter_var($_GET['jobId'], FILTER_SANITIZE_NUMBER_INT);
  $iDriverId =  filter_var($_POST['iDriverId'], FILTER_SANITIZE_NUMBER_INT);
  $type = 'partnerAcceptJob';



    $jsonData = array(
      'type'  => $type,
      'iDriverId'  => $iDriverId,
      'jobId'  => $jobId
    );

    $result = callVayaBiddingWebApiPost($jsonData);

    // var_dump($result);
    // exit;

    if ($result['Action'] == 1){

      echo "<script>";
        echo "alert('Job accepted successfully');";
        echo "window.location.href='../view/jobs ';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        // echo "alert('Please try again later');";
        echo "window.location.href='../view/jobs ';";
      echo "</script>";
    }
  }



//Reject Job

if (isset($_POST['reject'])) {

  // receive all input values from the form

  $jobId =  filter_var($_GET['jobId'], FILTER_SANITIZE_NUMBER_INT);
  $iDriverId =  filter_var($_POST['iDriverId'], FILTER_SANITIZE_NUMBER_INT);
  $rejectReason = filter_var($_POST['rejectReason'], FILTER_SANITIZE_STRING);


  $type = 'rejectJob';


    $jsonData = array(
      'type'  => $type,
      'iDriverId'  => $iDriverId,
      'jobId'  => $jobId,
      'rejectReason' => $rejectReason 

    );

    $result = callVayaBiddingWebApiPost($jsonData);

    // var_dump($result);
    // exit;

    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('Job rejected successfully');";
        echo "window.location.href='../view/jobs ';";
      echo "</script>";
    } else {
      echo "<script>";
      echo "alert('".$result['Message']."');";
        // echo "alert('Please try again later');";
        echo "window.location.href='../view/jobs ';";
      echo "</script>";
    }
  }



if (isset($_POST['counter'])) {

  // receive all input values from the form

  $jobId =  filter_var($_GET['jobId'], FILTER_SANITIZE_NUMBER_INT);
  $iDriverId =  filter_var($_POST['iDriverId'], FILTER_SANITIZE_NUMBER_INT);
  $counterOfferAmount = filter_var($_POST['counterOfferAmount'], FILTER_SANITIZE_NUMBER_INT);


  $type = 'counterOffer';

  // var_dump($jobId);
  // var_dump($counterOfferAmount);
  // var_dump($iDriverId);
  // exit;

    $jsonData = array(
      'type'  => $type,
      'iDriverId'  => $iDriverId,
      'jobId'  => $jobId,
      'counterOfferAmount' => $counterOfferAmount 

    );

    $result = callVayaBiddingWebApiPost($jsonData);

    // var_dump($result);
    // exit;

    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('Placed successfully');";
        echo "window.location.href='../view/jobs ';";
      echo "</script>";
    } else {
      echo "<script>";
      echo "alert('".$result['Message']."');";
        // echo "alert('Please try again later');";
        echo "window.location.href='../view/jobs ';";
      echo "</script>";
    }
  }


//Arrive 

if (isset($_POST['arrive'])) {

  // receive all input values from the form
  $bidId =  filter_var($_GET['bidId'], FILTER_SANITIZE_NUMBER_INT);
  $iDriverId =  filter_var($_POST['iDriverId'], FILTER_SANITIZE_NUMBER_INT);
  $type = 'partnerArrived';

    $jsonData = array(
      'type'  => $type,
      'iDriverId'  => $iDriverId,
      'bidId'  => $bidId
    );

    $result = callVayaBiddingWebApiPost($jsonData);

    // var_dump($result);
    // exit;

    if ($result['Action'] == 1){
 
      echo "<script>";
        echo "alert('arrived successfully');";
        echo "window.location.href='../view/jobs ';";
      echo "</script>";
      
    } else {
      echo "<script>";
        // echo "alert('".$result['Message']."');";
        echo "alert('Please try again later');";
        echo "window.location.href='../view/jobs ';";
      echo "</script>";
    }
  }


//Start Trip

if (isset($_POST['startTrip'])) {

  // receive all input values from the form


  $bidId =  filter_var($_GET['bidId'], FILTER_SANITIZE_NUMBER_INT);
  $partnerId =  filter_var($_POST['iDriverId'], FILTER_SANITIZE_NUMBER_INT);
  $vehicleId = filter_var($_POST['idriverVehicleId'], FILTER_SANITIZE_NUMBER_INT);
  $departureTime  = "2020-12-03T07:53:16.243";
  $arrivalTime  = "2020-12-03T07:53:16.243";
  $type = 'startTrip';

// var_dump($bidId);
// var_dump($partnerId);
// var_dump($vehicleId);
// var_dump($departureTime);
// var_dump($arrivalTime);

    $jsonData = array(
      'type'  => $type,
      'partnerId'  => $partnerId,
      'vehicleId'  => $vehicleId,
      'departureTime'  => $departureTime,
      'arrivalTime'  => $arrivalTime,
      'bidId'  => $bidId
    );

    $result = callVayaBiddingWebApiPost($jsonData);


    // var_dump($result);
    // exit;

    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('started trip successfully');";
        echo "window.location.href='../view/jobs ';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/jobs ';";
      echo "</script>";
    }
  }



//endTrip

if (isset($_POST['endTrip'])) {

  // receive all input values from the form
  $driverId =  filter_var($_POST['iDriverId'], FILTER_SANITIZE_NUMBER_INT);
  $tripId = filter_var($_POST['tripId'], FILTER_SANITIZE_NUMBER_INT);
  $distanceCovered  = 600;
  $googleDistanceCovered  = 600;
  $type = 'endTrip';

// var_dump($driverId);
// var_dump($tripId);
// exit;
// var_dump($distanceCovered);
// var_dump($googleDistanceCovered);

    $jsonData = array(
      'type'  => $type,
      'driverId'  => $driverId,
      'tripId'  => $tripId,
      'distanceCovered'  => $distanceCovered,
      'googleDistanceCovered'  => $googleDistanceCovered
    );

    $result = callVayaBiddingWebApiPost($jsonData);


    // var_dump($result);
    // exit;

    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('trip ended successfully');";
        echo "window.location.href='../view/jobs ';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/jobs ';";
      echo "</script>";
    }
  

}

//confirmDeliveryCode

if (isset($_POST['confirmDeliveryCode'])) {

  // receive all input values from the form
  $deliveryCode =  filter_var($_POST['deliveryCode'], FILTER_SANITIZE_NUMBER_INT);
  $type = 'confirmDeliveryCode';

// var_dump($deliveryCode);
// exit;


    $jsonData = array(
      'type'  => $type,
      'deliveryCode'  => $deliveryCode
    );

    $result = callVayaBiddingWebApiPost($jsonData);


    // var_dump($result);
    // exit;

    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('deliveryCode confirmed successfully');";
        echo "window.location.href='../view/jobs ';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "alert('Please try again later');";
        echo "window.location.href='../view/jobs ';";
      echo "</script>";
    }
  
}

// ratingCustomer

if (isset($_POST['createRating'])) {

  // receive all input values from the form

  $userId =  filter_var($_POST['idriverId'], FILTER_SANITIZE_NUMBER_INT);
  $comment = filter_var($_POST['comment'], FILTER_SANITIZE_STRING);
  $scale = filter_var($_POST['scale'], FILTER_SANITIZE_NUMBER_INT);
  $tripId = filter_var($_POST['tripId'], FILTER_SANITIZE_NUMBER_INT);
  $userType  = "Driver";
  $type = 'createRating';

  // var_dump($userId);
  // var_dump($comment);
  // var_dump($scale);
  // var_dump($tripId);
  // exit;


    $jsonData = array(
      'type'  => $type,
      'userId'  => $userId,
      'comment'  => $comment,
      'scale'  => $scale,
      'tripId'  => $tripId,
      'userType'  => $userType
    );

    $result = callVayaBiddingWebApiPost($jsonData);

    // var_dump($result);
    // exit;


    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('Partner rated Successfully ');";
        echo "window.location='../view/jobs ';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        // echo "alert('Please try again later');";
        echo "window.location='../view/jobs ';";
      echo "</script>";
    }
  }


// partnerRegistration

if (isset($_POST['partnerRegistration'])) {

  // receive all input values from the form

  $phoneNumber =  filter_var($_POST['phoneNumber'], FILTER_SANITIZE_NUMBER_INT);
  $name = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
  $surname = filter_var($_POST['surname'], FILTER_SANITIZE_STRING);
  $vPassword = filter_var($_POST['vPassword'], FILTER_SANITIZE_STRING);
  $referralCode = filter_var($_POST['referralCode'], FILTER_SANITIZE_STRING);
  $vEmail = filter_var($_POST['vEmail'], FILTER_SANITIZE_EMAIL);

  $partnerType = 'Individual';
  $type = 'partnerRegistration';

 

    $jsonData = array(
      'type'  => $type,
      'partnerType'  => $Individual,
      'name'  => $name,
      'surname'  => $surname,
      'vEmail'  => $vEmail,
      'phoneNumber'  => $phoneNumber,
      'vPassword'  => $vPassword,
      'referralCode'  => $referralCode
    );

    $result = callVayaBiddingWebApiPost($jsonData);

    var_dump($result);
    exit;


    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('Partner registration successful, please use it to login');";
        echo "window.location.href='../../customer ';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../../transporter ';";
      echo "</script>";
    }
  }



//updatePaymentStatus

if (isset($_POST['updatePaymentStatus'])) {

  // receive all input values from the form

  $jobId =  filter_var($_POST['jobId'], FILTER_SANITIZE_NUMBER_INT);
  $paymentStatus  = "Paid";
  $type = 'updatePaymentStatus';


  // var_dump($amount);
  // var_dump($msisdn);
  // var_dump($jobId);

  // exit;


    $jsonData = array(
      'type'  => $type,
      'jobId'  => $jobId,
      'paymentStatus'  => $paymentStatus
    );

    $result = callVayaBiddingWebApiPost($jsonData);

    // var_dump($result);

    // exit;


    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('Cash payment confirmed successfully');";
        echo "window.location.href='../view/jobs ';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        echo "window.location.href='../view/jobs ';";
      echo "</script>";
    }
  }

// SME Registration

if (isset($_POST['companyRegistration'])) {

  // receive all input values from the form

  $vEmail =  filter_var($_POST['vEmail'], FILTER_SANITIZE_EMAIL);
  $vPhone =  filter_var($_POST['vPhone'], FILTER_SANITIZE_NUMBER_INT);
  $vPassword = filter_var($_POST['vPassword'], FILTER_SANITIZE_STRING);
  $name = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
  $vCaddress = filter_var($_POST['vCaddress'], FILTER_SANITIZE_STRING);
  $zimraBPNumber = filter_var($_POST['zimraBPNumber'], FILTER_SANITIZE_STRING);
  $countryCode = filter_var($_POST['countryCode'], FILTER_SANITIZE_NUMBER_INT);
  $phoneCode = filter_var($_POST['phoneCode'], FILTER_SANITIZE_NUMBER_INT);


  $type = 'companyRegistration';
  $userType  = "Driver";

  // var_dump($jobId);
  // var_dump($customerId);
  // exit;

    $jsonData = array(
      'type'  => $type,
      'name'  => $name,
      'vCaddress'  => $vCaddress,
      'vEmail'  => $vEmail,
      'vPhone'  => $vPhone,
      'vPassword'  => $vPassword,
      'phoneCode'  => $phoneCode,
      'countryCode'  => $countryCode,
      'zimraBPNumber'  => $zimraBPNumber,
      'userType'  => $userType
    );

    $result = callVayaBiddingWebApiPost($jsonData);

    // var_dump($result);
    // exit;


    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('partner SME registration successful, please use it to login');";
        echo "window.location.href='../../vaya-bidding/partner_login ';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        // echo "alert('Please try again later');";
        echo "window.location.href='../../vaya-bidding/transporter ';";
      echo "</script>";
    }
  }

// IndividualRegistration

if (isset($_POST['IndividualRegistration'])) {

  // receive all input values from the form

  $vEmail =  filter_var($_POST['vEmail'], FILTER_SANITIZE_EMAIL);
  $phoneNumber =  filter_var($_POST['phoneNumber'], FILTER_SANITIZE_NUMBER_INT);
  $name = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
  $surname = filter_var($_POST['surname'], FILTER_SANITIZE_STRING);
  $referralCode = filter_var($_POST['referralCode'], FILTER_SANITIZE_STRING);
  $phoneCode  = '+263';
  $countryCode  = 'ZW';
  $country  = 'ZW';
  $vPassword = filter_var($_POST['vPassword'], FILTER_SANITIZE_STRING);
  $vehicleId = filter_var($_POST['vehicleTypeId'], FILTER_SANITIZE_NUMBER_INT);
  $servicePillarId = filter_var($_POST['servicePillarId'], FILTER_SANITIZE_NUMBER_INT);


  $type = 'partnerRegistration';
  $partnerType = 'Individual';

  // var_dump($vehicleId);
  // exit;

    $jsonData = array(
      'type'  => $type,
      'name'  => $name,
      'surname'  => $surname,
      'vEmail'  => $vEmail,
      'phoneNumber'  => $phoneNumber,
      'servicePillarId'  => $servicePillarId,
      'referralCode'  => $referralCode,
      'vPassword'  => $vPassword,
      'phoneCode'  => $phoneCode,
      'countryCode'  => $countryCode,
      'country'  => $country,
      'vehicleId'  => $vehicleId,
      'partnerType'  => $partnerType
    );

    $result = callVayaBiddingWebApiPost($jsonData);

    // var_dump($result);
    // exit;


    if ($result['Action'] == 1){
      echo "<script>";
        echo "alert('Partner individual registration successful.');";
        echo "window.location.href='../../vaya-bidding/partner_login ';";
      echo "</script>";
    } else {
      echo "<script>";
        echo "alert('".$result['Message']."');";
        // echo "alert('Please try again later');";
        echo "window.location.href='../../vaya-bidding/transporter ';";
      echo "</script>";
    }
  }



  ?>