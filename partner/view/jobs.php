<?php

session_start();

if (!isset($_SESSION['idriverId'])) {
  header("Location: ../../vaya-bidding/partner_login ");
  exit();
}

$idriverId = $_SESSION["idriverId"];
$phoneNumber = $_SESSION["phoneNumber"];
$idriverVehicleId = $_SESSION["idriverVehicleId"];
$partnerType = $_SESSION["partnerType"];
$partnerName = $_SESSION["name"];
$partnerSurname = $_SESSION["surname"];

// if ($partnerType == "Individual") {
//    echo $_SESSION["name"] . ' ' . $_SESSION["surname"];
// } else {
//   echo  $_SESSION["name"];
// }




// session_start();
// if(!isset($_SESSION['userData']['id'], $_SESSION['userData']['username'], $_SESSION['userData']['userType'], $_SESSION["sess_Token"]))
// {
// 	echo "<script>";
//     echo "window.location.href='../login ?lmsg=true';";
//   echo "</script>";
// 	exit;
// }

require_once('../controller/bid_process.php');

$active_jobs = getAllJobs($idriverVehicleId);
$assigned_jobs = listAllAssignedJobsByiDriverId($idriverId);
$accepted_jobs = listAllAssignedJobsByiDriverId($idriverId);
$collected_jobs = listAllCollectedJobsByiDriverId($idriverId);
$intransit_jobs = listAllInTransitJobsByiDriverId($idriverId);
$delivered_jobs = listAllDeliveredJobsByiDriverId($idriverId);
$cancelled_jobs = listAllCancelledJobsByiDriverId($idriverId);
$drivers =  getAllDrivers();

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    VAYA eLogistics
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="../assets/css/material-dashboard.css" rel="stylesheet" />


</head>

<body>
  <div class="wrapper ">
    <div class="sidebar" data-color="blue" data-background-color="black" data-image="../assets/img/gomo.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo"><a href="#" class="simple-text logo-normal">
          VAYA eLogistics
        </a></div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <!-- <li class="nav-item ">
            <a class="nav-link" href="./dashboard ">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./trip_management ">
              <i class="material-icons">content_paste</i>
              <p>Trip Management</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./fleet_management ">
              <i class="material-icons">content_paste</i>
              <p>Fleet Management</p>
            </a>
          </li> -->

          <li class="nav-item active">
            <a class="nav-link" href="./jobs" referrerpolicy="no-referrer | same-origin | origin | strict-origin | no-origin-when-downgrading">
              <i class="material-icons">local_shipping</i>
              <p>Jobs</p>
            </a>
          </li>
          <!-- <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="material-icons">content_paste</i>
              Reports
            </a>
            <div style=" background: linear-gradient(60deg, #1C7ACD, #1C7ACD);color: #fff;" class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="#">RTGS Payment Report</a>
              <a class="dropdown-item" href="#">USD Payment Report</a>
              <a class="dropdown-item" href="#">Trips Report</a>
            </div>
          </li> -->
          <!-- <li class="nav-item">
            <a class="nav-link" href="./configurations ">
              <i class="material-icons">content_paste</i>
              <p>Configurations</p>
            </a>
          </li> -->
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
          <h4 style="margin-left: 45px; color: #1C7ACD"> Welcome (<?php echo $partnerName . ' ' . $partnerSurname;?>)</h4>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <!-- <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div> -->
            </form>
            <ul class="navbar-nav">
              <li class="nav-item">
              <?php
              echo $date = date('Y-m-d H:i:s');
               ?>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <!-- <a class="dropdown-item" href="#">Profile</a> -->
                  <!-- <a class="dropdown-item" href="#">Settings</a> -->
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="logout ">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-tabs card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <!-- <span class="nav-tabs-title">JOBS:</span> -->
                      <ul class="nav nav-tabs" data-tabs="tabs">
                        <li class="nav-item ">
                          <a class="nav-link active" href="#pending" data-toggle="tab">
                            <i class="material-icons">lens</i> PENDING
                            <div class="ripple-container"></div>
                          </a>
                        </li>

                        <!-- <li class="nav-item">
                          <a class="nav-link" href="#accepted" data-toggle="tab">
                            <i class="material-icons">person_outline</i>ACCEPTED
                            <div class="ripple-container"></div>
                          </a>
                        </li> -->
                        <li class="nav-item">
                          <a class="nav-link" href="#assigned" data-toggle="tab">
                            <i class="material-icons">person_outline</i>ASSIGNED
                            <div class="ripple-container"></div>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#collected" data-toggle="tab">
                            <i class="material-icons">check</i>COLLECTED
                            <div class="ripple-container"></div>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#in_transit" data-toggle="tab">
                            <i class="material-icons">local_shipping</i> IN TRANSIT
                            <div class="ripple-container"></div>
                          </a>
                        </li>

                        <li class="nav-item">
                          <a class="nav-link" href="#delivered" data-toggle="tab">
                            <i class="material-icons">home</i> DELIVERED
                            <div class="ripple-container"></div>
                          </a>
                        </li>

                        <li class="nav-item">
                          <a class="nav-link" href="#cancelled" data-toggle="tab">
                            <i class="material-icons">cancel</i>CANCELLED
                            <div class="ripple-container"></div>
                          </a>
                        </li>

                      </ul>
                    </div>
                  </div>
                </div>
                <div class="card-body">
                  <div class="tab-content">
                    <div class="tab-pane active card-content table-responsive table-full-width" id="pending">
                      <table id="dt-Pending" class="table table-sm  table-bordered table-hover table-striped ">
                        <thead class=" text-primary">
                          <th width="15%">Job Number</th>
                          <th width="15%">Pick-up Location</th>
                          <th width="15%">Drop-off Location</th>
                          <th width="10%">Bid-Start Time</th>
                          <th width="10%">Bid-End Time</th>
                          <th width="10%">Job Status</th>
                          <th width="10%">Offer Amount</th>
                          <th width="10%">Type Of Goods</th>
                          <th width="10%">Action</th>
                   

                        </thead>
                        <tbody>

                          <?php if ($active_jobs['Action'] == 1) : ?>

                            <?php
                            $active_jobs['result']['content']  = isset($active_jobs['result']['content']) ?  $active_jobs['result']['content']  : '';
                            $active_jobs['result']['content'] = is_array($active_jobs['result']['content']) ?  $active_jobs['result']['content'] : array();
                            foreach ($active_jobs['result']['content'] as $active_job) :

                            ?>
                              <tr>
                                <td><?= $active_job['jobId'] ?></td>
                               <td><?= $active_job['pickupAddress'] ?></td>
                                <td><?= $active_job['dropOffAddress'] ?></td>
                                <td><?= $active_job['bidStartTime'] ?></td>
                                <td><?= $active_job['bidEndTime'] ?></td>
                                <td><?= $active_job['jobStatus'] ?></td>
                                <td><?=getCurrencyById($active_job['currencyId'])['result']['name'] . getCurrencyById($active_job['currencyId'])['result']['symbol'] . ' '. $active_job['amountOffer'] ?></td>
                                <td><?= $active_job['typeOfGoods'] ?></td>
                                <td>
                                <button type="button" rel="tooltip" title="View" class="btn btn-primary btn-link btn-sm">
                                  <i  data-target="#job<?= $active_job['jobId'] ?>" data-toggle="modal" class="material-icons">lens</i>
                                </button>
                              </td>
                              </tr>

                              <!-- Job Details Modal  -->
                              <div class="modal fade" id="job<?= $active_job['jobId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Job Details</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?jobId=<?= $active_job['jobId'] ?>" method="post" enctype="multipart/form-data">
                                      <div class="modal-body">
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control" placeholder="Job No" value="<?= $active_job['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Type Of Goods:</label>
                                            <input type="text" class="form-control" placeholder="Type of goods" value="<?= $active_job['typeOfGoods'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Offer Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($active_job['currencyId'])['result']['name'] . getCurrencyById($active_job['currencyId'])['result']['symbol'] . ' '. $active_job['amountOffer'] ?>" readonly>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" data-target="#counter<?= $active_job['jobId'] ?>" data-toggle="modal" class="btn btn-primary">Counter Offer</button>
                                        <button type="button" data-target="#reject<?= $active_job['jobId'] ?>" data-toggle="modal" class="btn btn-secondary">Reject Job</button>
                                        <button type="button" data-target="#accept<?= $active_job['jobId'] ?>" data-toggle="modal" class="btn btn-primary">Accept Job</button>

                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->

                              <!-- Counter Offer Modal  -->
                              <div class="modal fade" id="counter<?= $active_job['jobId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Partner Counter Offer</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?jobId=<?= $active_job['jobId'] ?>" method="POST">
                                      <div class="modal-body">
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control" placeholder="Job No" value="<?= $active_job['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Type Of Goods:</label>
                                            <input type="text" class="form-control" placeholder="Type of goods" value="<?= $active_job['typeOfGoods'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Offer Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($active_job['currencyId'])['result']['name'] . getCurrencyById($active_job['currencyId'])['result']['symbol'] . ' '. $active_job['amountOffer'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="form-group">
                                            <label for="counterOfferAmount" class="control-label col-sm-4">Counter Offer:</label>
                                            <input type="number" pattern="[0-9]+([\.,][0-9]+)?" step="0.01"  title="This should be a number with up to 2 decimal places." class="form-control" name="counterOfferAmount" required>
                                          </div>
                                        </div>
                                        <input type="hidden" class="form-control" name="iDriverId" value="<?php echo $idriverId; ?>" >
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <input type="submit" name="counter" class="btn btn-primary" value="Place Offer" />
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->

                              <!-- Reject Job Modal  -->
                              <div class="modal fade" id="reject<?= $active_job['jobId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Reject Job</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?jobId=<?= $active_job['jobId'] ?>" method="post" enctype="multipart/form-data">
                                      <div class="modal-body">
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control" placeholder="Job No" value="<?= $active_job['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Type Of Goods:</label>
                                            <input type="text" class="form-control" placeholder="Type of goods" value="<?= $active_job['typeOfGoods'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Offer Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($active_job['currencyId'])['result']['name'] . getCurrencyById($active_job['currencyId'])['result']['symbol'] . ' '. $active_job['amountOffer'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="form-group">
                                            <label for="rejectReason" class="control-label col-sm-4">Reject Reason:</label>
                                            <textarea class="form-control" name="rejectReason" rows="3" pattern="[A-Za-z0-9]+" title="Letters and numbers only, no punctuation or special characters" required></textarea>
                                          </div>
                                        </div>
                                        <input type="hidden" class="form-control" name="iDriverId" value="<?php echo $idriverId; ?>" readonly >
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <input type="submit" name="reject" class="btn btn-primary" value="Confirm" />
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->

                              <!-- Accept Job Modal  -->
                              <div class="modal fade" id="accept<?= $active_job['jobId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Accept Job</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?jobId=<?= $active_job['jobId'] ?>" method="post" enctype="multipart/form-data">
                                      <div class="modal-body">

                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control" placeholder="Job No" value="<?= $active_job['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Type Of Goods:</label>
                                            <input type="text" class="form-control" placeholder="Type of goods" value="<?= $active_job['typeOfGoods'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Offer Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($active_job['currencyId'])['result']['name'] . getCurrencyById($active_job['currencyId'])['result']['symbol'] . ' '. $active_job['amountOffer'] ?>" readonly>
                                          </div>
                                        </div>
                                        <input type="hidden" class="form-control" name="iDriverId" value="<?php echo $idriverId; ?>" >
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <input type="submit" name="accept" class="btn btn-primary" value="Confirm" />
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->

                            <?php endforeach; ?>

                          <?php else : ?>

                            <?= $active_jobs['Message'] ?>

                          <?php endif; ?>

                        </tbody>
                      </table>
                    </div>
                 
                     <!--assigned-->
                     <div class="tab-pane table-responsive card-content table-responsive table-full-width" id="assigned">
                      <table id="dt-assigned" width="150%" class="table table-sm table-bordered table-hover table-striped ">
                        <thead class=" text-primary">
                          <th width="15">Bid Number</th>
                          <th width="15%">job Number</th>
                          <th width="13%">Payment Method</th>
                          <th width="13%">Payment Status</th>
                          <th width="13%">Offer Amount</th>
                          <th width="13%">bidAmount</th>
                          <th width="15%">Pick-up Location</th>
                          <th width="15%">Drop-off Location</th>
                          <th width="15%">Driver Name</th>
                          <th width="13%">Action</th>
                        </thead>
                        <tbody>

                          <?php if ($assigned_jobs['Action'] == 1) : ?>

                            <?php

                            $assigned_jobs['result']['content']  = isset($assigned_jobs['result']['content'] ) ?   $assigned_jobs['result']['content']  : '';
                            $assigned_jobs['result']['content']  = is_array($assigned_jobs['result']['content'] ) ? $assigned_jobs['result']['content']  : array();
                            foreach ($assigned_jobs['result']['content']  as $assigned_job) :

                            ?>
                              <tr>
                                <td><?= $assigned_job['bidId'] ?></td>
                                <td><?= $assigned_job['jobId'] ?></td>
                                <td><?= $assigned_job['paymentMode'] ?></td>
                                <td><?= $assigned_job['paymentStatus'] ?></td>
                                <td><?=getCurrencyById($assigned_job['currencyId'])['result']['name'] . getCurrencyById($assigned_job['currencyId'])['result']['symbol'] . ' '. $assigned_job['offerAmount'] ?></td>
                                <td><?=getCurrencyById($assigned_job['currencyId'])['result']['name'] . getCurrencyById($assigned_job['currencyId'])['result']['symbol'] . ' '. $assigned_job['bidAmount'] ?></td>
                                <td><?= $assigned_job['pickupAddress'] ?></td>
                                <td><?= $assigned_job['dropOffAddress'] ?></td> 
                                <td><?= $assigned_job['partnerName'] ?></td> 
                                <td>
                                <button type="button" rel="tooltip" title="View" class="btn btn-primary btn-link btn-sm">
                                 <i  data-target="#bida<?= $assigned_job['bidId'] ?>" data-toggle="modal" class="material-icons">lens</i>
                                </button>
    
                              </td>
                      
                              </tr>

                              <!-- Job Details Modal  -->
                              <div class="modal fade" id="bida<?= $assigned_job['bidId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Job Details</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?bidId=<?= $assigned_job['bidId'] ?>" method="post" enctype="multipart/form-data">
                                      <div class="modal-body">
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid No:</label>
                                            <input type="text" class="form-control" placeholder="Bid No" value="<?= $assigned_job['bidId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control" placeholder="Job No" value="<?= $assigned_job['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Amount:</label>
                                            <input type="text" class="form-control" placeholder="Bid Amount" value="<?= getCurrencyById($assigned_job['currencyId'])['result']['name'] . getCurrencyById($assigned_job['currencyId'])['result']['symbol'] . ' '. $assigned_job['bidAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Offer Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($assigned_job['currencyId'])['result']['name'] . getCurrencyById($assigned_job['currencyId'])['result']['symbol'] . ' '. $assigned_job['offerAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" data-target="#arrive<?= $assigned_job['bidId'] ?>" data-toggle="modal" class="btn btn-primary">Arrive</button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->

                          
                              <!-- Arrive  Modal  -->
                              <div class="modal fade" id="arrive<?= $assigned_job['bidId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Arrive</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?bidId=<?= $assigned_job['bidId'] ?>" method="post" enctype="multipart/form-data">
                                      <div class="modal-body">

                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid No:</label>
                                            <input type="text" class="form-control" placeholder="Bid No" value="<?= $assigned_job['bidId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control" placeholder="Job No" value="<?= $assigned_job['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Amount:</label>
                                            <input type="text" class="form-control" placeholder="Bid Amount" value="<?= getCurrencyById($assigned_job['currencyId'])['result']['name'] . getCurrencyById($assigned_job['currencyId'])['result']['symbol'] . ' '. $assigned_job['bidAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Offer Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($assigned_job['currencyId'])['result']['name'] . getCurrencyById($assigned_job['currencyId'])['result']['symbol'] . ' '. $assigned_job['offerAmount'] ?>"readonly>
                                          </div>
                                        </div>
                                        <input type="hidden" class="form-control" name="iDriverId" value="<?php echo $idriverId; ?>" >
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <input type="submit" name="arrive"  class="btn btn-primary" value="Confirm" />
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->

                            <?php endforeach; ?>

                          <?php else : ?>

                            <?= $assigned_jobs['Message'] ?>

                          <?php endif; ?>

                        </tbody>
                      </table>
                    </div>
                    <div class="tab-pane card-content table-responsive table-full-width" id="collected">
                      <table id="dt-collected" width="140%" class="table table-sm table-bordered table-hover table-striped ">
                        <thead class=" text-primary">
                          <th width="15">Bid Number</th>
                          <th width="15%">job Number</th>
                          <th width="13%">Payment Method</th>
                          <th width="13%">Payment Status</th>
                          <th width="13%">Offer Amount</th>
                          <th width="13%">bidAmount</th>
                          <th width="15%">Pick-up Location</th>
                          <th width="15%">Drop-off Location</th>
                          <th width="13%">Action</th>
                        </thead>
                        <tbody>

                          <?php if ($collected_jobs['Action'] == 1) : ?>

                            <?php
                            $collected_jobs['result']['content']  = isset($collected_jobs['result']['content'] ) ?   $collected_jobs['result']['content']  : '';
                            $collected_jobs['result']['content']  = is_array($collected_jobs['result']['content'] ) ? $collected_jobs['result']['content']  : array();
                            foreach ($collected_jobs['result']['content']  as $collected_job) :

                            ?>
                              <tr >
                                <td><?= $collected_job['bidId'] ?></td>
                                <td><?= $collected_job['jobId'] ?></td>
                                <td><?= $collected_job['paymentMode'] ?></td>
                                <td><?= $collected_job['paymentStatus'] ?></td>
                                <td><?=getCurrencyById($collected_job['currencyId'])['result']['name'] . getCurrencyById($collected_job['currencyId'])['result']['symbol'] . ' '. $collected_job['offerAmount'] ?></td>
                                <td><?=getCurrencyById($collected_job['currencyId'])['result']['name'] . getCurrencyById($collected_job['currencyId'])['result']['symbol'] . ' '. $collected_job['bidAmount'] ?></td>
                                <td><?= $collected_job['pickupAddress'] ?></td>
                                <td><?= $collected_job['dropOffAddress'] ?></td> 
                                <td>
                                <button type="button" rel="tooltip" title="View" class="btn btn-primary btn-link btn-sm">
                                  <i data-target="#bidc<?= $collected_job['bidId'] ?>" data-toggle="modal" class="material-icons">lens</i>
                                </button>
    
                              </td>
                         
                              </tr>

                              <!-- Job Details Modal  -->
                              <div class="modal fade" id="bidc<?= $collected_job['bidId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Job Details</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?bidId=<?= $collected_job['bidId'] ?>" method="post" enctype="multipart/form-data">
                                      <div class="modal-body">
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid No:</label>
                                            <input type="text" class="form-control" placeholder="Bid No" value="<?= $collected_job['bidId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control" placeholder="Job No" value="<?= $collected_job['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Amount:</label>
                                            <input type="text" class="form-control" placeholder="Bid Amount" value="<?= getCurrencyById($collected_job['currencyId'])['result']['name'] . getCurrencyById($collected_job['currencyId'])['result']['symbol'] . ' '. $collected_job['bidAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Offer Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($collected_job['currencyId'])['result']['name'] . getCurrencyById($collected_job['currencyId'])['result']['symbol'] . ' '. $collected_job['offerAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="modal-footer">

                                      <?php
                                        if($collected_job['paymentStatus'] == "Paid"){

                                         echo "<button type='button' class='btn btn-primary'   data-toggle='modal' data-target='#startTrip" . $collected_job['bidId'] . "' >START TRIP</button>";            

                                        }
                                        else {

                                          echo "<h4 class='text-center text-primary'>Cannot start trip because payment Has not yet been made.</h4>"; 
                                                
                                      
                                        }
                                        ?>
                              
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                          <!--end modal-->
                           <!-- Cash Modal  -->
                          <div class="modal fade" id="cashPayment<?= $collected_job['bidId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog w-40">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Cash Payment Confirmation</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?bidId=<?= $collected_job['bidId'] ?>" method="POST">
                                      <div class="modal-body">
                                 
                                      <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job Status:</label>
                                            <input type="text" class="form-control" placeholder="Job Status" value="<?= $collected_job['bidStatus'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">pickUpLocation:</label>
                                            <input type="text" class="form-control" value="<?=  $collected_job['pickupAddress'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">dropOffLocation:</label>
                                            <input type="text" class="form-control"  value="<?= $collected_job['dropOffAddress'] ?>" readonly>
                                          </div>
                                        </div>
                    
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Offer Amount:</label>
                                            <input type="text" class="form-control"  value="<?= getCurrencyById($collected_job['currencyId'])['result']['name'] . getCurrencyById($collected_job['currencyId'])['result']['symbol'] . ' '. $collected_job['offerAmount'] ?>" readonly>
                                          </div>
                                        </div>                  
                                        <input type="hidden" class="form-control" name="jobId" value="<?= $collected_job['jobId'] ?>">
                                
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <input type="submit" name="updatePaymentStatus" class="btn btn-primary" value="Confirm" />
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->

                              <!-- startTrip  Modal  -->
                              <div class="modal fade" id="startTrip<?= $collected_job['bidId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Start Trip</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?bidId=<?= $collected_job['bidId'] ?>" method="post" enctype="multipart/form-data">
                                      <div class="modal-body">

                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid No:</label>
                                            <input type="text" class="form-control" placeholder="Bid No" value="<?= $collected_job['bidId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control" placeholder="Job No" value="<?= $collected_job['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Amount:</label>
                                            <input type="text" class="form-control" placeholder="Bid Amount" value="<?= getCurrencyById($collected_job['currencyId'])['result']['name'] . getCurrencyById($collected_job['currencyId'])['result']['symbol'] . ' '. $collected_job['bidAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Offer Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($collected_job['currencyId'])['result']['name'] . getCurrencyById($collected_job['currencyId'])['result']['symbol'] . ' '. $collected_job['offerAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                        <input type="hidden" class="form-control" name="iDriverId" value="<?php echo $idriverId; ?>" >
                                        <input type="hidden" class="form-control" name="idriverVehicleId" value="<?php echo $idriverVehicleId; ?>" >
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <input type="submit" name="startTrip" class="btn btn-primary" value="Confirm" />
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->
                            <?php endforeach; ?>

                          <?php else : ?>

                            <?= $collected_jobs['Message'] ?>

                          <?php endif; ?>

                        </tbody>
                      </table>
                    </div>
                    
                    <div class="tab-pane card-content table-responsive table-full-width" id="in_transit">
                      <table id="dt-intransit" width="140%" class="table  table-sm table-bordered table-hover table-striped ">
                        <thead class=" text-primary">
                          <th width="15">Bid Number</th>
                          <th width="15%">job Number</th>
                          <th width="13%">Payment Method</th>
                          <th width="13%">Payment Status</th>
                          <th width="13%">Offer Amount</th>
                          <th width="13%">bidAmount</th>
                          <th width="15%">Pick-up Location</th>
                          <th width="15%">Drop-off Location</th>
                          <th width="13%">Action</th>
                        </thead>
                        <tbody>

                          <?php if ($intransit_jobs['Action'] == 1) : ?>

                            <?php
                            $intransit_jobs['result']['content']  = isset($intransit_jobs['result']['content'] ) ?   $intransit_jobs['result']['content']  : '';
                            $intransit_jobs['result']['content']  = is_array($intransit_jobs['result']['content'] ) ? $intransit_jobs['result']['content']  : array();
                            foreach ($intransit_jobs['result']['content']  as $intransit_job) :

                            ?>
                              <tr>
                                <td><?= $intransit_job['bidId'] ?></td>
                                <td><?= $intransit_job['jobId'] ?></td>
                                <td><?= $intransit_job['paymentMode'] ?></td>
                                <td><?= $intransit_job['paymentStatus'] ?></td>
                                <td><?=getCurrencyById($intransit_job['currencyId'])['result']['name'] . getCurrencyById($intransit_job['currencyId'])['result']['symbol'] . ' '. $intransit_job['offerAmount'] ?></td>
                                <td><?=getCurrencyById($intransit_job['currencyId'])['result']['name'] . getCurrencyById($intransit_job['currencyId'])['result']['symbol'] . ' '. $intransit_job['bidAmount'] ?></td>
                            
                                <td><?= $intransit_job['pickupAddress'] ?></td>
                                <td><?= $intransit_job['dropOffAddress'] ?></td> 
                                <td>
                                <button type="button" rel="tooltip" title="View" class="btn btn-primary btn-link btn-sm">
                                  <i data-target="#bidi<?= $intransit_job['bidId'] ?>" data-toggle="modal" class="material-icons">lens</i>
                                </button>
    
                              </td>
                              </tr>

                              <!-- Job Details Modal  -->
                              <div class="modal fade" id="bidi<?= $intransit_job['bidId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Job Details</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?bidId=<?= $intransit_job['bidId'] ?>" method="post" enctype="multipart/form-data">
                                      <div class="modal-body">
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid No:</label>
                                            <input type="text" class="form-control" placeholder="Bid No" value="<?= $intransit_job['bidId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control" placeholder="Job No" value="<?= $intransit_job['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Amount:</label>
                                            <input type="text" class="form-control" placeholder="Bid Amount" value="<?= getCurrencyById($intransit_job['currencyId'])['result']['name'] . getCurrencyById($intransit_job['currencyId'])['result']['symbol'] . ' '. $intransit_job['bidAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Offer Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($intransit_job['currencyId'])['result']['name'] . getCurrencyById($intransit_job['currencyId'])['result']['symbol'] . ' '. $intransit_job['offerAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                        <!-- <button type="button" data-target="#trackjob<?= $intransit_job['bidId'] ?>" data-toggle="modal" class="btn btn-secondary">Tracking</button> -->
                        
                                        <!-- <button type="button" data-target="#help<?= $intransit_job['bidId'] ?>" data-toggle="modal" class="btn btn-secondary">Help</button> -->


                                        <button type="button" data-target="#endTrip<?= $intransit_job['bidId'] ?>" data-toggle="modal" class="btn btn-primary">End Trip</button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->
                                 <!-- Help  Modal  -->
                                <div class="modal fade" id="help<?= $intransit_job['bidId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Partner Contact Details</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?bidId=<?= $intransit_job['bidId'] ?>" method="post" enctype="multipart/form-data">
                                      <div class="modal-body">

                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid No:</label>
                                            <input type="text" class="form-control" placeholder="Bid No" value="<?= $intransit_job['bidId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control" placeholder="Job No" value="<?= $intransit_job['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Amount:</label>
                                            <input type="text" class="form-control" placeholder="Bid Amount" value="<?= getCurrencyById($intransit_job['currencyId'])['result']['name'] . getCurrencyById($intransit_job['currencyId'])['result']['symbol'] . ' '. $intransit_job['bidAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Offer Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($intransit_job['currencyId'])['result']['name'] . getCurrencyById($intransit_job['currencyId'])['result']['symbol'] . ' '. $intransit_job['offerAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                        <input type="hidden" class="form-control" name="tripId" value="<?= $intransit_job['tripId'] ?>">
                                        <input type="hidden" class="form-control" name="iDriverId" value="<?php echo $idriverId; ?>" readonly >
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <input type="submit" name="endTrip" class="btn btn-primary" value="New Vehicle" />
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->

                             <!-- Re-Assign Driver  Modal  -->
                              <div class="modal fade" id="assign<?= $intransit_job['bidId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Driver Details</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?bidId=<?= $intransit_job['bidId'] ?>" method="post" enctype="multipart/form-data">
                                      <div class="modal-body">

                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid No:</label>
                                            <input type="text" class="form-control" placeholder="Bid No" value="<?= $intransit_job['bidId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control" placeholder="Job No" value="<?= $intransit_job['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Amount:</label>
                                            <input type="text" class="form-control" placeholder="Bid Amount" value="<?= getCurrencyById($intransit_job['currencyId'])['result']['name'] . getCurrencyById($intransit_job['currencyId'])['result']['symbol'] . ' '. $intransit_job['bidAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Offer Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($intransit_job['currencyId'])['result']['name'] . getCurrencyById($intransit_job['currencyId'])['result']['symbol'] . ' '. $intransit_job['offerAmount'] ?>"readonly>
                                          </div>
                                        </div>
                                        <input type="hidden" class="form-control" name="iDriverId" value="<?php echo $idriverId; ?>"  >
                                        <input type="hidden" class="form-control" name="bidId" value="<?= $intransit_job['bidId'] ?>" >
                        
                                     
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                       
                                        <input type="hidden" class="form-control" name="assignDriver" value="true" >
                                        <button type="button" name ="assignDriver" onClick="assignDriver()"  class="btn btn-primary"> Confirm </button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->
                              <!-- endTrip  Modal  -->
                              <div class="modal fade" id="endTrip<?= $intransit_job['bidId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">End Trip</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?bidId=<?= $intransit_job['bidId'] ?>" method="post" enctype="multipart/form-data">
                                      <div class="modal-body">

                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid No:</label>
                                            <input type="text" class="form-control" placeholder="Bid No" value="<?= $intransit_job['bidId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control" placeholder="Job No" value="<?= $intransit_job['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Amount:</label>
                                            <input type="text" class="form-control" placeholder="Bid Amount" value="<?= getCurrencyById($intransit_job['currencyId'])['result']['name'] . getCurrencyById($intransit_job['currencyId'])['result']['symbol'] . ' '. $intransit_job['bidAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Offer Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($intransit_job['currencyId'])['result']['name'] . getCurrencyById($intransit_job['currencyId'])['result']['symbol'] . ' '. $intransit_job['offerAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                        <input type="hidden" class="form-control" name="tripId" value="<?= $intransit_job['tripId'] ?>">
                                        <input type="hidden" class="form-control" name="iDriverId" value="<?php echo $idriverId; ?>" readonly >
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <input type="submit" name="endTrip" class="btn btn-primary" value="Confirm" />
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->

                              <!-- confirmDeliveryCode & rating Modal  -->
                              <div class="modal fade" id="confirmDeliveryCode<?= $intransit_job['bidId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Confirm Delivery</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?bidId=<?= $intransit_job['bidId'] ?>" method="post" enctype="multipart/form-data">
                                      <div class="modal-body">

                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Trip No:</label>
                                            <input type="text" class="form-control" placeholder="Bid No" value="<?= $intransit_job['tripId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">pickUpLocation:</label>
                                            <input type="text" class="form-control" placeholder="Job No" value="<?=  $intransit_job['pickupAddress'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">dropOffLocation:</label>
                                            <input type="text" class="form-control" placeholder="Bid Amount" value="<?= $intransit_job['dropOffAddress'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Total Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($intransit_job['currencyId'])['result']['name'] . getCurrencyById($intransit_job['currencyId'])['result']['symbol'] . ' '. $intransit_job['offerAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Total Distance:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= $intransit_job['offerAmount'] ?>km" readonly>
                                          </div>
                                        </div>

                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Delivery Code:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" name="deliveryCode">
                                          </div>
                                        </div>

                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Comment:</label>
                                            <textarea class="form-control" name="rejectReason" rows="3" required></textarea>
                                          </div>
                                        </div>

                                        <input type="hidden" class="form-control" name="tripId" value="<?= $intransit_job['tripId'] ?>">
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <input type="submit" name="confirmDeliveryCode" class="btn btn-primary" value="Confirm" />
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <form name="currentLocation" id="currentLocation" action="../controller/bid_process" method="post">
                                <input type="hidden" class="form-control" name="tripId" value="<?= $intransit_job['tripId'] ?>">
                                <input type="hidden" class="form-control" name="longitude" id="long">
                                <input type="hidden" class="form-control" name="latitude" id="lat">
                              </form>

                              <!--end modal-->

                              <!-- Real Time Tracking  Modal  -->
                              <div class="modal fade" id="trackjob<?= $intransit_job['bidId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog  w-40">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Real Time Tracking</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process ?bidId=<?= $intransit_job['bidId'] ?>" method="post">
                                      <div class="modal-body">
                                        <input type="hidden" id="start1" value="<?=  $intransit_job['pickupAddress'] ?>"><!-- Starting Location -->

                                        <input type="hidden" id="end1" value="<?=  $intransit_job['dropOffAddress'] ?>"><!-- Ending Location -->

                                        <div class="container" id="map" style="height: 380px;width: 400px;"></div>
                                        <input type="hidden" id="myInput" class="form-control" name="tripId" value="<?= $intransit_job['tripId'] ?>">
                                      </div>

                                      <div class="modal-footer">

                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                              <!--end modal-->
                            <?php endforeach; ?>

                          <?php else : ?>

                            <?= $intransit_jobs['Message'] ?>

                          <?php endif; ?>

                        </tbody>
                      </table>
                    </div>
                    <div class="tab-pane card-content table-responsive table-full-width" id="delivered">
                      <table id="dt-delivered" width="140%" class="table table-sm table-bordered table-hover table-striped ">
                        <thead class=" text-primary">
                           <th width="15">Bid Number</th>
                          <th width="15%">job Number</th>
                          <th width="13%">Payment Method</th>
                          <th width="13%">Payment Status</th>
                          <th width="13%">Offer Amount</th>
                          <th width="13%">bidAmount</th>
                          <th width="15%">Pick-up Location</th>
                          <th width="15%">Drop-off Location</th>
                          <th width="15%">Action</th>
                     
                        </thead>
                        <tbody>

                          <?php if ($delivered_jobs['Action'] == 1) : ?>

                            <?php
                            $delivered_jobs['result']['content']  = isset($delivered_jobs['result']['content'] ) ?   $delivered_jobs['result']['content'] : '';
                            $delivered_jobs['result']['content']  = is_array($delivered_jobs['result']['content'] ) ? $delivered_jobs['result']['content']  : array();
                            foreach ($delivered_jobs['result']['content']  as $delivered_job) :

                            ?>
                              <tr>
                                <td><?= $delivered_job['bidId'] ?></td>
                                <td><?= $delivered_job['jobId'] ?></td>
                                <td><?= $delivered_job['paymentMode'] ?></td>
                                <td><?= $delivered_job['paymentStatus'] ?></td>
                                <td><?=getCurrencyById($delivered_job['currencyId'])['result']['name'] . getCurrencyById($delivered_job['currencyId'])['result']['symbol'] . ' '. $delivered_job['offerAmount'] ?></td>
                                <td><?=getCurrencyById($delivered_job['currencyId'])['result']['name'] . getCurrencyById($delivered_job['currencyId'])['result']['symbol'] . ' '. $delivered_job['bidAmount'] ?></td>
                                <td><?= $delivered_job['pickupAddress'] ?></td>
                                <td><?= $delivered_job['dropOffAddress'] ?></td> 
                                <td>
                                <button type="button" rel="tooltip" title="View" class="btn btn-primary btn-link btn-sm">
                                  <i data-target="#bidd<?= $delivered_job['bidId'] ?>" data-toggle="modal" class="material-icons">lens</i>
                                </button>
    
                              </td>
                         
                              </tr>
                         <!-- Job Details Modal  -->
                           <div class="modal fade" id="bidd<?= $delivered_job['bidId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                 <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Job Details</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?bidId=<?= $delivered_job['bidId'] ?>" method="post" enctype="multipart/form-data">
                                      <div class="modal-body">
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid No:</label>
                                            <input type="text" class="form-control" placeholder="Bid No" value="<?= $delivered_job['bidId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control" placeholder="Job No" value="<?= $delivered_job['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <!-- <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Amount:</label>
                                            <input type="text" class="form-control" placeholder="Bid Amount" value="<?= getCurrencyById($delivered_job['currencyId'])['result']['name'] . getCurrencyById($delivered_job['currencyId'])['result']['symbol'] . ' '. $delivered_job['bidAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Offer Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($delivered_job['currencyId'])['result']['name'] . getCurrencyById($delivered_job['currencyId'])['result']['symbol'] . ' '. $delivered_job['offerAmount'] ?>" readonly>
                                          </div>
                                        </div> -->
                                      </div>
                                      <div class="modal-footer">
                                        <!-- <button type="button" data-target="#ratecustomer<?= $delivered_job['bidId'] ?>" data-toggle="modal" class="btn btn-secondary">Rate Customer</button> -->

                                        <button type="button" data-target="#confirmDeliveryCode<?= $delivered_job['bidId'] ?>" data-toggle="modal" class="btn btn-primary">Delivery Code</button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->

                              <!--Modal -->
                           
                              <!-- confirmDeliveryCode & rating Modal  -->
                              <div class="modal fade" id="confirmDeliveryCode<?= $delivered_job['bidId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Confirm Delivery</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?bidId=<?= $delivered_job['bidId'] ?>" method="post" enctype="multipart/form-data">
                                      <div class="modal-body">

                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Trip No:</label>
                                            <input type="text" class="form-control" placeholder="Bid No" value="<?= $delivered_job['tripId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">pickUpLocation:</label>
                                            <input type="text" class="form-control" placeholder="Job No" value="<?=  $delivered_job['pickupAddress'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">dropOffLocation:</label>
                                            <input type="text" class="form-control" placeholder="Bid Amount" value="<?= $delivered_job['dropOffAddress'] ?>" readonly>
                                          </div>
                                        </div>
                                        <!-- <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Total Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($delivered_job['currencyId'])['result']['name'] . getCurrencyById($delivered_job['currencyId'])['result']['symbol'] . ' '. $delivered_job['offerAmount'] ?>" readonly>
                                          </div>
                                        </div> -->
            
                                        <div class="col">
                                          <div class="form-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Delivery Code:</label>
                                            <input type="text" class="form-control"  pattern="[0-9]+" title="please enter number only" name="deliveryCode">
                                          </div>
                                        </div>

                                        <input type="hidden" class="form-control" name="tripId" value="<?= $delivered_job['tripId'] ?>">
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <input type="submit" name="confirmDeliveryCode" class="btn btn-primary" value="Confirm" />
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->
                                         <!--Modal -->
                           
                              <!-- ratecustomer -->
                              <div class="modal fade" id="ratecustomer<?= $delivered_job['bidId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Rate Customer</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?bidId=<?= $delivered_job['bidId'] ?>" method="post" enctype="multipart/form-data">
                                      <div class="modal-body">

                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Trip No:</label>
                                            <input type="text" class="form-control" placeholder="Bid No" value="<?= $delivered_job['tripId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">pickUpLocation:</label>
                                            <input type="text" class="form-control" placeholder="Job No" value="<?=  $delivered_job['pickupAddress'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">dropOffLocation:</label>
                                            <input type="text" class="form-control" placeholder="Bid Amount" value="<?= $delivered_job['dropOffAddress'] ?>" readonly>
                                          </div>
                                        </div>
                                        <!-- <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Total Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($delivered_job['currencyId'])['result']['name'] . getCurrencyById($delivered_job['currencyId'])['result']['symbol'] . ' '. $delivered_job['offerAmount'] ?>" readonly>
                                          </div>
                                        </div> -->
                                        <div class="col">
                                          <div class="input-group">
                                      
                                            <label for="input-2" class="control-label col-sm-4">Rating:</label>
                                            <!-- <input type="number"  class="rating" name="scale" data-size="lg" title=""> -->
                                            <select class="form-control" name="scale">
                                              <option value ="1">1</option>
                                              <option value ="2">2</option>
                                              <option value ="3">3</option>
                                              <option value ="4">4</option>
                                              <option value ="5">5</option>
                                            </select>
                                          </div>
                                        </div> 
                                        <div class="col">
                                          <div class="form-group">
                                            <label for="rejectReason" class="control-label col-sm-4">Comment:</label>
                                            <textarea class="form-control" name="comment" pattern="[A-Za-z0-9]+" title="Letters and numbers only, no punctuation or special characters" rows="3" required></textarea>
                                          </div>
                                        </div>


                                        <input type="hidden" class="form-control" name="tripId" value="<?= $delivered_job['tripId'] ?>">
                                        
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <input type="submit" name="confirmDeliveryCode" class="btn btn-primary" value="Confirm" />
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->
                            <?php endforeach; ?>

                          <?php else : ?>

                            <?= $delivered_jobs['Message'] ?>

                          <?php endif; ?>

                        </tbody>
                      </table>
                    </div>
                    <div class="tab-pane card-content table-responsive table-full-width" id="cancelled">
                      <table id="dt-cancelled" width="140%" class="table table-sm  table-bordered table-hover table-striped ">
                        <thead class=" text-primary">
                          <th width="15">Bid Number</th>
                          <th width="15%">job Number</th>
                          <th width="13%">Payment Method</th>
                          <th width="13%">Payment Status</th>
                          <th width="13%">Offer Amount</th>
                          <!-- <th width="13%">bidAmount</th> -->
                          <th width="15%">Pick-up Location</th>
                          <th width="15%">Drop-off Location</th>
                          <!-- <th width="13%">Action</th> -->
                        </thead>
                        <tbody>

                          <?php if ($cancelled_jobs['Action'] == 1) : ?>

                            <?php
                            $cancelled_jobs['result']['content']  = isset($cancelled_jobs['result']['content'] ) ?   $cancelled_jobs['result']['content']  : '';
                            $cancelled_jobs['result']['content']  = is_array($cancelled_jobs['result']['content'] ) ? $cancelled_jobs['result']['content']  : array();
                            foreach ($cancelled_jobs['result']['content']  as $cancelled_job) :

                            ?>
                             <tr>
                                <td><?= $cancelled_job['bidId'] ?></td>
                                <td><?= $cancelled_job['jobId'] ?></td>
                                <td><?= $cancelled_job['paymentMode'] ?></td>
                                <td><?= $cancelled_job['paymentStatus'] ?></td>
                                <td><?=getCurrencyById($cancelled_job['currencyId'])['result']['name'] . getCurrencyById($cancelled_job['currencyId'])['result']['symbol'] . ' '. $cancelled_job['offerAmount'] ?></td>
                                <!-- <td><?=getCurrencyById($cancelled_job['currencyId'])['result']['name'] . getCurrencyById($cancelled_job['currencyId'])['result']['symbol'] . ' '. $cancelled_job['bidAmount'] ?></td>  -->
                                <td><?= $cancelled_job['pickupAddress'] ?></td>
                                <td><?= $cancelled_job['dropOffAddress'] ?></td> 
                                <!-- <td>
                                <button type="button" rel="tooltip" title="View" class="btn btn-primary btn-link btn-sm">
                                  <i data-target="#cjob<?= $cancelled_job['jobId'] ?>" data-toggle="modal" class="material-icons">lens</i>
                                </button>
    
                              </td> -->
                      
                              </tr>

                                    <!-- Job Details Modal  -->
                               <div class="modal fade" id="cjob<?= $cancelled_job['jobId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Job Details</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?jobId=<?= $cancelled_job['jobId'] ?>" method="post" enctype="multipart/form-data">
                                      <div class="modal-body">
                                      <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid No:</label>
                                            <input type="text" class="form-control"  value="<?= $cancelled_job['bidId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Status:</label>
                                            <input type="text" class="form-control"  value="<?= $cancelled_job['bidStatus'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Payment Method:</label>
                                            <input type="text" class="form-control"  value="<?= $cancelled_job['paymentMode'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control"  value="<?= $cancelled_job['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                   
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Offer Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($cancelled_job['currencyId'])['result']['name'] . getCurrencyById($cancelled_job['currencyId'])['result']['symbol'] . ' '. $cancelled_job['offerAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                        <!-- <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Amount:</label>
                                            <input type="text" class="form-control" value="<?= getCurrencyById($cancelled_job['currencyId'])['result']['name'] . getCurrencyById($cancelled_job['currencyId'])['result']['symbol'] . ' '. $cancelled_job['bidAmount'] ?>" readonly />
                                          </div>
                                        </div> -->
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Cancelled Reason:</label>
                                            <textarea class="form-control" name="rejectReason" rows="2" value="<?= $cancelled_job['rejectReason'] ?>" readonly ></textarea>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                      <button type="button" data-target="#pdelete<?= $cancelled_job['jobId'] ?>" data-toggle="modal" class="btn btn-primary">Delete Job</button>
                                        <button type="button" data-target="#pcounter<?= $cancelled_job['jobId'] ?>" data-toggle="modal" class="btn btn-secondary">Counter Offer</button>
                                       
                                        <button type="button" data-target="#paccept<?= $cancelled_job['jobId'] ?>" data-toggle="modal" class="btn btn-primary">Accept Job</button>

                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->

                              <!-- Counter Offer Modal  -->
                              <div class="modal fade" id="pcounter<?= $cancelled_job['jobId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Partner Counter Offer</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?jobId=<?= $cancelled_job['jobId'] ?>" method="POST">
                                      <div class="modal-body">
                                      <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid No:</label>
                                            <input type="text" class="form-control"  value="<?= $cancelled_job['bidId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Status:</label>
                                            <input type="text" class="form-control"  value="<?= $cancelled_job['bidStatus'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Payment Method:</label>
                                            <input type="text" class="form-control"  value="<?= $cancelled_job['paymentMode'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control"  value="<?= $cancelled_job['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                   
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Offer Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($cancelled_job['currencyId'])['result']['name'] . getCurrencyById($cancelled_job['currencyId'])['result']['symbol'] . ' '. $cancelled_job['offerAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                        <!-- <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Amount:</label>
                                            <input type="text" class="form-control" value="<?= getCurrencyById($cancelled_job['currencyId'])['result']['name'] . getCurrencyById($cancelled_job['currencyId'])['result']['symbol'] . ' '. $cancelled_job['bidAmount'] ?>" readonly />
                                          </div>
                                        </div> -->
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Cancelled Reason:</label>
                                            <textarea class="form-control" name="rejectReason" rows="2" value="<?= $cancelled_job['rejectReason'] ?>" readonly ></textarea>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="form-group">
                                            <label for="counterOfferAmount" class="control-label col-sm-4">Counter Offer:</label>
                                            <input type="text" class="form-control" name="counterOfferAmount" required>
                                          </div>
                                        </div>
                                        <input type="hidden" class="form-control" name="iDriverId" value="<?php echo $idriverId; ?>" readonly >
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <input type="submit" name="counter" class="btn btn-primary" value="Place Offer" />
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->

                              <!-- Delete Job Modal  -->
                              <div class="modal fade" id="pdelete<?= $cancelled_job['jobId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Delete Job</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?jobId=<?= $cancelled_job['jobId'] ?>" method="post" enctype="multipart/form-data">
                                      <div class="modal-body">
                                      <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid No:</label>
                                            <input type="text" class="form-control"  value="<?= $cancelled_job['bidId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Status:</label>
                                            <input type="text" class="form-control"  value="<?= $cancelled_job['bidStatus'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Payment Method:</label>
                                            <input type="text" class="form-control"  value="<?= $cancelled_job['paymentMode'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control"  value="<?= $cancelled_job['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                   
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Offer Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($cancelled_job['currencyId'])['result']['name'] . getCurrencyById($cancelled_job['currencyId'])['result']['symbol'] . ' '. $cancelled_job['offerAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                        <!-- <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Amount:</label>
                                            <input type="text" class="form-control" value="<?= getCurrencyById($cancelled_job['currencyId'])['result']['name'] . getCurrencyById($cancelled_job['currencyId'])['result']['symbol'] . ' '. $cancelled_job['bidAmount'] ?>" readonly />
                                          </div>
                                        </div> -->
                                        <div class="col">
                                          <div class="form-group">
                                            <label for="rejectReason" class="control-label col-sm-4">Reject Reason:</label>
                                            <textarea class="form-control" name="rejectReason" rows="3" required></textarea>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <input type="submit" name="delete" class="btn btn-primary" value="Confirm" />
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->

                              <!-- Accept Job Modal  -->
                              <div class="modal fade" id="paccept<?= $cancelled_job['jobId'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel">Accept Job</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <form action="../controller/bid_process?jobId=<?= $cancelled_job['jobId'] ?>" method="post" enctype="multipart/form-data">
                                      <div class="modal-body">

                                      <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid No:</label>
                                            <input type="text" class="form-control"  value="<?= $cancelled_job['bidId'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Status:</label>
                                            <input type="text" class="form-control"  value="<?= $cancelled_job['bidStatus'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Payment Method:</label>
                                            <input type="text" class="form-control"  value="<?= $cancelled_job['paymentMode'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Job No:</label>
                                            <input type="text" class="form-control"  value="<?= $cancelled_job['jobId'] ?>" readonly>
                                          </div>
                                        </div>
                                   
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Offer Amount:</label>
                                            <input type="text" class="form-control" placeholder="Offer Amount" value="<?= getCurrencyById($cancelled_job['currencyId'])['result']['name'] . getCurrencyById($cancelled_job['currencyId'])['result']['symbol'] . ' '. $cancelled_job['offerAmount'] ?>" readonly>
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Bid Amount:</label>
                                            <input type="text" class="form-control" value="<?= getCurrencyById($cancelled_job['currencyId'])['result']['name'] . getCurrencyById($cancelled_job['currencyId'])['result']['symbol'] . ' '. $cancelled_job['bidAmount'] ?>" readonly />
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="input-group">
                                            <label for="iDriverId" class="control-label col-sm-4">Cancelled Reason:</label>
                                            <textarea class="form-control" name="rejectReason"  rows="2" value="<?= $cancelled_job['rejectReason'] ?>" readonly ></textarea>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <input type="submit" name="accept" class="btn btn-primary" value="Confirm" />
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <!--end modal-->
                            <?php endforeach; ?>

                          <?php else : ?>

                            <?= $cancelled_jobs['Message'] ?>

                          <?php endif; ?>

                        </tbody>
                      </table>
                    </div>

                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>


      <footer class="footer">
        <div class="container-fluid">

          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>, designed with <i class="material-icons">favorite</i> by
            <a href="https://www.dev.vayaafrica.com/" referrerpolicy="no-referrer | same-origin | origin | strict-origin | no-origin-when-downgrading" target="_blank">VAYA</a>
          </div>
        </div>
      </footer>
    </div>
  </div>

  <!--   Core JS Files   -->
  <script src="../assets/js/core/jquery.min.js"></script>
  <script src="../assets/js/core/popper.min.js"></script>
  <script src="../assets/js/core/bootstrap-material-design.min.js"></script>
  <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!-- Plugin for the momentJs  -->
  <script src="../assets/js/plugins/moment.min.js"></script>
  <!--  Plugin for Sweet Alert -->
  <script src="../assets/js/plugins/sweetalert2.js"></script>
  <!-- Forms Validations Plugin -->
  <script src="../assets/js/plugins/jquery.validate.min.js"></script>
  <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
  <script src="../assets/js/plugins/jquery.bootstrap-wizard.js"></script>
  <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
  <script src="../assets/js/plugins/bootstrap-selectpicker.js"></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src="../assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
  <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
  <script src="../assets/js/plugins/jquery.dataTables.min.js"></script>
  <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
  <script src="../assets/js/plugins/bootstrap-tagsinput.js"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="../assets/js/plugins/jasny-bootstrap.min.js"></script>
  <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
  <script src="../assets/js/plugins/fullcalendar.min.js"></script>

  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="../assets/js/plugins/nouislider.min.js"></script>
  <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
  <!-- Library for adding dinamically elements -->
  <script src="../assets/js/plugins/arrive.min.js"></script>
  <!--  Google Maps Plugin    -->
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQTpHIDA-oUPXuJhXW7PHxRo_9OWUnoMs&callback=initMap&libraries=places,geometry"></script>
    
  <!-- Chartist JS -->
  <script src="../assets/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="../assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../assets/js/material-dashboard.js?v=2.1.2" type="text/javascript"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <script src="../assets/demo/demo.js"></script>


<script>
    $(document).ready(function() {
      $('#dt-Pending').dataTable({
        dom: 'Bfrtip',

        buttons: [
          'excel', 'csv'
        ],
        columnDefs: [{
          orderable: false,
          className: 'select-checkbox',
          targets: 0
        }],
        select: {
          style: 'multi',
          selector: 'td:first-child'
        }
      });
    });
  </script>

<script>
    $(document).ready(function() {
      $('#dt-accepted').dataTable({
        dom: 'Bfrtip',

        buttons: [
          'excel', 'csv'
        ],
        columnDefs: [{
          orderable: false,
          className: 'select-checkbox',
          targets: 0
        }],
        select: {
          style: 'multi',
          selector: 'td:first-child'
        }
      });
    });
  </script>

  <script>
    $(document).ready(function() {
      $('#dt-collected').dataTable({
        dom: 'Bfrtip',

        buttons: [
          'excel', 'csv'
        ],
        columnDefs: [{
          orderable: false,
          className: 'select-checkbox',
          targets: 0
        }],
        select: {
          style: 'multi',
          selector: 'td:first-child'
        }
      });
    });
  </script>
  <script>
    $(document).ready(function() {
      $('#dt-assigned').dataTable({
        dom: 'Bfrtip',

        buttons: [
          'excel', 'csv'
        ],
        columnDefs: [{
          orderable: false,
          className: 'select-checkbox',
          targets: 0
        }],
        select: {
          style: 'multi',
          selector: 'td:first-child'
        }
      });
    });
  </script>
  <script>
    $(document).ready(function() {
      $('#dt-cancelled').dataTable({
        dom: 'Bfrtip',

        buttons: [
          'excel', 'csv'
        ],
        columnDefs: [{
          orderable: false,
          className: 'select-checkbox',
          targets: 0
        }],
        select: {
          style: 'multi',
          selector: 'td:first-child'
        }
      });
    });
  </script>
  <script>
    $(document).ready(function() {
      $('#dt-intransit').dataTable({
        dom: 'Bfrtip',

        buttons: [
          'excel', 'csv'
        ],
        columnDefs: [{
          orderable: false,
          className: 'select-checkbox',
          targets: 0
        }],
        select: {
          style: 'multi',
          selector: 'td:first-child'
        }
      });
    });
  </script>
  <script>
    $(document).ready(function() {
      $('#dt-delivered').dataTable({
        dom: 'Bfrtip',

        buttons: [
          'excel', 'csv'
        ],
        columnDefs: [{
          orderable: false,
          className: 'select-checkbox',
          targets: 0
        }],
        select: {
          style: 'multi',
          selector: 'td:first-child'
        }
      });
    });
  </script>
  <script>
    $(document).ready(function() {
      $().ready(function() {
        $sidebar = $('.sidebar');

        $sidebar_img_container = $sidebar.find('.sidebar-background');

        $full_page = $('.full-page');

        $sidebar_responsive = $('body > .navbar-collapse');

        window_width = $(window).width();

        fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

        if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
          if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
            $('.fixed-plugin .dropdown').addClass('open');
          }

        }

        $('.fixed-plugin a').click(function(event) {
          // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
          if ($(this).hasClass('switch-trigger')) {
            if (event.stopPropagation) {
              event.stopPropagation();
            } else if (window.event) {
              window.event.cancelBubble = true;
            }
          }
        });

        $('.fixed-plugin .active-color span').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-color', new_color);
          }

          if ($full_page.length != 0) {
            $full_page.attr('filter-color', new_color);
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.attr('data-color', new_color);
          }
        });

        $('.fixed-plugin .background-color .badge').click(function() {
          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('background-color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-background-color', new_color);
          }
        });

        $('.fixed-plugin .img-holder').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).parent('li').siblings().removeClass('active');
          $(this).parent('li').addClass('active');


          var new_image = $(this).find("img").attr('src');

          if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            $sidebar_img_container.fadeOut('fast', function() {
              $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
              $sidebar_img_container.fadeIn('fast');
            });
          }

          if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $full_page_background.fadeOut('fast', function() {
              $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
              $full_page_background.fadeIn('fast');
            });
          }

          if ($('.switch-sidebar-image input:checked').length == 0) {
            var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
          }
        });

        $('.switch-sidebar-image input').change(function() {
          $full_page_background = $('.full-page-background');

          $input = $(this);

          if ($input.is(':checked')) {
            if ($sidebar_img_container.length != 0) {
              $sidebar_img_container.fadeIn('fast');
              $sidebar.attr('data-image', '#');
            }

            if ($full_page_background.length != 0) {
              $full_page_background.fadeIn('fast');
              $full_page.attr('data-image', '#');
            }

            background_image = true;
          } else {
            if ($sidebar_img_container.length != 0) {
              $sidebar.removeAttr('data-image');
              $sidebar_img_container.fadeOut('fast');
            }

            if ($full_page_background.length != 0) {
              $full_page.removeAttr('data-image', '#');
              $full_page_background.fadeOut('fast');
            }

            background_image = false;
          }
        });

        $('.switch-sidebar-mini input').change(function() {
          $body = $('body');

          $input = $(this);

          if (md.misc.sidebar_mini_active == true) {
            $('body').removeClass('sidebar-mini');
            md.misc.sidebar_mini_active = false;

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

          } else {

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

            setTimeout(function() {
              $('body').addClass('sidebar-mini');

              md.misc.sidebar_mini_active = true;
            }, 300);
          }

          // we simulate the window Resize so the charts will get updated in realtime.
          var simulateWindowResize = setInterval(function() {
            window.dispatchEvent(new Event('resize'));
          }, 180);

          // we stop the simulation of Window Resize after the animations are completed
          setTimeout(function() {
            clearInterval(simulateWindowResize);
          }, 1000);

        });
      });
    });
  </script>

<script>
    function calcDistance(p1, p2) { //p1 and p2 in the form of google.maps.LatLng object
      return (google.maps.geometry.spherical.computeDistanceBetween(p1, p2) / 1000).toFixed(3); //distance in KiloMeters
    }

    function getLocation() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
      } else {
        alert("Geolocation is not supported by this browser.");
      }
    }

    function showPosition(position) {

        document.getElementById("lat").value = position.coords.latitude;
        document.getElementById("long").value = position.coords.longitude;
        window.setTimeout(document.currentLocation.submit.bind(document.currentLocation), 5000);
    //   alert("Latitude: " + position.coords.latitude + " Longitude: " + position.coords.longitude);
    }

    function showError(error) {
      switch (error.code) {
        case error.PERMISSION_DENIED:
          alert("User denied the request for Geolocation.");
          break;
        case error.POSITION_UNAVAILABLE:
          alert("Location information is unavailable.");
          break;
        case error.TIMEOUT:
          alert("The request to get user location timed out.");
          break;
        case error.UNKNOWN_ERROR:
          alert("An unknown error occurred.");
          break;
      }
    }

    function initMap() {

      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 13,
        center: {
          lat: -17.7808295,
          lng: 31.0740816
        }
      });

      var waypts = []; //origin to destination via waypoints
      //waypts.push({location: 'indore', stopover: true});

      function continuouslyUpdatePosition(location) { //Take current location from location  and set marker to that location
        var inputVal = document.getElementById("myInput").value;
            // Displaying the value
            // alert(inputVal);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            var pos = JSON.parse(this.responseText);
            location.setPosition(new google.maps.LatLng(pos.lat, pos.lng));
            setTimeout(function() {
              continuouslyUpdatePosition(location);
            }, 5000);
          }
        };
        // xhttp.open("GET", "location ?tripId=1", true);

        xhttp.open("GET", "location ?tripId=" + inputVal , true);
        xhttp.send();
      }

      /* Distance between p1 & p2
      var p1 = new google.maps.LatLng(45.463688, 9.18814);
      var p2 = new google.maps.LatLng(46.0438317, 9.75936230000002);
      alert(calcDistance(p1,p2)+" Kilimeters");
      */

      //Make marker at any position in form of {lat,lng}
      function makeMarker(position /*, icon*/ ) {
        var marker = new google.maps.Marker({
          position: position,
          map: map,
          /*animation: google.maps.Animation.DROP,*/
          /*icon: icon,*/
        });
        return marker;
      }

      var icons = {
        end: new google.maps.MarkerImage('http://icons.iconarchive.com/icons/icons-land/vista-map-markers/32/Map-Marker-Push-Pin-1-Left-Pink-icon.png'),
        start: new google.maps.MarkerImage('http://icons.iconarchive.com/icons/icons-land/vista-map-markers/32/Map-Marker-Push-Pin-1-Left-Chartreuse-icon.png')
      };

      //Show suggestions for places, requires libraries=places in the google maps api script link
      var autocomplete1 = new google.maps.places.Autocomplete(document.getElementById('start1'));
      var autocomplete2 = new google.maps.places.Autocomplete(document.getElementById('end1'));

      var directionsService1 = new google.maps.DirectionsService;
      var directionsDisplay1 = new google.maps.DirectionsRenderer({
        polylineOptions: {
          strokeColor: "red"
        }, //path color
        //draggable: true,// change start, waypoints and destination by dragging
        /* Start and end marker with same image
        markerOptions : {icon: 'http://icons.iconarchive.com/icons/icons-land/vista-map-markers/32/Map-Marker-Push-Pin-1-Left-Pink-icon.png'},
        */
        //suppressMarkers: true
      });
      directionsDisplay1.setMap(map);
      var onChangeHandler1 = function() {
        calculateAndDisplayRoute(directionsService1, directionsDisplay1, $('#start1'), $('#end1'));
      };
      $('#start1,#end1').change(onChangeHandler1);

      function calculateAndDisplayRoute(directionsService, directionsDisplay, start, end) {
        directionsService.route({
          origin: start.val(),
          destination: end.val(),
          waypoints: waypts,
          travelMode: 'DRIVING'
        }, function(response, status) {
          if (status === 'OK') {
            directionsDisplay.setDirections(response);
            var leg = response.routes[0].legs[0];
            // Move marker along path from A to B
            var markers = [];
            for (var i = 0; i < leg.steps.length; i++) {
              var marker = makeMarker(leg.steps[i].start_location);
              markers.push(marker);
              marker.setMap(null);
            }
            tracePath(markers, 0);

            var location = makeMarker(leg.steps[0].start_location);
            continuouslyUpdatePosition(location);

          } else {
            window.alert('Directions request failed due to ' + status);
          }
        });
      }

      function tracePath(markers, index) { // move marker along path from A to B
        if (index == markers.length) return;
        markers[index].setMap(map);
        setTimeout(function() {
          markers[index].setMap(null);
          tracePath(markers, index + 1);
        }, 500);
      }

      calculateAndDisplayRoute(directionsService1, directionsDisplay1, $('#start1'), $('#end1'));
    }
  </script>

<script>
  //Assign partner  (Bids)
function assignDriver (){
  $.ajax({
			type: "POST",
			url: "../controller/bid_process",     
			data: $('form#assigndriver').serialize(),
      cache: false,
			success: function(response){
        var json = $.parseJSON(response);
        // console.log(json.Action);
        if(json.Action == 1) {
        $('.assignDriver').empty(); //clear apend
         $('.assignDriver').append("Driver assigned successfully");
          // setTimeout(function(){ window.location="jobs "; },5000);
        }else {
          $('.assignDriver').empty();
          $('.assignDriver').append(json.Message);
          // setTimeout(function(){ window.location="jobs "; },5000);

        }
        
			},
      error: function(jqXHR, textStatus, errorThrown) {
          //  console.log(jqXHR, textStatus, errorThrown);
          $('.assignDriver').empty();
          $('.assignDriver').append(errorThrown);
          // setTimeout(function(){ window.location="jobs "; },5000);
        }
		});
}
</script>
<script>
document.addEventListener('keydown', function() {
  if (event.keyCode == 123) {
    alert("This function has been disabled!");
    return false;
  } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) {
    alert("This function has been disabled!");
    return false;
  } else if (event.ctrlKey && event.keyCode == 85) {
    alert("This function has been disabled!");
    return false;
  }
}, false);

if (document.addEventListener) {
  document.addEventListener('contextmenu', function(e) {
    alert("This function has been disabled!");
    e.preventDefault();
  }, false);
} else {
  document.attachEvent('oncontextmenu', function() {
    alert("This function has been disabled!");
    window.event.returnValue = false;
  });
}
</script>
</body>

</html>