<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
    <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
    <meta name="author" content="PIXINVENT">
    <title>PostJob</title>
    <link rel="apple-touch-icon" href="../app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="../app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../app-assets/fonts/material-icons/material-icons.css">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/material-vendors.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/icheck/icheck.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/icheck/custom.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/extensions/datedropper.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/extensions/timedropper.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/css/material.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/material-extended.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/material-colors.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/css/core/menu/menu-types/material-vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/forms/wizard.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/css/pages/hospital-add-patient.css">

    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="../assets/css/style.css">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu material-vertical-layout material-layout 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">

    <!-- BEGIN: Header-->
    <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-dark bg-light navbar-shadow">
        <div class="navbar-wrapper">
            <div class="navbar-header">
                <ul class="nav navbar-nav flex-row">
                    <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                    <li class="nav-item"><a class="navbar-brand" href="pending_jobs ">
                            <h3 class="brand-text">VAYA TECHNOLOGIES</h3>
                        </a></li>
                    <li class="nav-item d-md-none"><a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="material-icons mt-50">more_vert</i></a></li>
                </ul>
            </div>
            <div class="navbar-container content">
                <div class="collapse navbar-collapse" id="navbar-mobile">
                    <ul class="nav navbar-nav mr-auto float-left">
                        <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle" href="#"><i class="ft-menu"></i></a></li>
                  
                   
                    </ul>
                    <ul class="nav navbar-nav float-right">
                     
                
                  
                        <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown"><span class="mr-1 user-name text-bold-700">John Doe</span><span class="avatar avatar-online"><img src="../app-assets/images/portrait/small/vaya_avatah.png" alt="avatar"><i></i></span></a>
                            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#"><i class="material-icons">person_outline</i> Edit Profile</a>
                                <div class="dropdown-divider"></div><a class="dropdown-item" href="customer_login "><i class="material-icons">power_settings_new</i> Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- END: Header-->


    <!-- BEGIN: Main Menu-->

    <div class="main-menu material-menu menu-fixed menu-dark menu-accordion    menu-shadow " data-scroll-to-active="true">
        <div class="user-profile">
            <!-- <div class="user-info text-center pb-2"><img class="user-img img-fluid rounded-circle w-25 mt-2" src="../../../app-assets/images/portrait/small/avatar-s-1.png" alt="" />
                <div class="name-wrapper d-block dropdown mt-1"><a class="white dropdown-toggle ml-2" id="user-account" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="user-name">Charlie Adams</span></a>
                    <div class="text-light">UX Designer</div>
                    <div class="dropdown-menu arrow"><a class="dropdown-item"><i class="material-icons align-middle mr-1">person</i><span class="align-middle">Profile</span></a><a class="dropdown-item"><i class="material-icons align-middle mr-1">message</i><span class="align-middle">Messages</span></a><a class="dropdown-item"><i class="material-icons align-middle mr-1">attach_money</i><span class="align-middle">Balance</span></a><a class="dropdown-item"><i class="material-icons align-middle mr-1">settings</i><span class="align-middle">Settings</span></a><a class="dropdown-item"><i class="material-icons align-middle mr-1">power_settings_new</i><span class="align-middle">Log Out</span></a></div>
                </div>
            </div> -->
        </div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <li class=" nav-item"><a href="#"><i class="material-icons">local_shipping</i><span class="menu-title" data-i18n="Patients">My Jobs</span></a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="pending_jobs "><i class="material-icons"></i><span>Pending</span></a>
                        </li>
                        <li><a class="menu-item" href="assigned_jobs "><i class="material-icons"></i><span>Assigned</span></a>
                        </li>
                        <li><a class="menu-item" href="intransit_jobs "><i class="material-icons"></i><span>InTransit</span></a>
                        </li>
                        <li><a class="menu-item" href="delivered_jobs "><i class="material-icons"></i><span>Delivered</span></a>
                        </li>
                  
                    </ul>
                </li>
            </ul>
        </div>
    </div>

    <!-- END: Main Menu-->
    <!-- BEGIN: Content-->
    <div class="app-content content">

     <div class="content-header row">
            <div class="content-header-dark bg-img col-12">
                <div class="row">
              
                    <div class="content-header-left col-md-9 col-12 mb-2">
                        <h3 class="content-header-title white">Post New Job</h3>
                        <div class="row breadcrumbs-top">
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="pending_jobs ">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="post_job ">Post Job</a>
                                    </li>
                                   
                                </ol>
                            </div>
                        </div>
                    </div>
               
                </div>
            </div>
        </div>
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <!-- Add Patient Form Wizard -->
                <section id="add-patient">
                    <div class="icon-tabs">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <!-- <div class="card-header">
                                        <h4 class="card-title blue">
                                            Post A New Job
                                        </h4>
                                        <a class="heading-elements-toggle" href="#">
                                            <i class="la la-ellipsis-h font-medium-3"> </i>
                                        </a>
                                    </div> -->
                                   
                                    <div class="card-content collapse show">
                                        <div class="card-body">
                                            <form action="#" class="add-patient-tabs steps-validation wizard-notification">
                                                <!-- step 1 => Cargo Details -->
                                                <h6>
                                                    <i class="step-icon la la-user font-medium-3"> </i>
                                                    Cargo Details
                                                </h6>
                                                <fieldset>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="firstName">
                                                                    Description:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input class="form-control required" id="lastName" name="lastname" type="text" />
                                                                <!-- <textarea class="form-control required" cols="5" id="history" name="history" rows="1"></textarea> -->
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="lastName">
                                                                    Value Of Goods:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input class="form-control required" id="lastNamep" name="lastname" type="text" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="address">
                                                                    Type Of Goods:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input class="form-control required" id="address" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="city">
                                                                    Weight:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input class="form-control required" id="contact" name="contact" type="number" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="state">
                                                                    Unit:
                                                                </label>
                                                                <select class="custom-select" id="state">
                                                                   
                                                                    <option value="gr">
                                                                        gr
                                                                    </option>
                                                                    <option value="newyork">
                                                                        kgs
                                                                    </option>
                                                                    <option value="texas">
                                                                        tonnes
                                                                    </option>
                                                                    <option value="california">
                                                                        pounds
                                                                    </option>
                                                                   
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="city">
                                                                    Length:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input class="form-control required" id="contactp" name="contact" type="number" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="state">
                                                                    Width:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input class="form-control required" id="contactc" name="contact" type="number" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="city">
                                                                    Height:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input class="form-control required" id="contactn" name="contact" type="number" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="state">
                                                                    Unit:
                                                                </label>
                                                                <select class="custom-select" id="state">
                                                                   
                                                                    <option value="gr">
                                                                        cm
                                                                    </option>
                                                                    <option value="newyork">
                                                                        m
                                                                    </option>
                                                                    <option value="texas">
                                                                        inches
                                                                    </option>
                                                               
                                                                   
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="contact">
                                                                    Manifest:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input class="form-control required" type="file" id="fileb">
                                                                <span class="file-custom"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="contact">
                                                                    Waybill:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input class="form-control required" type="file" id="file">
                                                                <span class="file-custom"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <!-- Step 2 => Delivery Details-->
                                                <h6>
                                                    <i class="step-icon la la-ambulance font-medium-3"> </i>
                                                    Delivery Details
                                                </h6>
                                                <fieldset>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="fnemergency">
                                                                    Pick-Up Location(Mark location):
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input class="form-control" data-toggle="modal" data-target="#pickup" id="fnemergency" name="fnemergency" required="" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="lnemergency">
                                                                    Drop-Off Location(Mark location):
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input class="form-control" data-toggle="modal" data-target="#dropoff" id="lnemergency" name="lnemergency" required="" type="text" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="city">
                                                                    Collection Date:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input type="text" class="form-control input-lg" id="minYear">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="state">
                                                                    Collection Time:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                            
                                                                <input type="text" class="form-control input-lg" id="textColor" >
                                                            </div>
                                                        </div>
                                                     
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="city">
                                                                    Delivery Date:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input type="text" class="form-control input-lg" id="dropTextWeight">
                                                               
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="state">
                                                                    Delivery Time:
                                                                </label>
                                                                <input type="text" class="form-control input-lg" id="timeformat">
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="cn">
                                                                    Vehicle Capacity:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <select class="custom-select" id="state">
                                                                   
                                                                    <option value="gr">
                                                                        cm
                                                                    </option>
                                                                    <option value="newyork">
                                                                        m
                                                                    </option>
                                                                    <option value="texas">
                                                                        inches
                                                                    </option>
                                                               
                                                                   
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="alternate">
                                                                    Service Pillar:
                                                                </label>
                                                                <select class="custom-select" id="state">
                                                                   
                                                                    <option value="gr">
                                                                        cm
                                                                    </option>
                                                                    <option value="newyork">
                                                                        m
                                                                    </option>
                                                                    <option value="texas">
                                                                        inches
                                                                    </option>
                                                               
                                                                   
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                          
                                     
                                                <!-- Step 3 => Insaurance Details -->
                                                <h6>
                                                    <i class="step-icon font-medium-3 ft-file-text"> </i>
                                                    Job Details
                                                </h6>
                                                <fieldset>
                                                    <div class="row icheck_minimal skin">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="pr-1">
                                                                    Goods In-Transit:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <span class="option-y">
                                                                    Yes
                                                                </span>
                                                                <input checked="" class="icheck-checkbox" id="ins-yes" name="ins" required="" type="radio" />
                                                                <span class="option-n">
                                                                    No
                                                                </span>
                                                                <input class="icheck-checkbox" id="ins-no" name="ins" required="" type="radio" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="company">
                                                                    Loading Requirements
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input class="form-control required" id="company" name="degree" required="" type="text" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="card">
                                                                    Offloading Requirements:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input class="form-control required" id="card" name="card" type="number" />
                                                            </div>
                                                        </div>
                                                    </div>
                                     
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="city">
                                                                    Bid-Start Date:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input type="text" class="form-control input-lg" id="dropTextColor">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="state">
                                                                    Bid-Start Time:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                            
                                                                <input type="text" class="form-control input-lg" id="time_init_animation">
                                                            </div>
                                                        </div>
                                                     
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="city">
                                                                    Bid-End Date:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input type="text" class="form-control input-lg" id="dropPrimaryColor">
                                                               
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="state">
                                                                    Bid-End Time:
                                                                </label>
                                                                <input type="text" class="form-control input-lg" id="primaryColor" >
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="row">
                                        
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="address">
                                                                    Payment Method:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <select class="custom-select" id="state">
                                                                   
                                                                    <option value="gr">
                                                                        gr
                                                                    </option>
                                                                    <option value="newyork">
                                                                        kgs
                                                                    </option>
                                                                    <option value="texas">
                                                                        tonnes
                                                                    </option>
                                                                    <option value="california">
                                                                        pounds
                                                                    </option>
                                                                   
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="city">
                                                                    Currency:
                                                                    <span class="danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <select class="custom-select" id="state">
                                                                   
                                                                    <option value="gr">
                                                                        gr
                                                                    </option>
                                                                    <option value="newyork">
                                                                        kgs
                                                                    </option>
                                                                    <option value="texas">
                                                                        tonnes
                                                                    </option>
                                                                    <option value="california">
                                                                        pounds
                                                                    </option>
                                                                   
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="state">
                                                                    Offer Amount::
                                                                </label>
                                                                <input class="form-control required" id="contact" name="contact" type="number" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>


    
                                                    <!-- Pick up location Modal -->
                                                
                                                   <div class="modal fade text-left" id="pickup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" aria-hidden="true">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="myModalLabel17">Pick-Up Location
                                                                </h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div id="maps-leaflet-user-location" class="height-250"></div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                                                                <button type="button" class="btn btn-outline-primary">Use This Location</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                        <!--Drof-Off  Modal -->
                                                        <div class="modal fade text-left" id="dropoff" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" aria-hidden="true">
                                                            <div class="modal-dialog modal-lg" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title" id="myModalLabel17">Drop-Off Location
                                                                        </h4>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <div id="maps-leaflet-user-location" class="height-250"></div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                                                                        <button type="button" class="btn btn-outline-primary">Use This Location</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light navbar-border navbar-shadow">
        <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2019 <a class="text-bold-800 grey darken-2" href="https://1.envato.market/modern_admin" target="_blank">PIXINVENT</a></span><span class="float-md-right d-none d-lg-block">Hand-crafted & Made with<i class="ft-heart pink"></i><span id="scroll-top"></span></span></p>
    </footer>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src="../app-assets/vendors/js/material-vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="../app-assets/vendors/js/extensions/jquery.steps.min.js"></script>
    <script src="../app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
    <script src="../app-assets/vendors/js/forms/icheck/icheck.min.js"></script>
    <script src="../app-assets/vendors/js/extensions/datedropper.min.js"></script>
    <script src="../app-assets/vendors/js/extensions/timedropper.min.js"></script>

    <!-- END: Page Vendor JS-->
    

    <!-- BEGIN: Theme JS-->
    <script src="../app-assets/js/core/app-menu.js"></script>
    <script src="../app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="../app-assets/js/scripts/pages/material-app.js"></script>
    <script src="../app-assets/js/scripts/pages/hospital-add-patient.js"></script>
    <script src="../app-assets/js/scripts/extensions/date-time-dropper.js"></script>
    <!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>