<?php
// error_reporting(E_ALL);
error_reporting(0);
include_once('curl_url.php');

    function callVayaBiddingWebApiPost($jsonData)
    {
        global $curl_url;
    
        $jsonDataEncoded = json_encode($jsonData);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $curl_url,
        
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $jsonDataEncoded,
            CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
            ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        // echo $response;
        // exit;
        return json_decode($response,true);
    }

    function callVayaBiddingBankPaymentsWebApiPost($jsonData)
    {
        global $pay_url;
        $jsonDataEncoded = json_encode($jsonData);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $pay_url,
        
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $jsonDataEncoded,
            CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
            ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        // echo $response;
        // exit;
        return json_decode($response,true);
    }


    ?>
