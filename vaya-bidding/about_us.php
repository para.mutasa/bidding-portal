<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    VAYA eLogistics
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="assets/css/material-kit.css" rel="stylesheet" />

</head>

<body class="profile-page sidebar-collapse">
  <nav class="navbar navbar-transparent navbar-color-on-scroll fixed-top navbar-expand-lg" color-on-scroll="100" id="sectionsNav">
    <div class="container">
      <div class="navbar-translate">
        <a class="navbar-brand" href="welcome ">
          VAYA eLogistics </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
        </button>
      </div>
      <div class="collapse navbar-collapse">
        <ul class="navbar-nav ml-auto">
          <li class="dropdown nav-item">
            <a href="welcome" class="nav-link" referrerpolicy="no-referrer | same-origin | origin | strict-origin | no-origin-when-downgrading">
              Home
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="about_us ">
              About Us
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="contact ">
              Contact Us
            </a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Support
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="faq ">FAQ</a>
              <!-- <a class="dropdown-item" href="#">Another action</a>
            <a class="dropdown-item" href="#">Something else here</a> -->
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Register
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownSubCMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Customer As</a>
                <div class="dropdown-submenu" aria-labelledby="navbarDropdownSubCMenuLink" aria-placement="right">
                  <a class="dropdown-item" href="customer ">Individual Customer</a>
                  <a class="dropdown-item" href="customersme ">SME Customer</a>
                </div>
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownSubPMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Partner As</a>
                <div class="dropdown-submenu" aria-labelledby="navbarDropdownSubPMenuLink" aria-placement="right">
                  <a class="dropdown-item" href="transporter ">Individual Partner</a>
                  <a class="dropdown-item" href="transportersme ">SME Partner</a>
                </div>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Log in
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="customer_login ">Customer</a>
              <a class="dropdown-item" href="partner_login ">Partner</a>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="page-header header-filter" data-parallax="true" style="background-image: url('assets/img/city-profile.jpg');"></div>
  <div class="main main-raised">
    <div class="profile-content">
      <div class="container">
        <div class="row">
          <div class="col-md-14 ml-auto mr-auto">
            <div class="card card-nav-tabs text-center">
              <div class="card-header card-header-primary">

                <button style="margin-right: 25vh;" class="btn btn-info btn-round" 
                  data-toggle="popover" title="Register Customer As" data-html="true"
                  data-content="<a href='customer ' class='btn btn-sm btn-info btn-round' target='_blank'>Individual</a> <br> <a href='customersme ' class='btn btn-sm btn-info btn-round' target='_blank'>SME</a>"
                  >Register As A Customer
                </button>

                <button style="margin-left: 25vh;" class="btn btn-info btn-round" 
                  data-toggle="popover" title="Register Partner As" data-html="true" data-placement="left"
                  data-content="<a href='transporter ' class='btn btn-sm btn-info btn-round' target='_blank'>Individual</a> <br> <a href='transportersme ' class='btn btn-sm btn-info btn-round' target='_blank'>SME</a>"
                  >Register As A Partner
                </button>

                <!-- Talk To Us -->
              </div>
            </div>
            <br />
            <div class="card card-nav-tabs text-center">
              <div class="card-header card-header-info">
                ABOUT US
              </div>
              <div class="card-body">
                <p class="card-text">Vaya eLogistics leverages on technology to provide an innovative and scalable digital logistics platform that aggregates end-to-end operations for Individuals, SMEs and Corporates across multiple sectors. It brings about transparent pricing, fast payments, and the ability for customers to book a load with the touch of a button. Vaya eLogistics is reshaping the trucking industry by helping carriers and shippers succeed at all levels and ensuring that the goods arrive on time, to the correct place and in the expected condition. 

Customer posts a delivery request specifying pick up and drop off location, vehicle size, amount they are willing to pay and desired timelines.

Transporters/Carriers meeting the required vehicle specifications receive the delivery requests and either accept or counter-bid [up or down] the customer’s offer

Customer/Shipper evaluates the responses from the tansporters before selecting desired transporter based on transporter rating, cost, and delivery time-frames.

                </p>

              </div>

            </div>
          </div>
          <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
              <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img class="d-block w-100" src="assets/img/bzns.jpg" alt="First slide">
              </div>
              <div class="carousel-item">
                <img class="d-block w-100" src="assets/img/mova.jpg" alt="Second slide">
              </div>
              
              <div class="carousel-item">
                <img class="d-block w-100" src="assets/img/best.jpg" alt="third slide">
              </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>

        </div>
      </div>
    </div>
    <footer class="footer footer-default">
      <div class="container">
        <nav class="float-left">
          <ul>
            <li>
              <a href="#" target="_blank" title="Follow us on Twitter">
                <i class="fa fa-twitter"></i>
              </a>
            </li>
            <li>
              <a href="#" target="_blank" title="Follow us on Facebook" rel="nofollow">
                <i class="fa fa-facebook-square"></i>
              </a>
            </li>
            <li>
              <a href="#" target="_blank" title="Follow us on Instagram" rel="nofollow">
                <i class="fa fa-instagram"></i>
              </a>
            </li>
          </ul>
        </nav>
        <div class="copyright float-right">
          &copy;
          <script>
            document.write(new Date().getFullYear())
          </script>, designed with <i class="material-icons">favorite</i> by
          <a href="https://www.dev.vayaafrica.com/" target="_blank">VAYA</a>
        </div>
      </div>
    </footer>
    <!--   Core JS Files   -->
    <script src="assets/js/core/jquery.min.js" type="text/javascript"></script>
    <script src="assets/js/core/popper.min.js" type="text/javascript"></script>
    <script src="assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
    <script src="assets/js/plugins/moment.min.js"></script>
    <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
    <script src="assets/js/plugins/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
    <!--  Google Maps Plugin    -->
    <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
    <script src="assets/js/material-kit.js?v=2.0.7" type="text/javascript"></script>
    <script>
document.addEventListener('keydown', function() {
  if (event.keyCode == 123) {
    alert("This function has been disabled!");
    return false;
  } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) {
    alert("This function has been disabled!");
    return false;
  } else if (event.ctrlKey && event.keyCode == 85) {
    alert("This function has been disabled!");
    return false;
  }
}, false);

if (document.addEventListener) {
  document.addEventListener('contextmenu', function(e) {
    alert("This function has been disabled!");
    e.preventDefault();
  }, false);
} else {
  document.attachEvent('oncontextmenu', function() {
    alert("This function has been disabled!");
    window.event.returnValue = false;
  });
}
</script>
</body>

</html>