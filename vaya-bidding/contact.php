<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
        VAYA eLogistics
    </title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="assets/css/material-kit.css" rel="stylesheet" />

</head>

<body class="profile-page sidebar-collapse">
    <nav class="navbar navbar-transparent navbar-color-on-scroll fixed-top navbar-expand-lg" color-on-scroll="100" id="sectionsNav">
        <div class="container">
            <div class="navbar-translate">
                <a class="navbar-brand" href="welcome ">
                    VAYA eLogistics </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="navbar-toggler-icon"></span>
                    <span class="navbar-toggler-icon"></span>
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="navbar-nav ml-auto">
                    <li class="dropdown nav-item">
                        <a href="welcome"referrerpolicy="no-referrer | same-origin | origin | strict-origin | no-origin-when-downgrading" class="nav-link">
                            Home
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="about_us ">
                            About Us
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            Contact Us
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Support
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="faq ">FAQ</a>
                            <!-- <a class="dropdown-item" href="#">Another action</a>
            <a class="dropdown-item" href="#">Something else here</a> -->
                        </div>
                    </li>
                    <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Register
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownSubCMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Customer As</a>
                <div class="dropdown-submenu" aria-labelledby="navbarDropdownSubCMenuLink" aria-placement="right">
                  <a class="dropdown-item" href="customer ">Individual Customer</a>
                  <a class="dropdown-item" href="customersme ">SME Customer</a>
                </div>
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownSubPMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Partner As</a>
                <div class="dropdown-submenu" aria-labelledby="navbarDropdownSubPMenuLink" aria-placement="right">
                  <a class="dropdown-item" href="transporter ">Individual Partner</a>
                  <a class="dropdown-item" href="transportersme ">SME Partner</a>
                </div>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Log in
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="customer_login ">Customer</a>
              <a class="dropdown-item" href="partner_login ">Partner</a>
            </div>
          </li>


                </ul>
            </div>
        </div>
    </nav>
    <div class="page-header header-filter" data-parallax="true" style="background-image: url('assets/img/city-profile.jpg');"></div>
    <div class="main main-raised">
        <div class="profile-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 ml-auto mr-auto">
                        <div class="card card-nav-tabs text-center">
                            <div class="card-header card-header-primary">

                                <button style="margin-right: 25vh;" class="btn btn-info btn-round" 
                                data-toggle="popover" title="Register Customer As" data-html="true"
                                data-content="<a href='customer ' class='btn btn-sm btn-info btn-round' target='_blank'>Individual</a> <br> <a href='customersme ' class='btn btn-sm btn-info btn-round' target='_blank'>SME</a>"
                                >Register As A Customer
                                </button>

                                <button style="margin-left: 25vh;" class="btn btn-info btn-round" 
                                data-toggle="popover" title="Register Partner As" data-html="true" data-placement="left"
                                data-content="<a href='transporter ' class='btn btn-sm btn-info btn-round' target='_blank'>Individual</a> <br> <a href='transportersme ' class='btn btn-sm btn-info btn-round' target='_blank'>SME</a>"
                                >Register As A Partner
                                </button>

                                <!-- Talk To Us -->
                            </div>
                            <div class="card-body">
                                <div class="container" id="map_pick" style="height: 450px;width: 1040px;"></div>
                            </div>

                        </div>
                    </div>
                    <div class="section section-contacts">
                        <div class="row">
                            <!-- <div class="col-md-12"> -->

                                <div class="col-md-6 pull-left">
                                    <div class="card card-nav-tabs text-center">
                                        <div class="card-header card-header-primary">
                                            GET IN TOUCH
                                        </div>
                                    </div>
                                    <h4 class="text-center description"> We uphold high standards of professionalism, trust and integrity in all our business dealings.</h4>
                                    <form class="contact-form" action="../customer/controller/bid_process" method="POST">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">Your Full Name</label>
                                                    <input type="text" name ="name" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">Your Email</label>
                                                    <input type="email" name="email" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleMessage" class="bmd-label-floating">Your Message</label>
                                            <textarea type="text" name="message" class="form-control" rows="3" id="exampleMessage"></textarea>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 ml-auto mr-auto text-center">
                                            <input type="submit" name="query" class="btn btn-primary  btn-raised" value="Send Message" />
                                                <!-- <button class="btn btn-primary  btn-raised">
                                                    Send Message
                                                </button> -->
                                            </div>
                                        </div>
                                    </form>
                                </div>


                                <div class="col-md-6 pull-right" style="float:right; margin-left: 0px;">
                                    <div class="card card-nav-tabs text-center">
                                        <div class="card-header card-header-info">
                                            CONTACT INFORMATION
                                        </div>

                                    </div>
                                    <div class="col-md-12">
                                        <h3 class=" text-primary">Harare</h3>
                                        <hr />
                                        <p style="text-align:justify;"><b>Address:</b>1 Armagh Ave, Harare,<br />
                                            
                                            <b>Call/Whatsapp:</b> +263787111530<br />
                                            <b>Email:</b>vayalogistics@cassavasmartech.co.zw</p>
                                    </div>
                                </div>


                            <!-- </div> -->
                        </div>
                    </div>

                    <!-- <div style="float:right; margin-left: 30vh;" class="section section-contacts">
                        <div class="row">
                            <div class="col-lg-14 ml-auto mr-auto">
                                <div class="section text-center">
                                    <div class="card card-nav-tabs text-center">
                                        <div class="card-header card-header-info">
                                            CONTACT INFORMATION
                                        </div>

                                    </div>
                                    <div class="col-md-14">
                                        <h3 class=" text-primary">HARARE</h3>
                                        <hr />
                                        <p style="text-align:justify;"><b>Address:</b>1 Armagh Ave, Harare,<br />
                                            <b>Telephone:</b> (+2634) 782869<br />
                                            <b>Mobile:</b> +263774222460<br />
                                            <b>Email:</b> info@vaya.com</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->


                </div>
            </div>
        </div>
        <footer class="footer footer-default">
            <div class="container">
                <nav class="float-left">
                    <ul>
                        <li>
                            <a href="#" target="_blank" title="Follow us on Twitter">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank" title="Follow us on Facebook" rel="nofollow">
                                <i class="fa fa-facebook-square"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank" title="Follow us on Instagram" rel="nofollow">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright float-right">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>, designed with <i class="material-icons">favorite</i> by
                    <a href="https://www.dev.vayaafrica.com/" target="_blank">VAYA</a>
                </div>
            </div>
        </footer>
        <!--   Core JS Files   -->
        <script type="text/javascript" src="https://maps.google.com/maps/api/js?libraries=places&key=AIzaSyDQTpHIDA-oUPXuJhXW7PHxRo_9OWUnoMs"></script>
        <script src="assets/js/core/jquery.min.js" type="text/javascript"></script>
        <script src="assets/js/core/popper.min.js" type="text/javascript"></script>
        <script src="assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
        <script src="assets/js/plugins/moment.min.js"></script>
        <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
        <script src="assets/js/plugins/bootstrap-datetimepicker.js" type="text/javascript"></script>
        <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
        <script src="assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
        <!--  Google Maps Plugin    -->
        <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
        <script src="assets/js/material-kit.js?v=2.0.7" type="text/javascript"></script>
        <script>
            $(function() {
                var lat = -17.821450,
                    lng = 31.064730,

                    latlng = new google.maps.LatLng(lat, lng),
                    image = 'https://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png';

                //zoomControl: true,
                //zoomControlOptions: google.maps.ZoomControlStyle.LARGE,

                var mapOptions = {
                        center: new google.maps.LatLng(lat, lng),
                        zoom: 13,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        panControl: true,
                        panControlOptions: {
                            position: google.maps.ControlPosition.TOP_RIGHT
                        },
                        zoomControl: true,
                        zoomControlOptions: {
                            style: google.maps.ZoomControlStyle.LARGE,
                            position: google.maps.ControlPosition.TOP_left
                        }
                    },
                    map = new google.maps.Map(document.getElementById('map_pick'), mapOptions),
                    marker = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        // draggable: true,
                        icon: image
                    });

                var input = document.getElementById('searchPickTextField');
                var autocomplete = new google.maps.places.Autocomplete(input, {
                    types: ["geocode"]
                });

                autocomplete.bindTo('bounds', map);
                var infowindow = new google.maps.InfoWindow();

                google.maps.event.addListener(autocomplete, 'place_changed', function(event) {
                    infowindow.close();
                    var place = autocomplete.getPlace();
                    if (place.geometry.viewport) {
                        map.fitBounds(place.geometry.viewport);
                    } else {
                        map.setCenter(place.geometry.location);
                        map.setZoom(17);
                    }

                    // moveMarker(place.name, place.geometry.location);
                    // $('.MapPickLat').val(place.geometry.location.lat());
                    // $('.MapPickLon').val(place.geometry.location.lng());
                });
                // google.maps.event.addListener(map, 'click', function(event) {
                //     $('.MapPickLat').val(event.latLng.lat());
                //     $('.MapPickLon').val(event.latLng.lng());
                //     infowindow.close();
                //     var geocoder = new google.maps.Geocoder();
                //     geocoder.geocode({
                //         "latLng": event.latLng
                //     }, function(results, status) {
                //         console.log(results, status);
                //         if (status == google.maps.GeocoderStatus.OK) {
                //             console.log(results);
                //             var lat = results[0].geometry.location.lat(),
                //                 lng = results[0].geometry.location.lng(),
                //                 placeName = results[0].address_components[0].long_name,
                //                 latlng = new google.maps.LatLng(lat, lng);


                //             moveMarker(placeName, latlng);
                //             $("#searchPickTextField").val(results[0].formatted_address);
                //             $("#pick_place").val(results[0].formatted_address);


                //         }
                //     });
                // });

                // function moveMarker(placeName, latlng) {
                //     marker.setIcon(image);
                //     marker.setPosition(latlng);
                //     infowindow.setContent(placeName);
                //     infowindow.open(map, marker);
                // }
            });
        </script>

        <script>
document.addEventListener('keydown', function() {
  if (event.keyCode == 123) {
    alert("This function has been disabled!");
    return false;
  } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) {
    alert("This function has been disabled!");
    return false;
  } else if (event.ctrlKey && event.keyCode == 85) {
    alert("This function has been disabled!");
    return false;
  }
}, false);

if (document.addEventListener) {
  document.addEventListener('contextmenu', function(e) {
    alert("This function has been disabled!");
    e.preventDefault();
  }, false);
} else {
  document.attachEvent('oncontextmenu', function() {
    alert("This function has been disabled!");
    window.event.returnValue = false;
  });
}
</script>

</body>

</html>