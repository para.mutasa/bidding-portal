<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    VAYA eLogistics
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="assets/css/material-kit.css" rel="stylesheet" />

</head>

<body class="login-page sidebar-collapse">
  <nav class="navbar navbar-transparent navbar-color-on-scroll fixed-top navbar-expand-lg" color-on-scroll="100" id="sectionsNav">
    <div class="container">
      <div class="navbar-translate">
        <a class="navbar-brand" href="#">
          VAYA eLogistics </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
        </button>
      </div>
      <div class="collapse navbar-collapse">
        <ul class="navbar-nav ml-auto">
          <li class="dropdown nav-item">
            <a href="welcome " class="nav-link">
              Home
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="about_us ">
              About Us
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="contact ">
              Contact Us
            </a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Support
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="faq ">FAQ</a>
              <!-- <a class="dropdown-item" href="#">Another action</a>
            <a class="dropdown-item" href="#">Something else here</a> -->
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Register
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownSubCMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Customer As</a>
                <div class="dropdown-submenu" aria-labelledby="navbarDropdownSubCMenuLink" aria-placement="right">
                  <a class="dropdown-item" href="customer ">Individual Customer</a>
                  <a class="dropdown-item" href="customersme ">SME Customer</a>
                </div>
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownSubPMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Partner As</a>
                <div class="dropdown-submenu" aria-labelledby="navbarDropdownSubPMenuLink" aria-placement="right">
                  <a class="dropdown-item" href="transporter ">Individual Partner</a>
                  <a class="dropdown-item" href="transportersme ">SME Partner</a>
                </div>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Log in
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="customer_login ">Customer</a>
              <a class="dropdown-item" href="partner_login ">Partner</a>
            </div>
          </li>

        </ul>
      </div>
    </div>
  </nav>
  <div class="page-header header-filter" style="background-image: url('assets/img/bg.jpg'); background-size: cover; background-position: top center;">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 col-md-5 ml-auto mr-auto">
          <div class="card card-login">
            <form class="form" method="post" action="../partner/controller/bid_process">
              <div class="card-header card-header-primary text-center">
                <h4 class="card-title">Transporter SME  Registration</h4>
                <!-- <div class="social-line">
                  <a href="#pablo" class="btn btn-just-icon btn-link">
                    <i class="fa fa-facebook-square"></i>
                  </a>
                  <a href="#pablo" class="btn btn-just-icon btn-link">
                    <i class="fa fa-twitter"></i>
                  </a>
                  <a href="#pablo" class="btn btn-just-icon btn-link">
                    <i class="fa fa-google"></i>
                  </a>
                </div> -->
              </div>
              <!-- <p class="description text-center">Or Be Classical</p> -->
              <div class="card-body">
                <div class="row">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="material-icons">face</i>
                      </span>
                    </div>
                    <input type="text" class="form-control" name="name" placeholder="company Name..." required>
                  </div>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="material-icons">face</i>
                      </span>
                    </div>
                    <input type="text" class="form-control" name="vCaddress" placeholder="Address ..." required>
                  </div>
                </div>
                <div class="row">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="material-icons">face</i>
                      </span>
                    </div>
                    <input type="text" class="form-control" name="countryCode" placeholder="country code..." required>
                  </div>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="material-icons">face</i>
                      </span>
                    </div>
                    <input type="text" class="form-control" name="phoneCode" placeholder="phoneCode ..." required>
                  </div>
                </div>

                <div class="row">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="material-icons">mail</i>
                      </span>
                    </div>
                    <input type="email" class="form-control" name="vEmail" placeholder="Email..." required>
                  </div>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="material-icons">settings_cell</i>
                      </span>
                    </div>
                    <input type="tel" class="form-control" name ="vPhone" placeholder="phone number" required>
                  </div>
                </div>

                <div class="row">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="material-icons">lock_outline</i>
                      </span>
                    </div>
                    <input type="password" class="form-control" name="vPassword" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" title="at least one number and one uppercase and lowercase letter, and at least 6 or more characters" placeholder="password..." required>
                  </div>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="material-icons">code</i>
                      </span>
                    </div>  
                    <input type="text" class="form-control" name="zimraBPNumber" placeholder="zimraBPNumber ">   </div>
                </div>
     
              </div>

              <!-- <div class="form-check">
                <label class="form-check-label">
                  <input class="form-check-input" type="checkbox" value="" checked="">
                  <span class="form-check-sign">
                    <span class="check"></span>
                  </span>
                  I agree to the <a href="#something">terms and conditions</a>.
                </label>
              </div> -->
              <div class="text-center">
                <input  type="submit"  class="btn btn-primary btn-round" name="companyRegistration"  value="Register">
              </div>
			  <!-- <div class="modal-footer">
                        <div class="forgot login-footer">
						<input type="submit" name="counter" class="btn btn-primary" value="Register" />
                            <span>Looking to
                                 <a href="javascript: showRegisterForm();">create an account</a>
                            ?</span>
                        </div>
                        <div class="forgot register-footer" style="display:none">
                             <span>Already have an account?</span>
                             <a href="javascript: showLoginForm();">Login</a>
                        </div>
                    </div> -->

       
            </form>
          </div>
        </div>
      </div>
    </div>
    <footer class="footer">
      <div class="container">
        <nav class="float-left">
          <ul>
            <li>
              <a href="#" target="_blank" title="Follow us on Twitter">
                <i class="fa fa-twitter"></i>
              </a>
            </li>
            <li>
              <a href="#" target="_blank" title="Follow us on Facebook" rel="nofollow">
                <i class="fa fa-facebook-square"></i>
              </a>
            </li>
            <li>
              <a href="#" target="_blank" title="Follow us on Instagram" rel="nofollow">
                <i class="fa fa-instagram"></i>
              </a>
            </li>
          </ul>
        </nav>
        <div class="copyright float-right">
          &copy;
          <script>
            document.write(new Date().getFullYear())
          </script>, designed with <i class="material-icons">favorite</i> by
          <a href="https://www.dev.vayaafrica.com/" target="_blank">VAYA</a>
        </div>
      </div>
    </footer>
  </div>
  <!--   Core JS Files   -->
  <script src="assets/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="assets/js/core/popper.min.js" type="text/javascript"></script>
  <script src="assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
  <script src="assets/js/plugins/moment.min.js"></script>
  <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
  <script src="assets/js/plugins/bootstrap-datetimepicker.js" type="text/javascript"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
  <script src="assets/js/material-kit.js?v=2.0.7" type="text/javascript"></script>

  <script>
document.addEventListener('keydown', function() {
  if (event.keyCode == 123) {
    alert("This function has been disabled!");
    return false;
  } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) {
    alert("This function has been disabled!");
    return false;
  } else if (event.ctrlKey && event.keyCode == 85) {
    alert("This function has been disabled!");
    return false;
  }
}, false);

if (document.addEventListener) {
  document.addEventListener('contextmenu', function(e) {
    alert("This function has been disabled!");
    e.preventDefault();
  }, false);
} else {
  document.attachEvent('oncontextmenu', function() {
    alert("This function has been disabled!");
    window.event.returnValue = false;
  });
}
</script>
</body>

</html>