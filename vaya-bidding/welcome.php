
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    VAYA LOGISTICS
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="assets/css/material-kit.css" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="assets/demo/demo.css" rel="stylesheet" />
</head>

<body class="landing-page sidebar-collapse">
  <nav class="navbar navbar-transparent navbar-color-on-scroll fixed-top navbar-expand-lg" color-on-scroll="100" id="sectionsNav">
    <div class="container">
      <div class="navbar-translate">
        <a class="navbar-brand" href="welcome ">
          VAYA eLogistics </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
        </button>
      </div>
      <div class="collapse navbar-collapse">
        <ul class="navbar-nav ml-auto">
          <li class="dropdown nav-item">
            <a href="welcome " class="nav-link">
              Home
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="about_us " target="_self">
              About Us
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="contact " target="_self">
              Contact Us
            </a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Support
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="faq ">FAQ</a>
              <!-- <a class="dropdown-item" href="#">Another action</a>
            <a class="dropdown-item" href="#">Something else here</a> -->
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Register
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownSubCMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Customer As</a>
                <div class="dropdown-submenu" aria-labelledby="navbarDropdownSubCMenuLink" aria-placement="right">
                  <a class="dropdown-item" href="customer ">Individual Customer</a>
                  <a class="dropdown-item" href="customersme ">SME Customer</a>
                </div>
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownSubPMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Partner As</a>
                <div class="dropdown-submenu" aria-labelledby="navbarDropdownSubPMenuLink" aria-placement="right">
                  <a class="dropdown-item" href="transporter ">Individual Partner</a>
                  <a class="dropdown-item" href="transportersme ">SME Partner</a>
                </div>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Log in
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="customer_login ">Customer</a>
              <a class="dropdown-item" href="partner_login ">Partner</a>
            </div>
          </li>

        </ul>
      </div>
    </div>
  </nav>
  <div class="page-header header-filter" data-parallax="true" style="background-image: url('assets/img/home.jpg')">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h1 class="title">ROAD CARGO TRANSPORTATION</h1>
          <h4>A leading digital transport platform</h4>
          <br>
          <a href="sign_up.html" target="_blank">
            <!-- <button class="btn btn-primary btn-round">Register</button> -->
          </a>
        </div>
      </div>
    </div>
  </div>
  <div class="main main-raised">
    <div class="container">
      <div class="col-xl-14 ml-auto mr-auto">
        <div class="card card-nav-tabs text-center">
          <div class="card-header card-header-primary">

            <!-- <a href="customer " target="_blank">
              <button style="margin-right: 25vh;" class="btn btn-info btn-round">Register As A Customer</button>
            </a> -->

            <button style="margin-right: 25vh;" class="btn btn-info btn-round" 
              data-toggle="popover" title="Register Customer As" data-html="true"
              data-content="<a href='customer' class='btn btn-sm btn-info btn-round' target='_blank'>Individual</a> <br> <a href='customersme ' class='btn btn-sm btn-info btn-round' target='_blank'>SME</a>"
              >Register As A Customer
            </button>

            <button style="margin-left: 25vh;" class="btn btn-info btn-round" 
              data-toggle="popover" title="Register Partner As" data-html="true" data-placement="left"
              data-content="<a href='transporter ' class='btn btn-sm btn-info btn-round' target='_blank'>Individual</a> <br> <a href='transportersme ' class='btn btn-sm btn-info btn-round' target='_blank'>SME</a>"
              >Register As A Partner
            </button>
            
          </div>
        </div>
      </div>
      <h4 class="text-center description">Connect to service providers for affordable, secure, and reliable transport services</h4>
      <!-- <div class="section text-center">
        <div class="row">
          <div class="col-md-14 ml-auto mr-auto">
            <div class="card card-nav-tabs text-center">
              <div class="card-header card-header-primary">
                HOW VAYA BIDDING WORKS
              </div>
              <div class="card-body">
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed porta ex ac tortor pellentesque interdum. In est ex, dignissim in consectetur quis, lobortis non mi. Morbi eget volutpat dolor. Quisque eget felis justo. Proin maximus lectus non turpis elementum sodales. Nam id porta metus. Cras commodo lorem ac tellus convallis, nec tempus mi facilisis. Mauris bibendum ipsum eu mattis efficitur</p><br />
                <div class="embed-responsive embed-responsive-16by9">
                  <video class="video-fluid z-depth-1" loop controls muted>
                    <source src="videos/vaya.mp4" type="video/mp4" />
                  </video>
                </div>
              </div> 
            </div>
          </div>
        </div>

      </div> -->
      <!-- <div class="section section-presentation">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <div class="description">
                <h4 class="header-text">It's beautiful</h4>
                <p>Morbi elementum quam ut est luctus cursus. Curabitur condimentum metus quis libero viverra posuere. Quisque purus libero, tincidunt eu nunc quis, aliquam scelerisque leo. Suspendisse sed risus id arcu euismod tincidunt. Nulla at ante et lectus tempus imperdiet at id massa. Phasellus eu sem congue, tristique augue ac, rhoncus ante. Nam in hendrerit purus, at ultrices ipsum. Integer vitae eleifend dolor, in malesuada dui. Phasellus tempus nisi a egestas luctus. Donec ultricies nulla sit amet mauris commodo lobortis. Morbi tincidunt sodales tempor. Fusce non ligula consectetur, molestie arcu quis, egestas mi. Sed euismod dolor ac odio auctor, et varius nisi porttitor. Phasellus egestas velit vel tortor varius, in suscipit lorem finibus..</p>
                </p>
                <p>Steps:
                  <p>
                    <ol>
                      <li>Aenean consequat lacus in maximus fringilla.</li>
                      <li>Quisque id mi pellentesque, facilisis quam quis, varius metus. </li>
                      <li>Quisque id mi pellentesque, facilisis quam quis, varius metus</li>
                    </ol>
              </div>
            </div>
            <div class="col-md-3 col-md-offset-1 hidden-xs">
              <img style="margin-left: 200px; width :50vh; height: 90vh; float: left; margin-top: -30px;" src="../assets/img/showcases/showcase-1/job.jpg">
            </div>
          </div>
        </div>
      </div> -->

      <div class="section text-center">
        <div class="card card-nav-tabs text-center">
          <div class="card-header card-header-primary">
            PLATFORM PARTNERS
          </div>
          <!-- <div class="card-body">
            <p class="card-text">Aenean consequat lacus in maximus fringilla. Quisque id mi pellentesque, facilisis quam quis, varius metus. Aenean laoreet, lacus dictum blandit ullamcorper, est lorem pellentesque sem, eget pellentesque est nunc ut sem. Vivamus venenatis id massa eu laoreet. Curabitur bibendum velit ac diam suscipit, ut gravida odio pellentesque. Vivamus diam arcu, vulputate sit amet consectetur a, lacinia sed elit. Vivamus hendrerit lacinia ligula ut pretium. Interdum et malesuada fames ac ante ipsum primis in faucibus.

            </p>

          </div> -->

        </div>
        <div class="team">
          <div class="row">
            <div class="col-md-3">
              <div class="team-player">
                <div class="card card-plain">
                  <div class="col-md-6 ml-auto mr-auto">
                    <img src="assets/img/partners/ownai.png" alt="Thumbnail Image" class="img-raised squared-circle img-fluid">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="team-player">
                <div class="card card-plain">
                  <div class="col-md-6 ml-auto mr-auto">
                    <img src="assets/img/partners/ecocash.png" alt="Thumbnail Image" class="img-raised squared-circle img-fluid">
                  </div>


                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="team-player">
                <div class="card card-plain">
                  <div class="col-md-6 ml-auto mr-auto">
                    <img src="assets/img/partners/ruzivo.png" alt="Thumbnail Image" class="img-raised squared-circle img-fluid">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="team-player">
                <div class="card card-plain">
                  <div class="col-md-6 ml-auto mr-auto">
                    <img src="assets/img/partners/ecosure.png" alt="Thumbnail Image" class="img-raised squared-circle img-fluid">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- <h4 class="text-center description">Nam tincidunt fringilla metus sit amet sagittis. In feugiat tincidunt placerat. Aliquam rutrum leo ex, eu euismod est pretium id. Sed eros nibh, lacinia pretium metus nec, tincidunt vestibulum elit. Aenean vel nibh libero. In hac habitasse platea dictumst. Suspendisse pretium tristique ligula ac finibus..</h4> -->
      <!-- <div class="section section-features">
        <div class="container">
            <h4 class="header-text text-center">Features</h4>
            <div class="row">
                <div class="col-md-4">
                  <div class="card" style="width: 20rem;">
                    <img class="card-img-top" src="https://images.unsplash.com/photo-1517303650219-83c8b1788c4c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=bd4c162d27ea317ff8c67255e955e3c8&auto=format&fit=crop&w=2691&q=80" rel="nofollow" alt="Card image cap">
                    <div class="card-body">
                      <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="card" style="width: 20rem;">
                    <img class="card-img-top" src="https://images.unsplash.com/photo-1517303650219-83c8b1788c4c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=bd4c162d27ea317ff8c67255e955e3c8&auto=format&fit=crop&w=2691&q=80" rel="nofollow" alt="Card image cap">
                    <div class="card-body">
                      <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="card" style="width: 20rem;">
                    <img class="card-img-top" src="https://images.unsplash.com/photo-1517303650219-83c8b1788c4c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=bd4c162d27ea317ff8c67255e955e3c8&auto=format&fit=crop&w=2691&q=80" rel="nofollow" alt="Card image cap">
                    <div class="card-body">
                      <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div> -->
      <!-- <div class="section section-presentation">
        <div class="container">
          <h4 class="header-text text-center">Testimonials</h4>
          <div id="carousel-example-generic" class="carousel fade" data-ride="carousel">
        
            <div class="carousel-inner" role="listbox">
              <div class="item">
                <div class="mask">
                  <img src="../../assets/img/faces/face4.jpg">
                </div>
                <div class="carousel-testimonial-caption">
                  <p>Jay Z, Producer</p>
                  <h3>"I absolutely love your app! It's truly amazing and looks awesome!"</h3>
                </div>
              </div>
              <div class="item active">
                <div class="mask">
                  <img src="../assets/img/faces/face3.jpg">
                </div>
                <div class="carousel-testimonial-caption">
                  <p>Drake, Artist</p>
                  <h3>"This is one of the most awesome apps I've ever seen! Wish you luck Creative Tim!"</h3>
                </div>
              </div>
              <div class="item">
                <div class="mask">
                  <img src="../assets/img/faces/face2.jpg">
                </div>
                <div class="carousel-testimonial-caption">
                  <p>Rick Ross, Musician</p>
                  <h3>"Loving this! Just picked it up the other day. Thank you for the work you put into this."</h3>
                </div>
              </div>
            </div>
            <ol class="carousel-indicators carousel-indicators-orange">
              <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
              <li data-target="#carousel-example-generic" data-slide-to="1"></li>
              <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            </ol>
          </div>
        </div>
      </div> -->

      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="assets/img/best.jpg" alt="First slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="assets/img/bzns1.jpg" alt="Second slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="assets/img/save.jpg" alt="Third slide">
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
      <!-- <div class="section section-contacts">
        <div class="row">
          <div class="col-md-8 ml-auto mr-auto">
            <div class="section text-center">
              <div class="card card-nav-tabs text-center">
                <div class="card-header card-header-primary">
                  GET IN TOUCH
                </div>

              </div>
              <h4 class="text-center description">We uphold high standards of professionalism, trust and integrity in all our business dealings. </h4>
              <form class="contact-form">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="bmd-label-floating">Your Name</label>
                      <input type="email" class="form-control">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="bmd-label-floating">Your Email</label>
                      <input type="email" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="exampleMessage" class="bmd-label-floating">Your Message</label>
                  <textarea type="email" class="form-control" rows="4" id="exampleMessage"></textarea>
                </div>
                <div class="row">
                  <div class="col-md-4 ml-auto mr-auto text-center">
                    <button class="btn btn-primary  btn-raised">
                      Send Message
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div> -->
    </div>
    <footer class="footer footer-default">
      <div class="container">
        <nav class="float-left">
          <ul>
            <li>
              <a href="#" target="_blank" title="Follow us on Twitter">
                <i class="fa fa-twitter"></i>
              </a>
            </li>
            <li>
              <a href="#" target="_blank" title="Follow us on Facebook" rel="nofollow">
                <i class="fa fa-facebook-square"></i>
              </a>
            </li>
            <li>
              <a href="#" target="_blank" title="Follow us on Instagram" rel="nofollow">
                <i class="fa fa-instagram"></i>
              </a>
            </li>
          </ul>
        </nav>
        <div class="copyright float-right">
          &copy;
          <script>
            document.write(new Date().getFullYear())
          </script>, designed with <i class="material-icons">favorite</i> by
          <a href="https://www.dev.vayaafrica.com/" target="_blank">VAYA</a>
        </div>
      </div>
    </footer>
    <!--   Core JS Files   -->
    <script src="assets/js/core/jquery.min.js" type="text/javascript"></script>
    <script src="assets/js/core/popper.min.js" type="text/javascript"></script>
    <script src="assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
    <script src="assets/js/plugins/moment.min.js"></script>
    <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
    <script src="assets/js/plugins/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
    <!--  Google Maps Plugin    -->
    <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
    <script src="assets/js/material-kit.js?v=2.0.7" type="text/javascript"></script>
    <script>
document.addEventListener('keydown', function() {
  if (event.keyCode == 123) {
    alert("This function has been disabled!");
    return false;
  } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) {
    alert("This function has been disabled!");
    return false;
  } else if (event.ctrlKey && event.keyCode == 85) {
    alert("This function has been disabled!");
    return false;
  }
}, false);

if (document.addEventListener) {
  document.addEventListener('contextmenu', function(e) {
    alert("This function has been disabled!");
    e.preventDefault();
  }, false);
} else {
  document.attachEvent('oncontextmenu', function() {
    alert("This function has been disabled!");
    window.event.returnValue = false;
  });
}
</script>
</body>

</html>